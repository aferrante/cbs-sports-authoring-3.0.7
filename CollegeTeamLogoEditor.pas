unit CollegeTeamLogoEditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids_ts, TSGrid, TSDBGrid, ExtCtrls, DBCtrls;

type
  TCollegeTeamLogoEditorDlg = class(TForm)
    tsDBGrid1: TtsDBGrid;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label2: TLabel;
    DBNavigator1: TDBNavigator;
    First: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CollegeTeamLogoEditorDlg: TCollegeTeamLogoEditorDlg;

implementation

uses DataModule;

{$R *.dfm}

end.
