object TwitterMessageEditorDlg: TTwitterMessageEditorDlg
  Left = 229
  Top = 87
  BorderStyle = bsDialog
  Caption = 'Twitter Message Database Editor'
  ClientHeight = 634
  ClientWidth = 1106
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 17
    Top = 90
    Width = 215
    Height = 16
    Caption = 'Twitter Messages in Database:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object First: TLabel
    Left = 80
    Top = 513
    Width = 31
    Height = 16
    Caption = 'First'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 209
    Top = 513
    Width = 34
    Height = 16
    Caption = 'Prior'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 343
    Top = 513
    Width = 32
    Height = 16
    Caption = 'Next'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 473
    Top = 513
    Width = 30
    Height = 16
    Caption = 'Last'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label8: TLabel
    Left = 603
    Top = 513
    Width = 28
    Height = 16
    Caption = 'Edit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label9: TLabel
    Left = 733
    Top = 513
    Width = 32
    Height = 16
    Caption = 'Post'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label10: TLabel
    Left = 857
    Top = 513
    Width = 49
    Height = 16
    Caption = 'Cancel'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label11: TLabel
    Left = 983
    Top = 513
    Width = 55
    Height = 16
    Caption = 'Refresh'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object NoteLabel: TLabel
    Left = 257
    Top = 90
    Width = 403
    Height = 16
    Caption = '(CTRL-CLICK left edge of row to select multiple messages)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object TwitterMessagesGrid: TtsDBGrid
    Left = 16
    Top = 112
    Width = 1073
    Height = 393
    CellSelectMode = cmNone
    CheckBoxStyle = stCheck
    Cols = 4
    DatasetType = dstStandard
    DataSource = dmMain.dsTwitterMessages
    DefaultRowHeight = 20
    ExactRowCount = True
    ExportDelimiter = ','
    FieldState = fsCustomized
    HeadingFont.Charset = DEFAULT_CHARSET
    HeadingFont.Color = clWindowText
    HeadingFont.Height = -13
    HeadingFont.Name = 'MS Sans Serif'
    HeadingFont.Style = [fsBold]
    HeadingHeight = 20
    HeadingParentFont = False
    ParentShowHint = False
    RowChangedIndicator = riAutoReset
    RowMoving = False
    ShowHint = False
    TabOrder = 0
    Version = '2.20.26'
    XMLExport.Version = '1.0'
    XMLExport.DataPacketVersion = '2.0'
    DataBound = True
    ColProperties = <
      item
        DataCol = 1
        FieldName = 'userName'
        Col.FieldName = 'userName'
        Col.Font.Charset = DEFAULT_CHARSET
        Col.Font.Color = clWindowText
        Col.Font.Height = -13
        Col.Font.Name = 'MS Sans Serif'
        Col.Font.Style = []
        Col.Heading = 'User Name'
        Col.ParentFont = False
        Col.ReadOnly = True
        Col.Width = 160
        Col.AssignedValues = '?'
      end
      item
        DataCol = 2
        FieldName = 'datetime'
        Col.FieldName = 'datetime'
        Col.Font.Charset = DEFAULT_CHARSET
        Col.Font.Color = clWindowText
        Col.Font.Height = -13
        Col.Font.Name = 'MS Sans Serif'
        Col.Font.Style = []
        Col.Heading = 'Date/Time Stamp'
        Col.ParentFont = False
        Col.ReadOnly = True
        Col.Width = 156
        Col.AssignedValues = '?'
      end
      item
        DataCol = 3
        FieldName = 'enable'
        Col.ControlType = ctCheck
        Col.FieldName = 'enable'
        Col.Heading = 'Enable'
        Col.AssignedValues = '?'
      end
      item
        DataCol = 4
        FieldName = 'message'
        Col.FieldName = 'message'
        Col.Font.Charset = DEFAULT_CHARSET
        Col.Font.Color = clWindowText
        Col.Font.Height = -13
        Col.Font.Name = 'MS Sans Serif'
        Col.Font.Style = []
        Col.Heading = 'Message'
        Col.ParentFont = False
        Col.Width = 654
        Col.AssignedValues = '?'
      end>
  end
  object BitBtn1: TBitBtn
    Left = 928
    Top = 584
    Width = 150
    Height = 41
    Caption = '&Close'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ModalResult = 1
    ParentFont = False
    TabOrder = 1
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
      F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
      000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
      338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
      45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
      3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
      F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
      000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
      338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
      4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
      8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
      333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
      0000}
    NumGlyphs = 2
  end
  object DBNavigator1: TDBNavigator
    Left = 30
    Top = 536
    Width = 1048
    Height = 33
    DataSource = dmMain.dsTwitterMessages
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbEdit, nbPost, nbCancel, nbRefresh]
    TabOrder = 2
  end
  object Panel1: TPanel
    Left = 16
    Top = 8
    Width = 1073
    Height = 73
    BevelWidth = 2
    TabOrder = 3
    object Label2: TLabel
      Left = 9
      Top = 7
      Width = 195
      Height = 16
      Caption = 'Message Filter and Options:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 257
      Top = 18
      Width = 161
      Height = 16
      Caption = 'Selected League/Sport'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 9
      Top = 40
      Width = 205
      Height = 16
      Caption = '(Filter Applies to Twitter Only)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LeagueSelectComboBox: TComboBox
      Left = 256
      Top = 39
      Width = 169
      Height = 24
      Style = csDropDownList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemHeight = 16
      ParentFont = False
      TabOrder = 0
      OnChange = LeagueSelectComboBoxChange
    end
    object LeagueFilterEnable: TCheckBox
      Left = 432
      Top = 43
      Width = 257
      Height = 17
      Caption = 'Filter By Selected League/Sport'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = LeagueFilterEnableClick
    end
    object UseTemplateWithoutSubHeader: TCheckBox
      Left = 728
      Top = 15
      Width = 273
      Height = 17
      Caption = 'Use Template without Sub-Header'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = LeagueFilterEnableClick
    end
    object OrderByHandleName: TCheckBox
      Left = 728
      Top = 43
      Width = 257
      Height = 17
      Caption = 'Order by Handle Name'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = OrderByHandleNameClick
    end
  end
  object AddMessagesBtn: TBitBtn
    Left = 393
    Top = 584
    Width = 321
    Height = 41
    Caption = 'Insert Selected Messages into Playlist'
    Default = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    OnClick = AddMessagesBtnClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
end
