unit ZipperEntryEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, AdStatLt, ExtCtrls, {ad3SpellBase, ad3Spell,} Grids_ts,
  TSGrid, OoMisc, ComCtrls, ad3SpellBase, ad3Spell, Globals, Spin, Menus;

type
  TZipperEntryEditorDlg = class(TForm)
    Panel11: TPanel;
    Label1: TLabel;
    SpellCheckIndicator: TApdStatusLight;
    Label35: TLabel;
    BitBtn2: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn1: TBitBtn;
    RecordGrid: TtsGrid;
    TemplateName: TLabel;
    Panel4: TPanel;
    Label38: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    EntryEnable: TCheckBox;
    EntryNote: TEdit;
    EntryStartEnableDate: TDateTimePicker;
    EntryStartEnableTime: TDateTimePicker;
    EntryEndEnableDate: TDateTimePicker;
    EntryEndEnableTime: TDateTimePicker;
    HiddenEdit: TEdit;
    AddictSpell31: TAddictSpell3;
    Label16: TLabel;
    DataModeLabel: TLabel;
    Label7: TLabel;
    EntryDwellTime: TSpinEdit;
    Label2: TLabel;
    StyleChipsGrid: TtsGrid;
    BitBtn3: TBitBtn;
    EntryPopup: TPopupMenu;
    CopyfromClipboard1: TMenuItem;
    N1: TMenuItem;
    Paste1: TMenuItem;
    CuttoClipboard1: TMenuItem;
    SponsorLogoRegionLabel: TLabel;
    SponsorLogoRegion: TSpinEdit;
    SponsorLogoRegionLabel2: TLabel;
    PersistentLogoCheckbox: TCheckBox;
    procedure BitBtn2Click(Sender: TObject);
    procedure RecordGridCellChanged(Sender: TObject; OldCol, NewCol, OldRow, NewRow: Integer);
    procedure RecordGridCellEdit(Sender: TObject; DataCol, DataRow: Integer; ByUser: Boolean);
    procedure ProcessFKey(Key: Word);
    procedure PostKeyEx32( key: Word; Const shift: TShiftState; specialkey: Boolean );
    procedure RecordGridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BitBtn3Click(Sender: TObject);
    procedure StyleChipsGridDblClick(Sender: TObject);
    procedure AddStyleChipCode;
    procedure CopyfromClipboard1Click(Sender: TObject);
    procedure Paste1Click(Sender: TObject);
    procedure CuttoClipboard1Click(Sender: TObject);
    procedure RecordGridClick(Sender: TObject);
    procedure RecordGridComboInit(Sender: TObject; Combo: TtsComboGrid; DataCol, DataRow: Integer);
    procedure RecordGridComboDropDown(Sender: TObject; Combo: TtsComboGrid; DataCol, DataRow: Integer);
    procedure RecordGridRowLoaded(Sender: TObject; DataRow: Integer);
    procedure RecordGridComboGetValue(Sender: TObject; Combo: TtsComboGrid;
      GridDataCol, GridDataRow, ComboDataRow: Integer; var Value: Variant);
  private
    { Private declarations }
  public
    HasManualLeague: Boolean;
    HasWeather: Boolean;
    HasConferenceChips: Boolean;
    SelectedLeague: SmallInt;
    CustomHeader: String[20];
    ConferenceChipName: String[20];
  end;

var
  ZipperEntryEditorDlg: TZipperEntryEditorDlg;
  ComboBoxValue: SmallInt;

implementation

uses Main;

{$R *.DFM}

//Perform spell check on entry
procedure TZipperEntryEditorDlg.BitBtn2Click(Sender: TObject);
begin
  //Set spellchecker data dictionary
  AddictSpell31.ConfigDictionaryDir.Add (SpellCheckerDictionaryDir);
  //Launch the spell checker
  HiddenEdit.Text := RecordGrid.Cell[3, RecordGrid.CurrentDataRow];
  AddictSpell31.CheckWinControl(HiddenEdit, ctAll);
  //Light the indicator
  SpellCheckIndicator.Lit := TRUE;
end;

//Handlers to turn off spell check indicator
procedure TZipperEntryEditorDlg.RecordGridCellChanged(Sender: TObject;
  OldCol, NewCol, OldRow, NewRow: Integer);
begin
  //Extinguish spell check light
  SpellCheckIndicator.Lit := FALSE;
end;
procedure TZipperEntryEditorDlg.RecordGridCellEdit(Sender: TObject;
  DataCol, DataRow: Integer; ByUser: Boolean);
begin
  //Extinguish spell check light
  SpellCheckIndicator.Lit := FALSE;
end;

//Procedure to process function keys
procedure TZipperEntryEditorDlg.ProcessFKey(Key: Word);
var
  StyleChipRecPtr: ^StyleChipRec;
  MyShiftState: TShiftState;
begin
  if (Key = VK_F1) then
  begin
    if (StyleChip_Collection.Count >= 1) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(0);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F2) then
  begin
    if (StyleChip_Collection.Count >= 2) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(1);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F3) then
  begin
    if (StyleChip_Collection.Count >= 3) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(2);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F4) then
  begin
    if (StyleChip_Collection.Count >= 4) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(3);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F5) then
  begin
    if (StyleChip_Collection.Count >= 5) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(4);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F6) then
  begin
    if (StyleChip_Collection.Count >= 6) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(5);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F7) then
  begin
    if (StyleChip_Collection.Count >= 7) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(6);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F8) then
  begin
    if (StyleChip_Collection.Count >= 8) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(7);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F9) then
  begin
    if (StyleChip_Collection.Count >= 9) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(8);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F10) then
  begin
    if (StyleChip_Collection.Count >= 10) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(9);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F11) then
  begin
    if (StyleChip_Collection.Count >= 11) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(10);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end
  else if (Key = VK_F12) then
  begin
    if (StyleChip_Collection.Count >= 12) then
    begin
      StyleChipRecPtr := StyleChip_Collection.At(11);
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
      PostKeyEx32(VK_END, MyShiftState, FALSE);
    end;
  end;
end;

//Handler for key down on grid - for processing function keys
procedure TZipperEntryEditorDlg.RecordGridKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  ProcessFKey(Key);
end;

//General procedure to send the specified key to the keyboard buffer
Procedure TZipperEntryEditorDlg.PostKeyEx32( key: Word; Const shift: TShiftState;
            specialkey: Boolean );
Type
  TShiftKeyInfo = Record
                    shift: Byte;
                    vkey : Byte;
                  End;
  byteset = Set of 0..7;
Const
  shiftkeys: Array [1..3] of TShiftKeyInfo =
    ((shift: Ord(ssCtrl); vkey: VK_CONTROL ),
     (shift: Ord(ssShift); vkey: VK_SHIFT ),
     (shift: Ord(ssAlt); vkey: VK_MENU ));
Var
  flag: DWORD;
  bShift: ByteSet absolute shift;
  i: Integer;
Begin
  For i := 1 To 3 Do Begin
    If shiftkeys[i].shift In bShift Then
      keybd_event( shiftkeys[i].vkey,
                   MapVirtualKey(shiftkeys[i].vkey, 0),
                   0, 0);
  End; { For }
  If specialkey Then
    flag := KEYEVENTF_EXTENDEDKEY
  Else
    flag := 0;

  keybd_event( key, MapvirtualKey( key, 0 ), flag, 0 );
  flag := flag or KEYEVENTF_KEYUP;
  keybd_event( key, MapvirtualKey( key, 0 ), flag, 0 );


  For i := 3 DownTo 1 Do Begin
    If shiftkeys[i].shift In bShift Then
      keybd_event( shiftkeys[i].vkey,
                   MapVirtualKey(shiftkeys[i].vkey, 0),
                   KEYEVENTF_KEYUP, 0);
  End; { For }
End; { PostKeyEx32 }

//Handlers for adding style chip code
procedure TZipperEntryEditorDlg.BitBtn3Click(Sender: TObject);
begin
  AddStyleChipCode;
end;
procedure TZipperEntryEditorDlg.StyleChipsGridDblClick(Sender: TObject);
begin
  AddStyleChipCode;
end;

//Procedure for adding style chip code
procedure TZipperEntryEditorDlg.AddStyleChipCode;
var
  i: SmallInt;
  StyleChipRecPtr: ^StyleChipRec;
  MyShiftState: TShiftState;
  Buffer1, Buffer2: PChar;
  Size: Integer;
  OutText: String;
begin
  if (StyleChip_Collection.Count > 0) then
  begin
    i := StyleChipsGrid.CurrentDataRow-1;
    if (i < 0) then i := 0;
    //Get the style chip code
    StyleChipRecPtr := StyleChip_Collection.At(i);
    //Add the style chip code to the text
    if (RecordGrid.CellReadOnly[3, RecordGrid.CurrentDataRow]= roOff) then
      RecordGrid.Cell[3, RecordGrid.CurrentDataRow] :=
        RecordGrid.Cell[3, RecordGrid.CurrentDataRow] + StyleChipRecPtr^.StyleChip_Code;
    //Set focus back to grid
    RecordGrid.SetFocus;
    PostKeyEx32(VK_END, MyShiftState, FALSE);
  end;
end;

//Handler to move cursor to end of line and un-highlight text when moving to next/previous row
procedure TZipperEntryEditorDlg.RecordGridRowLoaded(Sender: TObject;
  DataRow: Integer);
var
  MyShiftState: TShiftState;
begin
    PostKeyEx32(VK_END, MyShiftState, FALSE);
end;

//Handler to Copy the text from to the windows clipboard from the current cell of the grid
procedure TZipperEntryEditorDlg.CopyfromClipboard1Click(Sender: TObject);
begin
  //Select all text and copy to the clipboard
  RecordGrid.CurrentCell.SelectAll;
  RecordGrid.CurrentCell.CopyToClipboard;
end;

//Handler to paste the text from the windows clipboard into the current cell of the grid
procedure TZipperEntryEditorDlg.Paste1Click(Sender: TObject);
begin
  RecordGrid.CurrentCell.PasteFromClipboard;
end;

//Handler for cut to clipboard
procedure TZipperEntryEditorDlg.CuttoClipboard1Click(Sender: TObject);
begin
  //Select all text and copy to the clipboard
  RecordGrid.CurrentCell.SelectAll;
  RecordGrid.CurrentCell.CopyToClipboard;
  //Clear the text
  RecordGrid.CurrentCell.SelText := '';
end;

////////////////////////////////////////////////////////////////////////////////
// MANUAL WINNER FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Check to see if any winner/loser boxes need to be checked/unchecked
procedure TZipperEntryEditorDlg.RecordGridClick(Sender: TObject);
var
  i: SmallInt;
begin
  //If checking new visitor winner, check all visitor boxes and un-check home boxes
  if (RecordGrid.Cell[4, RecordGrid.CurrentDataRow] = 5) AND
     (RecordGrid.CellCheckBoxState[3,RecordGrid.CurrentDataRow] = cbChecked) then
  begin
    for i := 1 to RecordGrid.Rows do
      if (RecordGrid.Cell[4, i] = 5) then
        RecordGrid.CellCheckBoxState[3, i] := cbChecked;
    for i := 1 to RecordGrid.Rows do
      if (RecordGrid.Cell[4, i] = 6) then
        RecordGrid.CellCheckBoxState[3, i] := cbUnchecked;
  end
  //If unchecking visitor winner, uncheck all visitor winners
  else if (RecordGrid.Cell[4, RecordGrid.CurrentDataRow] = 5) AND
     (RecordGrid.CellCheckBoxState[3,RecordGrid.CurrentDataRow] = cbUnChecked) then
  begin
    for i := 1 to RecordGrid.Rows do
      if (RecordGrid.Cell[4, i] = 5) then
        RecordGrid.CellCheckBoxState[3, i] := cbUnChecked;
  end
  //If checking new home winner, check all home boxes and un-check visitor boxes
  else if (RecordGrid.Cell[4, RecordGrid.CurrentDataRow] = 6) AND
     (RecordGrid.CellCheckBoxState[3,RecordGrid.CurrentDataRow] = cbChecked) then
  begin
    for i := 1 to RecordGrid.Rows do
      if (RecordGrid.Cell[4, i] = 6) then
        RecordGrid.CellCheckBoxState[3, i] := cbChecked;
    for i := 1 to RecordGrid.Rows do
      if (RecordGrid.Cell[4, i] = 5) then
        RecordGrid.CellCheckBoxState[3, i] := cbUnchecked;
  end
  //If unchecking home winner, uncheck all home winners
  else if (RecordGrid.Cell[4, RecordGrid.CurrentDataRow] = 6) AND
     (RecordGrid.CellCheckBoxState[3,RecordGrid.CurrentDataRow] = cbUnChecked) then
  begin
    for i := 1 to RecordGrid.Rows do
      if (RecordGrid.Cell[4, i] = 6) then
        RecordGrid.CellCheckBoxState[3, i] := cbUnChecked;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// GRID COMBO BOX FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Init the combo grid
procedure TZipperEntryEditorDlg.RecordGridComboInit(Sender: TObject;
  Combo: TtsComboGrid; DataCol, DataRow: Integer);
begin
  //if (HasWeather) then
  if (DataRow > 1) then
  begin
    //Set list type
    Combo.DropDownStyle := ddDropDownList;
    //Set the number of rows displayed
    Combo.DropDownRows := 12;
    Combo.ValueCol := 1;
    //Set AutoSearch on so user input or drop down causes automatic positioning
    Combo.AutoSearch := asCenter;
    //Set the number of columns displayed to 1
    Combo.DropDownCols := 1;
    Combo.Grid.Cols := 1;
    Combo.Grid.HeadingOn := False;
    Combo.Grid.RowBarOn := False;
    Combo.Grid.DefaultColWidth := 150;
    //Set row properties
    //Combo.Grid.Rows := Weather_Icon_Collection.Count;
    Combo.Grid.Rows := Conference_Chip_Collection.Count;
    Combo.Grid.DefaultRowHeight := 20;
  end
  //else if (HasManualHeader) then
  else begin
    //Set list type
    Combo.DropDownStyle := ddDropDownList;
    //Set the number of rows displayed
    Combo.DropDownRows := 12;
    Combo.ValueCol := 1;
    //Set AutoSearch on so user input or drop down causes automatic positioning
    Combo.AutoSearch := asCenter;
    //Set the number of columns displayed to 1
    Combo.DropDownCols := 1;
    Combo.Grid.Cols := 3;
    Combo.Grid.HeadingOn := False;
    Combo.Grid.RowBarOn := False;
    Combo.Grid.DefaultColWidth := 150;
    //Set row properties
    Combo.Grid.Rows := CustomNewsHeader_Collection.Count;
    Combo.Grid.DefaultRowHeight := 20;
    //Combo.Grid.CurrentDataRow := 1;
  end;
end;

//Init the combo grid
procedure TZipperEntryEditorDlg.RecordGridComboDropDown(Sender: TObject;
  Combo: TtsComboGrid; DataCol, DataRow: Integer);
var
  i: SmallInt;
  WeatherIconRecPtr: ^WeatherIconRec;
  ConferenceChipRecPtr: ^ConferenceChipRec;
  CustomNewsheaderRecPtr: ^CustomNewsHeaderRec;
begin
  //Check if weather dropdown
  if (DataRow > 1) then
  begin
    //Set list type
    Combo.DropDownStyle := ddDropDownList;
    //Set the number of rows displayed
    //Combo.DropDownRows := Weather_Icon_Collection.Count;
    Combo.DropDownRows := 12;
    Combo.ValueCol := 1;
    //Set AutoSearch on so user input or drop down causes automatic positioning
    Combo.AutoSearch := asCenter;
    //Set the number of columns displayed to 1
    Combo.DropDownCols := 1;
    Combo.Grid.Cols := 1;
    Combo.Grid.HeadingOn := False;
    Combo.Grid.RowBarOn := False;
    Combo.Grid.DefaultColWidth := 150;
    //Set row properties
    //Combo.Grid.Rows := Weather_Icon_Collection.Count;
    Combo.Grid.Rows := Conference_Chip_Collection.Count;
    Combo.Grid.DefaultRowHeight := 20;
    //Populate the grid
    Combo.Grid.StoreData := TRUE;
//    if (Weather_Icon_Collection.Count > 0) then
//    begin
//      for i := 1 to Weather_Icon_Collection.Count do
//      begin
//        WeatherIconRecPtr := Weather_Icon_Collection.At(i-1);
//        Combo.Grid.Cell[1,i] := WeatherIconRecPtr^.Icon_Name;
//      end;
//    end;
    if (Conference_Chip_Collection.Count > 0) then
    begin
      for i := 1 to Conference_Chip_Collection.Count do
      begin
        ConferenceChipRecPtr := Conference_Chip_Collection.At(i-1);
        Combo.Grid.Cell[1,i] := ConferenceChipRecPtr^.Conference_Name;
      end;
    end;
  end
  //else if (HasManualLeague) then
  else begin
    //Set list type
    Combo.DropDownStyle := ddDropDownList;
    //Set the number of rows displayed
    Combo.DropDownRows := 12;
    Combo.ValueCol := 3;
    //Set AutoSearch on so user input or drop down causes automatic positioning
    Combo.AutoSearch := asCenter;
    //Set the number of columns displayed to 1
    Combo.DropDownCols := 1;
    Combo.Grid.Cols := 1;
    Combo.Grid.HeadingOn := False;
    Combo.Grid.RowBarOn := False;
    //Set row properties
    Combo.Grid.Rows := CustomNewsHeader_Collection.Count;
    Combo.Grid.DefaultRowHeight := 20;
    Combo.Grid.DefaultColWidth := 150;
    Combo.Grid.CurrentDataRow := 1;
    //Populate the grid
    Combo.Grid.StoreData := TRUE;
    if (CustomNewsheader_Collection.Count > 0) then
    begin
      for i := 1 to CustomNewsheader_Collection.Count do
      begin
        CustomNewsheaderRecPtr := CustomNewsheader_Collection.At(i-1);
        Combo.Grid.Cell[1,i] := CustomNewsheaderRecPtr^.HeaderText;
      end;
    end;
  end;
end;

//Handler to get values selected from dropdown - store in dialog variables
procedure TZipperEntryEditorDlg.RecordGridComboGetValue(Sender: TObject;
  Combo: TtsComboGrid; GridDataCol, GridDataRow, ComboDataRow: Integer;
  var Value: Variant);
var
  CustomNewsHeaderRecPtr: ^CustomNewsHeaderRec;
  WeatherIconRecPtr: ^WeatherIconRec;
  ConferenceChipRecPtr: ^ConferenceChipRec;
begin
  if (GridDataRow > 1) then
  begin
//    if (Weather_Icon_Collection.Count > 0) then
//    begin
//      WeatherIconRecPtr := Weather_Icon_Collection.At(ComboDataRow-1);
//      Value := WeatherIconRecPtr^.Icon_Name;
//    end;
    if (Conference_Chip_Collection.Count > 0) then
    begin
      ConferenceChipRecPtr := Conference_Chip_Collection.At(ComboDataRow-1);
      Value := ConferenceChipRecPtr^.Conference_Name;
      ConferenceChipName := ConferenceChipRecPtr^.Conference_Name;
    end;
  end
  else begin
    if (CustomNewsheader_Collection.Count > 0) then
    begin
      CustomNewsheaderRecPtr := CustomNewsheader_Collection.At(ComboDataRow-1);
      Value := CustomNewsHeaderRecPtr^.HeaderText;
      CustomHeader := CustomNewsHeaderRecPtr^.HeaderText;
    end;
  end;
end;

end.
