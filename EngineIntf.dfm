object EngineInterface: TEngineInterface
  Left = 446
  Top = 250
  Width = 265
  Height = 252
  Caption = 'Engine Interface'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object AdTerminal1: TAdTerminal
    Left = 66
    Top = 8
    Width = 145
    Height = 73
    Active = False
    CaptureFile = 'APROTERM.CAP'
    Scrollback = False
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clSilver
    Font.Height = -12
    Font.Name = 'Terminal'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 0
  end
  object PacketEnableTimer: TTimer
    Enabled = False
    Interval = 10
    OnTimer = PacketEnableTimerTimer
    Left = 24
    Top = 56
  end
  object TickerCommandDelayTimer: TTimer
    Enabled = False
    OnTimer = TickerCommandDelayTimerTimer
    Left = 24
    Top = 94
  end
  object PacketIndicatorTimer: TTimer
    Enabled = False
    Interval = 250
    OnTimer = PacketIndicatorTimerTimer
    Left = 88
    Top = 177
  end
  object TickerPacketTimeoutTimer: TTimer
    Enabled = False
    Interval = 10000
    OnTimer = TickerPacketTimeoutTimerTimer
    Left = 24
    Top = 132
  end
  object JumpToNextTickerRecordTimer: TTimer
    Enabled = False
    Interval = 10
    OnTimer = JumpToNextTickerRecordTimerTimer
    Left = 123
    Top = 94
  end
  object BugCommandDelayTimer: TTimer
    Enabled = False
    OnTimer = BugCommandDelayTimerTimer
    Left = 56
    Top = 94
  end
  object ExtraLineCommandDelayTimer: TTimer
    Enabled = False
    OnTimer = ExtraLineCommandDelayTimerTimer
    Left = 88
    Top = 94
  end
  object JumpToNextBugRecordTimer: TTimer
    Enabled = False
    Interval = 10
    OnTimer = JumpToNextBugRecordTimerTimer
    Left = 152
    Top = 94
  end
  object JumpToNextGametraxRecordTimer: TTimer
    Enabled = False
    Interval = 10
    OnTimer = JumpToNextGametraxRecordTimerTimer
    Left = 184
    Top = 94
  end
  object DisplayClockTimer: TTimer
    Enabled = False
    Interval = 10000
    Left = 56
    Top = 176
  end
  object BugPacketTimeoutTimer: TTimer
    Enabled = False
    Interval = 10000
    OnTimer = BugPacketTimeoutTimerTimer
    Left = 56
    Top = 133
  end
  object ExtraLinePacketTimeoutTimer: TTimer
    Enabled = False
    Interval = 10000
    OnTimer = ExtraLinePacketTimeoutTimerTimer
    Left = 88
    Top = 133
  end
  object EnginePort: TClientSocket
    Active = False
    ClientType = ctNonBlocking
    Port = 0
    OnConnect = EnginePortConnect
    OnDisconnect = EnginePortDisconnect
    OnRead = EnginePortRead
    OnError = EnginePortError
    Left = 24
    Top = 16
  end
end
