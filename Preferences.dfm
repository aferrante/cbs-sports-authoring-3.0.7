object Prefs: TPrefs
  Left = 501
  Top = 70
  BorderStyle = bsDialog
  Caption = 'User Preferences'
  ClientHeight = 747
  ClientWidth = 630
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 222
    Top = 700
    Width = 81
    Height = 38
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 326
    Top = 700
    Width = 81
    Height = 38
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkCancel
  end
  object Panel2: TPanel
    Left = 16
    Top = 220
    Width = 600
    Height = 304
    BevelWidth = 2
    TabOrder = 2
    object Label2: TLabel
      Left = 16
      Top = 8
      Width = 253
      Height = 16
      Caption = 'SQL Server ADO Connection Strings:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 16
      Top = 83
      Width = 184
      Height = 16
      Caption = 'VDS Sportbase Database:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 16
      Top = 27
      Width = 158
      Height = 16
      Caption = 'Main Ticker Database:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 16
      Top = 139
      Width = 141
      Height = 16
      Caption = 'CBS CSS Database:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 16
      Top = 195
      Width = 172
      Height = 16
      Caption = 'NFL Datamart Database:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 16
      Top = 248
      Width = 267
      Height = 16
      Caption = 'VDS Social Media Gateway Database:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Edit2: TEdit
      Left = 16
      Top = 49
      Width = 569
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object Edit3: TEdit
      Left = 16
      Top = 105
      Width = 569
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object Edit4: TEdit
      Left = 16
      Top = 161
      Width = 569
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object Edit5: TEdit
      Left = 16
      Top = 217
      Width = 569
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
    object Edit6: TEdit
      Left = 16
      Top = 270
      Width = 569
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
    end
  end
  object Panel1: TPanel
    Left = 16
    Top = 171
    Width = 600
    Height = 49
    BevelWidth = 2
    TabOrder = 3
    object Label1: TLabel
      Left = 18
      Top = 16
      Width = 263
      Height = 16
      Caption = 'Authoring Station ID (Must Be Unique):'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object SpinEdit1: TSpinEdit
      Left = 287
      Top = 12
      Width = 57
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 2
      MaxValue = 99
      MinValue = 1
      ParentFont = False
      TabOrder = 0
      Value = 1
    end
    object CheckBox1: TCheckBox
      Left = 359
      Top = 16
      Width = 212
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Graphics Generator Enable:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
  object Panel3: TPanel
    Left = 16
    Top = 525
    Width = 600
    Height = 71
    BevelWidth = 2
    TabOrder = 4
    object Label3: TLabel
      Left = 16
      Top = 8
      Width = 254
      Height = 16
      Alignment = taRightJustify
      Caption = 'Spellchecker Dictionary Folder Path:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Edit1: TEdit
      Left = 16
      Top = 30
      Width = 569
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
  end
  object Panel5: TPanel
    Left = 16
    Top = 8
    Width = 600
    Height = 163
    BevelWidth = 2
    TabOrder = 5
    object RadioGroup2: TRadioGroup
      Left = 14
      Top = 6
      Width = 202
      Height = 73
      Caption = 'Automated Data Source'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemIndex = 0
      Items.Strings = (
        'Primary (VDS)'
        '(Backup (CSS)')
      ParentFont = False
      TabOrder = 0
    end
    object CheckBox2: TCheckBox
      Left = 20
      Top = 108
      Width = 225
      Height = 17
      Caption = 'Enable Possession Indicator'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object CheckBox3: TCheckBox
      Left = 292
      Top = 84
      Width = 201
      Height = 17
      Caption = 'Enable Down/Yardage'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
    object CheckBox4: TCheckBox
      Left = 20
      Top = 84
      Width = 236
      Height = 17
      Caption = 'Enable Field Position Indicator'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
    end
    object RadioGroup3: TRadioGroup
      Left = 355
      Top = 6
      Width = 230
      Height = 73
      Caption = 'NFL Season/Week Info Source'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemIndex = 0
      Items.Strings = (
        'Primary (CSS)'
        'Backup (Manual)')
      ParentFont = False
      TabOrder = 4
    end
    object RadioGroup4: TRadioGroup
      Left = 227
      Top = 6
      Width = 118
      Height = 73
      Caption = 'CSS Data'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemIndex = 0
      Items.Strings = (
        'Disable'
        'Enable')
      ParentFont = False
      TabOrder = 5
    end
    object CheckBox5: TCheckBox
      Left = 292
      Top = 108
      Width = 221
      Height = 17
      Caption = 'Enable Live Data Updates'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
    end
    object CheckBox6: TCheckBox
      Left = 20
      Top = 132
      Width = 225
      Height = 17
      Caption = 'Enable Winner Indicator'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
    end
  end
  object Panel6: TPanel
    Left = 16
    Top = 596
    Width = 600
    Height = 100
    BevelWidth = 2
    TabOrder = 6
    object Label11: TLabel
      Left = 435
      Top = 34
      Width = 53
      Height = 16
      Caption = 'Week #'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 101
      Top = 34
      Width = 34
      Height = 16
      Caption = 'Year'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel
      Left = 16
      Top = 10
      Width = 379
      Height = 16
      Caption = 'NFL Season Year and Week (for VDS as Data Source):'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object SpinEdit2: TSpinEdit
      Left = 435
      Top = 59
      Width = 73
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxValue = 25
      MinValue = 1
      ParentFont = False
      TabOrder = 0
      Value = 1
    end
    object SpinEdit3: TSpinEdit
      Left = 99
      Top = 56
      Width = 62
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxValue = 2020
      MinValue = 2000
      ParentFont = False
      TabOrder = 1
      Value = 2020
    end
    object RadioGroup1: TRadioGroup
      Left = 199
      Top = 34
      Width = 201
      Height = 51
      Caption = 'Season Type'
      Columns = 3
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemIndex = 0
      Items.Strings = (
        'PRE'
        'REG'
        'POST')
      ParentFont = False
      TabOrder = 2
    end
  end
end
