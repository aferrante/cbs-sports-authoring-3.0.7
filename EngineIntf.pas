// Rev: 11/08/06  M. Dilworth  Video Design Software Inc.
unit EngineIntf;

////////////////////////////////////////////////////////////////////////////////
// NOTES:
// Game State defined as follows for this project
// 1 = Pre-Game
// 2 = In-game, clock running
// 3 = In game, no clock running (specific to CBS CSS)
// 4 = End of period
// 5 = Final
////////////////////////////////////////////////////////////////////////////////

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OoMisc, ADTrmEmu, AdPacket, AdPort, AdWnPort, ExtCtrls, Globals, FileCtrl,
  ScktComp;

type
  TimeRec = record
    Minutes: SmallInt;
    Seconds: SmallInt;
  end;

  TEngineInterface = class(TForm)
    AdTerminal1: TAdTerminal;
    PacketEnableTimer: TTimer;
    TickerCommandDelayTimer: TTimer;
    PacketIndicatorTimer: TTimer;
    TickerPacketTimeoutTimer: TTimer;
    JumpToNextTickerRecordTimer: TTimer;
    BugCommandDelayTimer: TTimer;
    ExtraLineCommandDelayTimer: TTimer;
    JumpToNextBugRecordTimer: TTimer;
    JumpToNextGametraxRecordTimer: TTimer;
    DisplayClockTimer: TTimer;
    BugPacketTimeoutTimer: TTimer;
    ExtraLinePacketTimeoutTimer: TTimer;
    EnginePort: TClientSocket;
    procedure PacketEnableTimerTimer(Sender: TObject);
    procedure TickerCommandDelayTimerTimer(Sender: TObject);
    procedure PacketIndicatorTimerTimer(Sender: TObject);
    procedure TickerPacketTimeoutTimerTimer(Sender: TObject);
    procedure JumpToNextTickerRecordTimerTimer(Sender: TObject);
    procedure WriteToErrorLog (ErrorString: String);
    procedure WriteToAsRunLog (AsRunString: String);
    procedure BugCommandDelayTimerTimer(Sender: TObject);
    procedure JumpToNextBugRecordTimerTimer(Sender: TObject);
    procedure ExtraLineCommandDelayTimerTimer(Sender: TObject);
    procedure JumpToNextGametraxRecordTimerTimer(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BugPacketTimeoutTimerTimer(Sender: TObject);
    procedure ExtraLinePacketTimeoutTimerTimer(Sender: TObject);
    procedure EnginePortConnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure EnginePortDisconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure EnginePortError(Sender: TObject; Socket: TCustomWinSocket;
      ErrorEvent: TErrorEvent; var ErrorCode: Integer);
    procedure EnginePortRead(Sender: TObject; Socket: TCustomWinSocket);
  private
    { Private declarations }
    function ProcessStyleChips(CmdStr: String): String;
  public
    { Public declarations }
    function GetMixedCase(InStr: String): String;
    function GetSponsorLogoFileName(LogoName: String): String;
    function CheckForActiveGametraxSponsorLogo: Boolean;

    procedure StartTickerData;
    procedure SendTickerRecord(NextRecord: Boolean; RecordIndex: SmallInt);
    procedure SendBugRecord(NextRecord: Boolean; RecordIndex: SmallInt);
    procedure SendGameTraxRecord(NextRecord: Boolean; RecordIndex: SmallInt);

    procedure TriggerGraphic (Mode: SmallInt);

    //Function to get CFB conference chip character value
    function GetConferenceChipCharacterValue(ConferenceName: String): Smallint;

    //CSS Specific functions
    function GetCSSGameTimeRecord(GTimeStr: String): TimeRec;

    //NFLDM Specific functions
    function GetNFLDMGamePhaseString(League: String; GameState: SmallInt; GamePhase: String): String;
    function GetNFLDMYardageIndidcatorValue(CurrentGameData: GameRec): SmallInt;
    function FormatNFLDMTimeString (InStr: String): String;
    function GetNFLDMGameTimeRecord(GTimeStr: String): TimeRec;
    function GetNFLDMGameState(GamePhase: String; GameTime: String): SmallInt;
    function GetNFLDMStatsString(StatType: SmallInt; Clubkey: LongInt):String;
    function GetNFLDMFantasyStatsString(StatType: SmallInt; Rank: SmallInt):String;

    //Stats Inc. Specific functions
    function FormatSIGameClock(TimeMin: SmallInt; TimeSec: SmallInt): String;
    function GetSIFootballGameSituationData(CurrentGameData: GameRec): FootballSituationRec;
    function GetSIGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;
    function GetSIGameState(State: String; TimeMin: String; TimeSec: String): SmallInt;
    function GetSIStatsString(SI_Gcode: String; StatType: SmallInt; IsVisitor: Boolean):String;

    //Sportsticker Specific functions
    function GetSTGameTimeString(GTimeStr: String): String;
    function GetSTGameState(LEAGUE: String; GTIME: String; GPHASE: SmallInt): Integer;
    function GetSTGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;

    function LoadTempTemplateFields(TemplateID: SmallInt): TemplateDefsRec;

    function TrimYear(InStr: String): String;
    function GetValueOfSymbol(PlaylistType: SmallInt; Symbolname: String; SymbolType: SmallInt; CurrentEntryIndex: SmallInt;
      CurrentGameData: GameRec; TickerMode: SmallInt): String;

    //Function to run stored procedures to get game information
    function GetGameData (League: String; SI_GCode, ST_Game_ID: String;
                          CSS_GameID, NFLDM_GameID: LongInt;
                          Primary_Data_Source, Backup_Data_Source: String): GameRec;

    function SplitCSSGamePhase(PhaseString: String): String;

    function GetConferenceChipText(ConferenceName: String): String;

    //For Twitter
    function GetTwitterMessageForGame(NFLGameID: LongInt): TwitterMessageRec;
    function GetTwitterMessageByID(TwitterMessageID: Extended): TwitterMessageRec;

    //Procedure for score update functions
    function GetScoringPlayDescription(GameID: LongInt): ScoringUpdateRec;
  end;

const
  SOT: String[1] = #1;
  ETX: String[1] = #3;
  EOT: String[1] = #4;
  GS: String[1] = #29;
  RS: String[1] = #30;
  NAK: String[1] = #21; //Used as No-Op
  EOB: String[1] = #23; //Send at end of last record; will cause SS 11 to be sent by engine

var
  EngineInterface: TEngineInterface;
  DisableCommandTimer: Boolean;

const
  //Boolean to select whether or not the data packet is used to trigger the next graphic
  UseDataPacket = TRUE;


implementation

uses Main, //Main form
     DataModule; //Data module

{$R *.DFM}
//Imit
procedure TEngineInterface.FormActivate(Sender: TObject);
begin
end;

////////////////////////////////////////////////////////////////////////////////
// SCORING PLAY TEST FUNCTION
////////////////////////////////////////////////////////////////////////////////
//Function to get play description for score update function
function TEngineInterface.GetScoringPlayDescription(GameID: LongInt): ScoringUpdateRec;
var
  i: SmallInt;
  EndPos: SmallInt;
  InStr, OutStr: String;
  Outrec: ScoringUpdateRec;
begin
  try
    with dmMain.DatamartQuery4 do
    begin
      Close;
      SQL.Clear;
      //Build query to get last scoring play; order is reverse chronological
      SQL.Add('/* */SELECT * FROM tblGameScoreInfo WHERE GameKey = ' +
        IntToStr(GameID) + ' ORDER BY Sequence DESC');
      //Get the data
      Open;

      //Process the data
      if (RecordCount > 0) then
      begin
        //Go to first record which will be the last scoring play
        First;
        InStr := FieldByName('ScoreDescription').AsString;
        OutRec.NFLClubCode := FieldByName('ClubCode').AsString;
        //Strip off all data in parenthesis
        OutStr := '';
        EndPos := Pos('(', InStr);
        if (EndPos = 0) then OutStr := InStr
        else if (EndPos > 1) then
          for i := 1 to EndPos-1 do OutStr := OutStr + InStr[i]
        else
          OutStr := '';
      end
      else begin
        OutStr := '';
      end;
    end;
    //Close query
    Close;
    if (ForceUpperCaseScoringPlayDescription) then OutStr := ANSIUpperCase(OutStr);
    //Replace "yd." with "yd"
    OutStr := StringReplace(OutStr, 'yd.', 'yd', [rfReplaceAll]);
    OutStr := StringReplace(OutStr, 'YD.', 'YD', [rfReplaceAll]);
  except
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred while trying to get scoring play information for scoring update.');
  end;
  OutRec.ScoreDescription := OutStr;
  GetScoringPlayDescription := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
// UTILITY FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Function to put a pipe between the quarter number and the suffix for the CSS game phase
function TEngineInterface.SplitCSSGamePhase(PhaseString: String): String;
var
  OutVal: String;
begin
  OutVal := ANSIUpperCase(PhaseString);
  OutVal := StringReplace(OutVal, 'ST', '|ST', [rfReplaceAll]);
  OutVal := StringReplace(OutVal, 'ND', '|ND', [rfReplaceAll]);
  OutVal := StringReplace(OutVal, 'RD', '|RD', [rfReplaceAll]);
  OutVal := StringReplace(OutVal, 'TH', '|TH', [rfReplaceAll]);
  //If not one of these values, append pipe to prevent superscript
  if (pos('|', OutVal) = 0) then OutVal := OutVal + '|';
  SplitCSSGamePhase := OutVal;
end;

//Function to do style chip substitutions
function TEngineInterface.ProcessStyleChips(CmdStr: String): String;
var
  i,j: SmallInt;
  StyleChipRecPtr: ^StyleChipRec;
  OutStr: String;
  SwitchPos: SmallInt;
begin
  //Check for presence of style chips
  //NOTE: Conversion to mixed case/upper case done in function to get field contents
  OutStr := CmdStr;
  if (StyleChip_Collection.Count > 0) AND (Length(CmdStr) >= 4) then
  begin
    for i := 0 to StyleChip_Collection.Count-1 do
    begin
      StyleChipRecPtr := StyleChip_Collection.At(i);
      //Check style chip type and substiture accordingly
      //Type 1 = Use substitution string
      if (StyleChipRecPtr^.StyleChip_Type = 1) then
        OutStr := StringReplace(OutStr, StyleChipRecPtr^.StyleChip_Code,
                  StyleChipRecPtr^.StyleChip_String, [rfReplaceAll])
      //Type 2 = Use substitution font & character
      else if (StyleChipRecPtr^.StyleChip_Type = 2) then
        OutStr := StringReplace(OutStr, StyleChipRecPtr^.StyleChip_Code,
                  '[f ' + IntToStr(StyleChipRecPtr^.StyleChip_FontCode) + ']' +
                  Char(StyleChipRecPtr^.StyleChip_CharacterCode), [rfReplaceAll])
    end;
  end;
  ProcessStyleChips := OutStr;
end;

////////////////////////////////////////////////////////////////////////////////
// Function to take the name of the CFB conference and return the character code
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetConferenceChipCharacterValue(ConferenceName: String): Smallint;
var
  i: SmallInt;
  OutVal: SmallInt;
  ConferenceChipRecPtr: ^ConferenceChipRec;
begin
  for i := 0 to Conference_Chip_Collection.Count-1 do
  begin
    ConferenceChipRecPtr := Conference_Chip_Collection.At(i);
    if (ConferenceChiprecPtr^.Conference_Name = ConferenceName) then
    begin
      OutVal := ConferenceChipRecPtr^.Conference_Chip_Character_Value;
    end;
  end;
  GetConferenceChipCharacterValue := OutVal;
end;

////////////////////////////////////////////////////////////////////////////////
// Function to take the name of the CFB conference and return the text string
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetConferenceChipText(ConferenceName: String): String;
var
  i,j: SmallInt;
  OutVal: String;
  ConferenceChipRecPtr: ^ConferenceChipRec;
  TempStr: String;
begin
  {
  for i := 0 to Conference_Chip_Collection.Count-1 do
  begin
    ConferenceChipRecPtr := Conference_Chip_Collection.At(i);
    if (ConferenceChiprecPtr^.Conference_Name = ConferenceName) then
    begin
      OutVal := ConferenceChipRecPtr^.Conference_Chip_Text;
      //Check for logos
      if (Length(OutVal) >= 3) AND (OutVal[1] = '[') then
      begin
        TempStr := '';
        j := 2;
        while (OutVal[j] <> ']') and (j <= Length(OutVal)) do
        begin
          TempStr := TempStr + OutVal[j];
          inc(j);
        end;
        OutVal := '[f 6]' + Chr(StrToIntDef(TempStr, 32));
      end;
    end;
  end;
  GetConferenceChipText := OutVal;
  }
  for i := 0 to Conference_Chip_Collection.Count-1 do
  begin
    ConferenceChipRecPtr := Conference_Chip_Collection.At(i);
    if (ConferenceChiprecPtr^.Conference_Name = ConferenceName) then
    begin
      OutVal := ConferenceChipRecPtr^.Conference_Chip_Text;
    end;
  end;
  GetConferenceChipText := OutVal;
end;

////////////////////////////////////////////////////////////////////////////////
// Function to take upper case string & return mixed-case string
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetMixedCase(InStr: String): String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := ANSILowerCase(InStr);
  if (Length(OutStr) > 0) then
  begin
    //Set first character to upper case if it's not a digit
    if (Ord(OutStr[1]) < 48) OR (Ord(OutStr[1]) > 57) then  OutStr[1] := Char(Ord(OutStr[1])-32);
    //Check if two words in state name, and set 2nd word to upper case
    if (Length(OutStr) > 2) then
    begin
      for i := 2 to Length(OutStr) do
      begin
        if (OutStr[i-1] = ' ') then OutStr[i] := Char(Ord(OutStr[i])-32);
      end;
    end;
  end;
  GetMixedCase := OutStr;
end;

////////////////////////////////////////////////////////////////////////////////
// Function to get sponsor logo file name based on logo description
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetSponsorLogoFilename(LogoName: String): String;
var
  i: SmallInt;
  OutStr: String;
  SponsorLogoPtr: ^SponsorLogoRec;
begin
  for i := 0 to SponsorLogo_Collection.Count-1 do
  begin
    SponsorLogoPtr := SponsorLogo_Collection.At(i);
    if (SponsorLogoPtr^.SponsorLogoName = LogoName) then
    begin
      OutStr := SponsorLogoPtr^.SponsorLogoFileName;
    end;
  end;
  GetSponsorLogoFilename := OutStr;
end;

////////////////////////////////////////////////////////////////////////////////
// Function to check the ticker collection for any sponsor logo with a valid time window
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.CheckForActiveGameTraxSponsorLogo: Boolean;
var
  i: SmallInt;
  GametraxRecPtr: ^GametraxRec;
  OutVal: Boolean;
begin
  //Init
  OutVal := FALSE;
  //Check collection for any sponsor logo with a valid time window
  if (Gametrax_Collection.Count > 0) then
  begin
    for i := 0 to Gametrax_Collection.Count-1 do
    begin
      GametraxRecPtr := Gametrax_Collection.At(i);
      if (GametraxRecPtr^.Template_ID = 1) AND (GametraxRecPtr^.StartEnableDateTime <= Now) AND
         (GametraxRecPtr^.EndEnableDateTime > Now) then OutVal := TRUE;
    end;
  end;
  CheckForActiveGametraxSponsorLogo := OutVal;
end;

////////////////////////////////////////////////////////////////////////////////
// Procedure to load temporary collection for fields (for current template);
// also returns associated template information
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.LoadTempTemplateFields(TemplateID: SmallInt): TemplateDefsRec;
var
  i: SmallInt;
  TemplateFieldsRecPtr, NewTemplateFieldsRecPtr: ^TemplateFieldsRec;
  TemplateRecPtr: ^TemplateDefsRec;
  OutRec: TemplateDefsRec;
  FoundMatch: Boolean;
begin
  //Clear the existing collection
  Temporary_Fields_Collection.Clear;
  Temporary_Fields_Collection.Pack;
  //First, get the engine template ID
  if (Template_Defs_Collection.Count > 0) then
  begin
    i := 0;
    FoundMatch := FALSE;
    repeat
      TemplateRecPtr := Template_Defs_Collection.At(i);
      if (TemplateID = TemplateRecPtr^.Template_ID) then
      begin
        OutRec.Template_ID := TemplateRecPtr^.Template_ID;
        OutRec.Template_Type := TemplateRecPtr^.Template_Type;
        OutRec.Template_Description := TemplateRecPtr^.Template_Description;
        OutRec.Record_Type := TemplateRecPtr^.Record_Type;
        OutRec.Engine_Template_ID := TemplateRecPtr^.Engine_Template_ID;
        OutRec.Default_Dwell := TemplateRecPtr^.Default_Dwell;
        OutRec.UsesGameData := TemplateRecPtr^.UsesGameData;
        OutRec.RequiredGameState := TemplateRecPtr^.RequiredGameState;
        FoundMatch := TRUE;
      end;
      Inc(i);
    until (FoundMatch) OR (i = Template_Defs_Collection.Count);
  end;
  //Now, get the fields for the template
  for i := 0 to Template_Fields_Collection.Count-1 do
  begin
    TemplateFieldsRecPtr := Template_Fields_Collection.At(i);
    if (TemplateFieldsRecPtr^.Template_ID = TemplateID) then
    begin
      GetMem (NewTemplateFieldsRecPtr, SizeOf(TemplateFieldsRec));
      With NewTemplateFieldsRecPtr^ do
      begin
        Template_ID := TemplateFieldsRecPtr^.Template_ID;
        Field_ID := TemplateFieldsRecPtr^.Field_ID;
        Field_Type := TemplateFieldsRecPtr^.Field_Type;
        Field_Is_Manual := TemplateFieldsRecPtr^.Field_Is_Manual;
        Field_Label := TemplateFieldsRecPtr^.Field_Label;
        Field_Prefix := TemplateFieldsRecPtr^.Field_Prefix;
        Field_Contents := TemplateFieldsRecPtr^.Field_Contents;
        Field_Suffix := TemplateFieldsRecPtr^.Field_Suffix;
        Field_Max_Length := TemplateFieldsRecPtr^.Field_Max_Length;
        Engine_Field_ID := TemplateFieldsRecPtr^.Engine_Field_ID;
      end;
      if (Temporary_Fields_Collection.Count <= 100) then
      begin
        //Add to collection
        Temporary_Fields_Collection.Insert(NewTemplateFieldsRecPtr);
        Temporary_Fields_Collection.Pack;
      end;
    end;
  end;
  LoadTempTemplateFields := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
// Utility function to strip off any ASCII characters above ASCII 127
////////////////////////////////////////////////////////////////////////////////
function StripPrefix(InStr: String): String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := '';
  if (Length(InStr) > 0) then
  begin
    for i := 1 to Length(InStr) do
    begin
      if (Ord(InStr[i]) <=127) then OutStr := OutStr + InStr[i];
    end;
  end;
  StripPrefix := OutStr;
end;

////////////////////////////////////////////////////////////////////////////////
// STATS INC. SPECIFIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Function to take a football game situation record and return the parsed
// situation data (FOR STATS INC.)
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetSIFootballGameSituationData(CurrentGameData: GameRec): FootballSituationRec;
var
  i: SmallInt;
  OutRec: FootballSituationRec;
  VTeamID, HTeamID, PossessionTeamID: Double;
  PossessionYardsFromGoal: SmallInt;
  TeamRecPtr: ^TeamRec;
  StrCursor, SubStrCounter: SmallInt;
  Substrings: Array[1..4] of String;
begin
  //Init
  HTeamID := 0;
  VTeamID := 0;
  StrCursor := 1;
  SubStrCounter := 1;
  for i := 1 to 4 do Substrings[i] := '';

  //Parse out situation string to components
  While (StrCursor <= Length(CurrentGameData.Situation)) do
  begin
    //Get substrings for data fields
    While (CurrentGameData.Situation[StrCursor] <> '-') do
    begin
      SubStrings[SubStrCounter] := SubStrings[SubStrCounter] + CurrentGameData.Situation[StrCursor];
      Inc(StrCursor);
    end;
    //Go to next field
    Inc(StrCursor);
    Inc(SubStrCounter);
  end;

  //Set values
  //Do ID of team with possession
  if (Trim(SubStrings[1]) <> '') then
  begin
    try
      PossessionTeamID := Trunc(StrToFloat(Substrings[1]));
    except
      PossessionTeamID := 0;
      WriteToErrorLog('Error occurred while trying to convert team ID for football possession');
    end;
  end
  else PossessionTeamID := 0;

  //Get yards from goal
  if (Trim(SubStrings[2]) <> '') then
  begin
    try
      PossessionYardsFromGoal := StrToInt(Substrings[2]);
    except
      PossessionYardsFromGoal := 0;
      WriteToErrorLog('Error occurred while trying to convert field position for football possession');
    end;
  end
  else PossessionYardsFromGoal := 0;

  //Get team IDs of visiting & home teams
  if (Team_Collection.Count > 0) then
  begin
    for i := 0 to Team_Collection.Count-1 do
    begin
      TeamRecPtr := Team_Collection.At(i);
      if (CurrentGameData.VMnemonic = TeamRecPtr^.ShortDisplayName1) AND
         (CurrentGameData.League = TeamRecPtr^.League) then
        VTeamID := TeamRecPtr^.StatsIncId;
    end;
    for i := 0 to Team_Collection.Count-1 do
    begin
      TeamRecPtr := Team_Collection.At(i);
      if (CurrentGameData.HMnemonic = TeamRecPtr^.ShortDisplayName1) AND
         (CurrentGameData.League = TeamRecPtr^.League) then
        HTeamID := TeamRecPtr^.StatsIncId;
    end;
  end;
  //Check to find team with possession
  if (VTeamID = PossessionTeamID) then
  begin
    OutRec.VisitorHasPossession := TRUE;
    OutRec.HomeHasPossession := FALSE;
  end
  else if (HTeamID = PossessionTeamID) then
  begin
    OutRec.VisitorHasPossession := FALSE;
    OutRec.HomeHasPossession := TRUE;
  end
  else begin
    OutRec.VisitorHasPossession := FALSE;
    OutRec.HomeHasPossession := FALSE;
  end;
  OutRec.YardsFromGoal := PossessionYardsFromGoal;
  //Set return value
  GetSIFootballGameSituationData := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
// Function to format the game clock into minutes and seconds (FOR STATS INC.)
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.FormatSIGameClock(TimeMin: SmallInt; TimeSec: SmallInt): String;
var
  SecsStr: String;
  OutStr: String;
begin
  //Check for manual game as indicated by min, sec = -1
  if (TimeSec = -1) AND (TimeMin = -1) then OutStr := ''
  else begin
    if (TimeSec < 10) then SecsStr := '0' + IntToStr(TimeSec)
    else SecsStr := IntToStr(TimeSec);
    if (TimeMin > 0) then
      OutStr := IntToStr(TimeMin) + ':' + SecsStr
    else
      OutStr := ':' + SecsStr;
  end;
  FormatSIGameClock := OutStr;
end;

////////////////////////////////////////////////////////////////////////////////
// Function to take a league and a game phase code, and return a game phase
// record (FOR STATS INC.)
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetSIGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;
var
  i: SmallInt;
  GamePhaseRecPtr: ^GamePhaseRec; //Pointer to game phase record type
  OutRec: GamePhaseRec;
  FoundRecord: Boolean;
begin
  FoundRecord := FALSE;
  OutRec.League := ' ';
  OutRec.ST_Phase_Code := 0;
  OutRec.SI_Phase_Code := 0;
  OutRec.Display_Period := ' ';
  OutRec.End_Is_Half := FALSE;
  OutRec.Display_Half := ' ';
  OutRec.Display_Final := ' ';
  OutRec.Display_Extended1 := ' ';
  OutRec.Display_Extended2 := ' ';
  OutRec.Game_OT := FALSE;
  if (Game_Phase_Collection.Count > 0) then
  begin
    i := 0;
    repeat
      GamePhaseRecPtr := Game_Phase_Collection.At(i);
      if (Trim(GamePhaseRecPtr^.League) = Trim(League)) AND (GamePhaseRecPtr^.SI_Phase_Code = GPhase) then
      begin
        OutRec.League := Trim(League);
        OutRec.ST_Phase_Code := GPhase;
        OutRec.SI_Phase_Code := GPhase;
        OutRec.Label_Period := Trim(GamePhaseRecPtr^.Label_Period);
        OutRec.Display_Period := Trim(GamePhaseRecPtr^.Display_Period);
        OutRec.End_Is_Half := GamePhaseRecPtr^.End_Is_Half;
        OutRec.Display_Half := GamePhaseRecPtr^.Display_Half;
        OutRec.Display_Half_Short := GamePhaseRecPtr^.Display_Half_Short;
        OutRec.Display_Half := Trim(GamePhaseRecPtr^.Display_Half);
        OutRec.Display_Final := Trim(GamePhaseRecPtr^.Display_Final);
        OutRec.Display_Final_Short := Trim(GamePhaseRecPtr^.Display_Final_Short);
        OutRec.Display_Extended1 := Trim(GamePhaseRecPtr^.Display_Extended1);
        OutRec.Display_Extended2 := Trim(GamePhaseRecPtr^.Display_Extended2);
        OutRec.Game_OT := GamePhaseRecPtr^.Game_OT;
        FoundRecord := TRUE;
      end;
      Inc(i);
    until (i = Game_Phase_Collection.Count) OR (FoundRecord = TRUE);
  end;
  GetSIGamePhaseRec := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
// Function to take game phase and clock from Stats Inc. & return a game state
// code
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetSIGameState(State: String; TimeMin: String; TimeSec: String): SmallInt;
var
  OutVal: SmallInt;
begin
  // 1 = Pre-game
  // 2 = In-game
  // 3 = In-game (no-clock)
  // 4 = End of period
  // 5 = Final
  //Set the game state
  if (Trim(State) = 'Pre-Game') then
    OutVal := 1
  //Check for end of period
  else if (Trim(State) = 'In-Progress') AND (TimeMin = '0') AND (TimeSec = '0') then
    OutVal := 4
  //Check for dinal
  else if (Trim(State) = 'Final') then
    OutVal := 5
  else if (Trim(dmMain.SportbaseQuery2.FieldByName('State').AsString) = 'Delayed') then
    OutVal := 6
  else if (Trim(State) = 'Postponed') then
    OutVal := 7
  else if (Trim(dmMain.SportbaseQuery2.FieldByName('State').AsString) = 'Suspended') then
    OutVal := 7
  else if (Trim(dmMain.SportbaseQuery2.FieldByName('State').AsString) = 'In-Progress') then
    OutVal := 2
  else
    OutVal := 0;
  GetSIGameState := OutVal;
end;

////////////////////////////////////////////////////////////////////////////////
// Function to Get Game Day Leader Stats by Team
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetSIStatsString(SI_GCode: String; StatType: SmallInt; IsVisitor: Boolean):String;
var
  i: SmallInt;
  OutStr: String;
  FirstInitial: String;
begin
  OutStr := '';
  try
    dmMain.SportBaseQuery5.Close;
    dmMain.SportBaseQuery5.SQL.Clear;
    //Build query based on stat type; request 25 data elements to insure all returned
    Case StatType of
         //CFB STATS
         //Passing
      1: begin
           //Check top 25 first
           if (IsVisitor) then
           begin
             dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CFBVisitorPassingLeaderByGameTop25 ' +
               QuotedStr(SI_GCode));
           end
           else begin
             dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CFBHomePassingLeaderByGameTop25 ' +
               QuotedStr(SI_GCode));
           end;
           //Get the data
           dmMain.SportbaseQuery5.Open;
           //No records found for Top 25, so check all other games
           if (dmMain.SportbaseQuery5.RecordCount = 0) then
           begin
             dmMain.SportBaseQuery5.Close;
             dmMain.SportBaseQuery5.SQL.Clear;
             //Check non Top 25 games
             if (IsVisitor) then
             begin
               dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CFBVisitorPassingLeaderByGameAll ' +
                 QuotedStr(SI_GCode));
             end
             else begin
               dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CFBHomePassingLeaderByGameAll ' +
                 QuotedStr(SI_GCode));
             end;
           end;
           //Get the data
           dmMain.SportbaseQuery5.Open;
         end;
         //Rushing
      2: begin
           //Check top 25 first
           if (IsVisitor) then
           begin
             dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CFBVisitorRushingLeaderByGameTop25 ' +
               QuotedStr(SI_GCode));
           end
           else begin
             dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CFBHomeRushingLeaderByGameTop25 ' +
               QuotedStr(SI_GCode));
           end;
           //Get the data
           dmMain.SportbaseQuery5.Open;
           //No records found for Top 25, so check all other games
           if (dmMain.SportbaseQuery5.RecordCount = 0) then
           begin
             dmMain.SportBaseQuery5.Close;
             dmMain.SportBaseQuery5.SQL.Clear;
             //Check non Top 25 games
             if (IsVisitor) then
             begin
               dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CFBVisitorRushingLeaderByGameAll ' +
                 QuotedStr(SI_GCode));
             end
             else begin
               dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CFBHomeRushingLeaderByGameAll ' +
                 QuotedStr(SI_GCode));
             end;
           end;
           //Get the data
           dmMain.SportbaseQuery5.Open;
         end;
         //Receiving
      3: begin
           //Check top 25 first
           if (IsVisitor) then
           begin
             dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CFBVisitorReceivingLeaderByGameTop25 ' +
               QuotedStr(SI_GCode));
           end
           else begin
             dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CFBHomeReceivingLeaderByGameTop25 ' +
               QuotedStr(SI_GCode));
           end;
           //Get the data
           dmMain.SportbaseQuery5.Open;
           //No records found for Top 25, so check all other games
           if (dmMain.SportbaseQuery5.RecordCount = 0) then
           begin
             dmMain.SportBaseQuery5.Close;
             dmMain.SportBaseQuery5.SQL.Clear;
             //Check non Top 25 games
             if (IsVisitor) then
             begin
               dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CFBVisitorReceivingLeaderByGameAll ' +
                 QuotedStr(SI_GCode));
             end
             else begin
               dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CFBHomeReceivingLeaderByGameAll ' +
                 QuotedStr(SI_GCode));
             end;
           end;
           //Get the data
           dmMain.SportbaseQuery5.Open;
         end;
         //CBB
         //Points
      6: begin
           //Check top 25 first
           if (IsVisitor) then
           begin
             dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBVisitorPointsLeaderByGameTop25 ' +
               QuotedStr(SI_GCode));
           end
           else begin
             dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBHomePointsLeaderByGameTop25 ' +
               QuotedStr(SI_GCode));
           end;
           //Get the data
           dmMain.SportbaseQuery5.Open;
           //No records found for Top 25, so check all other games
           if (dmMain.SportbaseQuery5.RecordCount = 0) then
           begin
             dmMain.SportBaseQuery5.Close;
             dmMain.SportBaseQuery5.SQL.Clear;
             //Check non Top 25 games
             if (IsVisitor) then
             begin
               dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBVisitorPointsLeaderByGameAll ' +
                 QuotedStr(SI_GCode));
             end
             else begin
               dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBHomePointsLeaderByGameAll ' +
                 QuotedStr(SI_GCode));
             end;
           end;
           //Get the data
           dmMain.SportbaseQuery5.Open;
         end;
         //Field Goals
      7: begin
           //Check top 25 first
           if (IsVisitor) then
           begin
             dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBVisitorFieldGoalLeaderByGameTop25 ' +
               QuotedStr(SI_GCode));
           end
           else begin
             dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBHomeFieldGoalLeaderByGameTop25 ' +
               QuotedStr(SI_GCode));
           end;
           //Get the data
           dmMain.SportbaseQuery5.Open;
           //No records found for Top 25, so check all other games
           if (dmMain.SportbaseQuery5.RecordCount = 0) then
           begin
             dmMain.SportBaseQuery5.Close;
             dmMain.SportBaseQuery5.SQL.Clear;
             //Check non Top 25 games
             if (IsVisitor) then
             begin
               dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBVisitorFieldGoalLeaderByGameAll ' +
                 QuotedStr(SI_GCode));
             end
             else begin
               dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBHomeFieldGoalLeaderByGameAll ' +
                 QuotedStr(SI_GCode));
             end;
           end;
           //Get the data
           dmMain.SportbaseQuery5.Open;
         end;
         //3 Point Field Goals
      8: begin
           //Check top 25 first
           if (IsVisitor) then
           begin
             dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBVisitor3PTFieldGoalLeaderByGameTop25 ' +
               QuotedStr(SI_GCode));
           end
           else begin
             dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBHome3PTFieldGoalLeaderByGameTop25 ' +
               QuotedStr(SI_GCode));
           end;
           //Get the data
           dmMain.SportbaseQuery5.Open;
           //No records found for Top 25, so check all other games
           if (dmMain.SportbaseQuery5.RecordCount = 0) then
           begin
             dmMain.SportBaseQuery5.Close;
             dmMain.SportBaseQuery5.SQL.Clear;
             //Check non Top 25 games
             if (IsVisitor) then
             begin
               dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBVisitor3PTFieldGoalLeaderByGameAll ' +
                 QuotedStr(SI_GCode));
             end
             else begin
               dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBHome3PTFieldGoalLeaderByGameAll ' +
                 QuotedStr(SI_GCode));
             end;
           end;
           //Get the data
           dmMain.SportbaseQuery5.Open;
         end;
         //Combined Field Goals
      9: begin
           //Check top 25 first
           if (IsVisitor) then
           begin
             dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBVisitorCombinedFieldGoalLeaderByGameTop25 ' +
               QuotedStr(SI_GCode));
           end
           else begin
             dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBHomeCombinedFieldGoalLeaderByGameTop25 ' +
               QuotedStr(SI_GCode));
           end;
           //Get the data
           dmMain.SportbaseQuery5.Open;
           //No records found for Top 25, so check all other games
           if (dmMain.SportbaseQuery5.RecordCount = 0) then
           begin
             dmMain.SportBaseQuery5.Close;
             dmMain.SportBaseQuery5.SQL.Clear;
             //Check non Top 25 games
             if (IsVisitor) then
             begin
               dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBVisitorCombinedFieldGoalLeaderByGameAll ' +
                 QuotedStr(SI_GCode));
             end
             else begin
               dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBHomeCombinedFieldGoalLeaderByGameAll ' +
                 QuotedStr(SI_GCode));
             end;
           end;
           //Get the data
           dmMain.SportbaseQuery5.Open;
         end;
         //Fouls
    10: begin
           //Check top 25 first
           if (IsVisitor) then
           begin
             dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBVisitorFoulLeaderByGameTop25 ' +
               QuotedStr(SI_GCode));
           end
           else begin
             dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBHomeFoulLeaderByGameTop25 ' +
               QuotedStr(SI_GCode));
           end;
           //Get the data
           dmMain.SportbaseQuery5.Open;
           //No records found for Top 25, so check all other games
           if (dmMain.SportbaseQuery5.RecordCount = 0) then
           begin
             dmMain.SportBaseQuery5.Close;
             dmMain.SportBaseQuery5.SQL.Clear;
             //Check non Top 25 games
             if (IsVisitor) then
             begin
               dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBVisitorFoulLeaderByGameAll ' +
                 QuotedStr(SI_GCode));
             end
             else begin
               dmMain.SportBaseQuery5.SQL.Add('/* */sp_GetSI_CBBHomeFoulLeaderByGameAll ' +
                 QuotedStr(SI_GCode));
             end;
           end;
           //Get the data
           dmMain.SportbaseQuery5.Open;
         end;
    end;

    //Process the data
    if (dmMain.SportbaseQuery5.RecordCount > 0) then
    begin
      Case StatType of
           //CFB
           //Passing
        1: Begin
             if (Length(dmMain.SportbaseQuery5.FieldByName('LastName').AsString) > 0) then
             begin
               if (Length(dmMain.SportbaseQuery5.FieldByName('FirstName').AsString) >= 1) then
                 FirstInitial := dmMain.SportbaseQuery5.FieldByName('FirstName').AsString[1] + '. '
               else FirstInitial := '';
               OutStr := OutStr + FirstInitial;
               OutStr := OutStr + dmMain.SportbaseQuery5.FieldByName('LastName').AsString;
               OutStr := OutStr + ': ' +  dmMain.SportbaseQuery5.FieldByName('Completions').AsString;
               OutStr := OutStr + '/' + dmMain.SportbaseQuery5.FieldByName('Attempts').AsString;
               OutStr := OutStr + ', ' + dmMain.SportbaseQuery5.FieldByName('Yards').AsString + ' YDS';
               if (dmMain.SportbaseQuery5.FieldByName('Touchdowns').AsInteger > 0) then
               begin
                 if (dmMain.SportbaseQuery5.FieldByName('Touchdowns').AsInteger = 1) then
                   OutStr := OutStr + ', TD'
                 else
                   OutStr := OutStr + ', ' + dmMain.SportbaseQuery5.FieldByName('Touchdowns').AsString + ' TD';
               end;
               if (dmMain.SportbaseQuery5.FieldByName('Interceptions').AsInteger > 0) then
               begin
                 if (dmMain.SportbaseQuery5.FieldByName('Interceptions').AsInteger = 1) then
                   OutStr := OutStr  + ', INT'
                 else
                   OutStr := OutStr  + ', ' + dmMain.SportbaseQuery5.FieldByName('Interceptions').AsString + ' INT';
               end;
             end;
           end;
           //Rushing
        2: Begin
             if (Length(dmMain.SportbaseQuery5.FieldByName('LastName').AsString) > 0) then
             begin
               if (Length(dmMain.SportbaseQuery5.FieldByName('FirstName').AsString) >= 1) then
                 FirstInitial := dmMain.SportbaseQuery5.FieldByName('FirstName').AsString[1] + '. '
               else FirstInitial := '';
               OutStr := OutStr + FirstInitial;
               OutStr := OutStr + dmMain.SportbaseQuery5.FieldByName('LastName').AsString;
               OutStr := OutStr + ': ' + dmMain.SportbaseQuery5.FieldByName('Attempts').AsString + ' RUSH';
               OutStr := OutStr + ', ' + dmMain.SportbaseQuery5.FieldByName('Yards').AsString + ' YDS';
               if (dmMain.SportbaseQuery5.FieldByName('Touchdowns').AsInteger > 0) then
               begin
                 if (dmMain.SportbaseQuery5.FieldByName('Touchdowns').AsInteger = 1) then
                   OutStr := OutStr + ', TD'
                 else
                   OutStr := OutStr + ', ' + dmMain.SportbaseQuery5.FieldByName('Touchdowns').AsString + ' TD';
               end;
             end;
           end;
           //Receiving
        3: Begin
             if (Length(dmMain.SportbaseQuery5.FieldByName('LastName').AsString) > 0) then
             begin
               if (Length(dmMain.SportbaseQuery5.FieldByName('FirstName').AsString) >= 1) then
                 FirstInitial := dmMain.SportbaseQuery5.FieldByName('FirstName').AsString[1] + '. '
               else FirstInitial := '';
               OutStr := OutStr + FirstInitial;
               OutStr := OutStr + dmMain.SportbaseQuery5.FieldByName('LastName').AsString;
               OutStr := OutStr + ': ' + dmMain.SportbaseQuery5.FieldByName('Receptions').AsString + ' REC';
               OutStr := OutStr + ', ' + dmMain.SportbaseQuery5.FieldByName('Yards').AsString + ' YDS';
               if (dmMain.SportbaseQuery5.FieldByName('Touchdowns').AsInteger > 0) then
               begin
                 if (dmMain.SportbaseQuery5.FieldByName('Touchdowns').AsInteger = 1) then
                   OutStr := OutStr + ', TD'
                 else
                   OutStr := OutStr + ', ' + dmMain.SportbaseQuery5.FieldByName('Touchdowns').AsString + ' TD';
               end;
             end;
           end;
           //CBB
           //Points
        6: Begin
             if (Length(dmMain.SportbaseQuery5.FieldByName('LastName').AsString) > 0) then
             begin
               if (Length(dmMain.SportbaseQuery5.FieldByName('FirstName').AsString) >= 1) then
                 FirstInitial := dmMain.SportbaseQuery5.FieldByName('FirstName').AsString[1] + '. '
               else FirstInitial := '';
               OutStr := OutStr + FirstInitial;
               OutStr := OutStr + dmMain.SportbaseQuery5.FieldByName('LastName').AsString;
               OutStr := OutStr + ': ' +  dmMain.SportbaseQuery5.FieldByName('Points').AsString + ' PTS';
             end;
           end;
           //Field Goals
        7: Begin
             if (Length(dmMain.SportbaseQuery5.FieldByName('LastName').AsString) > 0) then
             begin
               if (Length(dmMain.SportbaseQuery5.FieldByName('FirstName').AsString) >= 1) then
                 FirstInitial := dmMain.SportbaseQuery5.FieldByName('FirstName').AsString[1] + '. '
               else FirstInitial := '';
               OutStr := OutStr + FirstInitial;
               OutStr := OutStr + dmMain.SportbaseQuery5.FieldByName('LastName').AsString;
               OutStr := OutStr + ': ' +  dmMain.SportbaseQuery5.FieldByName('Points').AsString + ' PTS';
               OutStr := OutStr + ', ' + dmMain.SportbaseQuery5.FieldByName('Made').AsString + '/' +
                 dmMain.SportbaseQuery5.FieldByName('Attempts').AsString + ' FG';
             end;
           end;
           //3 Point Field Goals
        8: Begin
             if (Length(dmMain.SportbaseQuery5.FieldByName('LastName').AsString) > 0) then
             begin
               if (Length(dmMain.SportbaseQuery5.FieldByName('FirstName').AsString) >= 1) then
                 FirstInitial := dmMain.SportbaseQuery5.FieldByName('FirstName').AsString[1] + '. '
               else FirstInitial := '';
               OutStr := OutStr + FirstInitial;
               OutStr := OutStr + dmMain.SportbaseQuery5.FieldByName('LastName').AsString;
               OutStr := OutStr + ': ' +  dmMain.SportbaseQuery5.FieldByName('Points').AsString + ' PTS';
               OutStr := OutStr + ', ' + dmMain.SportbaseQuery5.FieldByName('Made').AsString + '/' +
                 dmMain.SportbaseQuery5.FieldByName('Attempts').AsString + ' 3-PT FG';
             end;
           end;
           //Combined Field Goals
        9: Begin
             if (Length(dmMain.SportbaseQuery5.FieldByName('LastName').AsString) > 0) then
             begin
               if (Length(dmMain.SportbaseQuery5.FieldByName('FirstName').AsString) >= 1) then
                 FirstInitial := dmMain.SportbaseQuery5.FieldByName('FirstName').AsString[1] + '. '
               else FirstInitial := '';
               OutStr := OutStr + FirstInitial;
               OutStr := OutStr + dmMain.SportbaseQuery5.FieldByName('LastName').AsString;
               OutStr := OutStr + ': ' +  dmMain.SportbaseQuery5.FieldByName('Points').AsString + ' PTS';
               OutStr := OutStr + ', ' + dmMain.SportbaseQuery5.FieldByName('Two_Point_Made').AsString + '/' +
                 dmMain.SportbaseQuery5.FieldByName('Two_Point_Attempts').AsString + ' FG';
               OutStr := OutStr + ' (' + dmMain.SportbaseQuery5.FieldByName('Three_Point_Made').AsString + '/' +
                 dmMain.SportbaseQuery5.FieldByName('Three_Point_Attempts').AsString + ' 3-PT)';

             end;
           end;
           //Fouls
      10: Begin
             if (Length(dmMain.SportbaseQuery5.FieldByName('LastName').AsString) > 0) then
             begin
               if (Length(dmMain.SportbaseQuery5.FieldByName('FirstName').AsString) >= 1) then
                 FirstInitial := dmMain.SportbaseQuery5.FieldByName('FirstName').AsString[1] + '. '
               else FirstInitial := '';
               OutStr := OutStr + FirstInitial;
               OutStr := OutStr + dmMain.SportbaseQuery5.FieldByName('LastName').AsString;
               OutStr := OutStr + ': ' +  dmMain.SportbaseQuery5.FieldByName('Points').AsString + ' PTS';
               OutStr := OutStr + ', ' +  dmMain.SportbaseQuery5.FieldByName('Fouls').AsString + ' FOULS';
             end;
           end;
      end; //Case
    end;
    //Close the query
    dmMain.SportbaseQuery5.Close;
  except
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred while trying to retrieve live CFB stats information');
  end;
  //Return
  if (OutStr = '') then OutStr := ' ';
  GetSIStatsString := ANSIUpperCase(OutStr);
end;

////////////////////////////////////////////////////////////////////////////////
// SPORTSTICKER SPECIFIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Function to get game state from (FOR SPORTSTICKER)
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetSTGameState(LEAGUE: String; GTIME: String; GPHASE: SmallInt): Integer;
begin
  if (LEAGUE = 'AL') OR (LEAGUE = 'ML') OR (LEAGUE = 'NL') OR (LEAGUE = 'CAC') OR
     (LEAGUE = 'GPFT') OR (LEAGUE = 'MLB') THEN
  begin
    if (GTIME = '9999') then
      //Game not started
      Result := 1
    else if (GTIME = 'FINA') OR (GTIME = 'SUM-') then
      //Game is final
      Result := 5
    else if (GTIME = 'END-') then
      //End of period/quarter
      Result := 4
    else if (StrToIntDef(Trim(GTIME), -1) <> -1) then
      //Game in progress
      Result := 2
    else
      //All other conditions
      Result := -1; // Game is suspended due to rain or cancelled etc. (GTIME = 'RAIN' , GTIME = 'SUSP', GTIME = 'CANC')
  end
  else begin
    if (GTIME = '9999') OR (GPHASE = 0) then
      //Game not started
      Result := 1
    else if (GTIME = 'FINA') OR (GTIME = 'SUM-') then
      //Game is final
      Result := 5
    else if (GTIME = 'END-') then
      //End of period/quarter
      Result := 4
    else if (GPHASE <> 0) AND (StrToIntDef(Trim(GTIME), -1) <> -1) then
      //Game in progress
      Result := 2
    else
      //All other conditions
      Result := -1; // Game is suspended due to rain or cancelled etc. (GTIME = 'RAIN' , GTIME = 'SUSP', GTIME = 'CANC')
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// Function to take a league and a game phase code, and return a game phase
// record (FOR SPORTSTICKER)
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetSTGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;
var
  i: SmallInt;
  GamePhaseRecPtr: ^GamePhaseRec; //Pointer to game phase record type
  OutRec: GamePhaseRec;
  FoundRecord: Boolean;
begin
  FoundRecord := FALSE;
  OutRec.League := ' ';
  OutRec.ST_Phase_Code := 0;
  OutRec.SI_Phase_Code := 0;
  OutRec.Display_Period := ' ';
  OutRec.End_Is_Half := FALSE;
  OutRec.Display_Half := ' ';
  OutRec.Display_Final := ' ';
  OutRec.Display_Extended1 := ' ';
  OutRec.Display_Extended2 := ' ';
  OutRec.Game_OT := FALSE;
  if (Game_Phase_Collection.Count > 0) then
  begin
    i := 0;
    repeat
      GamePhaseRecPtr := Game_Phase_Collection.At(i);
      if (Trim(GamePhaseRecPtr^.League) = Trim(League)) AND (GamePhaseRecPtr^.ST_Phase_Code = GPhase) then
      begin
        OutRec.League := Trim(League);
        OutRec.ST_Phase_Code := GPhase;
        OutRec.SI_Phase_Code := GPhase;
        OutRec.Label_Period := Trim(GamePhaseRecPtr^.Label_Period);
        OutRec.Display_Period := Trim(GamePhaseRecPtr^.Display_Period);
        OutRec.End_Is_Half := GamePhaseRecPtr^.End_Is_Half;
        OutRec.Display_Half := GamePhaseRecPtr^.Display_Half;
        OutRec.Display_Half_Short := GamePhaseRecPtr^.Display_Half_Short;
        OutRec.Display_Half := Trim(GamePhaseRecPtr^.Display_Half);
        OutRec.Display_Final := Trim(GamePhaseRecPtr^.Display_Final);
        OutRec.Display_Final_Short := Trim(GamePhaseRecPtr^.Display_Final_Short);
        OutRec.Display_Extended1 := Trim(GamePhaseRecPtr^.Display_Extended1);
        OutRec.Display_Extended2 := Trim(GamePhaseRecPtr^.Display_Extended2);
        OutRec.Game_OT := GamePhaseRecPtr^.Game_OT;
        FoundRecord := TRUE;
      end;
      Inc(i);
    until (i = Game_Phase_Collection.Count) OR (FoundRecord = TRUE);
  end;
  GetSTGamePhaseRec := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
//Function to get game time string from GT Server time value (FOR SPORTSTICKER)
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetSTGameTimeString(GTimeStr: String): String;
var
  Min, Sec: String;
begin
  //Blank out case where time = 9999 - for MLB
  if (GTimeStr = '9999') then Result := ' '
  //Actual clock time
  else if (StrToIntDef(Trim(GTimeStr), -1) <> -1) AND (Length(GTimeStr) = 4) then
  begin
    Min := GTimeStr[1] + GTimeStr[2];
    Sec := GTimeStr[3] + GTimeStr[4];
    Min := IntToStr(StrToInt(GTimeStr[1]+GTimeStr[2]));
    Result := Min + ':' + Sec;
  end
  else if (GTimeStr = 'END-') then Result := 'End'
  else if (GTimeStr = 'POST') then Result := 'PPD'
  else if (GTimeStr = 'SUSP') then Result := 'PPD'
  else if (GTimeStr = 'DELA') then Result := 'DLY'
  else if (GTimeStr = 'RAIN') then Result := 'RD'
  else if (GTimeStr = 'CANC') then Result := 'PPD'
  else Result := ' ';
end;

////////////////////////////////////////////////////////////////////////////////
// NFL DATAMART SPECIFIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//Function to take the NFL time as a 4-digit value and return (FOR NFL DM)
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetNFLDMGameTimeRecord(GTimeStr: String): TimeRec;
var
  Outrec: TimeRec;
  MinStr, SecStr: String;
begin
  Case Length(GTimeStr) of
    0: Begin
         OutRec.Minutes := 0;
         OutRec.Seconds := 0;
       end;
    1: Begin
         OutRec.Minutes := 0;
         OutRec.Seconds := StrToIntDef(GTimeStr[1], 0);
       end;
    2: Begin
         OutRec.Minutes := 0;
         OutRec.Seconds := StrToIntDef(GTimeStr[1]+GTimeStr[2], 0);
       end;
    3: Begin
         OutRec.Minutes := StrToIntDef(GTimeStr[1], 0);
         OutRec.Seconds := StrToIntDef(GTimeStr[2]+GTimeStr[3], 0);
       end;
    4: Begin
         OutRec.Minutes := StrToIntDef(GTimeStr[1]+GTimeStr[2], 0);
         OutRec.Seconds := StrToIntDef(GTimeStr[3]+GTimeStr[4], 0);
       end;
  end;
  GetNFLDMGameTimeRecord := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
//Function to take 4-digit clock time string from Datamart and format as "mm:ss"
// (FOR NFL DM)
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.FormatNFLDMTimeString (InStr: String): String;
var
  OutStr: String;
begin
  OutStr := '';
  InStr := Trim(InStr);
  Case Length(InStr) of
    0: Begin
         OutStr := '';
       end;
    1: Begin
         OutStr := ':0' + InStr[1];
       end;
    2: Begin
         OutStr := ':' + InStr[1] + InStr[2];
       end;
    3: Begin
         OutStr := InStr[1] + ':' + InStr[2] + InStr[3];
       end;
    4: Begin
         OutStr := InStr[1] + InStr[2] + ':' + InStr[3] + InStr[4];
       end;
  end;
  FormatNFLDMTimeString := OutStr;
end;

////////////////////////////////////////////////////////////////////////////////
// Function to take a league and a game phase code, and return a game phase
// record (FOR NFL DATAMART)
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetNFLDMGamePhaseString(League: String; GameState: SmallInt; GamePhase: String): String;
var
  i: SmallInt;
  OutStr: String;
  GamePhaseRecPtr: ^GamePhaseRec; //Pointer to game phase record type
  OutRec: GamePhaseRec;
  FoundRecord: Boolean;
begin
  //Check for pre-game
  if (GameState = 1) then OutStr := ''
  //Check for text values before checking game phase table
  else if (GameState = 4) AND (GamePhase = 'H') then OutStr := 'HALF'
  else if (GameState = 5) AND (GamePhase = 'F') then OutStr := 'FINAL'
  else if (GameState = 5) AND (GamePhase = 'f') then OutStr := 'FINAL/OT'
  //Not a text value for phase, so game is in progress - check for numerical value
  else begin
    if (Game_Phase_Collection.Count > 0) then
    begin
      i := 0;
      repeat
        GamePhaseRecPtr := Game_Phase_Collection.At(i);
        if (Trim(GamePhaseRecPtr^.League) = Trim(League)) AND (IntToStr(GamePhaseRecPtr^.NFLDM_Phase_Code) = GamePhase) then
        begin
          OutRec.Display_Period := Trim(GamePhaseRecPtr^.Display_Period);
          FoundRecord := TRUE;
        end;
        Inc(i);
      until (i = Game_Phase_Collection.Count) OR (FoundRecord = TRUE);
    end;
    //Game phase record was found, so set it
    if (FoundRecord) then
    begin
      //If in progress with clock, send display period
      if (GameState = 2) OR (GameState = 3) then OutStr := OutRec.Display_Period
      //Else, check for end of period
      else if (GameState = 4) then OutStr := 'END ' + OutRec.Display_Period;
    end
    //No record found - send blank
    else OutStr := ' ';
  end;
  GetNFLDMGamePhaseString := OutStr;
end;

////////////////////////////////////////////////////////////////////////////////
// Function to take game phase and clock from the NFLDM & return a game state
// code
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetNFLDMGameState(GamePhase: String; GameTime: String): SmallInt;
var
  OutVal: SmallInt;
begin
  // 1 = Pre-game
  // 2 = In-game
  // 3 = In-game (no-clock)
  // 4 = End of period
  // 5 = Final
  //Check for pre-game
  if (ANSIUpperCase(GamePhase) = 'P') OR (Trim(GamePhase) = '') then
    OutVal := 1
  //Check for no clock - should not occur in DM
  else if (Trim(GameTime) = '') then
    OutVal := 3
  //Check for end of period
  else if (GameTime = '0') AND
          ((GamePhase = '1') OR (GamePhase = '2') OR (GamePhase = '3') OR
          (GamePhase = '4') OR (GamePhase = '5') OR (GamePhase = '6')) then
    OutVal := 4
  //Check for halftime - flag as end of period
  else if (Trim(GamePhase) = 'H') then
    OutVal := 4
  //Check for final or final in OT
  else if (Trim(GamePhase) = 'F') OR (Trim(GamePhase) = 'f') then
    OutVal := 5
  //All other cases, game is in progress
  else
    OutVal := 2;

  GetNFLDMGameState := OutVal;
end;

////////////////////////////////////////////////////////////////////////////////
// Function to take the yardage value returned by the Datamart and return a value
// that can be passed to the engine (FOR NFL DATAMART)
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetNFLDMYardageIndidcatorValue(CurrentGameData: GameRec): SmallInt;
var
  i: SmallInt;
  OutVal: SmallInt;
  YardageValueStr: String;
  YardageValue: SmallInt;
  StartCursor: SmallInt;
begin
  //Only display if game in progress
  if (CurrentGameData.GameState = 2) then
  begin
    //Set values
    if (Length(CurrentGameData.Yardline) > 0) then
    begin
      //Parse out the yardage value - starts after space
      StartCursor := Pos(' ', CurrentGameData.Yardline) + 1;
      YardageValueStr := '';
      if (StartCursor <= Length(CurrentGameData.Yardline)) then
        for i := StartCursor to Length(CurrentGameData.Yardline) do
          YardageValueStr := YardageValueStr + CurrentGameData.Yardline[i];
      //Convert to integer
      YardageValue := StrToIntDef(YardageValueStr, 0);
      //Check for home team has possession in own territory
      if (CurrentGameData.HomeTeamHasPossession) AND
        (Pos(CurrentGameData.HMnemonic, CurrentGameData.Yardline) > 0) then
      begin
        OutVal := -1*(YardageValue);
      end
      //Check for home team has possession in visitor territory
      else if (CurrentGameData.HomeTeamHasPossession) AND
        (Pos(CurrentGameData.VMnemonic, CurrentGameData.Yardline) > 0) then
      begin
        OutVal := -1*(100-YardageValue);
      end
      //Check for visiting team has possession in own territory
      else if (CurrentGameData.HomeTeamHasPossession = FALSE) AND
        (Pos(CurrentGameData.VMnemonic, CurrentGameData.Yardline) > 0) then
      begin
        OutVal := YardageValue;
      end
      //Check for visiting team has possession in home territory
      else if (CurrentGameData.HomeTeamHasPossession = FALSE) AND
        (Pos(CurrentGameData.HMnemonic, CurrentGameData.Yardline) > 0) then
      begin
        OutVal := 100-YardageValue;
      end;
    end;
  end
  else OutVal := -9999;
  //Return
  GetNFLDMYardageIndidcatorValue := OutVal;
end;

////////////////////////////////////////////////////////////////////////////////
// Function to Get Game Day Leader Stats by Team
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetNFLDMStatsString(StatType: SmallInt; Clubkey: LongInt):String;
var
  i: SmallInt;
  SeasonCode: String;
  OutStr: String;
  FirstInitial: String;
begin
  OutStr := '';
  try
    dmMain.DatamartQuery3.Close;
    dmMain.DatamartQuery3.SQL.Clear;
    Case NFLWeekInfo.SeasonType of
      0: SeasonCode := 'PRE';
      1: SeasonCode := 'REG';
      2: SeasonCode := 'POST';
    end;
    //Build query based on stat type; request 25 data elements to insure all returned
    Case StatType of
         //Passing
      1: begin
           dmMain.DatamartQuery3.SQL.Add('/* */p_GetTopPassers ' +
             IntToStr(NFLWeekInfo.SeasonYear) + ', ' + QuotedStr(SeasonCode) + ', ' + IntToStr(NFLWeekInfo.Week) +
             ', 1, ' + IntToStr(ClubKey) + ', 0');
         end;
         //Rushing
      2: begin
           dmMain.DatamartQuery3.SQL.Add('/* */p_GetTopRushers ' +
             IntToStr(NFLWeekInfo.SeasonYear) + ', ' + QuotedStr(SeasonCode) + ', ' + IntToStr(NFLWeekInfo.Week) +
             ', 1, ' + IntToStr(ClubKey) + ', 0');
         end;
         //Receiving
      3: begin
           dmMain.DatamartQuery3.SQL.Add('/* */p_GetTopReceivers ' +
             IntToStr(NFLWeekInfo.SeasonYear) + ', ' + QuotedStr(SeasonCode) + ', ' + IntToStr(NFLWeekInfo.Week) +
             ', 1, ' + IntToStr(ClubKey) + ', 0');
         end;
         //Kicking
      4: begin
           dmMain.DatamartQuery3.SQL.Add('/* */p_GetTopScorers ' +
             IntToStr(NFLWeekInfo.SeasonYear) + ', ' + QuotedStr(SeasonCode) + ', ' + IntToStr(NFLWeekInfo.Week) +
             ', 1, ' + IntToStr(ClubKey) + ', 0');
         end;
         //Defensive
      5: begin
           dmMain.DatamartQuery3.SQL.Add('/* */p_GetTopTacklers ' +
             IntToStr(NFLWeekInfo.SeasonYear) + ', ' + QuotedStr(SeasonCode) + ', ' + IntToStr(NFLWeekInfo.Week) +
             ', 1, ' + IntToStr(ClubKey) + ', 0');
         end;
    end;

    //Get the data
    dmMain.DatamartQuery3.Open;

    //Process the data
    if (dmMain.DatamartQuery3.RecordCount > 0) then
    begin
      Case StatType of
           //Passing
        1: Begin
             if (Length(dmMain.DatamartQuery3.FieldByName('LastName').AsString) > 0) then
             begin
               if (Length(dmMain.DatamartQuery3.FieldByName('FirstName').AsString) >= 1) then
                 FirstInitial := dmMain.DatamartQuery3.FieldByName('FirstName').AsString[1] + '. '
               else FirstInitial := '';
               OutStr := OutStr + FirstInitial;
               OutStr := OutStr + dmMain.DatamartQuery3.FieldByName('LastName').AsString;
               OutStr := OutStr + ': ' +  dmMain.DatamartQuery3.FieldByName('Completions').AsString;
               OutStr := OutStr + '/' + dmMain.DatamartQuery3.FieldByName('Attempts').AsString;
               OutStr := OutStr + ', ' + dmMain.DatamartQuery3.FieldByName('Yards').AsString + ' YDS';
               if (dmMain.DatamartQuery3.FieldByName('Touchdowns').AsInteger > 0) then
               begin
                 if (dmMain.DatamartQuery3.FieldByName('Touchdowns').AsInteger = 1) then
                   OutStr := OutStr + ', TD'
                 else
                   OutStr := OutStr + ', ' + dmMain.DatamartQuery3.FieldByName('Touchdowns').AsString + ' TD';
               end;
               if (dmMain.DatamartQuery3.FieldByName('Interceptions').AsInteger > 0) then
               begin
                 if (dmMain.DatamartQuery3.FieldByName('Interceptions').AsInteger = 1) then
                   OutStr := OutStr  + ', INT'
                 else
                   OutStr := OutStr  + ', ' + dmMain.DatamartQuery3.FieldByName('Interceptions').AsString + ' INT';
               end;
             end;
           end;
           //Rushing
        2: Begin
             if (Length(dmMain.DatamartQuery3.FieldByName('LastName').AsString) > 0) then
             begin
               if (Length(dmMain.DatamartQuery3.FieldByName('FirstName').AsString) >= 1) then
                 FirstInitial := dmMain.DatamartQuery3.FieldByName('FirstName').AsString[1] + '. '
               else FirstInitial := '';
               OutStr := OutStr + FirstInitial;
               OutStr := OutStr + dmMain.DatamartQuery3.FieldByName('LastName').AsString;
               OutStr := OutStr + ': ' + dmMain.DatamartQuery3.FieldByName('Attempts').AsString + ' RUSH';
               OutStr := OutStr + ', ' + dmMain.DatamartQuery3.FieldByName('Yards').AsString + ' YDS';
               if (dmMain.DatamartQuery3.FieldByName('Touchdowns').AsInteger > 0) then
               begin
                 if (dmMain.DatamartQuery3.FieldByName('Touchdowns').AsInteger = 1) then
                   OutStr := OutStr + ', TD'
                 else
                   OutStr := OutStr + ', ' + dmMain.DatamartQuery3.FieldByName('Touchdowns').AsString + ' TD';
               end;
             end;
           end;
           //Receiving
        3: Begin
             if (Length(dmMain.DatamartQuery3.FieldByName('LastName').AsString) > 0) then
             begin
               if (Length(dmMain.DatamartQuery3.FieldByName('FirstName').AsString) >= 1) then
                 FirstInitial := dmMain.DatamartQuery3.FieldByName('FirstName').AsString[1] + '. '
               else FirstInitial := '';
               OutStr := OutStr + FirstInitial;
               OutStr := OutStr + dmMain.DatamartQuery3.FieldByName('LastName').AsString;
               OutStr := OutStr + ': ' + dmMain.DatamartQuery3.FieldByName('Receptions').AsString + ' REC';
               OutStr := OutStr + ', ' + dmMain.DatamartQuery3.FieldByName('Yards').AsString + ' YDS';
               if (dmMain.DatamartQuery3.FieldByName('Touchdowns').AsInteger > 0) then
               begin
                 if (dmMain.DatamartQuery3.FieldByName('Touchdowns').AsInteger = 1) then
                   OutStr := OutStr + ', TD'
                 else
                   OutStr := OutStr + ', ' + dmMain.DatamartQuery3.FieldByName('Touchdowns').AsString + ' TD';
               end;
             end;
           end;
           //Field Goals
        4: Begin
             if (Length(dmMain.DatamartQuery3.FieldByName('LastName').AsString) > 0) then
             begin
               if (dmMain.DatamartQuery3.FieldByName('FGMade').AsInteger > 0) then
               begin
                 if (Length(dmMain.DatamartQuery3.FieldByName('FirstName').AsString) >= 1) then
                   FirstInitial := dmMain.DatamartQuery3.FieldByName('FirstName').AsString[1] + '. '
                 else FirstInitial := '';
                 OutStr := OutStr + FirstInitial;
                 OutStr := OutStr + dmMain.DatamartQuery3.FieldByName('LastName').AsString;
                 OutStr := OutStr + ': ' + dmMain.DatamartQuery3.FieldByName('FGMade').AsString + '/' +
                   dmMain.DatamartQuery3.FieldByName('FGAttempts').AsString + ' FG/FGA';
               end;
             end;
           end;
           //Defense
        5: Begin
             if (Length(dmMain.DatamartQuery3.FieldByName('LastName').AsString) > 0) then
             begin
               if (Length(dmMain.DatamartQuery3.FieldByName('FirstName').AsString) >= 1) then
                 FirstInitial := dmMain.DatamartQuery3.FieldByName('FirstName').AsString[1] + '. '
               else FirstInitial := '';
               OutStr := OutStr + FirstInitial;
               OutStr := OutStr + dmMain.DatamartQuery3.FieldByName('LastName').AsString;
               OutStr := OutStr + ': ';
               OutStr := OutStr + dmMain.DatamartQuery3.FieldByName('Tackles').AsString + ' TCKLS';
               if (dmMain.DatamartQuery3.FieldByName('Sacks').AsInteger > 0) then
                 OutStr := OutStr + ', ' + dmMain.DatamartQuery3.FieldByName('Tackles').AsString + ' SACKS';
               if (dmMain.DatamartQuery3.FieldByName('ForcedFumbles').AsInteger > 0) then
                 OutStr := OutStr + ', ' + dmMain.DatamartQuery3.FieldByName('Tackles').AsString + ' FF';
               if (dmMain.DatamartQuery3.FieldByName('FumbleRecoveries').AsInteger > 0) then
               begin
                 if (dmMain.DatamartQuery3.FieldByName('FumbleRecoveries').AsInteger = 1) then
                   OutStr := OutStr + ', FUM REC'
                 else
                   OutStr := OutStr + ', ' + dmMain.DatamartQuery3.FieldByName('FumbleRecoveries').AsString + ' FUM REC';
               end;
             end;
           end;
      end; //Case
    end;
    //Close the query
    dmMain.DatamartQuery3.Close;
  except
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred while trying to retrieve live stats information');
  end;
  //Return
  if (OutStr = '') then OutStr := ' ';
  GetNFLDMStatsString := ANSIUpperCase(OutStr);
end;

////////////////////////////////////////////////////////////////////////////////
// Function to get NFL Fantasy stats
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetNFLDMFantasyStatsString(StatType: SmallInt; Rank: SmallInt):String;
var
  i,j,k: SmallInt;
  SeasonCode: String;
  OutStr: String;
  FirstInitial: String;
begin
  try
    //Setup query
    dmMain.DatamartQuery3.Close;
    dmMain.DatamartQuery3.SQL.Clear;
    Case NFLWeekInfo.SeasonType of
      0: SeasonCode := 'PRE';
      1: SeasonCode := 'REG';
      2: SeasonCode := 'POST';
    end;
    //Build query based on stat type; request 10 data elements; will advance to required index
    Case StatType of
         //Passing
      1: begin
           dmMain.DatamartQuery3.SQL.Add('/* */p_GetTopPassers ' +
             IntToStr(NFLWeekInfo.SeasonYear) + ', ' + QuotedStr(SeasonCode) + ', ' + IntToStr(NFLWeekInfo.Week) +
             ', 10, ' + IntToStr(0) + ', 0');
         end;
         //Rushing
      2: begin
           dmMain.DatamartQuery3.SQL.Add('/* */p_GetTopRushers ' +
             IntToStr(NFLWeekInfo.SeasonYear) + ', ' + QuotedStr(SeasonCode) + ', ' + IntToStr(NFLWeekInfo.Week) +
             ', 10, ' + IntToStr(0) + ', 0');
         end;
         //Receiving; sort by yards
      3: begin
           dmMain.DatamartQuery3.SQL.Add('/* */p_GetTopReceivers ' +
             IntToStr(NFLWeekInfo.SeasonYear) + ', ' + QuotedStr(SeasonCode) + ', ' + IntToStr(NFLWeekInfo.Week) +
             ', 10, ' + IntToStr(0) + ', 0, ' + QuotedStr('Yards'));
         end;
    end;

    //Get the data
    dmMain.DatamartQuery3.Open;

    //Process the data
    if (dmMain.DatamartQuery3.RecordCount > 0) then
    begin
      //Advance to required record
      if (Rank <= dmMain.DatamartQuery3.RecordCount) AND (Rank > 1) then
        for j := 1 to Rank-1 do dmMain.DatamartQuery3.Next;
      Case StatType of
           //Passing
        1: Begin
             if (Length(dmMain.DatamartQuery3.FieldByName('LastName').AsString) > 0) then
             begin
               if (Length(dmMain.DatamartQuery3.FieldByName('FirstName').AsString) >= 1) then
                 FirstInitial := dmMain.DatamartQuery3.FieldByName('FirstName').AsString[1] + '. '
               else FirstInitial := '';
               OutStr := OutStr + '[c 17]' + FirstInitial;
               OutStr := OutStr + dmMain.DatamartQuery3.FieldByName('LastName').AsString;
               OutStr := OutStr + ': [c 1]';
               OutStr := OutStr + dmMain.DatamartQuery3.FieldByName('Yards').AsString + ' YDS';
               if (dmMain.DatamartQuery3.FieldByName('Touchdowns').AsInteger > 0) then
               begin
                 if (dmMain.DatamartQuery3.FieldByName('Touchdowns').AsInteger = 1) then
                   OutStr := OutStr + ', TD'
                 else
                   OutStr := OutStr + ', ' + dmMain.DatamartQuery3.FieldByName('Touchdowns').AsString + ' TD';
               end;
               if (dmMain.DatamartQuery3.FieldByName('Interceptions').AsInteger > 0) then
               begin
                 if (dmMain.DatamartQuery3.FieldByName('Interceptions').AsInteger = 1) then
                   OutStr := OutStr  + ', INT'
                 else
                   OutStr := OutStr  + ', ' + dmMain.DatamartQuery3.FieldByName('Interceptions').AsString + ' INT';
               end;
             end;
           end;
           //Rushing
        2: Begin
             if (Length(dmMain.DatamartQuery3.FieldByName('LastName').AsString) > 0) then
             begin
               if (Length(dmMain.DatamartQuery3.FieldByName('FirstName').AsString) >= 1) then
                 FirstInitial := dmMain.DatamartQuery3.FieldByName('FirstName').AsString[1] + '. '
               else FirstInitial := '';
               OutStr := OutStr + '[c 17]' + FirstInitial;
               OutStr := OutStr + dmMain.DatamartQuery3.FieldByName('LastName').AsString;
               OutStr := OutStr + ': [c 1]' + dmMain.DatamartQuery3.FieldByName('Attempts').AsString + ' RUSH';
               OutStr := OutStr + ', ' + dmMain.DatamartQuery3.FieldByName('Yards').AsString + ' YDS';
               if (dmMain.DatamartQuery3.FieldByName('Touchdowns').AsInteger > 0) then
               begin
                 if (dmMain.DatamartQuery3.FieldByName('Touchdowns').AsInteger = 1) then
                   OutStr := OutStr + ', TD'
                 else
                   OutStr := OutStr + ', ' + dmMain.DatamartQuery3.FieldByName('Touchdowns').AsString + ' TD';
               end;
             end;
           end;
           //Receiving
        3: Begin
             if (Length(dmMain.DatamartQuery3.FieldByName('LastName').AsString) > 0) then
             begin
               if (Length(dmMain.DatamartQuery3.FieldByName('FirstName').AsString) >= 1) then
                 FirstInitial := dmMain.DatamartQuery3.FieldByName('FirstName').AsString[1] + '. '
               else FirstInitial := '';
               OutStr := OutStr + '[c 17]' + FirstInitial;
               OutStr := OutStr + dmMain.DatamartQuery3.FieldByName('LastName').AsString;
               OutStr := OutStr + ': [c 1]' + dmMain.DatamartQuery3.FieldByName('Receptions').AsString + ' REC';
               OutStr := OutStr + ', ' + dmMain.DatamartQuery3.FieldByName('Yards').AsString + ' YDS';
               if (dmMain.DatamartQuery3.FieldByName('Touchdowns').AsInteger > 0) then
               begin
                 if (dmMain.DatamartQuery3.FieldByName('Touchdowns').AsInteger = 1) then
                   OutStr := OutStr + ', TD'
                 else
                   OutStr := OutStr + ', ' + dmMain.DatamartQuery3.FieldByName('Touchdowns').AsString + ' TD';
               end;
             end;
           end;
      end; //Case
    end;
    //Close the query
    dmMain.DatamartQuery3.Close;
  except
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred while trying to retrieve live stats information');
  end;
  //Return
  if (OutStr = '') then OutStr := ' ';
  //Pad with leading blank
  GetNFLDMFantasyStatsString := ' ' + ANSIUpperCase(OutStr);
end;

////////////////////////////////////////////////////////////////////////////////
// CBS CSS SPECIFIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Function to take the CSS time as a 4-digit value and return (FOR CBS CSS)
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetCSSGameTimeRecord(GTimeStr: String): TimeRec;
var
  Outrec: TimeRec;
  MinStr, SecStr: String;
begin
  OutRec.Minutes := StrToIntDef(GTimeStr[1]+GTimeStr[2], 0);
  OutRec.Seconds := StrToIntDef(GTimeStr[4]+GTimeStr[5], 0);
  GetCSSGameTimeRecord := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
// GENERAL DATA FUNCTIONS FOR PROCESSING GAME DATA
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Function to query the appropriate data source and return the required data
////////////////////////////////////////////////////////////////////////////////
//Function to return abbreviation for day of week based on day number
function GetDayOfWeekAbbreviation(DateOfGame: TDateTime): String;
var
  OutStr: String;
  DayNumOfWeek: SmallInt;
begin
  DayNumOfWeek := DayOfWeek(DateOfGame);
  Case DayNumOfWeek of
    1: OutStr := 'SUN';
    2: OutStr := 'MON';
    3: OutStr := 'TUE';
    4: OutStr := 'WED';
    5: OutStr := 'THU';
    6: OutStr := 'FRI';
    7: OutStr := 'SAT';
  end;
  GetDayOfWeekAbbreviation := OutStr;
end;

//Function to get game data
function TEngineInterface.GetGameData (League: String; SI_GCode, ST_Game_ID: String;
                                       CSS_GameID, NFLDM_GameID: LongInt;
                                       Primary_Data_Source, Backup_Data_Source: String): GameRec;
var
  CurrentGamePhaseRec: GamePhaseRec;
  OutRec: GameRec;
  Wins,Losses,Ties: SmallInt;
  CSSVClubKey, CSSHClubKey: LongInt;
  GamePhaseData: GamePhaseRec;
  AdjustedGameStartTime: TDateTime;
begin
  //Check if valid league first
  if (League <> 'NONE') then
  begin
    //Set values for game IDs
    OutRec.SI_GCode := SI_GCode;
    OutRec.ST_Game_ID := ST_Game_ID;
    OutRec.CSS_GameID := CSS_GameID;
    OutRec.NFLDM_GameID := NFLDM_GameID;
    if (UseBackupDataSource = TRUE) then
      OutRec.DataSource := Backup_Data_Source
    else
      OutRec.DataSource := Primary_Data_Source;

    ////////////////////////////////////////////////////////////////////////////
    //Check for Stats Inc.
    ////////////////////////////////////////////////////////////////////////////
    if ((UseBackupDataSource = TRUE) AND (Backup_Data_Source = 'STATSINC')) OR
       ((UsebackupDataSource = FALSE) AND (Primary_Data_Source = 'STATSINC')) then
    begin
      try
        //Set league
        OutRec.League := League;
        //Execute required stored procedure
        dmMain.SportbaseQuery2.Active := FALSE;
        dmMain.SportbaseQuery2.SQL.Clear;
        //Note suffix for CBS-specific stored procedure
        dmMain.SportbaseQuery2.SQL.Add('/* */sp_GetSI' + League + 'GameInfoCBS' + ' ' + QuotedStr(SI_GCode));
        dmMain.SportbaseQuery2.Open;
        //Check for record count > 0 and process
        if (dmMain.SportbaseQuery2.RecordCount > 0) then
        begin
          OutRec.League := dmMain.SportbaseQuery2.FieldByName('League').AsString;
          //Get extra fields if it's an MLB game
          if (League = 'MLB') then
          begin
            OutRec.DoubleHeader := dmMain.SportbaseQuery2.FieldByName('DoubleHeader').AsBoolean;
            OutRec.DoubleHeaderGameNumber := dmMain.SportbaseQuery2.FieldByName('DoubleHeaderGameNumber').AsInteger;
            OutRec.Count := dmMain.SportbaseQuery2.FieldByName('Count').AsString;;
            OutRec.Situation := dmMain.SportbaseQuery2.FieldByName('Situation').AsString;;
          end
          else if (League = 'CFB') OR (League = 'CFL') OR (League = 'NFL')then
          begin
            OutRec.DoubleHeader := FALSE;
            OutRec.DoubleHeaderGameNumber := 0;
            OutRec.Count := '000';
            OutRec.Situation := dmMain.SportbaseQuery2.FieldByName('Situation').AsString;;
          end
          else begin
            OutRec.DoubleHeader := FALSE;
            OutRec.DoubleHeaderGameNumber := 0;
            OutRec.Count := '000';
            OutRec.Situation := 'FFF';
          end;
          //Set team strength variables for NHL
          if (League = 'NHL') then
          begin
            if (ANSIUpperCase(dmMain.SportbaseQuery2.FieldByName('VStrength').AsString) = 'Powerplay') then
              OutRec.Visitor_Param1 := 1
            else
              OutRec.Visitor_Param1 := 0;
            if (ANSIUpperCase(dmMain.SportbaseQuery2.FieldByName('HStrength').AsString) = 'Powerplay') then
              OutRec.Home_Param1 := 1
            else
              OutRec.Home_Param1 := 0;
          end
          else if (League = 'CFB') then
          begin
            OutRec.Visitor_Param1 := dmMain.SportbaseQuery2.FieldByName('VConferenceID').AsInteger;
            OutRec.Home_Param1 := dmMain.SportbaseQuery2.FieldByName('HConferenceID').AsInteger;
          end
          else begin
            OutRec.Visitor_Param1 := 0;
            OutRec.Home_Param1 := 0;
          end;
          //If college, get ranks
          if (League = 'CFB') OR (League = 'CBK') OR (League = 'CBB') then
          begin
            OutRec.VRank := dmMain.SportbaseQuery2.FieldByName('VRank').AsString;
            OutRec.HRank := dmMain.SportbaseQuery2.FieldByName('HRank').AsString;
          end
          else begin
            OutRec.VRank := ' ';
            OutRec.HRank := ' ';
          end;
          //Set remaining parameters
          OutRec.HName := dmMain.SportbaseQuery2.FieldByName('HName').AsString;
          OutRec.VName := dmMain.SportbaseQuery2.FieldByName('VName').AsString;
          OutRec.HMnemonic := dmMain.SportbaseQuery2.FieldByName('HMnemonic').AsString;
          OutRec.VMnemonic := dmMain.SportbaseQuery2.FieldByName('VMnemonic').AsString;
          OutRec.HScore := dmMain.SportbaseQuery2.FieldByName('HScore').AsInteger;
          OutRec.VScore := dmMain.SportbaseQuery2.FieldByName('VScore').AsInteger;

          //Set football spcecific fields
          OutRec.Down := 0;
          OutRec.YardsToGo := 0;
          OutRec.Yardline := '';
          //Set misc. fields
          OutRec.Description := OutRec.League + ': ' + OutRec.VName + ' @ ' + OutRec.HName;
          OutRec.Date := dmMain.SportbaseQuery2.FieldByName('Date').AsDateTime;
          OutRec.StartTime := dmMain.SportbaseQuery2.FieldByName('StartTime').AsDateTime;

          //Get game state                              I
          OutRec.GameState := GetSIGameState(dmMain.SportbaseQuery2.FieldByName('State').AsString,
            dmMain.SportbaseQuery2.FieldByName('TimeMin').AsString, dmMain.SportbaseQuery2.FieldByName('TimeSec').AsString);

          OutRec.HRecord := '';
          OutRec.VRecord := '';

          //Get the game phase
          GamePhaseData := GetSIGamePhaseRec(League, dmMain.SportbaseQuery2.FieldByName('Phase').AsInteger);

          //Check for end of period
          if (OutRec.GameState = 4) AND (GamePhaseData.End_Is_Half = FALSE) then
            OutRec.DisplayPhase := 'END ' + UpperCase(GamePhaseData.Display_Period)
          //Check for half
          else if (OutRec.GameState = 4) AND (GamePhaseData.End_Is_Half = TRUE) then
            OutRec.DisplayPhase := UpperCase(GamePhaseData.Display_Half)
          //Check for final
          else if (OutRec.GameState = 5) then
          begin
            //Game was on a different day, so show day
            if (Trunc(OutRec.Date) <> Trunc(Now)) then
              OutRec.DisplayPhase := 'F/' + GetDayOfWeekAbbreviation(OutRec.Date)
            else
              OutRec.DisplayPhase := UpperCase(GamePhaseData.Display_Final);
          end
          //Check for in-game
          else
            OutRec.DisplayPhase := UpperCase(GamePhaseData.Display_Period);

          //Display clock if game in progress
          if (OutRec.GameState = 1) then
            OutRec.DisplayClock := TimeToStr(dmMain.SportbaseQuery2.FieldByName('StartTime').AsDateTime)
          //Check for in-progress
          else if (OutRec.GameState = 2) then
            OutRec.DisplayClock := FormatSIGameClock(dmMain.SportbaseQuery2.FieldByName('TimeMin').AsInteger,
              dmMain.SportbaseQuery2.FieldByName('TimeSec').AsInteger)
          else
            OutRec.DisplayClock := ' ';
          //Don't need these fields for sports supported by Stats Inc.
          OutRec.HomeTeamHasPossession := FALSE;
          OutRec.CSSNote1Text := '';
          OutRec.CSSNote2Text := '';

          OutRec.DataFound := TRUE;
        end
        else OutRec.DataFound := FALSE;
      except
        if (ErrorLoggingEnabled = True) then
          WriteToErrorLog('Error occurred in GetGameData function for full game data');
        //Clear data found flag
        OutRec.DataFound := FALSE;
      end;
    end

    ////////////////////////////////////////////////////////////////////////////
    //Check for Datamart
    ////////////////////////////////////////////////////////////////////////////
    else if ((UseBackupDataSource = TRUE) AND (Backup_Data_Source = 'NFLDM')) OR
            ((UseBackupDataSource = FALSE) AND (Primary_Data_Source = 'NFLDM')) then
    begin
      try
        //Execute required stored procedure
        dmMain.DatamartQuery2.Active := FALSE;
        dmMain.DatamartQuery2.SQL.Clear;
        dmMain.DatamartQuery2.SQL.Add('/* */sp_VDSGetNFLDMGameInfo ' + IntToStr(NFLDM_GameID));
        dmMain.DatamartQuery2.Open;
        //Check for record count > 0 and process
        if (dmMain.DatamartQuery2.RecordCount > 0) then
        begin
          //Set simple & N/A fields
          OutRec.League := League;
          OutRec.DoubleHeader := FALSE;
          OutRec.DoubleHeaderGameNumber := 0;
          OutRec.Count := '000';
          OutRec.Situation := '';
          OutRec.VRank := ' ';
          OutRec.HRank := ' ';

          //Get team records
          //Execute required stored procedure for Visitor team
          dmMain.DatamartQuery3.Active := FALSE;
          dmMain.DatamartQuery3.SQL.Clear;
          dmMain.DatamartQuery3.SQL.Add('/* */sp_VDSGetTeamRecord ' +
            IntToStr(dmMain.DatamartQuery2.FieldByName('Season').AsInteger) + ',' +
            IntToStr(dmMain.DatamartQuery2.FieldByName('VClub_Key').AsInteger));
          dmMain.DatamartQuery3.Open;
          if (dmMain.DatamartQuery3.RecordCount > 0) then
          begin
            if (dmMain.DatamartQuery3.FieldByName('Tie').AsInteger = 0) then
              OutRec.VRecord := '(' + IntToStr(dmMain.DatamartQuery3.FieldByName('Win').AsInteger) +
               '-' + IntToStr(dmMain.DatamartQuery3.FieldByName('Loss').AsInteger) + ')'
            else
              OutRec.VRecord := '(' + IntToStr(dmMain.DatamartQuery3.FieldByName('Win').AsInteger) +
               '-' + IntToStr(dmMain.DatamartQuery3.FieldByName('Loss').AsInteger) +
               '-' + IntToStr(dmMain.DatamartQuery3.FieldByName('Tie').AsInteger) + ')'
          end;
          //Execute required stored procedure for Home team
          dmMain.DatamartQuery3.Active := FALSE;
          dmMain.DatamartQuery3.SQL.Clear;
          dmMain.DatamartQuery3.SQL.Add('/* */sp_VDSGetTeamRecord ' +
            IntToStr(dmMain.DatamartQuery2.FieldByName('Season').AsInteger) + ',' +
            IntToStr(dmMain.DatamartQuery2.FieldByName('HClub_Key').AsInteger));
          dmMain.DatamartQuery3.Open;
          if (dmMain.DatamartQuery3.RecordCount > 0) then
          begin
            if (dmMain.DatamartQuery3.FieldByName('Tie').AsInteger = 0) then
              OutRec.HRecord := '(' + IntToStr(dmMain.DatamartQuery3.FieldByName('Win').AsInteger) +
               '-' + IntToStr(dmMain.DatamartQuery3.FieldByName('Loss').AsInteger) + ')'
            else
              OutRec.HRecord := '(' + IntToStr(dmMain.DatamartQuery3.FieldByName('Win').AsInteger) +
               '-' + IntToStr(dmMain.DatamartQuery3.FieldByName('Loss').AsInteger) +
               '-' + IntToStr(dmMain.DatamartQuery3.FieldByName('Tie').AsInteger) + ')'
          end;

          //Set game state
          OutRec.GameState := GetNFLDMGameState(dmMain.DatamartQuery2.FieldByName('Phase').AsString,
            dmMain.DatamartQuery2.FieldByName('ClockTime').AsString);

          //Display clock if game in progress
          if (OutRec.GameState = 1) then
          begin
            AdjustedGameStartTime := dmMain.DatamartQuery2.FieldByName('StartTime').AsDateTime +
              (((ABS(dmMain.DatamartQuery2.FieldByName('GMTOffset').AsInteger))-5)/24);

            OutRec.DisplayClock := FormatDateTime('h:mm AM/PM', AdjustedGameStartTime) + ' ET';
          end
          else if (OutRec.GameState = 2) then
            OutRec.DisplayClock := FormatNFLDMTimeString(dmMain.DatamartQuery2.FieldByName('ClockTime').AsString)
          else
            OutRec.DisplayClock := ' ';

          //Set game phase string
          OutRec.DisplayPhase := GetNFLDMGamePhaseString('NFL', OutRec.GameState, dmMain.DatamartQuery2.FieldByName('Phase').AsString);

          //Set remaining parameters
          OutRec.HName := dmMain.DatamartQuery2.FieldByName('HName').AsString;
          OutRec.VName := dmMain.DatamartQuery2.FieldByName('VName').AsString;
          OutRec.HMnemonic := dmMain.DatamartQuery2.FieldByName('HMnemonic').AsString;
          OutRec.VMnemonic := dmMain.DatamartQuery2.FieldByName('VMnemonic').AsString;
          OutRec.HNFLMnemonic := dmMain.DatamartQuery2.FieldByName('HNFLMnemonic').AsString;
          OutRec.VNFLMnemonic := dmMain.DatamartQuery2.FieldByName('VNFLMnemonic').AsString;
          OutRec.HScore := dmMain.DatamartQuery2.FieldByName('HScore').AsInteger;
          OutRec.VScore := dmMain.DatamartQuery2.FieldByName('VScore').AsInteger;

          //Set club keys
          OutRec.Visitor_Param1 := dmMain.DatamartQuery2.FieldByName('VClub_Key').AsInteger;
          OutRec.Home_Param1 := dmMain.DatamartQuery2.FieldByName('HClub_Key').AsInteger;

          //Set football spcecific fields
          OutRec.Down := dmMain.DatamartQuery2.FieldByName('Down').AsInteger;
          OutRec.YardsToGo := dmMain.DatamartQuery2.FieldByName('YardsToGo').AsInteger;
          OutRec.Yardline := dmMain.DatamartQuery2.FieldByName('Yardline').AsString;
          OutRec.HomeTeamHasPossession := dmMain.DatamartQuery2.FieldByName('HomeTeamHasPossession').AsBoolean;

          //Set misc. fields
          OutRec.Description := OutRec.League + ': ' + OutRec.VName + ' @ ' + OutRec.HName;
          OutRec.Date := dmMain.DatamartQuery2.FieldByName('GameDate').AsDateTime;
          OutRec.StartTime := dmMain.DatamartQuery2.FieldByName('StartTime').AsDateTime;
          OutRec.DataFound := TRUE;
        end
        else OutRec.DataFound := FALSE;
      except
        if (ErrorLoggingEnabled = True) then
          WriteToErrorLog('Error occurred in GetGameData function for full game data');
        //Clear data found flag
        OutRec.DataFound := FALSE;
      end;
    end

    ////////////////////////////////////////////////////////////////////////////
    //Check for CSS
    ////////////////////////////////////////////////////////////////////////////
    else if (UseBackupDataSource = TRUE) AND (Backup_Data_Source = 'CBSCSS') OR
            (UseBackupDataSource = FALSE) AND (Primary_Data_Source = 'CBSCSS') then
    begin
      //Set data source
      OutRec.DataSource := 'CBSCSS';
      try
        //Execute required stored procedure
        dmMain.CSSQuery2.Active := FALSE;
        dmMain.CSSQuery2.SQL.Clear;
        dmMain.CSSQuery2.SQL.Add('/* */vds_GetGame ' + IntToStr(CSS_GameID));
        dmMain.CSSQuery2.Open;
        //Check for record count > 0 and process
        if (dmMain.CSSQuery2.RecordCount > 0) then
        begin
          OutRec.League := dmMain.CSSQuery2.FieldByName('sportCode').AsString;
          OutRec.DoubleHeader := FALSE;
          OutRec.DoubleHeaderGameNumber := 0;
          OutRec.Count := '000';
          OutRec.Situation := '';
          if (UseNCAASeeding) then
          begin
            OutRec.VRank := dmMain.CSSQuery2.FieldByName('awaySeed').AsString;
            OutRec.HRank := dmMain.CSSQuery2.FieldByName('homeSeed').AsString;
          end
          else begin
            OutRec.VRank := dmMain.CSSQuery2.FieldByName('awayStanding').AsString;
            OutRec.HRank := dmMain.CSSQuery2.FieldByName('homeStanding').AsString;
          end;

          //Set game state - adjusted to support end-of-period condition
          Case dmMain.CSSQuery2.FieldByName('BehaviorCode').AsInteger of
            //Pre-game
            1: OutRec.GameState := 1;
            //Running
            2: OutRec.GameState := 2;
            //Running, no clock
            3: OutRec.GameState := 3;
            //Final
            4: OutRec.GameState := 5;
          end;

          //Get team records
          //Away team
          Wins := StrToIntDef(TRIM(dmMain.CSSQuery2.FieldByName('awayWins').AsString), 0);
          Losses := StrToIntDef(TRIM(dmMain.CSSQuery2.FieldByName('awayLosses').AsString), 0);
          Ties := StrToIntDef(TRIM(dmMain.CSSQuery2.FieldByName('awayTies').AsString), 0);
          if (Ties = 0) then
            OutRec.VRecord := '(' + IntToStr(Wins) + '-' + IntToStr(Losses) + ')'
          else
            OutRec.VRecord := '(' + IntToStr(Wins) + '-' + IntToStr(Losses) + ' ' + IntToStr(Ties) + ')';
          //Home team
          Wins := StrToIntDef(TRIM(dmMain.CSSQuery2.FieldByName('homeWins').AsString), 0);
          Losses := StrToIntDef(TRIM(dmMain.CSSQuery2.FieldByName('homeLosses').AsString), 0);
          Ties := StrToIntDef(TRIM(dmMain.CSSQuery2.FieldByName('homeTies').AsString), 0);
          if (Ties = 0) then
            OutRec.HRecord := '(' + IntToStr(Wins) + '-' + IntToStr(Losses) + ')'
          else
            OutRec.HRecord := '(' + IntToStr(Wins) + '-' + IntToStr(Losses) + ' ' + IntToStr(Ties) + ')';

          //Get display clock
          if (OutRec.GameState = 2) then
            OutRec.DisplayClock := dmMain.CSSQuery2.FieldByName('Clock').AsString
          else
            OutRec.DisplayClock := '';

          //Get display period - show start time if pre-game
          if (OutRec.GameState = 1) then
          begin
            if (League = 'NFL') then
              OutRec.DisplayPhase := FormatDateTime('h:mm AM/PM', dmMain.CSSQuery2.FieldByName('StartTime').AsDateTime)
            else
              OutRec.DisplayPhase := dmMain.CSSQuery2.FieldByName('period').AsString
          end
          else
            OutRec.DisplayPhase := ANSIUpperCase(dmMain.CSSQuery2.FieldByName('Period').AsString);

          //Set club keys
          if (OutRec.League = 'NFL') then
          begin
            //Do visitor team
            dmMain.DatamartQuery3.Close;
            dmMain.DatamartQuery3.SQL.Clear;
            dmMain.DatamartQuery3.SQL.Add('/* */sp_VDSGetNFLDMClubKey ' +
              QuotedStr(dmMain.CSSQuery2.FieldByName('awayTricodeNFL').AsString) + ', ' + IntToStr(NFLWeekInfo.SeasonYear));
            dmMain.DatamartQuery3.Open;
            if (dmMain.DatamartQuery3.RecordCount > 0) then
              CSSVClubKey := dmMain.DatamartQuery3.FieldByName('ClubKey').AsInteger;
            OutRec.Visitor_Param1 := CSSVClubKey;
            //Do home team
            dmMain.DatamartQuery3.Close;
            dmMain.DatamartQuery3.SQL.Clear;
            dmMain.DatamartQuery3.SQL.Add('/* */sp_VDSGetNFLDMClubKey ' +
              QuotedStr(dmMain.CSSQuery2.FieldByName('homeTricodeNFL').AsString) + ', ' + IntToStr(NFLWeekInfo.SeasonYear));
            dmMain.DatamartQuery3.Open;
            if (dmMain.DatamartQuery3.RecordCount > 0) then
              CSSVClubKey := dmMain.DatamartQuery3.FieldByName('ClubKey').AsInteger;
            OutRec.Home_Param1 := CSSHClubKey;
          end;

          //Set remaining parameters
          OutRec.HName := dmMain.CSSQuery2.FieldByName('HomeName').AsString;
          OutRec.VName := dmMain.CSSQuery2.FieldByName('AwayName').AsString;
          OutRec.HMnemonic := dmMain.CSSQuery2.FieldByName('homeTricodeCBS').AsString;
          OutRec.VMnemonic := dmMain.CSSQuery2.FieldByName('awayTricodeCBS').AsString;
          //OutRec.HNFLMnemonic := dmMain.CSSQuery2.FieldByName('HomeTricodeNFL').AsString;
          //OutRec.VNFLMnemonic := dmMain.CSSQuery2.FieldByName('AwayTricodeNFL').AsString;
          OutRec.HScore := dmMain.CSSQuery2.FieldByName('homeScore').AsInteger;
          OutRec.VScore := dmMain.CSSQuery2.FieldByName('awayScore').AsInteger;

          //Set game note text
          OutRec.CSSNote1Text := dmMain.CSSQuery2.FieldByName('onelinenoteA').AsString;
          OutRec.CSSNote2Text := dmMain.CSSQuery2.FieldByName('onelinenoteB').AsString;

          //Get college logo names
          if (OutRec.League = 'CFB') OR (OutRec.League = 'CBB') OR (OutRec.League = 'WCB') then
          begin
            //Do home
            with dmMain.Query6 do
            begin
              Close;
              SQL.Clear;
              SQL.Add('SELECT GametraxTickerLogoFilename FROM CFB_Team_Logos_Names WHERE GametraxLong = ' +
                QuotedStr(OutRec.HName) + ' AND Sport = ' + QuotedStr(OutRec.League));
              Open;
              if (RecordCount > 0) then
              begin
                if (Trim(FieldByName('GametraxTickerLogoFilename').AsString) = '') then
                  OutRec.HCollegeLogoName := DEFAULT_COLLEGE_LOGO_NAME
                else
                  OutRec.HCollegeLogoName := FieldByName('GametraxTickerLogoFilename').AsString;
              end
              else OutRec.HCollegeLogoName := DEFAULT_COLLEGE_LOGO_NAME;
              Close;
            end;
            //Do visitor
            with dmMain.Query6 do
            begin
              Close;
              SQL.Clear;
              SQL.Add('SELECT GametraxTickerLogoFilename FROM CFB_Team_Logos_Names WHERE GametraxLong = ' +
                QuotedStr(OutRec.VName) + ' AND Sport = ' + QuotedStr(OutRec.League));
              Open;
              if (RecordCount > 0) then
              begin
                if (Trim(FieldByName('GametraxTickerLogoFilename').AsString) = '') then
                  OutRec.VCollegeLogoName := DEFAULT_COLLEGE_LOGO_NAME
                else
                  OutRec.VCollegeLogoName := FieldByName('GametraxTickerLogoFilename').AsString
              end
              else OutRec.VCollegeLogoName := DEFAULT_COLLEGE_LOGO_NAME;
              Close;
            end;
          //end
          //else begin
          //  OutRec.HCollegeLogoName := '';
          //  OutRec.VCollegeLogoName := '';
          end;

          //Set football specific fields
          if (League = 'NFL') then
          begin
            OutRec.Down := StrToIntDef(dmMain.CSSQuery2.FieldByName('Down').AsString, -1);
            OutRec.YardsToGo := StrToIntDef(dmMain.CSSQuery2.FieldByName('YTG').AsString, -1);
            OutRec.Yardline := dmMain.CSSQuery2.FieldByName('YdLine').AsString;
            if (TRIM(dmMain.CSSQuery2.FieldByName('homePoss').AsString) <> '') then
              OutRec.HomeTeamHasPossession := TRUE
            else if (TRIM(dmMain.CSSQuery2.FieldByName('awayPoss').AsString) <> '') then
              OutRec.HomeTeamHasPossession := FALSE;
          end
          else begin
            OutRec.Down := 0;
            OutRec.YardsToGo := 0;
            OutRec.Yardline := '';
            OutRec.HomeTeamHasPossession := FALSE;
          end;

          //Set misc. fields
          OutRec.Description := OutRec.League + ': ' + OutRec.VName + ' @ ' + OutRec.HName;
          OutRec.Date := Trunc(Now);
          OutRec.StartTime := Now;
          OutRec.DataFound := TRUE;
        end
        else OutRec.DataFound := FALSE;
      except
        if (ErrorLoggingEnabled = True) then
          WriteToErrorLog('Error occurred in GetGameData function for full game data');
        //Clear data found flag
        OutRec.DataFound := FALSE;
      end;
    end;
  end;
  //Return
  GetGameData := OutRec;
end;

//Function to trim the year from the SI game ID to keep it under the required string length
//for passing in the game ID
function TEngineInterface.TrimYear(InStr: String): String;
var
  OutStr: String;
begin
  if (Length(InStr) > 4) then
  OutStr := Copy(InStr, 5, 20);
  TrimYear := OutStr;
end;

////////////////////////////////////////////////////////////////////////////////
// Function to take a symbolic name and return the value of the string to be
// sent to the engine
////////////////////////////////////////////////////////////////////////////////
function TEngineInterface.GetValueOfSymbol(PlaylistType: SmallInt; Symbolname: String; SymbolType: SmallInt;
                                           CurrentEntryIndex: SmallInt; CurrentGameData: GameRec;
                                           TickerMode: SmallInt): String;
var
  i,j: SmallInt;
  TickerRecPtr: ^TickerRec;
  BugRecPtr: ^BugRec;
  GameTraxRecPtr: ^GameTraxRec;
  OutStr, OutStr2, OutStr3: String;
  TextFieldBias: SmallInt;
  GamePhaseData: GamePhaseRec;
  BaseName: String;
  CharPos: SmallInt;
  Suffix: String;
  SwitchPosMixed, SwitchPosUpper: SmallInt;
  SwitchPosStartBlack, SwitchPosEndBlack: SmallInt;
  TempStr, TempStr2: String;
  Days: array[1..7] of string;
  TwitterMessageData: TwitterMessageRec;
begin
  //Init
  OutStr := SymbolName;
  BaseName := '';
  Suffix := '';
  LongTimeFormat := 'h:mm AM/PM';

  //Set text field bias
  Case TickerMode of
    -1: TextFieldBias := 0;
     1: TextFieldBias := 25;
  end;

  //Get info on current record
  //Prevent bad indexes
  if (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
     TICKER: TickerRecPtr := StudioTicker_Collection.At(CurrentEntryIndex);
     BUG: BugRecPtr := StudioBug_Collection.At(CurrentEntryIndex);
     GAMETRAX: GameTraxRecPtr := Gametrax_Collection.At(CurrentEntryIndex);
    end;
  end;

  //Extract out base symbol name and any suffix
  CharPos := Pos('|', SymbolName);
  if (CharPos > 0) then
  begin
    //Get base name
    if (CharPos > 1) then
      for i := 1 to CharPos-1 do BaseName := BaseName + SymbolName[i];
    //Get suffix
    if (Length(Symbolname) > CharPos) then
      for i := CharPos+1 to Length(Symbolname) do Suffix := Suffix + SymbolName[i];
    //Set new base name
    SymbolName := BaseName;
  end;

  //START PROCESSING DATA FIELDS
  //Blank
  if (SymbolName = '$Blank') then OutStr := ' '

  //Blank
  else if (SymbolName = '$Null') then OutStr := ''

  //Asterisk
  else if (SymbolName = '$Asterisk') then OutStr := '*'

  //Force 4x3
  else if (SymbolName = '$4x3') then OutStr := '1'

  //Sponsor logo
  else if (SymbolName = '$Sponsor_Logo') then
  begin
    if (CurrentEntryIndex <> NOENTRYINDEX) then
    begin
      Case  PlaylistType of
        TICKER: OutStr := TickerRecPtr^.SponsorLogo_Name;
        GAMETRAX: OutStr := GametraxRecPtr^.SponsorLogo_Name;
      end;
    end
    else OutStr := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
  end
  //Promo logo
  else if (SymbolName = '$Promo_Logo') then
  begin
    if (CurrentEntryIndex <> NOENTRYINDEX) then
    begin
      Case  PlaylistType of
        TICKER: OutStr := TickerRecPtr^.SponsorLogo_Name;
      end;
    end
    else OutStr := dmMain.tblPromo_Logos.FieldByName('LogoFilename').AsString;
  end
  //League
  else if (SymbolName = '$League') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := UpperCase(TickerRecPtr^.Subleague_Mnemonic_Standard);
      BUG: OutStr := UpperCase(BugRecPtr^.League);
      //Modified for Version 2.3.0 to support CBS logos in league region
      GAMETRAX: begin
        if (Length(GameTraxRecPtr^.League) >= 3) AND (GameTraxRecPtr^.League[1] = '[') then
        begin
          TempStr := '';
          j := 2;
          while (GameTraxRecPtr^.League[j] <> ']') and (j <= Length(GameTraxRecPtr^.League)) do
          begin
            TempStr := TempStr + GameTraxRecPtr^.League[j];
            inc(j);
          end;
          OutStr := '[f 6]' + Chr(StrToIntDef(TempStr, 32));
        end
        else OutStr := UpperCase(GameTraxRecPtr^.League);
      end;
    end;
  end

  //User-defined text fields
  else if (SymbolName = '$Text_1') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[1+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[1];
      GAMETRAX: begin
        //Check for CFB Conference Chip first
        if (SymbolType = 10) then
        begin
          OutStr := GetConferenceChipText(GameTraxRecPtr^.UserData[1]);
          //Support logo substitution for conference chips area
          if (Length(OutStr) >= 3) AND (OutStr[1] = '[') then
          begin
            TempStr := '';
            j := 2;
            while (OutStr[j] <> ']') and (j <= Length(OutStr)) do
            begin
              TempStr := TempStr + OutStr[j];
              inc(j);
            end;
            OutStr := '[f 6]' + Chr(StrToIntDef(TempStr, 32));
          end;
        end
        else
          OutStr := GameTraxRecPtr^.UserData[1];
      end;
    end;
  end
  else if (SymbolName = '$Text_2') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[2+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[2];
      GAMETRAX: begin
        //Check for CFB Conference Chip first
        if (SymbolType = 10) then
        begin
          OutStr := GetConferenceChipText(GameTraxRecPtr^.UserData[2]);
          //Support logo substitution for conference chips area
          if (Length(OutStr) >= 3) AND (OutStr[1] = '[') then
          begin
            TempStr := '';
            j := 2;
            while (OutStr[j] <> ']') and (j <= Length(OutStr)) do
            begin
              TempStr := TempStr + OutStr[j];
              inc(j);
            end;
            OutStr := '[f 6]' + Chr(StrToIntDef(TempStr, 32));
          end;
        end
        else
          OutStr := GameTraxRecPtr^.UserData[2];
      end;
    end;
  end
  else if (SymbolName = '$Text_3') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[3+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[3];
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[3];
    end;
  end
  else if (SymbolName = '$Text_4') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[4+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[4];
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[4];
    end;
  end
  else if (SymbolName = '$Text_5') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[5+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[5];
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[5];
    end;
  end
  else if (SymbolName = '$Text_6') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[6+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[6];
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[6];
    end;
  end
  else if (SymbolName = '$Text_7') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[7+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[7];
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[7];
    end;
  end
  else if (SymbolName = '$Text_8') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[8+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[8];
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[8];
    end;
  end
  else if (SymbolName = '$Text_9') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[9+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[9];
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[9];
    end;
  end
  else if (SymbolName = '$Text_10') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[10];
      BUG: OutStr := BugRecPtr^.UserData[10];
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[10+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_11') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
     Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[11];
      BUG: OutStr := BugRecPtr^.UserData[11];
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[11+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_12') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[12];
      BUG: OutStr := BugRecPtr^.UserData[12];
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[12+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_13') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[13];
      BUG: OutStr := BugRecPtr^.UserData[13];
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[13+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_14') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[14];
      BUG: OutStr := BugRecPtr^.UserData[14];
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[14+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_15') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[15+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[15];
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[15];
    end;
  end
  else if (SymbolName = '$Text_16') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[16+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[16];
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[16];
    end;
  end
  else if (SymbolName = '$Text_17') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[17+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[17];
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[17];
    end;
  end
  else if (SymbolName = '$Text_18') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[18+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[18];
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[18];
    end;
  end
  else if (SymbolName = '$Text_19') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[19+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[19];
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[19];
    end;
  end
  else if (SymbolName = '$Text_20') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[20+TextFieldBias];
      BUG: OutStr := BugRecPtr^.UserData[20];
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[20];
    end;
  end
  else if (SymbolName = '$Text_21') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[21+TextFieldBias];
      BUG: OutStr := '';
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[21];
    end;
  end
  else if (SymbolName = '$Text_22') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[22+TextFieldBias];
      BUG: OutStr := '';
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[22];
    end;
  end
  else if (SymbolName = '$Text_23') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[23+TextFieldBias];
      BUG: OutStr := '';
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[23];
    end;
  end
  else if (SymbolName = '$Text_24') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[24+TextFieldBias];
      BUG: OutStr := '';
      GAMETRAX: OutStr := GameTraxRecPtr^.UserData[24];
    end;
  end
  //Note - used for conference chip in Gametrax for NCAA
  else if (SymbolName = '$Text_25') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[25+TextFieldBias];
      BUG: OutStr := '';
      GAMETRAX: OutStr := Chr(StrToInt(Trim(GameTraxRecPtr^.UserData[24])));
    end;
  end

  //Game related data
  else if (SymbolName = '$Game_ID') then
  begin
    //Using VDS supplied data
    if (UseBackupDataSource = FALSE) then
    begin
      Case PlaylistType of
        TICKER: OutStr := IntToStr(CurrentGameData.NFLDM_GameID);
        BUG: OutStr := IntToStr(CurrentGameData.NFLDM_GameID);
        GAMETRAX: OutStr := IntToStr(CurrentGameData.NFLDM_GameID);
      end;
    end
    //CSS
    else begin
      Case PlaylistType of
        TICKER: OutStr := IntToStr(CurrentGameData.CSS_GameID);
        BUG: OutStr := IntToStr(CurrentGameData.CSS_GameID);
        GAMETRAX: OutStr := IntToStr(CurrentGameData.CSS_GameID);
      end;
    end;
    //Append asterisk if live updates disabled
    if (DisplayFeatureFlags.EnableLiveUpdates=FALSE) then
    OutStr := '*' + OutStr;
  end

  //Game ID - Stats Inc.
  else if (SymbolName = '$Game_ID_SI') then
  begin
    if (UseBackupDataSource = FALSE) then
    begin
      Case PlaylistType of
        TICKER: OutStr := TrimYear(CurrentGameData.SI_GCode);
        BUG: OutStr := TrimYear(CurrentGameData.SI_GCode);
        GAMETRAX: OutStr := trimYear(CurrentGameData.SI_GCode);
      end;
    end
    //CSS
    else begin
      Case PlaylistType of
        TICKER: OutStr := IntToStr(CurrentGameData.CSS_GameID);
        BUG: OutStr := IntToStr(CurrentGameData.CSS_GameID);
        GAMETRAX: OutStr := IntToStr(CurrentGameData.CSS_GameID);
      end;
    end;
    //Append asterisk if live updates disabled
    if (DisplayFeatureFlags.EnableLiveUpdates=FALSE) then
    OutStr := '*' + OutStr;
  end

  //Game start time
  else if (SymbolName = '$Start_Time') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
      OutStr := 'GAME 2'
    else begin
      //Check to see if start time more than 24 hours ahead. If so, show day of week.
      if ((Trunc(CurrentGameData.Date) + CurrentGameData.StartTime) - Now > 1) then
      begin
        Days[1] := 'SUNDAY';
        Days[2] := 'MONDAY';
        Days[3] := 'TUESDAY';
        Days[4] := 'WEDNESDAY';
        Days[5] := 'THURSDAY';
        Days[6] := 'FRIDAY';
        Days[7] := 'SATURDAY';
        OutStr := Days[DayOfWeek(CurrentGameData.Date)];
      end
      else
        OutStr := TimeToStr(CurrentGameData.StartTime) + ' ET';
    end;
  end

  else if (SymbolName = '$Start_Time_No_Timezone') then OutStr := TimeToStr(CurrentGameData.StartTime)

  else if (SymbolName = '$Start_Time_No_Suffix') then
  begin
    OutStr := TimeToStr(CurrentGameData.StartTime);
    OutStr := StringReplace(OutStr, 'AM', ' ', [rfReplaceAll]);
    OutStr := StringReplace(OutStr, 'PM', ' ', [rfReplaceAll]);
    OutStr := Trim(OutStr) + ' ET';
  end

  else if (SymbolName = '$Visitor_Name') then
  begin
    OutStr := UpperCase(CurrentGameData.VName);
  end

  else if (SymbolName = '$Home_Name') then
  begin
    OutStr := UpperCase(CurrentGameData.HName);
  end

  else if (SymbolName = '$Visitor_Mnemonic') then
  begin
    OutStr := UpperCase(CurrentGameData.VMnemonic);
  end

  else if (SymbolName = '$Home_Mnemonic') then
  begin
    OutStr := UpperCase(CurrentGameData.HMnemonic);
  end

  else if (SymbolName = '$Visitor_Logo') then
  begin
    if (Trim(CurrentGameData.VMnemonic) = '') then
      OutStr := 'CLEAR'
    else
      OutStr := UpperCase(CurrentGameData.VMnemonic);
  end
  else if (SymbolName = '$Visitor_Logo_Cropped') then
  begin
    if (Trim(CurrentGameData.VMnemonic) = '') then
      OutStr := 'CLEAR'
    else
      OutStr := 'N_' + UpperCase(CurrentGameData.VMnemonic);
  end
  else if (SymbolName = '$Visitor_Logo_College') then
  begin
    if (Trim(CurrentGameData.VCollegeLogoName) = '') then
      OutStr := 'CLEAR'
    else
      OutStr := UpperCase(CurrentGameData.VCollegeLogoName);
  end

  else if (SymbolName = '$Home_Logo') then
  begin
    if (Trim(CurrentGameData.HMnemonic) = '') then
      OutStr := 'CLEAR'
    else
      OutStr := UpperCase(CurrentGameData.HMnemonic);
  end
  else if (SymbolName = '$Home_Logo_Cropped') then
  begin
    if (Trim(CurrentGameData.HMnemonic) = '') then
      OutStr := 'CLEAR'
    else
      OutStr := 'N_' + UpperCase(CurrentGameData.HMnemonic);
  end
  else if (SymbolName = '$Home_Logo_College') then
  begin
    if (Trim(CurrentGameData.HCollegeLogoName) = '') then
      OutStr := 'CLEAR'
    else
      OutStr := UpperCase(CurrentGameData.HCollegeLogoName);
  end

  //Visitor Record + Possession Indicator + Name for NFL
  else if (SymbolName = '$Visitor_Name_Record_Possession') then
  begin
    //Insert possession if applicable
    if (CurrentGameData.GameState = 2) then
    begin
      if (CurrentGameData.HomeTeamHasPossession = FALSE) AND (DisplayFeatureFlags.EnablePossessionIndicator) then
      begin
        if (ABS(GetNFLDMYardageIndidcatorValue(CurrentGameData)) >= 80) then
          OutStr := FOOTBALLPOSSESSIONREDZONE
        else
          OutStr := FOOTBALLPOSSESSION;
      end
      else OutStr := FOOTBALLNOPOSSESSION;
    end
    else OutStr := FOOTBALLNOPOSSESSION;
    OutStr := OutStr + '|';
    if (CurrentGameData.VRecord <> '') AND
       ((CurrentGameData.GameState = 1) OR (CurrentGameData.GameState = 5)) then
      //OutStr := OutStr + '[c 17]' + CurrentGameData.VRecord + ' ';
      OutStr := OutStr + CurrentGameData.VRecord + ' ';
    OutStr := OutStr + '|';
    //OutStr := OutStr + '[c 1]' + UpperCase(CurrentGameData.VName);
    OutStr := OutStr + UpperCase(CurrentGameData.VName);
  end

  //Home Record + Possession Indicator + Name for NFL
  else if (SymbolName = '$Home_Name_Record_Possession') then
  begin
    //Insert possession if applicable
    if (CurrentGameData.GameState = 2) then
    begin
      if (CurrentGameData.HomeTeamHasPossession = TRUE) AND (DisplayFeatureFlags.EnablePossessionIndicator) then
      begin
        if (ABS(GetNFLDMYardageIndidcatorValue(CurrentGameData)) >= 80) then
          OutStr := FOOTBALLPOSSESSIONREDZONE
        else
          OutStr := FOOTBALLPOSSESSION;
      end
      else OutStr := FOOTBALLNOPOSSESSION;
    end
    else OutStr := FOOTBALLNOPOSSESSION;
    OutStr := OutStr + '|';
    if (CurrentGameData.VRecord <> '') AND
       ((CurrentGameData.GameState = 1) OR (CurrentGameData.GameState = 5)) then
      //OutStr := OutStr + '[c 17]' + CurrentGameData.HRecord + ' ';
      OutStr := OutStr + CurrentGameData.HRecord + ' ';
    OutStr := OutStr + '|';
    //OutStr := OutStr + '[c 1]' + UpperCase(CurrentGameData.HName);
    OutStr := OutStr + UpperCase(CurrentGameData.HName);
  end

  //Visitor Rank + Name for CFB
  else if (SymbolName = '$Visitor_Name_Rank') then
  begin
    //Insert rank if applicable
    if (Trim(CurrentGameData.VRank) <> '') then
      OutStr := '|' + CurrentGameData.VRank + '|'
    else
      OutStr := '||';
    OutStr := OutStr + UpperCase(CurrentGameData.VName);
  end

  //Home Rank + Name for CFB
  else if (SymbolName = '$Home_Name_Rank') then
  begin
    //Insert rank if applicable
    if (Trim(CurrentGameData.HRank) <> '') then
      OutStr := '|' + CurrentGameData.HRank + '|'
    else
      OutStr := '||';
    OutStr := OutStr + UpperCase(CurrentGameData.HName);
  end

  //Added 11/08/12 for CFB/CBB Rank + Name + Record
  //Visitor
  else if (SymbolName = '$Visitor_Name_Rank_Record') then
  begin
    //Insert rank if applicable
    if (Trim(CurrentGameData.VRank) <> '') then
      OutStr := '|' + CurrentGameData.VRank + '|'
    else
      OutStr := '||';
    OutStr := OutStr + UpperCase(CurrentGameData.VName);
    //Add record
    if (Trim(CurrentGameData.VRecord) <> '') then
      OutStr := OutStr + '| ' + CurrentGameData.VRecord;
  end

  //Home
  else if (SymbolName = '$Home_Name_Rank_Record') then
  begin
    //Insert rank if applicable
    if (Trim(CurrentGameData.HRank) <> '') then
      OutStr := '|' + CurrentGameData.HRank + '|'
    else
      OutStr := '||';
    OutStr := OutStr + UpperCase(CurrentGameData.HName);
    //Add record
    if (Trim(CurrentGameData.HRecord) <> '') then
      OutStr := OutStr + '| ' + CurrentGameData.HRecord;
  end

  //Visitor Rank + Name for CBB
  else if (SymbolName = '$Visitor_Name_Rank_NCAA_Tournament') then
  begin
    //Insert rank if applicable
    if (Trim(CurrentGameData.VRank) <> '') then
      OutStr := CurrentGameData.VRank + '|'
    else
      OutStr := '|';
    OutStr := OutStr + UpperCase(CurrentGameData.VName);
    //Add record
    if (Trim(CurrentGameData.HRecord) <> '') and (EnableTeamRecordsForNCAATournament) then
      OutStr := OutStr + '| ' + CurrentGameData.VRecord;
  end

  //Home Rank + Name for CFB
  else if (SymbolName = '$Home_Name_Rank_NCAA_Tournament') then
  begin
    //Insert rank if applicable
    if (Trim(CurrentGameData.HRank) <> '') then
      OutStr := CurrentGameData.HRank + '|'
    else
      OutStr := '|';
    OutStr := OutStr + UpperCase(CurrentGameData.HName);
    //Add record
    if (Trim(CurrentGameData.HRecord) <> '') and (EnableTeamRecordsForNCAATournament) then
      OutStr := OutStr + '| ' + CurrentGameData.HRecord;
  end

  //Added 11/08/12 for CFB/CBB Rank + Name + Record
  //Visitor
  else if (SymbolName = '$Visitor_Name_Rank_Record') then
  begin
    //Insert rank if applicable
    if (Trim(CurrentGameData.VRank) <> '') then
      OutStr := '|' + CurrentGameData.VRank + '|'
    else
      OutStr := '||';
    OutStr := OutStr + UpperCase(CurrentGameData.VName);
    //Add record
    if (Trim(CurrentGameData.VRecord) <> '') then
      OutStr := OutStr + '| ' + CurrentGameData.VRecord;
  end

  //Home
  else if (SymbolName = '$Home_Name_Rank_Record') then
  begin
    //Insert rank if applicable
    if (Trim(CurrentGameData.HRank) <> '') then
      OutStr := '|' + CurrentGameData.HRank + '|'
    else
      OutStr := '||';
    OutStr := OutStr + UpperCase(CurrentGameData.HName);
    //Add record
    if (Trim(CurrentGameData.HRecord) <> '') then
      OutStr := OutStr + '| ' + CurrentGameData.HRecord;
  end

  //Conference chip
  else if (SymbolName = '$Conference_Chip') then
  begin
    //Get character from ASCII value
    //OutStr := Chr(StrToInt(Trim(GameTraxRecPtr^.UserData[24])));
    OutStr := Chr(StrToInt('202'));
  end

  //Visitor Possession Indicator + Mnemonic for NFL
  else if (SymbolName = '$Visitor_Mnemonic_Possession') then
  begin
    //Insert possession if applicable
    if (CurrentGameData.GameState = 2) then
    begin
      if (CurrentGameData.HomeTeamHasPossession = FALSE) AND (DisplayFeatureFlags.EnablePossessionIndicator) then
      begin
        if (ABS(GetNFLDMYardageIndidcatorValue(CurrentGameData)) >= 80) then
          OutStr := FOOTBALLPOSSESSIONREDZONE
        else
          OutStr := FOOTBALLPOSSESSION;
      end
      else OutStr := FOOTBALLNOPOSSESSION;
    end
    else OutStr := FOOTBALLNOPOSSESSION;
    OutStr := OutStr + '|';
    //OutStr := OutStr + '[c 1]' + UpperCase(CurrentGameData.VMnemonic);
    OutStr := OutStr + UpperCase(CurrentGameData.VMnemonic);
  end

  //Home Possession Indicator + Mnemomic for NFL
  else if (SymbolName = '$Home_Mnemonic_Possession') then
  begin
    //Insert possession if applicable
    if (CurrentGameData.GameState = 2) then
    begin
      if (CurrentGameData.HomeTeamHasPossession = TRUE) AND (DisplayFeatureFlags.EnablePossessionIndicator) then
      begin
        if (ABS(GetNFLDMYardageIndidcatorValue(CurrentGameData)) >= 80) then
          OutStr := FOOTBALLPOSSESSIONREDZONE
        else
          OutStr := FOOTBALLPOSSESSION;
      end
      else OutStr := FOOTBALLNOPOSSESSION;
    end
    else OutStr := '';
    OutStr := OutStr + '|';
    //OutStr := OutStr + '[c 1]' + UpperCase(CurrentGameData.HMnemonic);
    OutStr := OutStr + UpperCase(CurrentGameData.HMnemonic);
  end

  //For triggering 4x3 mode for game finals & pre-game
  else if (SymbolName = '$Pregame_Final_Flag') then
  begin
    if (CurrentGameData.GameState = 1) OR (CurrentGameData.GameState = 5) then
      OutStr := '1'
    else
      OutStr := '0';
    //Force to 0 if test flag set
    if (Force16x9Mode) then OutStr := '0';
  end

  //Visitor Mnemonic for CFB
  else if (SymbolName = '$Visitor_Mnemonic_College') then
  begin
    OutStr := '|';
    OutStr := OutStr + UpperCase(CurrentGameData.VMnemonic);
  end

  //Home Mnemonic for CFB
  else if (SymbolName = '$Home_Mnemonic_College') then
  begin
    OutStr := '|';
    OutStr := OutStr + UpperCase(CurrentGameData.HMnemonic);
  end

  //Visitor Rank + Mnemonic for CFB
  else if (SymbolName = '$Visitor_Mnemonic_Rank') then
  begin
    //Insert possession if applicable
    if (Trim(CurrentGameData.VRank) <> '') then
      OutStr := CurrentGameData.VRank + '|'
    else
      OutStr := '|';
    OutStr := OutStr + UpperCase(CurrentGameData.VMnemonic);
  end

  //Home Rank + Mnemonic for CFB
  else if (SymbolName = '$Home_Mnemonic_Rank') then
  begin
    //Insert rank if applicable
    if (Trim(CurrentGameData.HRank) <> '') then
      OutStr := CurrentGameData.HRank + '|'
    else
      OutStr := '|';
    OutStr := OutStr + UpperCase(CurrentGameData.HMnemonic);
  end

  //Visitor score
  else if (SymbolName = '$Visitor_Score') then
  begin
    //Don't display score if game is postponed or suspended, or if delayed and at top of 1st
    if (CurrentGameData.GameState <> 1) AND (CurrentGameData.GameState <> 6) AND (CurrentGameData.GameState <> 7) AND
       (CurrentGameData.GameState <> 8) then
    begin
      OutStr := IntToStr(CurrentGameData.VScore);
      //Highlight winner in yellow
      //if (CurrentGameData.GameState = 5) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      //  OutStr := '[c 17]' + OutStr;
    end
    else
      OutStr := ' ';
  end

  //Home score
  else if (SymbolName = '$Home_Score') then
  begin
    //Don't display score if game is postponed or suspended
    if (CurrentGameData.GameState <> 1) AND (CurrentGameData.GameState <> 6) AND (CurrentGameData.GameState <> 7) AND
       (CurrentGameData.GameState <> 8) then
    begin
      OutStr := IntToStr(CurrentGameData.HScore);
      //Highlight winner in yellow
      //if (CurrentGameData.GameState = 5) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      //  OutStr := '[c 17]' + OutStr;
    end
    else
      OutStr := ' ';
  end

  //Game clock
  else if (SymbolName = '$Game_Clock') then
  begin
    OutStr := CurrentGameData.DisplayClock;
  end

  //Game phase
  else if (SymbolName = '$Game_Phase') then
  begin
    OutStr := CurrentGameData.DisplayPhase;
  end

  //Game clock + phase phase
  else if (SymbolName = '$Game_Clock_Phase') then
  begin
    //Highlight game final
    if (CurrentGameData.GameState = 5) then
      OutStr := CurrentGameData.DisplayClock + '|' + SplitCSSGamePhase(CurrentGameData.DisplayPhase)
      //OutStr := CurrentGameData.DisplayClock + '| ' + SplitCSSGamePhase('[c 17]' + CurrentGameData.DisplayPhase)
    else
      OutStr := CurrentGameData.DisplayClock + '|' + SplitCSSGamePhase(CurrentGameData.DisplayPhase)
  end

  //Game phase + clock
  else if (SymbolName = '$Game_Phase_Clock') then
  begin
    //Highlight game final
    if (CurrentGameData.GameState = 5) then
      //OutStr := SplitCSSGamePhase('[c 17]' + CurrentGameData.DisplayPhase) + ' |' + CurrentGameData.DisplayClock
      OutStr := SplitCSSGamePhase(CurrentGameData.DisplayPhase) + '|' + CurrentGameData.DisplayClock
    else
      OutStr := SplitCSSGamePhase(CurrentGameData.DisplayPhase) + '|' + CurrentGameData.DisplayClock
  end

  //Down & distance for football
  else if (SymbolName = '$Down_Distance') then
  begin
    if (CurrentGameData.GameState = 2) AND (DisplayFeatureFlags.EnableDownYardage) then
    begin
      Case CurrentGameData.Down of
        1: OutStr := '[f 6]1[f11][b -12]ST';
        2: OutStr := '[f 6]2[f11][b -12]ND';
        3: OutStr := '[f 6]3[f11][b -12]RD';
        4: OutStr := '[f 6]4[f11][b -12]TH';
      end;
      OutStr := OutStr + '[b 0][f6] & ' + IntToStr(CurrentGameData.YardsToGo);
    end
    else OutStr := '';
  end

  else if (SymbolName = '$Field_Yardage_Indicator') then
  begin
    if (GetNFLDMYardageIndidcatorValue(CurrentGameData) <> -9999) AND
       (CurrentGameData.GameState = 2) AND (DisplayFeatureFlags.EnableFieldPosition) then
    begin
      OutStr := IntToStr(GetNFLDMYardageIndidcatorValue(CurrentGameData))
    end
    else OutStr := '';
  end

  //NFLDM stats
  //QB Stats - Visitor
  else if (SymbolName = '$QB_Stats_Visitor') then
  begin
    OutStr := '[c 17]' + CurrentGameData.VMnemonic + '[c 1] ' + GetNFLDMStatsString (PASSING, CurrentGameData.Visitor_Param1);
  end
  //QB Stats - Home
  else if (SymbolName = '$QB_Stats_Home') then
  begin
    OutStr := '[c 17]' + CurrentGameData.HMnemonic + '[c 1] ' + GetNFLDMStatsString (PASSING, CurrentGameData.Home_Param1);
  end
  //RB Stats - Visitor
  else if (SymbolName = '$RB_Stats_Visitor') then
  begin
    OutStr := '[c 17]' + CurrentGameData.VMnemonic + '[c 1] ' + GetNFLDMStatsString (RUSHING, CurrentGameData.Visitor_Param1);
  end
  //RB Stats - Home
  else if (SymbolName = '$RB_Stats_Home') then
  begin
    OutStr := '[c 17]' + CurrentGameData.HMnemonic + '[c 1] ' + GetNFLDMStatsString (RUSHING, CurrentGameData.Home_Param1);
  end
  //WR Stats - Visitor
  else if (SymbolName = '$WR_Stats_Visitor') then
  begin
    OutStr := '[c 17]' + CurrentGameData.VMnemonic + '[c 1] ' + GetNFLDMStatsString (RECEIVING, CurrentGameData.Visitor_Param1);
  end
  //WR Stats - Home
  else if (SymbolName = '$WR_Stats_Home') then
  begin
    OutStr := '[c 17]' + CurrentGameData.HMnemonic + '[c 1] ' + GetNFLDMStatsString (RECEIVING, CurrentGameData.Home_Param1);
  end
  //PK Stats - Visitor
  else if (SymbolName = '$PK_Stats_Visitor') then
  begin
    OutStr := '[c 17]' + CurrentGameData.VMnemonic + '[c 1] ' + GetNFLDMStatsString (KICKING, CurrentGameData.Visitor_Param1);
  end
  //PK Stats - Home
  else if (SymbolName = '$PK_Stats_Home') then
  begin
    OutStr := '[c 17]' + CurrentGameData.HMnemonic + '[c 1] ' + GetNFLDMStatsString (KICKING, CurrentGameData.Home_Param1);
  end
  //DEF Stats - Visitor
  else if (SymbolName = '$DEF_Stats_Visitor') then
  begin
    OutStr := '[c 17]' + CurrentGameData.VMnemonic + '[c 1] ' + GetNFLDMStatsString (DEFENSIVE, CurrentGameData.Visitor_Param1);
  end
  //DEF Stats - Home
  else if (SymbolName = '$DEF_Stats_Home') then
  begin
    OutStr := '[c 17]' + CurrentGameData.HMnemonic + '[c 1] ' + GetNFLDMStatsString (DEFENSIVE, CurrentGameData.Home_Param1);
  end

  //NFL Fantasy Stats; Index/Rank is passed in using $Text_2 field
  //Passing Stats
  else if (SymbolName = '$Fantasy_Stats_Passing') then
  begin
    if (CurrentEntryIndex <> NOENTRYINDEX) then
      OutStr := GetNFLDMFantasyStatsString (PASSING, StrToIntDef(GametraxRecPtr^.UserData[1], 1));
  end
  //Rushing Stats
  else if (SymbolName = '$Fantasy_Stats_Rushing') then
  begin
    if (CurrentEntryIndex <> NOENTRYINDEX) then
      OutStr := GetNFLDMFantasyStatsString (RUSHING, StrToIntDef(GametraxRecPtr^.UserData[1], 1));
  end
  //Receiving Stats
  else if (SymbolName = '$Fantasy_Stats_Receiving') then
  begin
    if (CurrentEntryIndex <> NOENTRYINDEX) then
      OutStr := GetNFLDMFantasyStatsString (RECEIVING, StrToIntDef(GametraxRecPtr^.UserData[1], 1));
  end

  //Stats Inc stats for CFB
  //QB Stats - Visitor
  else if (SymbolName = '$QB_Stats_Visitor_CFB') then
  begin
    OutStr := '[c 17]' + CurrentGameData.VMnemonic + '[c 1] ' + GetSIStatsString (CurrentGameData.SI_GCode, PASSING, TRUE);
  end
  //QB Stats - Home
  else if (SymbolName = '$QB_Stats_Home_CFB') then
  begin
    OutStr := '[c 17]' + CurrentGameData.HMnemonic + '[c 1] ' + GetSIStatsString (CurrentGameData.SI_GCode, PASSING, FALSE);
  end
  //RB Stats - Visitor
  else if (SymbolName = '$RB_Stats_Visitor_CFB') then
  begin
    OutStr := '[c 17]' + CurrentGameData.VMnemonic + '[c 1] ' + GetSIStatsString (CurrentGameData.SI_GCode, RUSHING, TRUE);
  end
  //RB Stats - Home
  else if (SymbolName = '$RB_Stats_Home_CFB') then
  begin
    OutStr := '[c 17]' + CurrentGameData.HMnemonic + '[c 1] ' + GetSIStatsString (CurrentGameData.SI_GCode, RUSHING, FALSE);
  end
  //WR Stats - Visitor
  else if (SymbolName = '$WR_Stats_Visitor_CFB') then
  begin
    OutStr := '[c 17]' + CurrentGameData.VMnemonic + '[c 1] ' + GetSIStatsString (CurrentGameData.SI_GCode, RECEIVING, TRUE);
  end
  //WR Stats - Home
  else if (SymbolName = '$WR_Stats_Home_CFB') then
  begin
    OutStr := '[c 17]' + CurrentGameData.HMnemonic + '[c 1] ' + GetSIStatsString (CurrentGameData.SI_GCode, RECEIVING, FALSE);
  end

  //Stats Inc stats for CBB
  //Points Stats - Visitor
  else if (SymbolName = '$Points_Visitor_CBB') then
  begin
    OutStr := '[c 17]' + CurrentGameData.VMnemonic + '[c 1] ' + GetSIStatsString (CurrentGameData.SI_GCode, POINTS, TRUE);
  end
  //Points Stats - Home
  else if (SymbolName = '$Points_Home_CBB') then
  begin
    OutStr := '[c 17]' + CurrentGameData.HMnemonic + '[c 1] ' + GetSIStatsString (CurrentGameData.SI_GCode, POINTS, FALSE);
  end
  //Field Goals Stats - Visitor
  else if (SymbolName = '$Field_Goals_Visitor_CBB') then
  begin
    OutStr := '[c 17]' + CurrentGameData.VMnemonic + '[c 1] ' + GetSIStatsString (CurrentGameData.SI_GCode, FIELDGOALS, TRUE);
  end
  //Field Goals Stats - Home
  else if (SymbolName = '$Field_Goals_Home_CBB') then
  begin
    OutStr := '[c 17]' + CurrentGameData.HMnemonic + '[c 1] ' + GetSIStatsString (CurrentGameData.SI_GCode, FIELDGOALS, FALSE);
  end
  //3 Point Field Goals Stats - Visitor
  else if (SymbolName = '$3PT_Field_Goals_Visitor_CBB') then
  begin
    OutStr := '[c 17]' + CurrentGameData.VMnemonic + '[c 1] ' + GetSIStatsString (CurrentGameData.SI_GCode, THREEPTFIELDGOALS, TRUE);
  end
  //3 Point Field Goals Stats - Home
  else if (SymbolName = '$3PT_Field_Goals_Home_CBB') then
  begin
    OutStr := '[c 17]' + CurrentGameData.HMnemonic + '[c 1] ' + GetSIStatsString (CurrentGameData.SI_GCode, THREEPTFIELDGOALS, FALSE);
  end
  //Combined Field Goals Stats - Visitor
  else if (SymbolName = '$Combined_Field_Goals_Visitor_CBB') then
  begin
    OutStr := '[c 17]' + CurrentGameData.VMnemonic + '[c 1] ' + GetSIStatsString (CurrentGameData.SI_GCode, COMBINEDFIELDGOALS, TRUE);
  end
  //Combined Field Goals Stats - Home
  else if (SymbolName = '$Combined_Field_Goals_Home_CBB') then
  begin
    OutStr := '[c 17]' + CurrentGameData.HMnemonic + '[c 1] ' + GetSIStatsString (CurrentGameData.SI_GCode, COMBINEDFIELDGOALS, FALSE);
  end
  //Fouls Stats - Visitor
  else if (SymbolName = '$Fouls_Visitor_CBB') then
  begin
    OutStr := '[c 17]' + CurrentGameData.VMnemonic + '[c 1] ' + GetSIStatsString (CurrentGameData.SI_GCode, FOULS, TRUE);
  end
  //Fouls Stats - Home
  else if (SymbolName = '$Fouls_Home_CBB') then
  begin
    OutStr := '[c 17]' + CurrentGameData.HMnemonic + '[c 1] ' + GetSIStatsString (CurrentGameData.SI_GCode, FOULS, FALSE);
  end

  //Rank for college games (Visitor)
  else if (SymbolName = '$Visitor_Rank') then
  begin
    OutStr := CurrentGameData.VRank;
  end

  //Rank for college games (Home)
  else if (SymbolName = '$Home_Rank') then
  begin
    OutStr := CurrentGameData.HRank;
  end

  //Winner indicator
  else if (SymbolName = '$Winner_Indicator') then
  begin
    //Check for Final
    if (CurrentGameData.GameState = 5) AND (DisplayFeatureFlags.EnableWinnerIndicator) then
      OutStr := SHOW_WINNER_INDICATOR
    else
      OutStr := HIDE_WINNER_INDICATOR;
  end

  //CSS Game Note text
  else if (SymbolName = '$CSS_Note1') then
  begin
    OutStr := CurrentGameData.CSSNote1Text;
  end
  else if (SymbolName = '$CSS_Note2') then
  begin
    OutStr := CurrentGameData.CSSNote2Text;
  end

  //For twitter
  else if (Symbolname = '$Twitter_Handle_NFL') then
  begin
    OutStr := GetTwitterMessageForGame(CurrentGameData.NFLDM_GameID).Twitter_Handle;
  end
  else if (Symbolname = '$Twitter_Message_NFL') then
  begin
    OutStr := GetTwitterMessageForGame(CurrentGameData.NFLDM_GameID).Twitter_Message;
  end
  else if (Symbolname = '$Twitter_Handle_Message_NFL') then
  begin
    TwitterMessageData := GetTwitterMessageForGame(CurrentGameData.NFLDM_GameID);
    OutStr := '[c 17]' + TwitterMessageData.Twitter_Handle + ': [c 1]' + TwitterMessageData.Twitter_Message;
  end
  else if (Symbolname = '$Twitter_Handle_Manual') then
  begin
    OutStr := Trim(GameTraxRecPtr^.UserData[1]);
  end
  else if (Symbolname = '$Twitter_Message_Manual') then
  begin
    OutStr := Trim(GameTraxRecPtr^.UserData[2]);
  end
  else if (Symbolname = '$Twitter_Handle_Message_Manual') then
  begin
    OutStr := '[c 17]' + Trim(GameTraxRecPtr^.UserData[1]) + ': [c 1]' + Trim(GameTraxRecPtr^.UserData[2]);
  end

  //Added for 2013 new look
  else if (Symbolname = '$Game_Note_Visitor') then
  begin
    OutStr := '[c 17]' + CurrentGameData.VMnemonic + '[c 1] ' + GameTraxRecPtr^.UserData[1];
  end
  else if (Symbolname = '$Game_Note_Home') then
  begin
    OutStr := '[c 17]' + CurrentGameData.HMnemonic + '[c 1] ' + GameTraxRecPtr^.UserData[1];
  end

  //Added for 2013 new look
  else if (Symbolname = '$Game_Note_Visitor') then
  begin
    OutStr := '[c 17]' + CurrentGameData.VMnemonic + ': [c 1]' + GameTraxRecPtr^.UserData[1];
  end
  else if (Symbolname = '$Game_Note_Home') then
  begin
    OutStr := '[c 17]' + CurrentGameData.HMnemonic + ': [c 1]' + GameTraxRecPtr^.UserData[1];
  end;

  //Return
  GetValueOfSymbol := OutStr;
end;

//Function for getting the latest Twitter message posted for a specific game
function TEngineInterface.GetTwitterMessageForGame(NFLGameID: LongInt): TwitterMessageRec;
var
  OutRec: TwitterMessageRec;
begin
  OutRec.Message_Found := FALSE;
  OutRec.Twitter_Handle := '';
  OutRec.Twitter_Message := '';
  try
    with dmMain.TwitterDataQuery do
    begin
      Close;
      SQL.Clear;
      SQL.Add('sp_GetTwitterMessagesByGame ' + IntToStr(NFLGameID));
      Open;
      if (RecordCount > 0) then
      begin
        OutRec.Twitter_Handle := '@' + FieldByName('userName').AsString;
        OutRec.Twitter_Message := FieldByName('message').AsString;
        OutRec.Message_Found := TRUE;
      end;
      Close;
    end;
  except

  end;
  //Return
  GetTwitterMessageForGame := OutRec;
end;

//Function for getting a Twitter message using its ID
function TEngineInterface.GetTwitterMessageByID(TwitterMessageID: Extended): TwitterMessageRec;
var
  OutRec: TwitterMessageRec;
begin
  OutRec.Message_Found := FALSE;
  OutRec.Twitter_Handle := '';
  OutRec.Twitter_Message := '';
  try
    with dmMain.TwitterDataQuery do
    begin
      Close;
      SQL.Clear;
      SQL.Add('sp_GetTwitterMessageByID ' + FloatToStr(TwitterMessageID));
      Open;
      if (RecordCount > 0) then
      begin
        OutRec.Twitter_Handle := '@' + FieldByName('userName').AsString;
        OutRec.Twitter_Message := FieldByName('message').AsString;
        OutRec.Message_Found := TRUE;
      end;
      Close;
    end;
  except

  end;
  //Return
  GetTwitterMessageByID := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
// TICKER CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
// Procedure to trigger current graphic
////////////////////////////////////////////////////////////////////////////////
procedure TEngineInterface.TriggerGraphic (Mode: SmallInt);
var
  CmdStr: String;
begin
  try
    //Build & send command string
    CmdStr := SOT + 'TG' + ETX + IntToStr(Mode) + EOT;
    if (SocketConnected) AND (EngineParameters.Enabled = TRUE) then EnginePort.Socket.SendText(CmdStr);
    //If aborting, reset packet and disable packet timeout timer
    if (Mode = 2) AND (SocketConnected) then
    begin
      //Disable packet timeout timer
      TickerPacketTimeoutTimer.Enabled := FALSE;
    end;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to trigger ticker on the graphics engine.');
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// Procedure to start ticker, bugs and applicable extra line
// Sets all initial data and sends first data record
////////////////////////////////////////////////////////////////////////////////
procedure TEngineInterface.StartTickerData;
var
  i,j: SmallInt;
  TempStr, CmdStr: String;
  TickerRecPtr: ^TickerRec;
  BugRecPtr: ^BugRec;
  GameTraxRecPtr: ^GameTraxRec;
  FoundGame, FoundStat: Boolean;
  VisitorScore: SmallInt;
  HomeScore: SmallInt;
  VisitorColor: SmallInt;
  HomeColor: SmallInt;
  HomeRank: String;
  VisitorRank: String;
  HomeName: String;
  Visitorname: String;
  SponsorLogoToUse: String;
  NextStatStr: String;
  FieldStr: String;
  PhaseStr, TimeStr: String;
  VLeader, HLeader: Boolean;
  GameHasStarted, GameIsFinal, GameIsHalf: Boolean;
  GTIME: String;
  GPHASE: SmallInt;
  CurrentGamePhaseRec: GamePhaseRec;
  CurrentGameState: SmallInt;
  RecordIsSponsor: Boolean;
  CurrentLeague: String;
  LineupData: LineupText;
  TemplateInfo: TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CurrentGameData: GameRec;
  BugClockPhaseStr: String;
  OkToGo: Boolean;
begin
  //Init flags
  GameHasStarted := FALSE;
  GameIsFinal:= FALSE;
  GameIsHalf := FALSE;
  RecordIsSponsor := FALSE;
  LastPageWasFantasyStat := FALSE;
  try
    //////////////////////////////////////////////////////////////////////////
    // START TICKER LOGIC
    //////////////////////////////////////////////////////////////////////////
    //Only proceed if at leat one story in Ticker
    if (StudioTicker_Collection.Count > 0) then
    begin
      //Make sure it;s a valid index
      if (CurrentTickerEntryIndex < 0) then CurrentTickerEntryIndex := 0;

      //Init FoundGame flag; used to prevent sending of commands if game not found
      OKToGo := TRUE;
      FoundGame := TRUE;
      //Turn on packet
      //ApdDataPacket1.Enabled := FALSE;
      //ApdDataPacket1.Enabled := TRUE;
      //Set enable
      PacketEnable := TRUE;
      //Sleep for 250 mS to give packet time to init
      //Sleep(250);

      //Send first record
      //Set default mode - always = 2 for animated mode
      CmdStr := SOT + 'SM' + ETX + '2' + GS;

      //Send the first record for the Ticker; check to make sure it's a valid index
      if (CurrentTickerEntryIndex <= StudioTicker_Collection.Count-1) then
      begin
        j := 0;
        TickerRecPtr := StudioTicker_Collection.At(CurrentTickerEntryIndex);
//        While ((TickerRecPtr^.Enabled = FALSE) OR (TickerRecPtr^.StartEnableDateTime > CurrentTime) OR
//              (TickerRecPtr^.EndEnableDateTime <= CurrentTime)) AND (j < StudioTicker_Collection.Count) do
        While ((TickerRecPtr^.Enabled = FALSE) OR NOT((TickerRecPtr^.StartEnableDateTime <= Now) AND
              (TickerRecPtr^.EndEnableDateTime > Now))) AND (j < StudioTicker_Collection.Count) do
        begin
          Inc (j);
          Inc(CurrentTickerEntryIndex);
          if (CurrentTickerEntryIndex = StudioTicker_Collection.Count) then
            CurrentTickerEntryIndex := 0;
          TickerRecPtr := StudioTicker_Collection.At(CurrentTickerEntryIndex);
        end;

        //Set flag if no enabled records found
        if (j >= StudioTicker_Collection.Count) then OKToGo := FALSE;

        //Proceed if records found
        if (OKToGo) then
        begin
          //MAIN PROCESSING LOOP FOR TICKER FIELDS
          //Get template info and load the termporary fields collection
          try
            TemplateInfo := LoadTempTemplateFields(TickerRecPtr^.Template_ID);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to load template fields');
          end;

          //Get game data if template requires it
          if (TemplateInfo.UsesGameData) then
          begin
            try
              //Get game data if template requires it
              if (TemplateInfo.UsesGameData) then
              begin
                //Use full game data
                CurrentGameData := GetGameData(TickerRecPtr^.League, TickerRecPtr^.SI_GCode,
                  TickerRecPtr^.ST_Game_ID, TickerRecPtr^.CSS_GameID, TickerRecPtr^.NFLDM_GameID,
                  TickerRecPtr^.Primary_Data_Source, TickerRecPtr^.Backup_Data_Source);
              end;
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying to get game data for ticker');
            end;
            FoundGame := CurrentGameData.DataFound;
          end;

          //Check the game state vs. the state required by the template if not a match, increment
          //Also do check for enabled entries and check time window
          if (TemplateInfo.RequiredGameState > 0) AND (CurrentGameData.GameState <> TemplateInfo.RequiredGameState) AND
            //Special case for postponed, delayed or suspended MLB games
            (NOT((CurrentGameData.GameState > 3) AND (TemplateInfo.RequiredGameState = 3))) then
          Repeat
            Inc(CurrentTickerEntryIndex);
            if (CurrentTickerEntryIndex = StudioTicker_Collection.Count) then
            begin
              MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
              CurrentTickerEntryIndex := 0;
            end;
            TickerRecPtr := StudioTicker_Collection.At(CurrentTickerEntryIndex);
            TemplateInfo := LoadTempTemplateFields(TickerRecPtr^.Template_ID);
            if (TemplateInfo.UsesGameData) then
            begin
              try
                //Use full game data
                CurrentGameData := GetGameData(TickerRecPtr^.League, TickerRecPtr^.SI_GCode,
                  TickerRecPtr^.ST_Game_ID, TickerRecPtr^.CSS_GameID, TickerRecPtr^.NFLDM_GameID,
                  TickerRecPtr^.Primary_Data_Source, TickerRecPtr^.Backup_Data_Source);
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to get game data for ticker');
              end;
              FoundGame := CurrentGameData.DataFound;
            end
            else CurrentGameData.GameState := -1;
          Until (TemplateInfo.RequiredGameState = 0) OR (CurrentGameData.GameState = TemplateInfo.RequiredGameState) OR
                (CurrentGameData.GameState = -1) AND ((TickerRecPtr^.Enabled = TRUE) AND
                (TickerRecPtr^.StartEnableDateTime <= Now) AND (TickerRecPtr^.EndEnableDateTime > Now));

          //Set the default dwell time - minimum = 2000mS; set packet timeout interval
          if (TickerRecPtr^.DwellTime > 2000) then
          begin
            TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
            TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 5000;
          end
          else begin
            TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
            TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 20000;
          end;

          //Set the template type
          CmdStr := CmdStr + 'ST' + ETX + IntToStr(TemplateInfo.Engine_Template_ID) + GS;

          //Add the fields from the temporary fields collection
          if (Temporary_Fields_Collection.Count > 0) then
          begin
            for j := 0 to Temporary_Fields_Collection.Count-1 do
            begin
              TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
              //Filter out fields ID values of -1 => League designator, etc.
              if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
              begin
                //Add region command
                //If not a symbolic fields, send field contents directly
                if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
                begin
                  TempStr := TemplateFieldsRecPtr^.Field_Contents;
                  //Check for special case of NFL Fantasy Stats - append character to trigger animation
                  //if (TemplateFieldsRecPtr^.Engine_Field_ID = 1004) AND NOT (LastPageWasFantasyStat) AND
                  //   (TemplateInfo.Template_ID >= FANTASY_START_TEMPLATTE) AND (TemplateInfo.Template_ID <= FANTASY_END_TEMPLATTE) then
                  //begin
                  //  TempStr := '#' + TempStr;
                  //  LastPageWasFantasyStat := TRUE;
                  //end;
                  //Add prefix and suffix from collection
                  TempStr := TemplateFieldsRecPtr^.Field_Prefix + TempStr + TemplateFieldsRecPtr^.Field_Suffix;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                  CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) +
                    ETX + TempStr + GS;
                end
                //It's a symbolic name, so get the field contents
                else begin
                  TempStr := GetValueOfSymbol(TICKER, TemplateFieldsRecPtr^.Field_Contents, TemplateFieldsRecPtr^.Field_Type, CurrentTickerEntryIndex,
                    CurrentGameData, CurrentTickerDisplayMode);
                  //Add prefix and suffix from collection
                  TempStr := TemplateFieldsRecPtr^.Field_Prefix + TempStr + TemplateFieldsRecPtr^.Field_Suffix;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                  //Check to see if it's the bug clock/phase field; if so, store for insertion into line-up
                  CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) + ETX +
                    TempStr + GS;
                end;
              end;
            end;
            //Write out to the as-run log if it's a sponsor logo
            if (Trim(TickerRecPtr^.SponsorLogo_Name) <> '') then
              WriteToAsRunLog('Started display of sponsor logo: ' + CurrentSponsorLogoName);
          end;

          //Set NFL Fantasy template status
          if (TemplateInfo.Template_ID >= FANTASY_START_TEMPLATTE) AND (TemplateInfo.Template_ID <= FANTASY_END_TEMPLATTE) then
            LastPageWasFantasyStat := TRUE
          else
            LastPageWasFantasyStat := FALSE;

          //Set data row indicator in Ticker grid
          MainForm.PlaylistGrid.CurrentDataRow := CurrentTickerEntryIndex+1;
          MainForm.PlaylistGrid.SetTopLeft(1, MainForm.PlaylistGrid.CurrentDataRow);

          //Resubstitute lower-case formatting strings and line wrap command
          CmdStr := StringReplace(CmdStr, '[B', '[b', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[C', '[c', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[R', '[r', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[F', '[f', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[T', '[t', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[X', '[x', [rfReplaceAll]);
          //CmdStr := StringReplace(CmdStr, '|', '[r 0]', [rfReplaceAll]);

          //Substitute style chip codes
          CmdStr := ProcessStyleChips(CmdStr);

          //Send the command with a trigger
          CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
          if (SocketConnected) AND (EngineParameters.Enabled = TRUE) AND (FoundGame = TRUE) then
          begin
            try
              EnginePort.Socket.SendText(CmdStr)
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying send ticker command to engine');
            end;
            //Enable packet timeout timer
            TickerPacketTimeoutTimer.Enabled := TRUE;
            //Start timer to send next record
            if (USEDATAPACKET = FALSE) then TickerCommandDelayTimer.Enabled := TRUE;
          end
          else if (FoundGame = FALSE) then
          begin
            //Enable delay timer
            if (RunningTicker = TRUE) then JumpToNextTickerRecordTimer.Enabled := TRUE;
          end;
        end;
      end;
    end;
    //////////////////////////////////////////////////////////////////////////
    // END TICKER LOGIC
    //////////////////////////////////////////////////////////////////////////

    //Delay to stagger displays
    Sleep(2000);

    //////////////////////////////////////////////////////////////////////////
    // START BUG LOGIC
    //////////////////////////////////////////////////////////////////////////
    //Only proceed if at leat one story in BUG
    if (StudioBug_Collection.Count > 0) then
    begin
      //Make sure it;s a valid index
      if (CurrentBugEntryIndex < 0) then CurrentBugEntryIndex := 0;
      //Init
      BugClockPhaseStr := ' ';
      OKToGo := TRUE;
      //Init FoundGame flag; used to prevent sending of commands if game not found
      FoundGame := TRUE;
      //Turn on packet
      //ApdDataPacket1.Enabled := FALSE;
      //ApdDataPacket1.Enabled := TRUE;
      //Set enable
      PacketEnable := TRUE;
      //Sleep for 250 mS to give packet time to init
      //Sleep(250);

      //Send first record
      //Set default mode - always = 2 for animated mode
      CmdStr := SOT + 'SM' + ETX + '2' + GS;

      //Send the first record for the Bug; check to make sure it's a valid index
      if (CurrentBugEntryIndex <= StudioBug_Collection.Count-1) then
      begin
        j := 0;
        BugRecPtr := StudioBug_Collection.At(CurrentBugEntryIndex);
        While ((BugRecPtr^.Enabled = FALSE) OR NOT((BugRecPtr^.StartEnableDateTime <= Now) AND
              (BugRecPtr^.EndEnableDateTime > Now))) AND (j < StudioBug_Collection.Count) do
        begin
          Inc (j);
          Inc(CurrentBugEntryIndex);
          if (CurrentBugEntryIndex = StudioBug_Collection.Count) then
            CurrentBugEntryIndex := 0;
          BugRecPtr := StudioBug_Collection.At(CurrentBugEntryIndex);
        end;
        //Set flag if no enabled records found
        if (j >= StudioBug_Collection.Count) then OKToGo := FALSE;

        //Proceed if records found
        if (OKToGo) then
        begin
          //MAIN PROCESSING LOOP FOR BUG FIELDS
          //Get template info and load the termporary fields collection
          //Add bias to template ID if in Live Event mode
          if (CurrentTickerDisplayMode = 1) then
            TemplateInfo := LoadTempTemplateFields(BugRecPtr^.Template_ID+50)
          else
            TemplateInfo := LoadTempTemplateFields(BugRecPtr^.Template_ID);

          //Get game data if required
          if (TemplateInfo.UsesGameData) then
          begin
            try
              //Get game data if template requires it
              if (TemplateInfo.UsesGameData) then
              begin
                //Use full game data
                CurrentGameData := EngineInterface.GetGameData(BugRecPtr^.League, BugRecPtr^.SI_GCode,
                  BugRecPtr^.ST_Game_ID, BugRecPtr^.CSS_GameID, BugRecPtr^.NFLDM_GameID,
                  BugRecPtr^.Primary_Data_Source, BugRecPtr^.Backup_Data_Source);
              end;
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying to get game data for ticker');
            end;
            FoundGame := CurrentGameData.DataFound;
          end;

          //Check the game state vs. the state required by the template if not a match, increment
          //Also do check for enabled entries and check time window
          if (TemplateInfo.RequiredGameState > 0) AND (CurrentGameData.GameState <> TemplateInfo.RequiredGameState) AND
            //Special case for postponed, delayed or suspended MLB games
            (NOT((CurrentGameData.GameState > 3) AND (TemplateInfo.RequiredGameState = 3))) then
          Repeat
            Inc(CurrentBugEntryIndex);
            if (CurrentBugEntryIndex = StudioBug_Collection.Count) then
            begin
              MainForm.LoadPlaylistCollection(Bug, 0, CurrentBugPlaylistID);
              CurrentBugEntryIndex := 0;
            end;
            BugRecPtr := StudioBug_Collection.At(CurrentBugEntryIndex);
            //Add bias to template ID if in Live Event mode
            if (CurrentTickerDisplayMode = 1) then
              TemplateInfo := LoadTempTemplateFields(BugRecPtr^.Template_ID+50)
            else
              TemplateInfo := LoadTempTemplateFields(BugRecPtr^.Template_ID);
            if (TemplateInfo.UsesGameData) then
            begin
              try
                //Use full game data
                CurrentGameData := EngineInterface.GetGameData(BugRecPtr^.League, BugRecPtr^.SI_GCode,
                  BugRecPtr^.ST_Game_ID, BugRecPtr^.CSS_GameID, BugRecPtr^.NFLDM_GameID,
                  BugRecPtr^.Primary_Data_Source, BugRecPtr^.Backup_Data_Source);
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to get game data for bug');
              end;
              FoundGame := CurrentGameData.DataFound;
            end
            else CurrentGameData.GameState := -1;
          Until (TemplateInfo.RequiredGameState = 0) OR (CurrentGameData.GameState = TemplateInfo.RequiredGameState) OR
                (CurrentGameData.GameState = -1) AND ((BugRecPtr^.Enabled = TRUE) AND
                (BugRecPtr^.StartEnableDateTime <= Now) AND (BugRecPtr^.EndEnableDateTime > Now));

          //Set the default dwell time - minimum = 2000mS; set packet timeout interval
          if (BugRecPtr^.DwellTime > 2000) then
          begin
            BugCommandDelayTimer.Interval := BugRecPtr^.DwellTime;
            BugPacketTimeoutTimer.Interval := BugRecPtr^.DwellTime + 5117;
          end
          else begin
            BugCommandDelayTimer.Interval := 2000;
            BugPacketTimeoutTimer.Interval := BugRecPtr^.DwellTime + 7117;
          end;

          //Set the template type
          CmdStr := CmdStr + 'ST' + ETX + IntToStr(TemplateInfo.Engine_Template_ID) + GS;
          //Add the fields from the temporary fields collection
          if (Temporary_Fields_Collection.Count > 0) then
          begin
            for j := 0 to Temporary_Fields_Collection.Count-1 do
            begin
              TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
              //Filter out fields ID values of -1 => League designator
              if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
              begin
                //Add region command
                //If not a symbolic fields, send field contents directly
                if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
                begin
                  TempStr := TemplateFieldsRecPtr^.Field_Contents;
                  //Add prefix and suffix from collection
                  TempStr := TemplateFieldsRecPtr^.Field_Prefix + TempStr + TemplateFieldsRecPtr^.Field_Suffix;
                  //if (Trim(TempStr) = '') then TempStr := ' ';
                  CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) +
                    ETX + TempStr + GS;
                end
                //It's a symbolic name, so get the field contents
                else begin
                  try
                    TempStr := GetValueOfSymbol(BUG, TemplateFieldsRecPtr^.Field_Contents, TemplateFieldsRecPtr^.Field_Type, CurrentBugEntryIndex,
                      CurrentGameData, CurrentTickerDisplayMode);
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to get value for bug data field');
                  end;
                  //Add prefix and suffix from collection
                  TempStr := TemplateFieldsRecPtr^.Field_Prefix + TempStr + TemplateFieldsRecPtr^.Field_Suffix;
                  //if (Trim(TempStr) = '') then TempStr := ' ';
                  //Check to see if it's the bug clock/phase field; if so, store for insertion into line-up
                  if (TemplateFieldsRecPtr^.Engine_Field_ID = 2000) then
                    BugClockPhaseStr := TempStr
                  else
                    CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) + ETX + TempStr + GS;
                end;
              end;
            end;
          end;

          //Set data row indicator in Ticker grid
          MainForm.PlaylistGrid.CurrentDataRow := CurrentBugEntryIndex+1;
          MainForm.PlaylistGrid.SetTopLeft(1, MainForm.PlaylistGrid.CurrentDataRow);

          //Set lineup text; bug clock phase string is set above in fields loop
          if (CurrentTickerDisplayMode = 1) then BugClockPhaseStr := ' ';
          if (Trim(BugRecPtr^.League) = 'NONE') then
            CmdStr := CmdStr + 'SI' + ETX + ' ' + ETX + BugClockPhaseStr +
                      ETX + ' ' + ETX + ' ' + ETX + ' ' + GS
          else
            CmdStr := CmdStr + 'SI' + ETX + BugRecPtr^.League + ETX + BugClockPhaseStr +
                      ETX + ' ' + ETX + ' ' + ETX + ' ' + GS;

          //Resubstitute lower-case formatting strings and line wrap command
          CmdStr := StringReplace(CmdStr, '[B', '[b', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[C', '[c', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[R', '[r', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[F', '[f', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[T', '[t', [rfReplaceAll]);
          //CmdStr := StringReplace(CmdStr, '|', '[r 0]', [rfReplaceAll]);

          //Substitute style chip codes
          CmdStr := ProcessStyleChips(CmdStr);

          //Send the command with a trigger
          CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
          if (SocketConnected) AND (EngineParameters.Enabled = TRUE) AND (FoundGame = TRUE) then
          begin
            try
              EnginePort.Socket.SendText(CmdStr)
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying send bug command to engine');
            end;
            //Enable packet timeout timer
            BugPacketTimeoutTimer.Enabled := TRUE;
            //Start timer to send next record
            if (USEDATAPACKET = FALSE) then BugCommandDelayTimer.Enabled := TRUE;
          end
          else if (FoundGame = FALSE) then
          begin
            //Enable delay timer
            if (RunningTicker = TRUE) then JumpToNextBugRecordTimer.Enabled := TRUE;
          end;
        end;
      end;
    end;
    //////////////////////////////////////////////////////////////////////////
    // END BUG LOGIC
    //////////////////////////////////////////////////////////////////////////

    //Delay to stagger displays
    Sleep(2000);

    //////////////////////////////////////////////////////////////////////////
    // START GAMETRAX LOGIC
    //////////////////////////////////////////////////////////////////////////
    //Only proceed if at leat one story in ExtraLine
    if (GameTrax_Collection.Count > 0) then
    begin
      //Make sure it;s a valid index
      if (CurrentExtraLineEntryIndex < 0) then CurrentExtraLineEntryIndex := 0;

      OKToGo := TRUE;
      //Init FoundGame flag; used to prevent sending of commands if game not found
      FoundGame := TRUE;
      //Set enable
      PacketEnable := TRUE;
      //Sleep for 250 mS to give packet time to init
      //Sleep(250);

      //Send first record
      //Set default mode - always = 2 for animated mode
      CmdStr := SOT + 'SM' + ETX + '2' + GS;

      //Send the first record for the ExtraLine; check to make sure it's a valid index
      if (CurrentExtraLineEntryIndex <= GameTrax_Collection.Count-1) then
      begin
        j := 0;
        GameTraxRecPtr := GameTrax_Collection.At(CurrentExtraLineEntryIndex);
//        While ((GameTraxRecPtr^.Enabled = FALSE) OR (GameTraxRecPtr^.StartEnableDateTime > Now) OR
//              (GameTraxRecPtr^.EndEnableDateTime <= Now)) AND (j < GameTrax_Collection.Count) do
        While ((GameTraxRecPtr^.Enabled = FALSE) OR NOT((GameTraxRecPtr^.StartEnableDateTime <= Now) AND
              (GameTraxRecPtr^.EndEnableDateTime > Now))) AND (j < GameTrax_Collection.Count) do
        begin
          Inc (j);
          Inc(CurrentExtraLineEntryIndex);
          if (CurrentExtraLineEntryIndex = GameTrax_Collection.Count) then
            CurrentExtraLineEntryIndex := 0;
          GameTraxRecPtr := GameTrax_Collection.At(CurrentExtraLineEntryIndex);
        end;

        //Set flag if no enabled records found
        if (j >= GameTrax_Collection.Count) then OKToGo := FALSE;

        //Proceed if records found
        if (OKToGo) then
        begin
          //MAIN PROCESSING LOOP FOR ExtraLine FIELDS
          //Get template info and load the termporary fields collection
          try
            TemplateInfo := LoadTempTemplateFields(GameTraxRecPtr^.Template_ID);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to load template fields');
          end;

          //Get game data if template requires it
          if (TemplateInfo.UsesGameData) then
          begin
            try
              //Get game data if template requires it
              if (TemplateInfo.UsesGameData) then
              begin
                //Use full game data
                CurrentGameData := EngineInterface.GetGameData(GameTraxRecPtr^.League, GameTraxRecPtr^.SI_GCode,
                  GameTraxRecPtr^.ST_Game_ID, GameTraxRecPtr^.CSS_GameID, GameTraxRecPtr^.NFLDM_GameID,
                  GameTraxRecPtr^.Primary_Data_Source, GameTraxRecPtr^.Backup_Data_Source);
              end;
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying to get game data for ExtraLine');
            end;
            FoundGame := CurrentGameData.DataFound;
          end;

          //Check the game state vs. the state required by the template if not a match, increment
          //Also do check for enabled entries and check time window
          if (TemplateInfo.RequiredGameState > 0) AND (CurrentGameData.GameState <> TemplateInfo.RequiredGameState) AND
            //Special case for postponed, delayed or suspended MLB games
            (NOT((CurrentGameData.GameState > 3) AND (TemplateInfo.RequiredGameState = 3))) then
          Repeat
            Inc(CurrentExtraLineEntryIndex);
            if (CurrentExtraLineEntryIndex = GameTrax_Collection.Count) then
            begin
              MainForm.LoadPlaylistCollection(GAMETRAX, 0, CurrentExtraLinePlaylistID);
              CurrentExtraLineEntryIndex := 0;
            end;
            GameTraxRecPtr := GameTrax_Collection.At(CurrentExtraLineEntryIndex);
            TemplateInfo := LoadTempTemplateFields(GameTraxRecPtr^.Template_ID);
            if (TemplateInfo.UsesGameData) then
            begin
              try
                //Use full game data
                CurrentGameData := EngineInterface.GetGameData(GameTraxRecPtr^.League, GameTraxRecPtr^.SI_GCode,
                  GameTraxRecPtr^.ST_Game_ID, GameTraxRecPtr^.CSS_GameID, GameTraxRecPtr^.NFLDM_GameID,
                  GameTraxRecPtr^.Primary_Data_Source, GameTraxRecPtr^.Backup_Data_Source);
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to get game data for ExtraLine');
              end;
              FoundGame := CurrentGameData.DataFound;
            end
            else CurrentGameData.GameState := -1;
          Until (TemplateInfo.RequiredGameState = 0) OR (CurrentGameData.GameState = TemplateInfo.RequiredGameState) OR
                (CurrentGameData.GameState = -1) AND ((GameTraxRecPtr^.Enabled = TRUE) AND
                (GameTraxRecPtr^.StartEnableDateTime <= Now) AND (GameTraxRecPtr^.EndEnableDateTime > Now));

          //Set the default dwell - minimum = 2000mS
          if (GameTraxRecPtr^.DwellTime > 2000) then
          begin
            ExtraLineCommandDelayTimer.Interval := GameTraxRecPtr^.DwellTime + 212;
            ExtraLinePacketTimeoutTimer.Interval := GameTraxRecPtr^.DwellTime + 5212;
          end
          else begin
            ExtraLineCommandDelayTimer.Interval := GameTraxRecPtr^.DwellTime;
            ExtraLinePacketTimeoutTimer.Interval := GameTraxRecPtr^.DwellTime + 30212;
          end;

          //Set the template type
          CmdStr := CmdStr + 'ST' + ETX + IntToStr(TemplateInfo.Engine_Template_ID) + GS;
          //Add the fields from the temporary fields collection
          if (Temporary_Fields_Collection.Count > 0) then
          begin
            for j := 0 to Temporary_Fields_Collection.Count-1 do
            begin
              TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
              //Filter out fields ID values of -1 => League designator
              if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
              begin
                //Add region command
                //If not a symbolic fields, send field contents directly
                if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
                begin
                  TempStr := TemplateFieldsRecPtr^.Field_Contents;
                  //Append # character if first Fantasy stat to trigger intro animation
                  //if (TemplateFieldsRecPtr^.Engine_Field_ID = 1004) AND NOT (LastPageWasFantasyStat) AND
                  //   (TemplateInfo.Template_ID >= FANTASY_START_TEMPLATTE) AND (TemplateInfo.Template_ID <= FANTASY_END_TEMPLATTE) then
                  //begin
                  //  TempStr := '#' + TempStr;
                  //  LastPageWasFantasyStat := TRUE;
                  //end;
                  //Add prefix and suffix from collection
                  TempStr := TemplateFieldsRecPtr^.Field_Prefix + TempStr + TemplateFieldsRecPtr^.Field_Suffix;
                  //if (Trim(TempStr) = '') then TempStr := ' ';
                  CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) +
                    ETX + TempStr + GS;
                end
                //It's a symbolic name, so get the field contents
                else begin
                  try
                    TempStr := GetValueOfSymbol(GAMETRAX, TemplateFieldsRecPtr^.Field_Contents, TemplateFieldsRecPtr^.Field_Type, CurrentExtraLineEntryIndex,
                      CurrentGameData, CurrentTickerDisplayMode);
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to get value for main ExtraLine data field');
                  end;
                  //Add prefix and suffix from collection
                  TempStr := TemplateFieldsRecPtr^.Field_Prefix + TempStr + TemplateFieldsRecPtr^.Field_Suffix;
                  //if (Trim(TempStr) = '') then TempStr := ' ';
                  CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) + ETX + TempStr + GS;
                end;
              end;
            end;
          end;

          //Set NFL Fantasy template status
          if (TemplateInfo.Template_ID >= FANTASY_START_TEMPLATTE) AND (TemplateInfo.Template_ID <= FANTASY_END_TEMPLATTE) then
            LastPageWasFantasyStat := TRUE
          else
            LastPageWasFantasyStat := FALSE;

          //Set data row indicator in ExtraLine grid
          MainForm.PlaylistGrid.CurrentDataRow := CurrentExtraLineEntryIndex+1;
          MainForm.PlaylistGrid.SetTopLeft(1, MainForm.PlaylistGrid.CurrentDataRow);

          //Add sponsor logo name if CFB or CBK
          if (GameTraxRecPtr^.League = 'CFB') OR (GameTraxRecPtr^.League = 'CBB') then
          begin
            if (Trim(CurrentSponsorLogoName) <> '') then
              CmdStr := CmdStr + 'SD' + ETX + IntToStr(1006) + ETX + CurrentSponsorLogoName + GS
            else
              CmdStr := CmdStr + 'SD' + ETX + IntToStr(1006) + ETX + '*' + GS;
          end;

          //Resubstitute lower-case formatting strings and line wrap command
          CmdStr := StringReplace(CmdStr, '[B', '[b', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[C', '[c', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[R', '[r', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[F', '[f', [rfReplaceAll]);
          CmdStr := StringReplace(CmdStr, '[T', '[t', [rfReplaceAll]);
          //CmdStr := StringReplace(CmdStr, '|', '[r 0]', [rfReplaceAll]);

          //Substitute style chip codes
          CmdStr := ProcessStyleChips(CmdStr);

          //Send the command with a trigger
          CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
          if (SocketConnected) AND (EngineParameters.Enabled = TRUE) AND (FoundGame = TRUE) then
          begin
            try
              EnginePort.Socket.SendText(CmdStr);
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying send ExtraLine command to engine');
            end;
            //Enable packet timeout timer
            ExtraLinePacketTimeoutTimer.Enabled := TRUE;
            //Start timer to send next record
            if (USEDATAPACKET = FALSE) then ExtraLineCommandDelayTimer.Enabled := TRUE;
          end
          else if (FoundGame = FALSE) then
          begin
            //Enable delay timer
            if (RunningTicker = TRUE) then JumpToNextGameTraxRecordTimer.Enabled := TRUE;
          end;
        end;
      end;
    end;
    //////////////////////////////////////////////////////////////////////////
    // END EXTRA LINE LOGIC
    //////////////////////////////////////////////////////////////////////////

  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to send initial text and start ticker');
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// FUNCTIONS FOR ADVANCING RECORDS
////////////////////////////////////////////////////////////////////////////////
// Handler for packet requesting more data for ticker
////////////////////////////////////////////////////////////////////////////////
procedure TEngineInterface.EnginePortRead(Sender: TObject; Socket: TCustomWinSocket);
var
  Data: String;
  i: SmallInt;
  CurrentTemplateIDNum: SmallInt;
  CurrentTemplateIDNumStr: String;
  RequiredDelay: SmallInt;
  Index, Duration: SmallInt;
  DurationStr: String;
begin
  Data := Socket.ReceiveText;
  if (USEDATAPACKET) then
  begin
    CurrentTemplateIDNumStr := '';
    CurrentTemplateIDNum := 0;
    //Disable timeout timer
    //Enable delay timer for next command if not previously in single command mode
    //Set current template ID
    if (Pos('FAIL', ANSIUpperCase(Data)) = 0) AND (Pos('Refused', ANSIUpperCase(Data)) = 0) then
    begin
      if (RunningTicker = TRUE) AND (DisableCommandTimer = FALSE) then
      begin
        //Case CurrentTemplateIDNum of
        //1..29, 80..250: begin
        //         if (USEDATAPACKET) then TickerCommandDelayTimer.Enabled := TRUE;
        //         TickerPacketTimeoutTimer.Enabled := FALSE;
        //       end;
        //30..39: begin
        //         if (USEDATAPACKET) then BugCommandDelayTimer.Enabled := TRUE;
        //         BugPacketTimeoutTimer.Enabled := FALSE;
        //       end;
        //50..59: begin
        //         if (USEDATAPACKET) then ExtraLineCommandDelayTimer.Enabled := TRUE;
        //         ExtraLinePacketTimeoutTimer.Enabled := FALSE;
        //       end;
        //end;
        //Parse out duration
        Index := Pos(':', ANSIUpperCase(Data));
        if (Index > 0) AND (Index < Length(Data)) then
        begin
          //Get duration string
          DurationStr := Copy(ANSIUpperCase(Data), Index+1,Length(Data)-Index);
          //Convert to integer, default to 5 seconds
          Duration := StrToIntDef(DurationStr, 5000);
        end;
        //Set up command timer
        if (USEDATAPACKET) then
        begin
          //Set timer - convert frames to mS; add to default dwell time for specified template
          ExtraLineCommandDelayTimer.Interval := ExtraLineCommandDelayTimer.Interval + TRUNC((Duration/30)*1000);
          MainForm.ApdStatusLight3.Lit := FALSE;
          ExtraLineCommandDelayTimer.Enabled := TRUE;
        end;
        ExtraLinePacketTimeoutTimer.Enabled := FALSE;
      end;
    end
    else if (Pos('FAIL', ANSIUpperCase(Data)) <> 0) then
    begin
      WriteToErrorLog('Command status = FAILED; Return String: ' + Data);
    end
    else if (Pos('Refused', ANSIUpperCase(Data)) <> 0) then
    begin
      WriteToErrorLog('Command status = CONNECTION REFUSED; Return String: ' + Data);
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// DISPLAY DWELL TIMERS
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// TICKER
////////////////////////////////////////////////////////////////////////////////
//Handler for timer to send next command
procedure TEngineInterface.TickerCommandDelayTimerTimer(Sender: TObject);
begin
  if (StudioTicker_Collection.Count > 0) then
  begin
    //Prevent retrigger
    TickerCommandDelayTimer.Enabled := FALSE;
    //Send next record
    SendTickerRecord(TRUE, 0);
  end;
end;
//Handler for 1 mS timer used to send next command when jumping over stats headers
procedure TEngineInterface.JumpToNextTickerRecordTimerTimer(Sender: TObject);
begin
  if (StudioTicker_Collection.Count > 0) then
  begin
    //Prevent retrigger
    JumpToNextTickerRecordTimer.Enabled := FALSE;
    //Send next record
    SendTickerRecord(TRUE, 0);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// BUGS
////////////////////////////////////////////////////////////////////////////////
procedure TEngineInterface.BugCommandDelayTimerTimer(Sender: TObject);
begin
  if (StudioBug_Collection.Count > 0) then
  begin
    //Prevent retrigger
    BugCommandDelayTimer.Enabled := FALSE;
    //Send next record
    SendBugRecord(TRUE, 0);
  end;
end;

procedure TEngineInterface.JumpToNextBugRecordTimerTimer(Sender: TObject);
begin
  if (StudioBug_Collection.Count > 0) then
  begin
    //Prevent retrigger
    JumpToNextBugRecordTimer.Enabled := FALSE;
    //Send next record
    SendBugRecord(TRUE, 0);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// GAMETRAX
////////////////////////////////////////////////////////////////////////////////
procedure TEngineInterface.ExtraLineCommandDelayTimerTimer(Sender: TObject);
begin
  //Prevent retrigger
  if (GameTrax_Collection.Count > 0) then
  begin
    ExtraLineCommandDelayTimer.Enabled := FALSE;
    //Send next record
    SendGameTraxRecord(TRUE, 0);
    MainForm.ApdStatusLight3.Lit := TRUE;
  end;
end;

procedure TEngineInterface.JumpToNextGameTraxRecordTimerTimer(Sender: TObject);
begin
  if (GameTrax_Collection.Count > 0) then
  begin
    //Prevent retrigger
    JumpToNextGameTraxRecordTimer.Enabled := FALSE;
    //Send next record
    SendGameTraxRecord(TRUE, 0);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TICKER
////////////////////////////////////////////////////////////////////////////////
// Procedure to send next record after receipt of packet
////////////////////////////////////////////////////////////////////////////////
procedure TEngineInterface.SendTickerRecord(NextRecord: Boolean; RecordIndex: SmallInt);
var
  i,j: SmallInt;
  TempStr, CmdStr: String;
  TickerRecPtr, TickerRecPtrNew: ^TickerRec;
  FoundGame: Boolean;
  VisitorScore: SmallInt;
  HomeScore: SmallInt;
  VisitorColor: SmallInt;
  HomeColor: SmallInt;
  HomeRank: String;
  VisitorRank: String;
  HomeName: String;
  Visitorname: String;
  IsLast: Boolean;
  OverlayIndex, OverlayIndexNext: SmallInt;
  IsNewOverlay, PadText: Boolean;
  InGameStatsRequired: Boolean;
  FoundGameInDatamart: Boolean;
  PhaseStr, TimeStr: String;
  VLeader, HLeader: Boolean;
  GameHasStarted, GameIsFinal, GameIsHalf: Boolean;
  GTIME: String;
  GPHASE: SmallInt;
  CurrentGamePhaseRec: GamePhaseRec;
  CurrentGameState: SmallInt;
  RecordIsSponsor: Boolean;
  CurrentLeague: String;
  LineupData: LineupText;
  TemplateInfo: TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CurrentGameData: GameRec;
  OkToGo: Boolean;
begin
  //Init flags
  GameHasStarted := FALSE;
  GameIsFinal:= FALSE;
  GameIsHalf := FALSE;
  //Init FoundGame flag - used to prevent inadverant commands from being sent if game not found
  FoundGame := TRUE;
  RecordIsSponsor := FALSE;

  //Turn on indicator
  PacketIndicatorTimer.Enabled := TRUE;
  MainForm.ApdStatusLight2.Lit := TRUE;


  //Disable packets and enable timer to re-enable packets
  //PacketEnable := FALSE;
  PacketEnableTimer.Enabled := TRUE;
  try
    //Init
    IsLast := FALSE;

    //Make sure we haven't dumped put
    if ((TickerAbortFlag = FALSE) AND (StudioTicker_Collection.Count > 0)) then
    begin
      OKToGo := TRUE;
      //Check if flag set to just send next record
      if (NEXTRECORD = TRUE) then
      begin
        //Make sure there's at least one record left before sending more data;
        //If not, reset to top if in looping mode
        if (CurrentTickerEntryIndex >= StudioTicker_Collection.Count-1) AND
           (LoopTicker = TRUE) then
        begin
          try
            //Reload the Ticker collection in case it has been modified; clear flag to indicate
            //it's not the first time through
            MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
            if (StudioTicker_Collection.Count > 0) then CurrentTickerEntryIndex := 0;
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to reload main ticker collection');
          end;
          //Check time window and enable
          TickerRecPtr := StudioTicker_Collection.At(CurrentTickerEntryIndex);
          //Make sure record is enabled and within time window
          j := 0;
          While ((TickerRecPtr^.Enabled = FALSE) OR NOT ((TickerRecPtr^.StartEnableDateTime <= Now) AND
                (TickerRecPtr^.EndEnableDateTime > Now))) AND (j < StudioTicker_Collection.Count) do
          begin
            Inc(CurrentTickerEntryIndex);
            if (CurrentTickerEntryIndex = StudioTicker_Collection.Count) then
            begin
              try
                MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
                CurrentTickerEntryIndex := 0;
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to reload main ticker collection');
              end;
            end;
            Inc(j);
          end;
          //Set flag if no enabled records found
          if (j >= StudioTicker_Collection.Count) then OKToGo := FALSE;
        end
        //Not last record, so increment Ticker playout collection object index
        else if (CurrentTickerEntryIndex < StudioTicker_Collection.Count-1) then
        begin
          //Make sure record is enabled and within time window
          j := 0;
          Repeat
            Inc(CurrentTickerEntryIndex);
            if (CurrentTickerEntryIndex = StudioTicker_Collection.Count) then
            begin
              try
                MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
                CurrentTickerEntryIndex := 0;
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to reload main ticker collection');
              end;
            end;
            TickerRecPtr := StudioTicker_Collection.At(CurrentTickerEntryIndex);
            Inc (j);
          Until ((TickerRecPtr^.Enabled = TRUE) AND (TickerRecPtr^.StartEnableDateTime <= Now) AND
                (TickerRecPtr^.EndEnableDateTime > Now)) OR (j >= StudioTicker_Collection.Count);
          //Set flag if no enabled records found
          if (j >= StudioTicker_Collection.Count) then OKToGo := FALSE;
        end
        //At end and not looping, so clear screen
        else if (CurrentTickerEntryIndex >= StudioTicker_Collection.Count-1) AND
                (LoopTicker = FALSE) then
        begin
          //Increment to end
          Inc(CurrentTickerEntryIndex);
        end;
        //Set flag if at last collection obejct and not looping
        if (CurrentTickerEntryIndex = StudioTicker_Collection.Count-1) AND
           (LoopTicker = FALSE) then IsLast := TRUE;
        //Enable command timer
        DisableCommandTimer := FALSE;
      end
      //Triggering specific record, so set current entry index
      else begin
        CurrentTickerEntryIndex := RecordIndex;
        //Disble command timer
        DisableCommandTimer := TRUE;
      end;

      //Proceed if not at end or in looping mode and at beggining
      if (CurrentTickerEntryIndex <= StudioTicker_Collection.Count-1) AND (OKToGo) then
      begin
        //Get pointer to current record
        TickerRecPtr := StudioTicker_Collection.At(CurrentTickerEntryIndex);

        //Set default mode
        CmdStr := SOT + 'SM' + ETX + '2' + GS;

        ////////////////////////////////////////////////////////////////////////
        //MAIN PROCESSING LOOP FOR TICKER FIELDS
        ////////////////////////////////////////////////////////////////////////
        //Get template info and load the termporary fields collection
        TemplateInfo := LoadTempTemplateFields(TickerRecPtr^.Template_ID);

        //Get game data if template requires it
        if (TemplateInfo.UsesGameData) then
        begin
          try
            //Use full game data
            CurrentGameData := EngineInterface.GetGameData(TickerRecPtr^.League, TickerRecPtr^.SI_GCode,
              TickerRecPtr^.ST_Game_ID, TickerRecPtr^.CSS_GameID, TickerRecPtr^.NFLDM_GameID,
              TickerRecPtr^.Primary_Data_Source, TickerRecPtr^.Backup_Data_Source);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to get game data for ticker');
          end;
          FoundGame := CurrentGameData.DataFound;
        end;

        //Check the game state vs. the state required by the template if not a match, increment
        //Also do check for enabled entries and check time window
        if (TemplateInfo.RequiredGameState > 0) AND (CurrentGameData.GameState <> TemplateInfo.RequiredGameState) AND
           //Special case for postponed, delayed or suspended MLB games
           (NOT((CurrentGameData.GameState > 3) AND (TemplateInfo.RequiredGameState = 3))) AND
           (NEXTRECORD = TRUE) then
        Repeat
          Inc(CurrentTickerEntryIndex);
          if (CurrentTickerEntryIndex = StudioTicker_Collection.Count) then
          begin
            MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
            CurrentTickerEntryIndex := 0;
          end;
          TickerRecPtr := StudioTicker_Collection.At(CurrentTickerEntryIndex);
          TemplateInfo := LoadTempTemplateFields(TickerRecPtr^.Template_ID);
          if (TemplateInfo.UsesGameData) then
          begin
            try
              //Use full game data
              CurrentGameData := EngineInterface.GetGameData(TickerRecPtr^.League, TickerRecPtr^.SI_GCode,
                TickerRecPtr^.ST_Game_ID, TickerRecPtr^.CSS_GameID, TickerRecPtr^.NFLDM_GameID,
                TickerRecPtr^.Primary_Data_Source, TickerRecPtr^.Backup_Data_Source);
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying to get game data for ticker');
            end;
            FoundGame := CurrentGameData.DataFound;
          end
          else CurrentGameData.GameState := -1;
        Until (TemplateInfo.RequiredGameState = 0) OR (CurrentGameData.GameState = TemplateInfo.RequiredGameState) OR
              (CurrentGameData.GameState = -1) AND ((TickerRecPtr^.Enabled = TRUE) AND
              (TickerRecPtr^.StartEnableDateTime <= Now) AND (TickerRecPtr^.EndEnableDateTime > Now));

        //Set the default dwell time - minimum = 2000mS; set packet timeout interval
        if (TickerRecPtr^.DwellTime > 2000) then
        begin
          //If sponsor logo dwell is specified, it's a sponsor logo, so use that for the dwell time
          if (TickerRecPtr^.SponsorLogo_Dwell > 0) then
          begin
            TickerCommandDelayTimer.Interval := (TickerRecPtr^.SponsorLogo_Dwell*1000);
            TickerPacketTimeoutTimer.Interval := (TickerRecPtr^.SponsorLogo_Dwell*1000) + 5000;
          end
          else begin
            TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
            TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 30000;
          end;
        end
        else begin
          TickerCommandDelayTimer.Interval := 2000;
          TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 7000;
        end;

        //Set the template type
        CmdStr := CmdStr + 'ST' + ETX + IntToStr(TemplateInfo.Engine_Template_ID) + GS;

        //Check to see if active sponsor logo needs to be cleared
        if (CheckForActiveGametraxSponsorLogo = FALSE) then CurrentSponsorLogoName := '';

        //Only add sponsor logo if CFB or CBB
        if (TickerRecPtr^.League = 'CFB') OR (TickerRecPtr^.League = 'CBB') then
        begin
          //Add the sponsor logo region if specified and if not a full sponsor page (TID = 1)
          if (TRIM(CurrentSponsorLogoName) <> '') AND (TemplateInfo.Template_ID <> 1000) then
            CmdStr := CmdStr + 'SD' + ETX + '1006' + ETX + CurrentSponsorLogoName + GS
          else
            CmdStr := CmdStr + 'SD' + ETX + '1006' + ETX + '*' + GS;
        end;

        //Add the fields from the temporary fields collection
        if (Temporary_Fields_Collection.Count > 0) then
        begin
          for j := 0 to Temporary_Fields_Collection.Count-1 do
          begin
            TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
            //Filter out fields ID values of -1 => League designator
            if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
            begin
              //Add region command
              //If not a symbolic fields, send field contents directly
              if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
              begin
                TempStr := TemplateFieldsRecPtr^.Field_Contents;
                //Add prefix and suffix from collection
                TempStr := TemplateFieldsRecPtr^.Field_Prefix + TempStr + TemplateFieldsRecPtr^.Field_Suffix;
                //if (Trim(TempStr) = '') then TempStr := ' ';
                CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) +
                  ETX + TempStr + GS;
              end
              //It's a symbolic name, so get the field contents
              else begin
                try
                  TempStr := GetValueOfSymbol(TICKER, TemplateFieldsRecPtr^.Field_Contents, TemplateFieldsRecPtr^.Field_Type, CurrentTickerEntryIndex,
                    CurrentGameData, CurrentTickerDisplayMode);
                except
                  if (ErrorLoggingEnabled = True) then
                    WriteToErrorLog('Error occurred while trying to get value for main ticker data field');
                end;
                //Add prefix and suffix from collection
                TempStr := TemplateFieldsRecPtr^.Field_Prefix + TempStr + TemplateFieldsRecPtr^.Field_Suffix;
                //if (Trim(TempStr) = '') then TempStr := ' ';
                CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) + ETX + TempStr + GS;
              end;
            end;
          end;
          //Write out to the as-run log if it's a sponsor logo
          if (Trim(TickerRecPtr^.SponsorLogo_Name) <> '') then
            WriteToAsRunLog('Started display of sponsor logo: ' + CurrentSponsorLogoName);
        end;

        //Set data row indicator in Ticker grid
        if (NEXTRECORD) then
        begin
          try
            MainForm.PlaylistGrid.CurrentDataRow := CurrentTickerEntryIndex+1;
            MainForm.PlaylistGrid.SetTopLeft(1, MainForm.PlaylistGrid.CurrentDataRow);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to reposition main ticker grid cursor to correct record');
          end;
        end;

        //Resubstitute lower-case formatting strings and line wrap command
        CmdStr := StringReplace(CmdStr, '[B', '[b', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[C', '[c', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[R', '[r', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[F', '[f', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[T', '[t', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[X', '[x', [rfReplaceAll]);
        //CmdStr := StringReplace(CmdStr, '|', '[r 0]', [rfReplaceAll]);

        //Substitute style chip codes
        try
          CmdStr := ProcessStyleChips(CmdStr);
        except
          if (ErrorLoggingEnabled = True) then
            WriteToErrorLog('Error occurred while trying to process style chips for main ticker record');
        end;

        //Send the new data
        CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
        if (SocketConnected) AND (EngineParameters.Enabled = TRUE) AND (FoundGame = TRUE) then
        begin
          //Enable packet timeout timer
          if (NEXTRECORD) then TickerPacketTimeoutTimer.Enabled := TRUE;
          //Send command
          try
            EnginePort.Socket.SendText(CmdStr);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying send ticker command to engine');
          end;
          //Start timer to send next record
          if (USEDATAPACKET = FALSE) AND (NEXTRECORD = TRUE) then TickerCommandDelayTimer.Enabled := TRUE;
        end
        //Game not found, so don't send commands & go to next command
        else if (FoundGame = FALSE) then
        begin
          //Enable delay timer
          if (RunningTicker = TRUE) then JumpToNextTickerRecordTimer.Enabled := TRUE;
        end;
      end;
    end;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to send additional records to ticker');
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// BUG
////////////////////////////////////////////////////////////////////////////////
// Procedure to send next record after receipt of packet
////////////////////////////////////////////////////////////////////////////////
procedure TEngineInterface.SendBugRecord(NextRecord: Boolean; RecordIndex: SmallInt);
var
  i,j: SmallInt;
  TempStr, CmdStr: String;
  BugRecPtr: ^BugRec;
  FoundGame: Boolean;
  VisitorScore: SmallInt;
  HomeScore: SmallInt;
  VisitorColor: SmallInt;
  HomeColor: SmallInt;
  HomeRank: String;
  VisitorRank: String;
  HomeName: String;
  Visitorname: String;
  IsLast: Boolean;
  OverlayIndex, OverlayIndexNext: SmallInt;
  IsNewOverlay, PadText: Boolean;
  InGameStatsRequired: Boolean;
  FoundGameInDatamart: Boolean;
  PhaseStr, TimeStr: String;
  VLeader, HLeader: Boolean;
  GameHasStarted, GameIsFinal, GameIsHalf: Boolean;
  GTIME: String;
  GPHASE: SmallInt;
  CurrentGamePhaseRec: GamePhaseRec;
  CurrentGameState: SmallInt;
  RecordIsSponsor: Boolean;
  CurrentLeague: String;
  LineupData: LineupText;
  TemplateInfo: TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CurrentGameData: GameRec;
  BugClockPhaseStr: String;
  OkToGo: Boolean;
begin
  //Init flags
  GameHasStarted := FALSE;
  GameIsFinal:= FALSE;
  GameIsHalf := FALSE;
  //Init FoundGame flag - used to prevent inadverant commands from being sent if game not found
  FoundGame := TRUE;
  RecordIsSponsor := FALSE;

  //Turn on indicator
  PacketIndicatorTimer.Enabled := TRUE;
  MainForm.ApdStatusLight2.Lit := TRUE;

  //Disable packets and enable timer to re-enable packets
  //PacketEnable := FALSE;
  PacketEnableTimer.Enabled := TRUE;
  try
    //Init
    IsLast := FALSE;

    //Make sure we haven't dumped put
    if ((TickerAbortFlag = FALSE) AND (StudioBug_Collection.Count > 0)) then
    begin
      //Init
      BugClockPhaseStr := ' ';
      OKToGo := TRUE;
      //Check if flag set to just send next record
      if (NEXTRECORD = TRUE) then
      begin
         //Make sure there's at least one record left before sending more data;
        //If not, reset to top if in looping mode
        if (CurrentBugEntryIndex >= StudioBug_Collection.Count-1) AND
           (LoopTicker = TRUE) then
        begin
          //Reload the Bug collection in case it has been modified; clear flag to indicate
          //it's not the first time through
          try
            MainForm.LoadPlaylistCollection(Bug, 0, CurrentBugPlaylistID);
            if (StudioBug_Collection.Count > 0) then CurrentBugEntryIndex := 0;
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to reload bug collection');
          end;
          //Check time window and enable
          BugRecPtr := StudioBug_Collection.At(CurrentBugEntryIndex);
          //Make sure record is enabled and within time window
          j := 0;
          While ((BugRecPtr^.Enabled = FALSE) OR NOT ((BugRecPtr^.StartEnableDateTime <= Now) AND
                (BugRecPtr^.EndEnableDateTime > Now))) AND (j < StudioBug_Collection.Count) do
          begin
            Inc(CurrentBugEntryIndex);
            if (CurrentBugEntryIndex = StudioBug_Collection.Count) then
            begin
              try
                MainForm.LoadPlaylistCollection(Bug, 0, CurrentBugPlaylistID);
                CurrentBugEntryIndex := 0;
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to reload bug collection.');
              end;
            end;
            Inc(j);
          end;
          //Set flag if no enabled records found
          if (j >= StudioBug_Collection.Count) then OKToGo := FALSE;
        end
        //Not last record, so increment Bug playout collection object index
        else if (CurrentBugEntryIndex < StudioBug_Collection.Count-1) then
        begin
          //Make sure record is enabled and within time window
          j := 0;
          Repeat
            Inc(CurrentBugEntryIndex);
            if (CurrentBugEntryIndex = StudioBug_Collection.Count) then
            begin
              try
                MainForm.LoadPlaylistCollection(Bug, 0, CurrentBugPlaylistID);
                CurrentBugEntryIndex := 0;
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to reload bug collection');
              end;
            end;
            BugRecPtr := StudioBug_Collection.At(CurrentBugEntryIndex);
            Inc (j);
          Until ((BugRecPtr^.Enabled = TRUE) AND (BugRecPtr^.StartEnableDateTime <= Now) AND
                (BugRecPtr^.EndEnableDateTime > Now)) OR (j >= StudioBug_Collection.Count);
          //Set flag if no enabled records found
          if (j >= StudioBug_Collection.Count) then OKToGo := FALSE;
        end
        //At end and not looping, so clear screen
        else if (CurrentBugEntryIndex >= StudioBug_Collection.Count-1) AND
                (LoopTicker = FALSE) then
        begin
          //Increment to end
          Inc(CurrentBugEntryIndex);
        end;
        //Set flag if at last collection obejct and not looping
        if (CurrentBugEntryIndex = StudioBug_Collection.Count-1) AND
           (LoopTicker = FALSE) then IsLast := TRUE;
        //Enable command timer
        DisableCommandTimer := FALSE;
      end
      //Triggering specific record, so set current entry index
      else begin
        CurrentBugEntryIndex := RecordIndex;
        //Disble command timer
        DisableCommandTimer := TRUE;
      end;

      //Proceed if not at end or in looping mode and at beggining
      if (CurrentBugEntryIndex <= StudioBug_Collection.Count-1) AND (OKToGo = TRUE) then
      begin
        //Get pointer to current record
        BugRecPtr := StudioBug_Collection.At(CurrentBugEntryIndex);

        //Set default mode
        CmdStr := SOT + 'SM' + ETX + '2' + GS;

        ////////////////////////////////////////////////////////////////////////
        //MAIN PROCESSING LOOP FOR Bug FIELDS
        ////////////////////////////////////////////////////////////////////////
        //Get template info and load the termporary fields collection
        //Add bias to template ID if in Live Event mode
        if (CurrentTickerDisplayMode = 1) then
          TemplateInfo := LoadTempTemplateFields(BugRecPtr^.Template_ID+50)
        else
          TemplateInfo := LoadTempTemplateFields(BugRecPtr^.Template_ID);

        //Get game data if template requires it
        if (TemplateInfo.UsesGameData) then
        begin
          try
            //Use full game data
            CurrentGameData := EngineInterface.GetGameData(BugRecPtr^.League, BugRecPtr^.SI_GCode,
              BugRecPtr^.ST_Game_ID, BugRecPtr^.CSS_GameID, BugRecPtr^.NFLDM_GameID,
              BugRecPtr^.Primary_Data_Source, BugRecPtr^.Backup_Data_Source);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to get game data for bug');
          end;
          FoundGame := CurrentGameData.DataFound;
        end;

        //Check the game state vs. the state required by the template if not a match, increment
        //Also do check for enabled entries and check time window
        if (TemplateInfo.RequiredGameState > 0) AND (CurrentGameData.GameState <> TemplateInfo.RequiredGameState) AND
           //Special case for postponed, delayed or suspended MLB games
           (NOT((CurrentGameData.GameState > 3) AND (TemplateInfo.RequiredGameState = 3))) AND
           (NEXTRECORD = TRUE) then
        Repeat
          Inc(CurrentBugEntryIndex);
          if (CurrentBugEntryIndex = StudioBug_Collection.Count) then
          begin
            MainForm.LoadPlaylistCollection(Bug, 0, CurrentBugPlaylistID);
            CurrentBugEntryIndex := 0;
          end;
          BugRecPtr := StudioBug_Collection.At(CurrentBugEntryIndex);
          //Get template information
          if (CurrentTickerDisplayMode = 1) then
            TemplateInfo := LoadTempTemplateFields(BugRecPtr^.Template_ID+50)
          else
            TemplateInfo := LoadTempTemplateFields(BugRecPtr^.Template_ID);
          if (TemplateInfo.UsesGameData) then
          begin
            try
              //Use full game data
              CurrentGameData := EngineInterface.GetGameData(BugRecPtr^.League, BugRecPtr^.SI_GCode,
                BugRecPtr^.ST_Game_ID, BugRecPtr^.CSS_GameID, BugRecPtr^.NFLDM_GameID,
                BugRecPtr^.Primary_Data_Source, BugRecPtr^.Backup_Data_Source);
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying to get game data for bug');
            end;
            FoundGame := CurrentGameData.DataFound;
          end
          else CurrentGameData.GameState := -1;
        Until (TemplateInfo.RequiredGameState = 0) OR (CurrentGameData.GameState = TemplateInfo.RequiredGameState) OR
              (CurrentGameData.GameState = -1) AND ((BugRecPtr^.Enabled = TRUE) AND
              (BugRecPtr^.StartEnableDateTime <= Now) AND (BugRecPtr^.EndEnableDateTime > Now));

        //Set the default dwell time - minimum = 2000mS; set packet timeout interval
        if (BugRecPtr^.DwellTime > 2000) then
        begin
          BugCommandDelayTimer.Interval := BugRecPtr^.DwellTime;
          BugPacketTimeoutTimer.Interval := BugRecPtr^.DwellTime + 5117;
        end
        else begin
          BugCommandDelayTimer.Interval := 2000;
          BugPacketTimeoutTimer.Interval := BugRecPtr^.DwellTime + 7117;
        end;

        //Set the template type
        CmdStr := CmdStr + 'ST' + ETX + IntToStr(TemplateInfo.Engine_Template_ID) + GS;
        //Add the fields from the temporary fields collection
        if (Temporary_Fields_Collection.Count > 0) then
        begin
          for j := 0 to Temporary_Fields_Collection.Count-1 do
          begin
            TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
            //Filter out fields ID values of -1 => League designator
            if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
            begin
              //Add region command
              //If not a symbolic fields, send field contents directly
              if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
              begin
                TempStr := TemplateFieldsRecPtr^.Field_Contents;
                //Add prefix and suffix from collection
                TempStr := TemplateFieldsRecPtr^.Field_Prefix + TempStr + TemplateFieldsRecPtr^.Field_Suffix;
                //if (Trim(TempStr) = '') then TempStr := ' ';
                CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) +
                  ETX + TempStr + GS;
              end
              //It's a symbolic name, so get the field contents
              else begin
                try
                  TempStr := GetValueOfSymbol(BUG, TemplateFieldsRecPtr^.Field_Contents, TemplateFieldsRecPtr^.Field_Type, CurrentBugEntryIndex,
                    CurrentGameData, CurrentTickerDisplayMode);
                except
                  if (ErrorLoggingEnabled = True) then
                    WriteToErrorLog('Error occurred while trying to get value for bug data field');
                end;
                //Add prefix and suffix from collection
                TempStr := TemplateFieldsRecPtr^.Field_Prefix + TempStr + TemplateFieldsRecPtr^.Field_Suffix;
                //if (Trim(TempStr) = '') then TempStr := ' ';
                //Check to see if it's the bug clock/phase field; if so, store for insertion into line-up
                if (TemplateFieldsRecPtr^.Engine_Field_ID = 2000) then
                  BugClockPhaseStr := TempStr
                else
                  CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) + ETX + TempStr + GS;
              end;
            end;
          end;
        end;

        //Set data row indicator in Bug grid
        if (NEXTRECORD) then
        begin
          try
            MainForm.PlaylistGrid.CurrentDataRow := CurrentBugEntryIndex+1;
            MainForm.PlaylistGrid.SetTopLeft(1, MainForm.PlaylistGrid.CurrentDataRow);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to reposition bug grid cursor to correct record');
          end;
        end;

        //Set lineup text
        if (CurrentTickerDisplayMode = 1) then BugClockPhaseStr := ' ';
        if (Trim(BugRecPtr^.League) = 'NONE') then
          CmdStr := CmdStr + 'SI' + ETX + ' ' + ETX + BugClockPhaseStr +
                    ETX + ' ' + ETX + ' ' + ETX + ' ' + GS
        else
          CmdStr := CmdStr + 'SI' + ETX + BugRecPtr^.League + ETX + BugClockPhaseStr +
                    ETX + ' ' + ETX + ' ' + ETX + ' ' + GS;

        //Resubstitute lower-case formatting strings and line wrap command
        CmdStr := StringReplace(CmdStr, '[B', '[b', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[C', '[c', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[R', '[r', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[F', '[f', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[T', '[t', [rfReplaceAll]);
        //CmdStr := StringReplace(CmdStr, '|', '[r 0]', [rfReplaceAll]);

        //Substitute style chip codes
        try
          CmdStr := ProcessStyleChips(CmdStr);
        except
          if (ErrorLoggingEnabled = True) then
            WriteToErrorLog('Error occurred while trying to process style chips for bug record');
        end;

        //Send the new data
        CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
        if (SocketConnected) AND (EngineParameters.Enabled = TRUE) AND (FoundGame = TRUE) then
        begin
          //Enable packet timeout timer
          if (NEXTRECORD) then BugPacketTimeoutTimer.Enabled := TRUE;
          //Send command
          try
            EnginePort.Socket.SendText(CmdStr);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying send bug command to engine');
          end;
          //Start timer to send next record
          if (USEDATAPACKET = FALSE) AND (NEXTRECORD = TRUE) then BugCommandDelayTimer.Enabled := TRUE;
        end
        //Game not found, so don;t send commands & go to next command
        else if (FoundGame = FALSE) then
        begin
          //Enable delay timer
          if (RunningTicker = TRUE) then JumpToNextBugRecordTimer.Enabled := TRUE;
        end;
      end;
    end;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to send additional records to Bug');
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// GAME TRAX TICKER
////////////////////////////////////////////////////////////////////////////////
// Procedure to send next record after receipt of packet or on timer
////////////////////////////////////////////////////////////////////////////////
procedure TEngineInterface.SendGameTraxRecord(NextRecord: Boolean; RecordIndex: SmallInt);
var
  i,j: SmallInt;
  TempStr, CmdStr: String;
  GameTraxRecPtr, GameTraxRecPtrNew: ^GameTraxRec;
  FoundGame: Boolean;
  VisitorScore: SmallInt;
  HomeScore: SmallInt;
  VisitorColor: SmallInt;
  HomeColor: SmallInt;
  HomeRank: String;
  VisitorRank: String;
  HomeName: String;
  Visitorname: String;
  IsLast: Boolean;
  InGameStatsRequired: Boolean;
  FoundGameInDatamart: Boolean;
  PhaseStr, TimeStr: String;
  VLeader, HLeader: Boolean;
  GameHasStarted, GameIsFinal, GameIsHalf: Boolean;
  GTIME: String;
  GPHASE: SmallInt;
  CurrentGamePhaseRec: GamePhaseRec;
  CurrentGameState: SmallInt;
  CurrentLeague: String;
  LineupData: LineupText;
  TemplateInfo: TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CurrentGameData: GameRec;
  OkToGo: Boolean;
begin
  //Init flags
  GameHasStarted := FALSE;
  GameIsFinal:= FALSE;
  GameIsHalf := FALSE;
  //Init FoundGame flag - used to prevent inadverant commands from being sent if game not found
  FoundGame := TRUE;

  //Turn on indicator
  PacketIndicatorTimer.Enabled := TRUE;
  MainForm.ApdStatusLight2.Lit := TRUE;

  //Disable packets and enable timer to re-enable packets
  //PacketEnable := FALSE;
  PacketEnableTimer.Enabled := TRUE;
  try
    //Init
    IsLast := FALSE;

    //Make sure we haven't dumped put
    if ((TickerAbortFlag = FALSE) AND (GameTrax_Collection.Count > 0)) then
    begin
      OKToGo := TRUE;
      //Check if flag set to just send next record
      if (NEXTRECORD = TRUE) then
      begin
        //Make sure there's at least one record left before sending more data;
        //If not, reset to top if in looping mode
        if (CurrentExtraLineEntryIndex >= GameTrax_Collection.Count-1) AND
           (LoopTicker = TRUE) then
        begin
          try
            //Reload the ExtraLine collection in case it has been modified; clear flag to indicate
            //it's not the first time through
            MainForm.LoadPlaylistCollection(GAMETRAX, 0, CurrentExtraLinePlaylistID);
            if (GameTrax_Collection.Count > 0) then CurrentExtraLineEntryIndex := 0;
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to reload main ExtraLine collection');
          end;
          //Check time window and enable
          GameTraxRecPtr := GameTrax_Collection.At(CurrentExtraLineEntryIndex);
          //Make sure record is enabled and within time window
          j := 0;
          While ((GameTraxRecPtr^.Enabled = FALSE) OR NOT((GameTraxRecPtr^.StartEnableDateTime <= Now) AND
                (GameTraxRecPtr^.EndEnableDateTime > Now))) AND (j < GameTrax_Collection.Count) do
          begin
            Inc(CurrentExtraLineEntryIndex);
            if (CurrentExtraLineEntryIndex = GameTrax_Collection.Count) then
            begin
              try
                MainForm.LoadPlaylistCollection(GAMETRAX, 0, CurrentExtraLinePlaylistID);
                CurrentExtraLineEntryIndex := 0;
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to reload main ExtraLine collection');
              end;
            end;
            Inc(j);
          end;
          //Set flag if no enabled records found
          if (j >= GameTrax_Collection.Count) then OKToGo := FALSE;
        end
        //Not last record, so increment ExtraLine playout collection object index
        else if (CurrentExtraLineEntryIndex < GameTrax_Collection.Count-1) then
        begin
          //Make sure record is enabled and within time window
          j := 0;
          Repeat
            Inc(CurrentExtraLineEntryIndex);
            if (CurrentExtraLineEntryIndex = GameTrax_Collection.Count) then
            begin
              try
                MainForm.LoadPlaylistCollection(GAMETRAX, 0, CurrentExtraLinePlaylistID);
                CurrentExtraLineEntryIndex := 0;
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to reload main ExtraLine collection');
              end;
            end;
            GameTraxRecPtr := GameTrax_Collection.At(CurrentExtraLineEntryIndex);
            Inc (j);
          Until ((GameTraxRecPtr^.Enabled = TRUE) AND (GameTraxRecPtr^.StartEnableDateTime <= Now) AND
                (GameTraxRecPtr^.EndEnableDateTime > Now)) OR (j >= GameTrax_Collection.Count);
          //Set flag if no enabled records found
          if (j >= GameTrax_Collection.Count) then OKToGo := FALSE;
        end
        //At end and not looping, so clear screen
        else if (CurrentExtraLineEntryIndex >= GameTrax_Collection.Count-1) AND
                (LoopTicker = FALSE) then
        begin
          //Increment to end
          Inc(CurrentExtraLineEntryIndex);
        end;
        //Set flag if at last collection obejct and not looping
        if (CurrentExtraLineEntryIndex = GameTrax_Collection.Count-1) AND
           (LoopTicker = FALSE) then IsLast := TRUE;
        //Enable command timer
        DisableCommandTimer := FALSE;
      end
      //Triggering specific record, so set current entry index
      else begin
        CurrentExtraLineEntryIndex := RecordIndex;
        //Disble command timer
        DisableCommandTimer := TRUE;
      end;

      //Proceed if not at end or in looping mode and at beggining
      if (CurrentExtraLineEntryIndex <= GameTrax_Collection.Count-1) AND (OkToGo) then
      begin
        //Get pointer to current record
        GameTraxRecPtr := GameTrax_Collection.At(CurrentExtraLineEntryIndex);

        //Set default mode
        CmdStr := SOT + 'SM' + ETX + '2' + GS;

        ////////////////////////////////////////////////////////////////////////
        //MAIN PROCESSING LOOP FOR ExtraLine FIELDS
        ////////////////////////////////////////////////////////////////////////
        //Get template info and load the termporary fields collection
        TemplateInfo := LoadTempTemplateFields(GameTraxRecPtr^.Template_ID);

        //Get game data if template requires it
        if (TemplateInfo.UsesGameData) then
        begin
          try
            //Use full game data
            CurrentGameData := EngineInterface.GetGameData(GameTraxRecPtr^.League, GameTraxRecPtr^.SI_GCode,
              GameTraxRecPtr^.ST_Game_ID, GameTraxRecPtr^.CSS_GameID, GameTraxRecPtr^.NFLDM_GameID,
              GameTraxRecPtr^.Primary_Data_Source, GameTraxRecPtr^.Backup_Data_Source);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to get game data for ExtraLine');
          end;
          FoundGame := CurrentGameData.DataFound;
        end;

        //Check the game state vs. the state required by the template if not a match, increment
        //Also do check for enabled entries and check time window
        if (TemplateInfo.RequiredGameState > 0) AND (CurrentGameData.GameState <> TemplateInfo.RequiredGameState) AND
           //Special case for postponed, delayed or suspended MLB games
           (NOT((CurrentGameData.GameState > 3) AND (TemplateInfo.RequiredGameState = 3))) AND
           (NEXTRECORD = TRUE) then
        Repeat
          Inc(CurrentExtraLineEntryIndex);
          if (CurrentExtraLineEntryIndex = GameTrax_Collection.Count) then
          begin
            MainForm.LoadPlaylistCollection(GAMETRAX, 0, CurrentExtraLinePlaylistID);
            CurrentExtraLineEntryIndex := 0;
          end;
          GameTraxRecPtr := GameTrax_Collection.At(CurrentExtraLineEntryIndex);
          TemplateInfo := LoadTempTemplateFields(GameTraxRecPtr^.Template_ID);
          if (TemplateInfo.UsesGameData) then
          begin
            try
              //Use full game data
              CurrentGameData := EngineInterface.GetGameData(GameTraxRecPtr^.League, GameTraxRecPtr^.SI_GCode,
                GameTraxRecPtr^.ST_Game_ID, GameTraxRecPtr^.CSS_GameID, GameTraxRecPtr^.NFLDM_GameID,
                GameTraxRecPtr^.Primary_Data_Source, GameTraxRecPtr^.Backup_Data_Source);
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying to get game data for ExtraLine');
            end;
            FoundGame := CurrentGameData.DataFound;
          end
          else CurrentGameData.GameState := -1;
        Until (TemplateInfo.RequiredGameState = 0) OR (CurrentGameData.GameState = TemplateInfo.RequiredGameState) OR
              (CurrentGameData.GameState = -1) AND ((GameTraxRecPtr^.Enabled = TRUE) AND
              (GameTraxRecPtr^.StartEnableDateTime <= Now) AND (GameTraxRecPtr^.EndEnableDateTime > Now));

        //Set the default dwell - minimum = 2000mS
        if (GameTraxRecPtr^.DwellTime > 2000) then
        begin
          ExtraLineCommandDelayTimer.Interval := GameTraxRecPtr^.DwellTime + 212;
          ExtraLinePacketTimeoutTimer.Interval := GameTraxRecPtr^.DwellTime + 5212;
        end
        else begin
          ExtraLineCommandDelayTimer.Interval := GameTraxRecPtr^.DwellTime;
          ExtraLinePacketTimeoutTimer.Interval := GameTraxRecPtr^.DwellTime + 30212;
        end;

        //Set the template type
        CmdStr := CmdStr + 'ST' + ETX + IntToStr(TemplateInfo.Engine_Template_ID) + GS;
        //Add the fields from the temporary fields collection
        if (Temporary_Fields_Collection.Count > 0) then
        begin
          for j := 0 to Temporary_Fields_Collection.Count-1 do
          begin
            TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
            //Filter out fields ID values of -1 => League designator
            if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
            begin
              //Add region command
              //If not a symbolic fields, send field contents directly
              if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
              begin
                TempStr := TemplateFieldsRecPtr^.Field_Contents;
                //Append # character if first Fantasy stat to trigger intro animation
                //if (TemplateFieldsRecPtr^.Engine_Field_ID = 1004) AND NOT (LastPageWasFantasyStat) AND
                //   (TemplateInfo.Template_ID >= FANTASY_START_TEMPLATTE) AND (TemplateInfo.Template_ID <= FANTASY_END_TEMPLATTE) then
                //begin
                //  TempStr := '#' + TempStr;
                //  LastPageWasFantasyStat := TRUE;
                //end;
                //Add prefix and suffix from collection
                TempStr := TemplateFieldsRecPtr^.Field_Prefix + TempStr + TemplateFieldsRecPtr^.Field_Suffix;
                //if (Trim(TempStr) = '') then TempStr := ' ';
                CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) +
                  ETX + TempStr + GS;
              end
              //It's a symbolic name, so get the field contents
              else begin
                try
                  TempStr := GetValueOfSymbol(GAMETRAX, TemplateFieldsRecPtr^.Field_Contents, TemplateFieldsRecPtr^.Field_Type, CurrentExtraLineEntryIndex,
                    CurrentGameData, CurrentTickerDisplayMode);
                except
                  if (ErrorLoggingEnabled = True) then
                    WriteToErrorLog('Error occurred while trying to get value for main ExtraLine data field');
                end;
                //Add prefix and suffix from collection
                TempStr := TemplateFieldsRecPtr^.Field_Prefix + TempStr + TemplateFieldsRecPtr^.Field_Suffix;
                //if (Trim(TempStr) = '') then TempStr := ' ';
                CmdStr := CmdStr + 'SD' + ETX + IntToStr(TemplateFieldsRecPtr^.Engine_Field_ID) + ETX + TempStr + GS;
              end;
            end;
          end;
        end;

        //Set data row indicator in ExtraLine grid
        if (NEXTRECORD) then
        begin
          try
            MainForm.PlaylistGrid.CurrentDataRow := CurrentExtraLineEntryIndex+1;
            MainForm.PlaylistGrid.SetTopLeft(1, MainForm.PlaylistGrid.CurrentDataRow);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to reposition main ExtraLine grid cursor to correct record');
          end;
        end;

        //Only add sponsor logo if CFB or CBB
        //Version 3.0.7 modified for NCAA tournament Gametrax look
        if (GameTraxRecPtr^.League = 'CFB') OR (GameTraxRecPtr^.League = 'CBB') or
           (GameTraxRecPtr^.Template_ID = NCAATournamentLookNotesTemplateID) then
        begin
          //Add sponsor logo name
          if (Trim(CurrentSponsorLogoName) <> '') then
              CmdStr := CmdStr + 'SD' + ETX + IntToStr(1006) + ETX + CurrentSponsorLogoName + GS
            else
              CmdStr := CmdStr + 'SD' + ETX + IntToStr(1006) + ETX + '*' + GS;
        end;

        //Set NFL Fantasy template status
        if (TemplateInfo.Template_ID >= FANTASY_START_TEMPLATTE) AND (TemplateInfo.Template_ID <= FANTASY_END_TEMPLATTE) then
          LastPageWasFantasyStat := TRUE
        else
          LastPageWasFantasyStat := FALSE;

        //Resubstitute lower-case formatting strings and line wrap command
        CmdStr := StringReplace(CmdStr, '[B', '[b', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[C', '[c', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[R', '[r', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[F', '[f', [rfReplaceAll]);
        CmdStr := StringReplace(CmdStr, '[T', '[t', [rfReplaceAll]);
        //CmdStr := StringReplace(CmdStr, '|', '[r 0]', [rfReplaceAll]);

        //Substitute style chip codes
        try
          CmdStr := ProcessStyleChips(CmdStr);
        except
          if (ErrorLoggingEnabled = True) then
            WriteToErrorLog('Error occurred while trying to process style chips for main ExtraLine record');
        end;

        //Send the new data
        CmdStr := CmdStr + 'TG' + ETX + '1' + EOT;
        if (SocketConnected) AND (EngineParameters.Enabled = TRUE) AND (FoundGame = TRUE) then
        begin
          //Enable packet timeout timer
          if (NEXTRECORD) then ExtraLinePacketTimeoutTimer.Enabled := TRUE;
          //Send command
          try
            //If breaking news, clear the logo
            //if (ScheduledPlaylistInfo[3].Playlist_ID = 0) then DisableLogoClock;
            EnginePort.Socket.SendText(CmdStr);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying send ExtraLine command to engine');
          end;
          //Start timer to send next record
          if (USEDATAPACKET = FALSE) AND (NEXTRECORD = TRUE) then ExtraLineCommandDelayTimer.Enabled := TRUE;
        end
        //Game not found, so don't send commands & go to next command
        else if (FoundGame = FALSE) then
        begin
          //Enable delay timer
          if (RunningTicker = TRUE) then JumpToNextGameTraxRecordTimer.Enabled := TRUE;
        end;
      end;
    end;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to send additional records to ExtraLine');
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// SOCKET EVENT HANDLES
////////////////////////////////////////////////////////////////////////////////
//Handler for packet re-enable timer
procedure TEngineInterface.PacketEnableTimerTimer(Sender: TObject);
begin
  //Disable to prevent retrigger
  PacketEnableTimer.Enabled := FALSE;
  //Re-enable packets
  PacketEnable := TRUE;
end;

//Handler to turn off packet recevied indicator after 250 mS
procedure TEngineInterface.PacketIndicatorTimerTimer(Sender: TObject);
begin
  PacketIndicatorTimer.Enabled := FALSE;
  MainForm.ApdStatusLight2.Lit := FALSE;
end;

////////////////////////////////////////////////////////////////////////////////
// PACKET TIMEOUT TIMERS
////////////////////////////////////////////////////////////////////////////////
//Handler for ticker packet timeout timer - sends next command if status command not
//received
procedure TEngineInterface.TickerPacketTimeoutTimerTimer(Sender: TObject);
begin
  //Disable timer to prevent retriggering
  TickerPacketTimeoutTimer.Enabled := FALSE;
  //Enable delay timer
  if (RunningTicker = TRUE) then
    //Send next record
    SendTickerRecord(TRUE, 0);
  //Log the timeout
  if (ErrorLoggingEnabled = True) then
  begin
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('A timeout occurred while waiting for a Main Ticker command return status from the graphics engine.');
  end;
end;
//Handler for bug packet timeout timer - sends next command if status command not
//received
procedure TEngineInterface.BugPacketTimeoutTimerTimer(Sender: TObject);
begin
  //Disable timer to prevent retriggering
  BugPacketTimeoutTimer.Enabled := FALSE;
  //Enable delay timer
  if (RunningTicker = TRUE) then
    //Send next record
    SendBugRecord(TRUE, 0);
  //Log the timeout
  if (ErrorLoggingEnabled = True) then
  begin
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('A timeout occurred while waiting for a Bug command return status from the graphics engine.');
  end;
end;
//Handler for extra line packet timeout timer - sends next command if status command not
//received
procedure TEngineInterface.ExtraLinePacketTimeoutTimerTimer(Sender: TObject);
begin
  //Disable timer to prevent retriggering
  ExtraLinePacketTimeoutTimer.Enabled := FALSE;
  //Enable delay timer
  if (RunningTicker = TRUE) then
    //Send next record
    SendGameTraxRecord(TRUE, 0);
  //Log the timeout
  if (ErrorLoggingEnabled = True) then
  begin
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('A timeout occurred while waiting for an ExtraLine command return status from the graphics engine.');
  end;
end;

//Handler to light indicator
procedure TEngineInterface.EnginePortConnect(Sender: TObject; Socket: TCustomWinSocket);
begin
  MainForm.ApdStatusLight1.Lit := TRUE;
  SocketConnected := TRUE;
end;

//Handler to turn off indicator
procedure TEngineInterface.EnginePortDisconnect(Sender: TObject; Socket: TCustomWinSocket);
begin
  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  MessageDlg('The connection to the graphics engine was lost. ' +
             'You will not be able to control the graphics engine. If you wish to try ' +
             'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
             'from the main program menu.', mtError, [mbOK], 0);
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred with connection to ticker graphics engine');
  end;
end;

//Handler for socket error
procedure TEngineInterface.EnginePortError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  MessageDlg('The connection to the graphics engine was lost. ' +
             'You will not be able to control the graphics engine. If you wish to try ' +
             'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
             'from the main program menu.', mtError, [mbOK], 0);
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred with connection to ticker graphics engine');
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// LOGGING FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToErrorLog (ErrorString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   ErrorLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists('c:\TSTN_Error_LogFiles') = FALSE) then CreateDir('c:\TSTN_Error_LogFiles');
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   ErrorLogStr := TimeStr + '    ' + DateStr + '    ' + ErrorString;
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := 'c:\TSTN_Error_Logfiles\TSTNErrorLog' + DayStr + '.txt';
   AssignFile (ErrorLogFile, FileName);
   //Write error information to one line in file
   //If file doesn't exist create a new one. Otherwise, check to see if it's one
   //month old. If so, delete & create a new one. Otherwise, append error to file
   if (Not (FileExists(FileName))) then
   begin
      ReWrite (ErrorLogFile);
      try
         Writeln (ErrorLogFile, ErrorLogStr);      finally         CloseFile (ErrorLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (ErrorLogFile);      try         Readln (ErrorLogFile, TestStr);      finally         CloseFile (ErrorLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (ErrorLogFile);         {Create a new logfile}         ReWrite (ErrorLogFile);         try
            Writeln (ErrorLogFile, ErrorLogStr);         finally            CloseFile (ErrorLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (ErrorLogFile);         try
            Writeln (ErrorLogFile, ErrorLogStr);         finally            CloseFile (ErrorLogFile);         end;      end;   end;end;

// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToAsRunLog (AsRunString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   AsRunLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
   DirectoryStr: String;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists(AsRunLogFileDirectoryPath) = FALSE) then
   begin
     if (DirectoryExists('c:\TSTN_AsRun_LogFiles') = FALSE) then
       CreateDir('c:\TSTN_AsRun_LogFiles');
     DirectoryStr := 'c:\TSTN_AsRun_LogFiles\';
   end
   else DirectoryStr := AsRunLogFileDirectoryPath;
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   AsRunLogStr := TimeStr + '    ' + DateStr + '    ' + AsRunString;
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := DirectoryStr + 'TSTNAsRunLog' + DayStr + '.txt';
   AssignFile (AsRunLogFile, FileName);
   {If (FileExists (ErrorLogFile)) then Erase (ErrorLogFile);}
   {Write error information to one line in file}
   {If file doesn't exist create a new one. Otherwise, check to see if it's one }
   {month old. If so, delete & create a new one. Otherwise, append error to file}
   if (Not (FileExists(FileName))) then begin
      ReWrite (AsRunLogFile);
      try
         Writeln (AsRunLogFile, AsRunLogStr);      finally         CloseFile (AsRunLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (AsRunLogFile);      try         Readln (AsRunLogFile, TestStr);      finally         CloseFile (AsRunLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then      begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (AsRunLogFile);         {Create a new logfile}         ReWrite (AsRunLogFile);         try
            Writeln (AsRunLogFile, AsRunLogStr);         finally            CloseFile (AsRunLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (AsRunLogFile);         try
            Writeln (AsRunLogFile, AsRunLogStr);         finally            CloseFile (AsRunLogFile);         end;      end;   end;end;

end.


