object ConferenceChipEditorDlg: TConferenceChipEditorDlg
  Left = 368
  Top = 170
  BorderStyle = bsDialog
  Caption = 'NCAA Conference Chip Editor'
  ClientHeight = 547
  ClientWidth = 698
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 16
    Top = 11
    Width = 665
    Height = 470
    BevelWidth = 2
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 14
      Width = 241
      Height = 20
      Caption = 'NCAA Conference Chip Editor'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 118
      Top = 398
      Width = 37
      Height = 20
      Caption = 'First'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 170
      Top = 398
      Width = 38
      Height = 20
      Caption = 'Prior'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 227
      Top = 398
      Width = 37
      Height = 20
      Caption = 'Next'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 285
      Top = 398
      Width = 36
      Height = 20
      Caption = 'Last'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 343
      Top = 398
      Width = 33
      Height = 20
      Caption = 'Edit'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 395
      Top = 398
      Width = 37
      Height = 20
      Caption = 'Post'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 443
      Top = 398
      Width = 56
      Height = 20
      Caption = 'Cancel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 510
      Top = 398
      Width = 36
      Height = 20
      Caption = 'Refr'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object tsDBGrid1: TtsDBGrid
      Left = 18
      Top = 40
      Width = 631
      Height = 353
      CellSelectMode = cmNone
      CheckBoxStyle = stCheck
      Cols = 4
      DatasetType = dstStandard
      DataSource = dmMain.dsCFB_Conference_Chips
      DefaultRowHeight = 18
      ExactRowCount = True
      ExportDelimiter = ','
      FieldState = fsCustomized
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HeadingFont.Charset = DEFAULT_CHARSET
      HeadingFont.Color = clWindowText
      HeadingFont.Height = -13
      HeadingFont.Name = 'MS Sans Serif'
      HeadingFont.Style = [fsBold]
      HeadingHeight = 20
      HeadingParentFont = False
      ParentFont = False
      ParentShowHint = False
      RowChangedIndicator = riAutoReset
      RowMoving = False
      ShowHint = False
      TabOrder = 0
      Version = '2.20.26'
      XMLExport.Version = '1.0'
      XMLExport.DataPacketVersion = '2.0'
      DataBound = True
      ColProperties = <
        item
          DataCol = 1
          FieldName = 'Conference_Index'
          Col.FieldName = 'Conference_Index'
          Col.Heading = 'Index'
          Col.Width = 60
          Col.AssignedValues = '?'
        end
        item
          DataCol = 2
          FieldName = 'Conference_Name'
          Col.FieldName = 'Conference_Name'
          Col.Heading = 'Conference Name'
          Col.Width = 139
          Col.AssignedValues = '?'
        end
        item
          DataCol = 3
          FieldName = 'Conference_Abbreviation'
          Col.FieldName = 'Conference_Abbreviation'
          Col.Heading = 'Abbreviation'
          Col.Width = 156
          Col.AssignedValues = '?'
        end
        item
          DataCol = 4
          FieldName = 'Conference_Chip_Text'
          Col.FieldName = 'Conference_Chip_Text'
          Col.Heading = 'Display Text'
          Col.Width = 241
          Col.AssignedValues = '?'
        end>
    end
    object DBNavigator1: TDBNavigator
      Left = 108
      Top = 424
      Width = 448
      Height = 33
      DataSource = dmMain.dsCFB_Conference_Chips
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbEdit, nbPost, nbCancel, nbRefresh]
      TabOrder = 1
    end
  end
  object BitBtn1: TBitBtn
    Left = 296
    Top = 496
    Width = 105
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkClose
  end
end
