unit Globals;

interface

uses
  StBase, StColl;

type
  TickerRec = Record
    Event_Index: Smallint;
    Event_GUID: TGUID;
    Enabled: Boolean;
    Is_Child: Boolean;
    League: String[20];
    Subleague_Mnemonic_Standard: String[10];
    Mnemonic_LiveEvent: String[10];
    Template_ID: SmallInt;
    Record_Type: SmallInt;
    SI_GCode: String[50];
    ST_Game_ID: String[50];
    CSS_GameID: LongInt;
    NFLDM_GameID: LongInt;
    Primary_Data_Source: String[10];
    Backup_Data_Source: String[10];
    SponsorLogo_Name: String[50];
    SponsorLogo_Dwell: SmallInt;
    SponsorLogo_Region: SmallInt;
    StatStoredProcedure: String[50];
    StatType: SmallInt;
    StatTeam: String[50];
    StatLeague: String[20];
    StatRecordNumber: SmallInt;
    UserData: Array[1..25] of String[255];
    StartEnableDateTime: TDateTime;
    EndEnableDateTime: TDateTime;
    Selected: Boolean;
    DwellTime: LongInt;
    Description: String[100];
    Comments: String[100];
  end;

  BugRec = Record
    Event_Index: Smallint;
    Event_GUID: TGUID;
    Enabled: Boolean;
    Is_Child: Boolean;
    League: String[20];
    Template_ID: SmallInt;
    Record_Type: SmallInt;
    SI_GCode: String[50];
    ST_Game_ID: String[50];
    CSS_GameID: LongInt;
    NFLDM_GameID: LongInt;
    Primary_Data_Source: String[10];
    Backup_Data_Source: String[10];
    UserData: Array[1..25] of String[100];
    StartEnableDateTime: TDateTime;
    EndEnableDateTime: TDateTime;
    Selected: Boolean;
    DwellTime: LongInt;
    Description: String[100];
    Comments: String[100];
  end;

  GameTraxRec = Record
    Event_Index: Smallint;
    Event_GUID: TGUID;
    Enabled: Boolean;
    Is_Child: Boolean;
    League: String[20];
    Template_ID: SmallInt;
    Record_Type: SmallInt;
    SI_GCode: String[50];
    ST_Game_ID: String[50];
    CSS_GameID: LongInt;
    NFLDM_GameID: LongInt;
    Primary_Data_Source: String[10];
    Backup_Data_Source: String[10];
    SponsorLogo_Name: String[50];
    SponsorLogo_Dwell: SmallInt;
    SponsorLogo_Region: SmallInt;
    UserData: Array[1..25] of String[255];
    StartEnableDateTime: TDateTime;
    EndEnableDateTime: TDateTime;
    Selected: Boolean;
    DwellTime: LongInt;
    Description: String[100];
    Comments: String[100];
  end;

  ColorRec = record
    Description: String;
    DBIndexValue: SmallInt;
  end;

  TeamRec = record
    ID: Double;
    League: String[25];
    LeagueID: Double;
    City: String[50];
    Team: String[50];
    FullName: String[100];
    SportstickerMnemonic: String[20];
    StatsIncId: Double;
    StatsIncAlias: String[100];
    OddsId: Double;
    ShortDisplayName1: String[50];
    ShortDisplayName2: String[50];
    MediumDisplayName1: String[75];
    MediumDisplayName2: String[75];
    LongDisplayName1: String[100];
    LongDisplayName2: String[100];
    Division: String[50];
    Conference: String[50];
    TeamLogoName: String[50];
  end;

  StatRec = Record
    StatLeague: String[10];
    StatType: SmallInt;
    StatDescription: String[50];
    StatHeader: String[50];
    StatAbbreviation: String[10];
    StatStoredProcedure: String[50];
    StatDataField: String[20];
    UseStatQualifier: Boolean;
    StatQualifier: String[100];
    StatQualifierValue: SmallInt;
  end;

  SelectedStatRec = record
    StatType: SmallInt;
    StatStoredProcedure: String;
    StatLeague: String;
    StatTeam: String;
  end;

  SponsorLogoRec = Record
    SponsorLogoIndex: SmallInt;
    SponsorLogoName: String[20];
    SponsorLogoFilename: String[20];
  end;

  PromoLogoRec = Record
    PromoLogoIndex: SmallInt;
    PromoLogoName: String[20];
    PromoLogoFilename: String[20];
  end;

  GamePhaseRec = record
    League: String[10];
    ST_Phase_Code: SmallInt;
    SI_Phase_Code: SmallInt;
    CSS_Phase_Code: SmallInt;
    NFLDM_Phase_Code: SmallInt;
    Label_Period: String[20];
    Display_Period: String[10];
    End_Is_Half: Boolean;
    Display_Half_Short: String[10];
    Display_Half: String[20];
    Display_Final_Short: String[10];
    Display_Final: String[20];
    Display_Extended1: String[10];
    Display_Extended2: String[10];
    Game_OT: Boolean;
  end;

  TemplateDefsRec = record
    Template_ID: SmallInt;
    Template_Type: SmallInt;
    Template_Description: String[100];
    Record_Type: SmallInt;
    Engine_Template_ID: SmallInt;
    Default_Dwell: Word;
    ManualLeague: Boolean;
    EnableForOddsOnly: Boolean;
    UsesGameData: Boolean;
    RequiredGameState: SmallInt;
    Template_Has_Children: Boolean;
    Template_Is_Child: Boolean;
    StartEnableDateTime: TDateTime;
    EndEnableDateTime: TDateTime;
  end;

  ChildTemplateIDRec = record
    Template_ID: SmallInt;
    Child_Template_ID: SmallInt;
    Child_Default_Enable_State: Boolean;
  end;

  TemplateFieldsRec = record
    Template_ID: SmallInt;
    Field_ID: SmallInt;
    Field_Type: SmallInt;
    Field_Is_Manual: Boolean;
    Field_Label: String[50];
    Field_Prefix: String[50];
    Field_Contents: String[50];
    Field_Suffix: String[50];
    Field_Max_Length: SmallInt;
    Engine_Field_ID: SmallInt;
  end;

  CategoryRec = record
    Category_ID: SmallInt;
    Category_Name: String[50];
    Category_Label: String[10];
    Category_Description: String[100];
    Category_Is_Sport: Boolean;
    Category_Sport: String[10];
    Primary_Data_Source: String[20];
    Backup_Data_Source: String[20];
  end;

  CategoryTemplatesRec = record
    Category_ID: SmallInt;
    Template_ID: SmallInt;
    Template_Type: SmallInt;
    Template_Description: String[100];
    Template_IsOddsOnly: Boolean;
  end;

  SportRec = record
    Sport_Mnemonic: String[10];
    Sport_Description: String[50];
    Sport_Games_Table_Name: String[100];
    Sport_Stats_Query: String[250];
  end;

  GameRec = record
    DataFound: Boolean;
    SI_GCode: String[50];
    ST_Game_ID: String[50];
    CSS_GameID: LongInt;
    NFLDM_GameID: LongInt;
    DataSource: String[10];
    League: String[50];
    DoubleHeader: Boolean;
    DoubleHeaderGameNumber: SmallInt;
    HName: String[50];
    VName: String[50];
    HMnemonic: String[10];
    VMnemonic: String[10];
    HNFLMnemonic: String[10];
    VNFLMnemonic: String[10];
    HRank: String[4];
    VRank: String[4];
    HRecord: String[10];
    VRecord: String[10];
    HScore: SmallInt;
    VScore: SmallInt;
    GameState: SmallInt;
    DisplayPhase: String[20];
    DisplayClock: String[8];
    Count: String[5];
    Situation: String[50];
    Down: SmallInt;
    YardsToGo: SmallInt;
    Yardline: String[20];
    HomeTeamHasPossession: Boolean;
    Description: String[100];
    Date: TDateTime;
    StartTime: TDateTime;
    Visitor_Param1: SmallInt;
    Home_Param1: SmallInt;
    CSSNote1Text: String[255];
    CSSNote2Text: String[255];
    HCollegeLogoName: String[150];
    VCollegeLogoName: String[150];
  end;

  RecordTypeRec = record
    Playlist_Type: SmallInt;
    Record_Type: SmallInt;
    Record_Description: String[50];
  end;

  LineupText = Record
    UseLineup: Boolean;
    TextFields: Array[1..5] of String;
  end;

  EngineRec = Record
    Enabled: Boolean;
    IPAddress: String;
    Port: String;
  end;

  PlaylistInfoRec = record
    PlaylistName: String[50];
    LastPlaylistSaveTimeString: String[50];
  end;

  StyleChipRec = record
    StyleChip_Index: SmallInt;
    StyleChip_Description: String[50];
    StyleChip_Code: String[4];
    StyleChip_Type: SmallInt;
    StyleChip_String: String[20];
    StyleChip_FontCode: SmallInt;
    StyleChip_CharacterCode: SmallInt;
  end;

  CustomNewsHeaderRec = record
    HeaderIndex: SmallInt;
    HeaderText: String[20];
  end;

  AutomatedLeagueRec = record
    SI_LeagueCode: String[10];
    ST_LeagueCode: String[10];
    CSS_LeagueCode: String[10];
    Display_Mnemonic: String[10];
    Primary_Source: String[10];
    Backup_Source: String[10];
  end;

  WeatherIconRec = record
    Icon_Description: String[50];
    Icon_Name: String[50];
  end;

  FootballSituationRec = record
    VisitorHasPossession: Boolean;
    HomeHasPossession: Boolean;
    YardsFromGoal: SmallInt;
  end;

  NFLWeekInfoRec = record
    SeasonYear: SmallInt;
    Week: SmallInt;
    SeasonType: SmallInt;
  end;

  DisplayFeatureFlagsRec = record
    EnablePossessionIndicator: Boolean;
    EnableDownYardage: Boolean;
    EnableFieldPosition: Boolean;
    EnableWinnerIndicator: Boolean;
    EnableLiveUpdates: Boolean;
  end;

  ScheduleGameRec = record
    CSS_GameID: LongInt;
    NFLDM_GameID: LongInt;
    SI_GameID: String[50];
    ST_GameID: String[10];
    Description: String[30];
  end;

  ConferenceChipRec = record
    Conference_Name: String[50];
    Conference_Chip_Character_Value: SmallInt;
    Conference_Chip_Text: String[50];
  end;

  NewsPromoUpdateRec = record
    Header: String[50];
    UserData: String[250];
  end;

  TwitterTalentRec = record
    Twitter_Handle: String[100];
    Talent_Name: String[100];
    List_Name: String[100];
    League_Mnemonic: String[10];
    Talent_Enable: Boolean;
  end;

  TwitterMessageRec = record
    Message_Found: Boolean;
    Twitter_Handle: String[100];
    Twitter_Message: String[150];
  end;

  ScoringUpdateRec = record
    ScoreDescription: String[255];
    NFLClubCode: String[10];
  end;

  GameTraxOverrideRec = Record
    Header: String[20];
    Template_ID: SmallInt;
    UserData: String[255];
    NFLDMGameID: LongInt;
    CSSGameID: LongInt;
  end;
  
var
  PrefsFile: TextFile;
  Operator: String;
  Description: String;
  Schedule_Game_Collection: TStCollection; //Collection used for current game schedule
  User_Collection: TStCollection; //Collection used for user logins
  StudioTicker_Collection: TStCollection; //Collection used for zipper entries
  StudioBug_Collection: TStCollection; //Collection used for zipper entries
  Gametrax_Collection: TStCollection; //Collection used for zipper entries
  Temp_StudioTicker_Collection: TStCollection; //Collection used for zipper entries
  Temp_StudioBug_Collection: TStCollection; //Collection used for zipper entries
  Temp_Gametrax_Collection: TStCollection; //Collection used for zipper entries
  Team_Collection: TStCollection; //Collection used for NCAA teams
  Ticker_Playout_Collection: TStCollection; //Collection used for zipper playout
  Overlay_Collection: TStCollection; //Collection used for overlays
  SponsorLogo_Collection: TStCollection; //Collection used for Sponsor logos
  PromoLogo_Collection: TStCollection; //Collection used for Promo logos
  Stat_Collection: TStCollection; //Collection used for stats types
  Temp_Stat_Collection: TStCollection; //Collection used for stats types
  Game_Phase_Collection: TStCollection; //Collection used for stats types
  Template_Defs_Collection: TStCollection; //Collection used for Template definitions
  Child_Template_ID_Collection: TStCollection; //Collection uses for child template ID definitions
  Template_Fields_Collection: TStCollection; //Collection for template fields
  Temporary_Fields_Collection: TStCollection; //Collection for template fields
  Categories_Collection: TStCollection; //Collection for ticker categories
  Category_Templates_Collection: TStCollection; //Collection for templates that apply to ticker categories
  Sports_Collection: TStCollection; //Collection for ticker categories
  RecordType_Collection: TStCollection; //Collection for record types
  StyleChip_Collection: TStCollection; //Collection for style chips
  CustomNewsHeader_Collection: TStCollection; //Collection for league codes
  Automated_League_Collection: TStCollection; //Collection used for leagues
  Weather_Icon_Collection: TStCollection; //Collection used for weather icons
  Conference_Chip_Collection: TStCollection; //Collection used for NCAA conference chips
  NewsPromo_Update_Collection: TStCollection; //Collection used for temporaray storage of news/promo updates
  Twitter_Talent_Collection: TStCollection; //Collection used for temporaray storage of news/promo updates

  DBConnectionString, DBConnectionString2, DBConnectionString3, DBConnectionString4: String;
  VDSSocialMediaDBConnectionString: String;
  StationID: SmallInt;
  SearchText: String;
  SpellCheckerDictionaryDir: String;
  LoginComplete: Boolean;
  LastPageIndex: SmallInt;
  LoopDefaultZipper: Boolean;
  CurrentTickerEntryIndex: SmallInt;
  CurrentBugEntryIndex: SmallInt;
  CurrentExtraLineEntryIndex: SmallInt;
  CurrentTickerPlaylistID: Double;
  CurrentBugPlaylistID: Double;
  CurrentExtraLinePlaylistID: Double;
  LastOverlay: String;
  NewOverlayPending: Boolean;
  ZipperColors: Array[0..8] of ColorRec;
  GameEntryMode: SmallInt; //1 = NCAA, 2 = NFL
  MaxCharsMatchupNotes, MaxCharsGameNotes: SmallInt;
  CurrentLeague: String;
  UseXMLGTServer: Boolean;
  SelectedRecordForCopy: SmallInt;
  SelectForCut: Boolean;
  TemporaryTickerDataArray: Array[1..1500] of TickerRec;
  TemporaryTickerDataCount: SmallInt;
  TemporaryBugDataArray: Array[1..500] of TickerRec;
  TemporaryBugDataCount: SmallInt;
  TemporaryGameTraxDataArray: Array[1..500] of TickerRec;
  TemporaryGameTraxDataCount: SmallInt;
  SocketConnected: Boolean;
  EngineParameters: EngineRec;
  Error_Condition: Boolean;
  ErrorLoggingEnabled: Boolean;
  AsRunLoggingEnabled: Boolean;
  PacketEnable: Boolean;
  CurrentTickerDisplayMode: SmallInt;
  CurrentSponsorLogoName: String;
  CurrentSponsorLogoDwell: SmallInt;
  RunningTicker: Boolean;
  TickerAbortFlag: Boolean;
  LoopTicker: Boolean;
  ErrorLogFile: TextFile; {File for logging hardware interface errors}
  AsRunLogFile: TextFile; {File for logging sponsors & GPIs}
  AsRunLogFileDirectoryPath: String;
  PlaylistInfo: Array[1..6] of PlaylistInfoRec;
  LogoClockMode: SmallInt;
  CurrentTimeZone: SmallInt;
  LogoClockEnabled: Boolean;
  LastDuplicationCount: SmallInt;
  EngineIPAddress: String;
  NFLWeekInfo: NFLWeekInfoRec;
  UseBackupDataSource: Boolean;
  AllowDataSourceSelection: Boolean;
  CurrentDataSource: String;
  DisplayFeatureFlags: DisplayFeatureFlagsRec;
  NFLSeasonWeekInfoSource: SmallInt;
  ConnectToCSS: Boolean;
  LastPageWasFantasyStat: Boolean;
  Force16x9Mode: Boolean;

  //To select seeding instead of ranking for NCAA Tournament
  UseNCAASeeding: Boolean;

  Twitter_Manual_Template_NFL: SmallInt;
  Twitter_Manual_Template_NFL_No_Subheader: SmallInt;
  Twitter_Manual_Template_Non_NFL: SmallInt;
  Twitter_Manual_Template_Non_NFL_No_Subheader: SmallInt;

  ForceUpperCaseScoringPlayDescription: Boolean;

  //For NCAA Tournament
  UseNCAATournamentHeaders: Boolean;

  EnableTeamRecordsForNCAATournament: Boolean;

  //Added to support notes template for NCAA Tournament
  NCAATournamentLookNotesTemplateID: SmallInt;

const
  //Default delay for pages
  DEFAULTDELAY = 5000;

  //For playlist modes
  TICKER = 1;
  BUG = 2;
  GAMETRAX = 3;

  //For control
  NOENTRYINDEX = -1;

  //For record moves
  UP = -1;
  DOWN = 1;

  //Football symbols
  FOOTBALLPOSSESSION = #202;
  FOOTBALLPOSSESSIONREDZONE = #201;
  FOOTBALLNOPOSSESSION = #181;

  FIELDARROWLEFT = #180;
  FIELDARROWRIGHT = #181;

  //Flags for special animations
  SHOW_WINNER_INDICATOR = '1';
  HIDE_WINNER_INDICATOR = '0';

  //Used for space between team records and team names
  HALFSPACE = #183;

  //Define stats types
  //CFB
  PASSING = 1;
  RUSHING = 2;
  RECEIVING = 3;
  KICKING = 4;
  DEFENSIVE = 5;
  //CBB
  POINTS = 6;
  FIELDGOALS = 7;
  THREEPTFIELDGOALS = 8;
  COMBINEDFIELDGOALS = 9;
  FOULS = 10;

  //For Fantasy overrides
  FANTASY_START_TEMPLATTE = 44;
  FANTASY_END_TEMPLATTE = 46;

  //For news/promos overrides
  NEWS_PROMO_TEMPLATE = 1;

  //For scoring update play description
  NEUTRAL_SCORING_UPDATE_PLAY_DESCRIPTION = 9996;
  VISITOR_SCORING_UPDATE_PLAY_DESCRIPTION = 9997;
  HOME_SCORING_UPDATE_PLAY_DESCRIPTION = 9998;
  SCORE_ALERT_ANIMATION = 9999;

  DEFAULT_COLLEGE_LOGO_NAME = 'CFB_PLACEHOLDER';

implementation

end.
