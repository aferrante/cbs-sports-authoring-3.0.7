object EnableDateTimeEditorDlg: TEnableDateTimeEditorDlg
  Left = 829
  Top = 440
  BorderStyle = bsDialog
  Caption = 'Playlist Entry Option Editor'
  ClientHeight = 369
  ClientWidth = 297
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 16
    Top = 8
    Width = 265
    Height = 305
    BevelWidth = 2
    TabOrder = 0
    object Label41: TLabel
      Left = 15
      Top = 180
      Width = 110
      Height = 16
      Caption = 'Start Date/Time'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label42: TLabel
      Left = 15
      Top = 244
      Width = 105
      Height = 16
      Caption = 'End Date/Time'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label38: TLabel
      Left = 16
      Top = 28
      Width = 96
      Height = 16
      Caption = 'Editorial Note'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 16
      Top = 123
      Width = 153
      Height = 16
      Caption = 'Dwell Time (Seconds)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object EntryStartEnableDate: TDateTimePicker
      Left = 16
      Top = 200
      Width = 105
      Height = 24
      Date = 38760.853829386570000000
      Time = 38760.853829386570000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object EntryStartEnableTime: TDateTimePicker
      Left = 136
      Top = 200
      Width = 112
      Height = 24
      Date = 38760.853983645840000000
      Time = 38760.853983645840000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Kind = dtkTime
      ParentFont = False
      TabOrder = 1
    end
    object EntryEndEnableDate: TDateTimePicker
      Left = 16
      Top = 264
      Width = 105
      Height = 24
      Date = 38760.855092766210000000
      Time = 38760.855092766210000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object EntryEndEnableTime: TDateTimePicker
      Left = 136
      Top = 264
      Width = 112
      Height = 24
      Date = 38760.855219178250000000
      Time = 38760.855219178250000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Kind = dtkTime
      ParentFont = False
      TabOrder = 3
    end
    object EntryNote: TEdit
      Left = 16
      Top = 48
      Width = 232
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 100
      ParentFont = False
      TabOrder = 4
    end
    object NoteChangeEnable: TCheckBox
      Left = 14
      Top = 8
      Width = 145
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Change All Notes'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      Visible = False
    end
    object DwellTimeEnable: TCheckBox
      Left = 14
      Top = 88
      Width = 188
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Change All Dwell Times'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      Visible = False
    end
    object EntryDwellTime: TSpinEdit
      Left = 179
      Top = 116
      Width = 53
      Height = 26
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      MaxValue = 600
      MinValue = 1
      ParentFont = False
      TabOrder = 7
      Value = 2
    end
  end
  object BitBtn1: TBitBtn
    Left = 52
    Top = 328
    Width = 89
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 156
    Top = 328
    Width = 89
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Kind = bkCancel
  end
end
