object dmMain: TdmMain
  OldCreateOrder = False
  Left = 177
  Height = 838
  Width = 1280
  object dbCBS: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB'
    Left = 16
    Top = 27
  end
  object tblTicker_Groups: TADOTable
    Connection = dbCBS
    IndexFieldNames = 'Playlist_Description'
    TableName = 'Ticker_Groups'
    Left = 104
    Top = 147
  end
  object tblTicker_Elements: TADOTable
    Connection = dbCBS
    TableName = 'Ticker_Elements'
    Left = 104
    Top = 91
  end
  object dsTicker_Groups: TDataSource
    DataSet = tblTicker_Groups
    Left = 264
    Top = 147
  end
  object Query1: TADOQuery
    Connection = dbCBS
    Parameters = <>
    Left = 392
    Top = 32
  end
  object dsQuery1: TDataSource
    DataSet = Query1
    Left = 512
    Top = 30
  end
  object tblCustom_Segments: TADOTable
    Connection = dbCBS
    TableName = 'Custom_Segments'
    Left = 104
    Top = 27
  end
  object tblScheduled_Ticker_Groups: TADOTable
    Connection = dbCBS
    TableName = 'Scheduled_Ticker_Groups'
    Left = 104
    Top = 211
  end
  object dsScheduled_Ticker_Groups: TDataSource
    DataSet = tblScheduled_Ticker_Groups
    Left = 265
    Top = 211
  end
  object tblSponsor_Logos: TADOTable
    Connection = dbCBS
    TableName = 'Sponsor_Logos'
    Left = 394
    Top = 279
  end
  object dsSponsor_Logos: TDataSource
    DataSet = tblSponsor_Logos
    Left = 514
    Top = 280
  end
  object tblGame_Notes: TADOTable
    Connection = dbCBS
    TableName = 'Game_Notes'
    Left = 392
    Top = 212
  end
  object tblLeagues: TADOTable
    Connection = dbCBS
    TableName = 'Leagues'
    Left = 394
    Top = 415
  end
  object tblPromo_Logos: TADOTable
    Connection = dbCBS
    TableName = 'Promo_Logos'
    Left = 394
    Top = 351
  end
  object dsPromo_Logos: TDataSource
    DataSet = tblPromo_Logos
    Left = 514
    Top = 352
  end
  object dbSportBase: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=SportBase'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 632
    Top = 32
  end
  object SportbaseQuery: TADOQuery
    Connection = dbSportBase
    Parameters = <>
    Left = 632
    Top = 192
  end
  object dsSportbaseQuery: TDataSource
    DataSet = SportbaseQuery
    Left = 768
    Top = 192
  end
  object Query2: TADOQuery
    Connection = dbCBS
    Parameters = <>
    Left = 392
    Top = 88
  end
  object dsQuery2: TDataSource
    DataSet = Query2
    Left = 512
    Top = 86
  end
  object Query3: TADOQuery
    Connection = dbCBS
    Parameters = <>
    Left = 392
    Top = 152
  end
  object SportbaseQuery2: TADOQuery
    Connection = dbSportBase
    Parameters = <>
    Left = 632
    Top = 272
  end
  object tblBug_Elements: TADOTable
    Connection = dbCBS
    TableName = 'Bug_Elements'
    Left = 104
    Top = 275
  end
  object tblBug_Groups: TADOTable
    Connection = dbCBS
    TableName = 'Bug_Groups'
    Left = 104
    Top = 331
  end
  object tblScheduled_Bug_Groups: TADOTable
    Connection = dbCBS
    TableName = 'Scheduled_Bug_Groups'
    Left = 104
    Top = 395
  end
  object dsBug_Groups: TDataSource
    DataSet = tblBug_Groups
    Left = 264
    Top = 331
  end
  object dsScheduled_Bug_Groups: TDataSource
    DataSet = tblScheduled_Bug_Groups
    Left = 265
    Top = 395
  end
  object tblGameTrax_Elements: TADOTable
    Connection = dbCBS
    TableName = 'GameTrax_Elements'
    Left = 104
    Top = 451
  end
  object tblGameTrax_Groups: TADOTable
    Connection = dbCBS
    TableName = 'GameTrax_Groups'
    Left = 104
    Top = 61
  end
  object tblScheduled_GameTrax_Groups: TADOTable
    Connection = dbCBS
    TableName = 'Scheduled_GameTrax_Groups'
    Left = 104
    Top = 125
  end
  object dsGameTrax_Groups: TDataSource
    DataSet = tblGameTrax_Groups
    Left = 264
    Top = 61
  end
  object dsScheduled_GameTrax_Groups: TDataSource
    DataSet = tblScheduled_GameTrax_Groups
    Left = 265
    Top = 125
  end
  object SportbaseQuery3: TADOQuery
    Connection = dbSportBase
    Parameters = <>
    Left = 632
    Top = 336
  end
  object dsSportbaseQuery3: TDataSource
    DataSet = SportbaseQuery3
    Left = 736
    Top = 336
  end
  object tblTemplate_Defs: TADOTable
    Connection = dbCBS
    TableName = 'Template_Defs_2010'
    Left = 104
    Top = 181
  end
  object tblTeams: TADOTable
    Connection = dbSportBase
    TableName = 'Teams'
    Left = 630
    Top = 527
  end
  object dsTeams: TDataSource
    DataSet = tblTeams
    Left = 766
    Top = 528
  end
  object tblGame_Phase_Codes: TADOTable
    Connection = dbCBS
    TableName = 'Game_Phase_Codes_2010'
    Left = 630
    Top = 89
  end
  object dsGame_Phase_Codes: TDataSource
    DataSet = tblGame_Phase_Codes
    Left = 766
    Top = 90
  end
  object tblCustom_News_Headers: TADOTable
    Connection = dbCBS
    TableName = 'Custom_News_Headers'
    Left = 104
    Top = 253
  end
  object dsLeague_Codes: TDataSource
    DataSet = tblCustom_News_Headers
    Left = 265
    Top = 253
  end
  object dbCSS: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=CompScoreSys'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 912
    Top = 32
  end
  object CSSQuery: TADOQuery
    Connection = dbCSS
    Parameters = <>
    Left = 912
    Top = 88
  end
  object dbNFLDatamart: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=NFLDataMart'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 1120
    Top = 32
  end
  object DatamartQuery: TADOQuery
    Connection = dbNFLDatamart
    Parameters = <>
    Left = 1120
    Top = 88
  end
  object dsCSSQuery: TDataSource
    DataSet = CSSQuery
    Left = 992
    Top = 86
  end
  object dsDatamartQuery: TDataSource
    DataSet = DatamartQuery
    Left = 1224
    Top = 86
  end
  object tblFlags: TADOTable
    Connection = dbCBS
    TableName = 'Flags'
    Left = 104
    Top = 309
  end
  object CSSQuery2: TADOQuery
    Connection = dbCSS
    Parameters = <>
    Left = 912
    Top = 152
  end
  object DatamartQuery2: TADOQuery
    Connection = dbNFLDatamart
    Parameters = <>
    Left = 1120
    Top = 152
  end
  object tblNFLGames: TADOTable
    Connection = dbCBS
    TableName = 'NFL_Games'
    Left = 104
    Top = 373
  end
  object DatamartQuery3: TADOQuery
    Connection = dbNFLDatamart
    Parameters = <>
    Left = 1120
    Top = 224
  end
  object tblGameTrax_Regional_Game_Exclusions: TADOTable
    Connection = dbCBS
    TableName = 'GameTrax_Regional_Game_Exclusions'
    Left = 104
    Top = 509
  end
  object dsGameTrax_Regional_Game_Exclusions: TDataSource
    DataSet = tblGameTrax_Regional_Game_Exclusions
    Left = 265
    Top = 501
  end
  object tblGame_Updates: TADOTable
    Connection = dbCBS
    TableName = 'Game_Updates'
    Left = 104
    Top = 573
  end
  object SportbaseQuery4: TADOQuery
    Connection = dbSportBase
    Parameters = <>
    Left = 632
    Top = 400
  end
  object dsCFB_Conference_Chips: TDataSource
    DataSet = tblCFB_Conference_Chips
    Left = 766
    Top = 146
  end
  object tblCFB_Conference_Chips: TADOTable
    Connection = dbCBS
    TableName = 'CFB_Conference_Chips_2010'
    Left = 630
    Top = 137
  end
  object SportbaseQuery5: TADOQuery
    Connection = dbSportBase
    Parameters = <>
    Left = 632
    Top = 456
  end
  object tblFantasy_Updates: TADOTable
    Connection = dbCBS
    TableName = 'Fantasy_Updates'
    Left = 264
    Top = 645
  end
  object tblNFL_GSIS_Overrides: TADOTable
    Connection = dbCBS
    TableName = 'NFL_GSIS_Overrides'
    Left = 104
    Top = 640
  end
  object dsNFL_GSIS_Overrides: TDataSource
    DataSet = tblNFL_GSIS_Overrides
    Left = 264
    Top = 568
  end
  object tblNews_Promo_Updates: TADOTable
    Connection = dbCBS
    TableName = 'News_Promo_Updates'
    Left = 104
    Top = 704
  end
  object tblTwitterGameAssignments: TADOTable
    Connection = dbCBS
    TableName = 'Talent_Twitter_Game_Assignments'
    Left = 104
    Top = 757
  end
  object dsTwitterGameAssignments: TDataSource
    DataSet = tblTwitterGameAssignments
    Left = 273
    Top = 757
  end
  object dsTwitterMessages: TDataSource
    DataSet = TwitterMessagesQuery
    Left = 1161
    Top = 365
  end
  object dbVDSSocialMediaGateway: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB'
    Left = 920
    Top = 365
  end
  object tblTwitterTalentInfo: TADOTable
    Connection = dbCBS
    TableName = 'Talent_Twitter_Info'
    Left = 424
    Top = 757
  end
  object dsTwitterTalentInfo: TDataSource
    DataSet = tblTwitterTalentInfo
    Left = 593
    Top = 757
  end
  object TwitterDataQuery: TADOQuery
    Connection = dbCBS
    Parameters = <>
    Left = 720
    Top = 752
  end
  object TwitterMessagesQuery: TADOQuery
    Connection = dbVDSSocialMediaGateway
    Parameters = <>
    Left = 1048
    Top = 365
  end
  object tblTwitterMessages: TADOTable
    Connection = dbVDSSocialMediaGateway
    TableName = 'messages'
    Left = 1048
    Top = 421
  end
  object DatamartQuery4: TADOQuery
    Connection = dbNFLDatamart
    Parameters = <>
    Left = 1120
    Top = 288
  end
  object tblTournament_Headers: TADOTable
    Connection = dbCBS
    TableName = 'Tournament_Headers'
    Left = 408
    Top = 640
  end
  object dsTournament_Headers: TDataSource
    DataSet = tblTournament_Headers
    Left = 528
    Top = 640
  end
  object Query6: TADOQuery
    Connection = dbCBS
    Parameters = <>
    Left = 636
    Top = 611
  end
  object tblCFB_Team_Logos_Names: TADOTable
    Connection = dbCBS
    TableName = 'CFB_Team_Logos_Names'
    Left = 394
    Top = 479
  end
  object dsCFB_Team_Logos_Names: TDataSource
    DataSet = tblCFB_Team_Logos_Names
    Left = 522
    Top = 480
  end
  object dsTemplate_Defs: TDataSource
    DataSet = tblTemplate_Defs
    Left = 264
    Top = 179
  end
end
