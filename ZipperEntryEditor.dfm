object ZipperEntryEditorDlg: TZipperEntryEditorDlg
  Left = 125
  Top = 72
  BorderStyle = bsDialog
  Caption = 'Ticker Entry Editor'
  ClientHeight = 590
  ClientWidth = 1262
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = EntryPopup
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel11: TPanel
    Left = 280
    Top = 9
    Width = 969
    Height = 568
    BevelWidth = 2
    TabOrder = 0
    object Label1: TLabel
      Left = 262
      Top = 12
      Width = 166
      Height = 16
      Caption = 'Ticker Template Fields:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object SpellCheckIndicator: TApdStatusLight
      Left = 938
      Top = 522
      Width = 14
      Height = 30
      Lit = False
      LitColor = clLime
      NotLitColor = clBtnFace
    end
    object Label35: TLabel
      Left = 847
      Top = 530
      Width = 84
      Height = 16
      Caption = 'Spell Check'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object TemplateName: TLabel
      Left = 434
      Top = 12
      Width = 507
      Height = 16
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label16: TLabel
      Left = 262
      Top = 45
      Width = 185
      Height = 16
      Caption = 'Ticker Playlist Data Select'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DataModeLabel: TLabel
      Left = 457
      Top = 42
      Width = 185
      Height = 21
      Alignment = taCenter
      AutoSize = False
      Caption = 'STANDARD MODE'
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Label2: TLabel
      Left = 14
      Top = 52
      Width = 83
      Height = 16
      Caption = 'Style Chips:'
      FocusControl = BitBtn1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object BitBtn2: TBitBtn
      Left = 262
      Top = 524
      Width = 190
      Height = 30
      Caption = '&Spell Check'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = BitBtn2Click
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF003FF999903333
        333333377777FFF33333FF9FFFF9993333333F7F3FF7773FF333009F00F03399
        3333777F7737FF773F33FF9FFFF9933393333F73FFF7733373F300F999903333
        393377377777F33337F3FFFFFFF0333339333FF33337F333373300FFFFF03333
        93337733FFF7F3337333FFF00000333333333F377777FF33FF330FF0FF999339
        93337337F3777FF77F33FFF0F993993993333337F77377F77F33FFF003339939
        93333337733F77377FFFFFF03399933999933FF733777337777F000339933339
        93997773377F3FF77F7733333993993993993333377F77377F77333333999339
        9993333333777337777333333333333333333333333333333333}
      NumGlyphs = 2
    end
    object BitBtn4: TBitBtn
      Left = 485
      Top = 524
      Width = 190
      Height = 30
      Caption = 'Add &Playlist Entry'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 0
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333333FFFFFFFFF333333000000000033333377777777773333330FFFFF
        FFF03333337F333333373333330FFFFFFFF03333337F3FF3FFF73333330F00F0
        00F03333F37F773777373330330FFFFFFFF03337FF7F3F3FF3F73339030F0800
        F0F033377F7F737737373339900FFFFFFFF03FF7777F3FF3FFF70999990F00F0
        00007777777F7737777709999990FFF0FF0377777777FF37F3730999999908F0
        F033777777777337F73309999990FFF0033377777777FFF77333099999000000
        3333777777777777333333399033333333333337773333333333333903333333
        3333333773333333333333303333333333333337333333333333}
      NumGlyphs = 2
    end
    object BitBtn1: TBitBtn
      Left = 709
      Top = 524
      Width = 89
      Height = 30
      Caption = '&Cancel'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      Kind = bkCancel
    end
    object RecordGrid: TtsGrid
      Left = 261
      Top = 72
      Width = 692
      Height = 441
      CellSelectMode = cmNone
      CheckBoxStyle = stCheck
      Cols = 3
      DefaultRowHeight = 20
      ExportDelimiter = ','
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HeadingFont.Charset = DEFAULT_CHARSET
      HeadingFont.Color = clWindowText
      HeadingFont.Height = -13
      HeadingFont.Name = 'MS Sans Serif'
      HeadingFont.Style = [fsBold]
      HeadingHeight = 20
      HeadingParentFont = False
      ParentFont = False
      ParentShowHint = False
      Rows = 4
      ShowHint = False
      TabOrder = 3
      Version = '2.20.26'
      XMLExport.Version = '1.0'
      XMLExport.DataPacketVersion = '2.0'
      OnCellChanged = RecordGridCellChanged
      OnCellEdit = RecordGridCellEdit
      OnClick = RecordGridClick
      OnComboDropDown = RecordGridComboDropDown
      OnComboGetValue = RecordGridComboGetValue
      OnComboInit = RecordGridComboInit
      OnKeyDown = RecordGridKeyDown
      OnRowLoaded = RecordGridRowLoaded
      ColProperties = <
        item
          DataCol = 1
          Col.Heading = 'Index'
          Col.ReadOnly = True
          Col.Width = 49
        end
        item
          DataCol = 2
          Col.Heading = 'Field Description'
          Col.ReadOnly = True
          Col.Width = 205
        end
        item
          DataCol = 3
          Col.Heading = 'Field Contents (Standard Mode)'
          Col.Width = 408
        end>
    end
    object StyleChipsGrid: TtsGrid
      Left = 13
      Top = 72
      Width = 234
      Height = 441
      CheckBoxStyle = stCheck
      Cols = 2
      DefaultRowHeight = 20
      ExportDelimiter = ','
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      GridMode = gmListBox
      HeadingFont.Charset = DEFAULT_CHARSET
      HeadingFont.Color = clWindowText
      HeadingFont.Height = -13
      HeadingFont.Name = 'MS Sans Serif'
      HeadingFont.Style = [fsBold]
      HeadingHeight = 20
      HeadingParentFont = False
      ParentFont = False
      ParentShowHint = False
      Rows = 4
      RowSelectMode = rsSingle
      ShowHint = False
      TabOrder = 4
      Version = '2.20.26'
      XMLExport.Version = '1.0'
      XMLExport.DataPacketVersion = '2.0'
      OnDblClick = StyleChipsGridDblClick
      ColProperties = <
        item
          DataCol = 1
          Col.Heading = 'Code'
          Col.ReadOnly = True
          Col.Width = 45
        end
        item
          DataCol = 2
          Col.Heading = 'Description'
          Col.ReadOnly = True
          Col.Width = 205
        end>
    end
    object BitBtn3: TBitBtn
      Left = 35
      Top = 524
      Width = 193
      Height = 30
      Caption = 'Add Style Chip Code'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = BitBtn3Click
      Glyph.Data = {
        E6000000424DE60000000000000076000000280000000E0000000E0000000100
        0400000000007000000000000000000000001000000000000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3300333333333333330033333333333333003333300033333300333330F03333
        3300333330F033333300330000F000033300330FFFFFFF033300330000F00003
        3300333330F033333300333330F0333333003333300033333300333333333333
        33003333333333333300}
    end
  end
  object Panel4: TPanel
    Left = 9
    Top = 10
    Width = 264
    Height = 567
    BevelWidth = 2
    TabOrder = 1
    object Label38: TLabel
      Left = 16
      Top = 165
      Width = 96
      Height = 16
      Caption = 'Editorial Note'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label41: TLabel
      Left = 15
      Top = 245
      Width = 110
      Height = 16
      Caption = 'Start Date/Time'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label42: TLabel
      Left = 15
      Top = 309
      Width = 105
      Height = 16
      Caption = 'End Date/Time'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label43: TLabel
      Left = 14
      Top = 9
      Width = 141
      Height = 16
      Caption = 'Ticker Entry Options'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 16
      Top = 84
      Width = 153
      Height = 16
      Caption = 'Dwell Time (Seconds)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object SponsorLogoRegionLabel: TLabel
      Left = 16
      Top = 436
      Width = 152
      Height = 16
      Caption = 'Sponsor Logo Region'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object SponsorLogoRegionLabel2: TLabel
      Left = 35
      Top = 452
      Width = 115
      Height = 16
      Caption = '(0 = All Regions)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object EntryEnable: TCheckBox
      Left = 14
      Top = 50
      Width = 114
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Entry Enable'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 0
    end
    object EntryNote: TEdit
      Left = 16
      Top = 185
      Width = 232
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 100
      ParentFont = False
      TabOrder = 1
    end
    object EntryStartEnableDate: TDateTimePicker
      Left = 16
      Top = 265
      Width = 105
      Height = 24
      Date = 38760.853829386570000000
      Time = 38760.853829386570000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object EntryStartEnableTime: TDateTimePicker
      Left = 136
      Top = 265
      Width = 112
      Height = 24
      Date = 38760.853983645840000000
      Time = 38760.853983645840000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Kind = dtkTime
      ParentFont = False
      TabOrder = 3
    end
    object EntryEndEnableDate: TDateTimePicker
      Left = 16
      Top = 329
      Width = 105
      Height = 24
      Date = 38760.855092766210000000
      Time = 38760.855092766210000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
    end
    object EntryEndEnableTime: TDateTimePicker
      Left = 136
      Top = 329
      Width = 112
      Height = 24
      Date = 38760.855219178250000000
      Time = 38760.855219178250000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Kind = dtkTime
      ParentFont = False
      TabOrder = 5
    end
    object HiddenEdit: TEdit
      Left = 15
      Top = 377
      Width = 121
      Height = 21
      TabOrder = 6
      Visible = False
    end
    object EntryDwellTime: TSpinEdit
      Left = 179
      Top = 77
      Width = 53
      Height = 26
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      MaxValue = 600
      MinValue = 1
      ParentFont = False
      TabOrder = 7
      Value = 2
    end
    object SponsorLogoRegion: TSpinEdit
      Left = 179
      Top = 440
      Width = 62
      Height = 26
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      MaxValue = 20
      MinValue = 0
      ParentFont = False
      TabOrder = 8
      Value = 0
    end
    object PersistentLogoCheckbox: TCheckBox
      Left = 14
      Top = 122
      Width = 243
      Height = 17
      Caption = 'Make Sponsor Logo Persistent'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 9
    end
  end
  object AddictSpell31: TAddictSpell3
    ConfigStorage = csRegistry
    ConfigID = '%UserName%'
    ConfigFilename = '%AppDir%\Spell.cfg'
    ConfigRegistryKey = 'Software\Addictive Software\%AppName%'
    ConfigDictionaryDir.Strings = (
      '%AppDir%')
    ConfigAvailableOptions = [soUpcase, soNumbers, soPrimaryOnly, soRepeated, soDUalCaps]
    ConfigUseMSWordCustom = True
    ConfigDefaultMain.Strings = (
      'American.adm')
    ConfigDefaultActiveCustom = '%ConfigID%.adu'
    ConfigDefaultOptions = [soLiveSpelling, soLiveCorrect, soInternet, soAbbreviations, soRepeated, soDUalCaps]
    ConfigDefaultUseMSWordCustom = False
    SuggestionsAutoReplace = False
    SuggestionsLearning = True
    SuggestionsLearningDict = '%AppDir%\%UserName%_sp.adl'
    QuoteChars = '>'
    DialogInitialPos = ipLastUserPos
    DialogSelectionAvoid = saAvoid
    DialogShowImmediate = False
    DialogShowModal = False
    EndMessage = emExceptCancel
    EndCursorPosition = epOriginal
    EndMessageWordCount = False
    MaxUndo = -1
    MaxSuggestions = -1
    KeepDictionariesActive = False
    SynchronousCheck = True
    UseHourglassCursor = True
    CommandsVisible = [sdcIgnore, sdcIgnoreAll, sdcChange, sdcChangeAll, sdcAdd, sdcAutoCorrect, sdcUndo, sdcHelp, sdcCancel, sdcOptions, sdcCustomDictionary, sdcCustomDictionaries, sdcConfigOK, sdcAddedEdit, sdcAutoCorrectEdit, sdcExcludedEdit, sdcInternalEdit, sdcMainDictFolderBrowse, sdcResetDefaults]
    CommandsEnabled = [sdcIgnore, sdcIgnoreAll, sdcChange, sdcChangeAll, sdcAdd, sdcAutoCorrect, sdcUndo, sdcHelp, sdcCancel, sdcOptions, sdcCustomDictionary, sdcCustomDictionaries, sdcConfigOK, sdcAddedEdit, sdcAutoCorrectEdit, sdcExcludedEdit, sdcInternalEdit, sdcMainDictFolderBrowse, sdcResetDefaults]
    PhoneticSuggestions = True
    PhoneticMaxDistance = 4
    PhoneticDivisor = 2
    PhoneticDepth = 2
    MappingAutoReplace = True
    UseExcludeWords = True
    UseAutoCorrectFirst = False
    RecheckReplacedWords = True
    ResumeFromLastPosition = True
    AllowedCases = cmInitialCapsOrUpcase
    UILanguage = ltEnglish
    UIType = suiDialog
    UILanguageFontControls.Charset = DEFAULT_CHARSET
    UILanguageFontControls.Color = clWindowText
    UILanguageFontControls.Height = -11
    UILanguageFontControls.Name = 'MS Sans Serif'
    UILanguageFontControls.Style = []
    UILanguageFontText.Charset = DEFAULT_CHARSET
    UILanguageFontText.Color = clWindowText
    UILanguageFontText.Height = -11
    UILanguageFontText.Name = 'MS Sans Serif'
    UILanguageFontText.Style = []
    UILanguageUseFonts = False
    Left = 234
    Top = 21
  end
  object EntryPopup: TPopupMenu
    Left = 290
    Top = 20
    object CuttoClipboard1: TMenuItem
      Caption = 'Cu&t'
      OnClick = CuttoClipboard1Click
    end
    object CopyfromClipboard1: TMenuItem
      Caption = '&Copy'
      OnClick = CopyfromClipboard1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Paste1: TMenuItem
      Caption = '&Paste'
      OnClick = Paste1Click
    end
  end
end
