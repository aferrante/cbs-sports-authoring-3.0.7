unit TwitterMessageEditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids_ts, TSGrid, TSDBGrid, ExtCtrls, DBCtrls;

type
  TTwitterMessageEditorDlg = class(TForm)
    TwitterMessagesGrid: TtsDBGrid;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    DBNavigator1: TDBNavigator;
    First: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Panel1: TPanel;
    Label2: TLabel;
    LeagueSelectComboBox: TComboBox;
    LeagueFilterEnable: TCheckBox;
    Label6: TLabel;
    AddMessagesBtn: TBitBtn;
    NoteLabel: TLabel;
    UseTemplateWithoutSubHeader: TCheckBox;
    Label7: TLabel;
    OrderByHandleName: TCheckBox;
    procedure LeagueFilterEnableClick(Sender: TObject);
    procedure LeagueSelectComboBoxChange(Sender: TObject);
    procedure AddMessagesBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure OrderByHandleNameClick(Sender: TObject);
  private
    procedure ApplyFilter;
  public
    InsertMode: Boolean;
  end;

var
  TwitterMessageEditorDlg: TTwitterMessageEditorDlg;

implementation

uses DataModule,
     Globals, Main;

{$R *.dfm}

procedure TTwitterMessageEditorDlg.LeagueFilterEnableClick(Sender: TObject);
begin
  ApplyFilter;
end;
procedure TTwitterMessageEditorDlg.LeagueSelectComboBoxChange(Sender: TObject);
begin
  ApplyFilter;
end;

//Procedure to apply filter to Twitter messages as specified
procedure TTwitterMessageEditorDlg.ApplyFilter;
var
  i: SmallInt;
  TwitterTalentRecPtr: ^TwitterTalentRec;
  CollectionCount: SmallInt;
  QueryStr: String;
begin
  //Refresh Talent collection
  try
    //Load in the data from the Twitter Talent table; iterate for all records in table
    Twitter_Talent_Collection.Clear;
    Twitter_Talent_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Talent_Twitter_Info WHERE League_Mnemonic = ' + QuotedStr(LeagueSelectComboBox.Text));
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to collection
        GetMem (TwitterTalentRecPtr, SizeOf(TwitterTalentRec));
        With TwitterTalentRecPtr^ do
        begin
          Twitter_Handle := dmMain.Query1.FieldByName('Twitter_Handle').AsString;
          Talent_Name := dmMain.Query1.FieldByName('Talent_Name').AsString;
          List_Name := dmMain.Query1.FieldByName('List_Name').AsString;
          League_Mnemonic := dmMain.Query1.FieldByName('League_Mnemonic').AsString;
          If (Twitter_Talent_Collection.Count <= 100) then
          begin
            //Add to collection
            Twitter_Talent_Collection.Insert(TwitterTalentRecPtr);
            Twitter_Talent_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;
  except

  end;

  //Enable/disable filter as specified
  if (LeagueFilterEnable.Checked) and (Twitter_Talent_Collection.Count > 0) then
  begin
    //Build query
    QueryStr := 'SELECT * FROM Messages WHERE ';
    for i := 0 to Twitter_Talent_Collection.Count-1 do
    begin
      TwitterTalentRecPtr := Twitter_Talent_Collection.At(i);
      if (TwitterTalentRecPtr^.League_Mnemonic = LeagueSelectComboBox.Text) then
        QueryStr := QueryStr + '(userName = ' + QuotedStr(TwitterTalentRecPtr^.Twitter_Handle) + ')' + ' OR ';
    end;
    //Remove last 'OR' from query string
    QueryStr := Copy(QueryStr, 1, Length(QueryStr)-4);
    if (OrderByHandleName.Checked) then
      QueryStr := QueryStr + ' ORDER BY userName ASC, dateTime DESC'
    else
      QueryStr := QueryStr + ' ORDER BY dateTime DESC, userName ASC';
    with dmMain.TwitterMessagesQuery do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add(QueryStr);
      Active := TRUE;
    end;
  end
  else begin
    with dmMain.TwitterMessagesQuery do
    begin
      QueryStr := 'SELECT * FROM Messages';
      if (OrderByHandleName.Checked) then
        QueryStr := QueryStr + ' ORDER BY userName ASC, dateTime DESC'
      else
        QueryStr := QueryStr + ' ORDER BY dateTime DESC, userName ASC';
      Active := FALSE;
      SQL.Clear;
      SQL.Add(QueryStr);
      Active := TRUE;
    end;
  end;
end;

//Add messages if operator didn't dump out
procedure TTwitterMessageEditorDlg.AddMessagesBtnClick(Sender: TObject);
var
  i, j, k: SmallInt;
  User_Data: Array[1..25] of String[255];
  EventGUID: TGUID;
  RowSelected: Boolean;
  GameTraxRecPtr: ^GameTraxRec;
  CurrentTemplateDefs: TemplateDefsRec;
  CategoryRecPtr: ^CategoryRec;
  SaveCurrentListRow, SaveTopListRow: Variant;
begin
  try
    //Save row in playlist to restore later
    SaveCurrentListRow := MainForm.PlaylistGrid.CurrentDataRow;
    SaveTopListRow := MainForm.PlaylistGrid.TopRow ;
    CategoryRecPtr := Categories_Collection.At(MainForm.LeagueTab.TabIndex);
    //Set template
    if (Trim(CategoryRecPtr^.Category_Sport) = 'NFL') then
    begin
      if (UseTemplateWithoutSubHeader.Checked) then
        CurrentTemplateDefs := MainForm.GetTemplateInformation(TWITTER_MANUAL_TEMPLATE_NFL_NO_SUBHEADER)
      else
        CurrentTemplateDefs := MainForm.GetTemplateInformation(TWITTER_MANUAL_TEMPLATE_NFL);
    end
    else begin
      if (UseTemplateWithoutSubHeader.Checked) then
        CurrentTemplateDefs := MainForm.GetTemplateInformation(TWITTER_MANUAL_TEMPLATE_NON_NFL_NO_SUBHEADER)
      else
        CurrentTemplateDefs := MainForm.GetTemplateInformation(TWITTER_MANUAL_TEMPLATE_NON_NFL);
    end;
    //Go to first record
    dmMain.TwitterMessagesQuery.First;
    for i := 1 to TwitterMessagesGrid.Rows do
    begin
      RowSelected := TwitterMessagesGrid.RowSelected[TwitterMessagesGrid.CurrentDataRow];
      if (RowSelected) then
      begin
        GetMem (GameTraxRecPtr, SizeOf(GameTraxRec));
        With GameTraxRecPtr^ do
        begin
          //Set values for collection record
          Event_Index := GameTrax_Collection.Count+1;
          CreateGUID(EventGUID);
          Event_GUID := EventGUID;
          Enabled := TRUE;
          Template_ID := CurrentTemplateDefs.Template_ID;
          Record_Type := MainForm.GetRecordTypeFromTemplateID(Template_ID);
          //Get category & league information
          CategoryRecPtr := Categories_Collection.At(MainForm.LeagueTab.TabIndex);
          if (Trim(CategoryRecPtr^.Category_Sport) <> '') then
            League := CategoryRecPtr^.Category_Sport
          else
            League := 'NONE';
          Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
          Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
          SI_GCode := '0';
          ST_Game_ID := '0'; //Default to blank for now
          CSS_GameID := 0;
          NFLDM_GameID := 0; //Default to blank for now
          Description := '';
          //Clear Is Child bit
          Is_Child := FALSE;
          //Add sponsor logo information
          SponsorLogo_Name := ' ';
          SponsorLogo_Dwell := 0;
          SponsorLogo_Region := 0;
          //Set Twitter data
          UserData[1] := '@' + dmMain.TwitterMessagesQuery.FieldByName('userName').AsString;
          UserData[2] := dmMain.TwitterMessagesQuery.FieldByName('message').AsString;
          //Set remaining fields
          for k := 3 to 25 do UserData[k] := User_Data[k];
          //Set the start/end enable times to the template defaults
          StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
          EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
          DwellTime := MainForm.GetTemplateInformation(Template_ID).Default_Dwell;
          Selected := FALSE;
          Comments := ' ';
          //Set append/insert mode
          if not (InsertMode) then
          begin
            //Insert object to collection
            GameTrax_Collection.Insert(GameTraxRecPtr);
            GameTrax_Collection.Pack;
          end
          else if (InsertMode) then
          begin
            //Insert object to collection
            if (MainForm.PlaylistGrid.CurrentDataRow > 0) then
              GameTrax_Collection.AtInsert(MainForm.PlaylistGrid.CurrentDataRow-1, GameTraxRecPtr)
            else
              GameTrax_Collection.Insert(GameTraxRecPtr);
          end;
        end;
      end;
      //Next record
      dmMain.TwitterMessagesQuery.Next;
    end;
    //Refresh grid
    MainForm.RefreshPlaylistGrid;
    //Refresh the fields grid
    MainForm.RefreshEntryFieldsGrid(CurrentTickerDisplayMode);
    //Restore current grid row
    if (SaveCurrentListRow < 1) then SaveCurrentListRow := 1;
    MainForm.PlaylistGrid.CurrentDataRow := SaveCurrentListRow;
    MainForm.PlaylistGrid.TopRow := SaveTopListRow;
  finally
    //Close form
    Close;
  end;
end;

//Apply filter on form activate
procedure TTwitterMessageEditorDlg.FormActivate(Sender: TObject);
begin
  ApplyFilter;
end;

//Handler for checkbox to sort by handle name
procedure TTwitterMessageEditorDlg.OrderByHandleNameClick(Sender: TObject);
begin
  ApplyFilter;
end;

end.
