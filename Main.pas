////////////////////////////////////////////////////////////////////////////////
// Authoring application for The Score Television Network
// V1.0  06/05/06  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// V1.0.1  06/06/06  M. Dilworth  Video Design Software
// Added support for "split" text fields for odds
////////////////////////////////////////////////////////////////////////////////
// V1.0.2  06/07/06  M. Dilworth  Video Design Software
// Fixed bug that caused corruption in playlists (missing SELECTED field in
// operations).
////////////////////////////////////////////////////////////////////////////////
// V1.0.3  06/09/06  M. Dilworth  Video Design Software
// Fixed bug that caused corruption in playlists (problem with parent templates)
////////////////////////////////////////////////////////////////////////////////
// V1.0.6  06/12/06  M. Dilworth  Video Design Software
// Fixed bug that locked out insertions after 500 playlist entries (changed to
// 1500 entries)
////////////////////////////////////////////////////////////////////////////////
// V1.0.7  06/13/06  M. Dilworth  Video Design Software
// Fixed bug with manual weather entry dropdowns
////////////////////////////////////////////////////////////////////////////////
// V1.0.8  06/16/06  M. Dilworth  Video Design Software
// Added special case to blank out odds data when not available
////////////////////////////////////////////////////////////////////////////////
// V1.0.9  06/22/06  M. Dilworth  Video Design Software
// Fixed bug where editing of bug start/end enable times didn't work; put in
// extra safeguards to prevent corrupted records in database
////////////////////////////////////////////////////////////////////////////////
// V1.0.10  06/29/06  M. Dilworth  Video Design Software
// Fixed bug that did not allow editing of dwell times via pop-up.
////////////////////////////////////////////////////////////////////////////////
// V1.0.11  08/23/06  M. Dilworth  Video Design Software
// Implemented modifications to support college sports and live event mode.
////////////////////////////////////////////////////////////////////////////////
// V1.0.12  08/29/06  M. Dilworth  Video Design Software
// Implemented modifications to prevent league name of NONE from showing up in
// lineup.
////////////////////////////////////////////////////////////////////////////////
// V1.0.13  09/19/06  M. Dilworth  Video Design Software
// Fixed problem with team lookup for football field position info function.
////////////////////////////////////////////////////////////////////////////////
// V1.0.14  10/11/06  M. Dilworth  Video Design Software
// Modified to close database connections on application close.
////////////////////////////////////////////////////////////////////////////////
// V1.0.15  10/13/06  M. Dilworth  Video Design Software
// Modified to support editing of game phase information.
////////////////////////////////////////////////////////////////////////////////
// V1.0.16  11/09/06  M. Dilworth  Video Design Software
// Modified so that get game data function works for leaegue = CBB (for ranks).
////////////////////////////////////////////////////////////////////////////////
// V1.0.18  08/02/07  M. Dilworth  Video Design Software
// Modified to include dialog for adding custom header categories.
////////////////////////////////////////////////////////////////////////////////
// V1.0.0  08/25/07  M. Dilworth  Video Design Software
// Starting build for CBS ticker
////////////////////////////////////////////////////////////////////////////////
// V1.0.1  08/25/07  M. Dilworth  Video Design Software
// Added support for switching to CSS as backup
////////////////////////////////////////////////////////////////////////////////
// V1.0.2  08/25/07  M. Dilworth  Video Design Software
// Added support for live stats
////////////////////////////////////////////////////////////////////////////////
// V1.2.0  09/12/07  M. Dilworth  Video Design Software
// Added support for live stats
////////////////////////////////////////////////////////////////////////////////
// V1.2.1  09/22/07  M. Dilworth  Video Design Software
// Modified to support Game Updates for CFB
////////////////////////////////////////////////////////////////////////////////
// V1.2.2  09/22/07  M. Dilworth  Video Design Software
// Modified to prevent jumping to top of grid after entry append/insert
////////////////////////////////////////////////////////////////////////////////
// V1.3.0  09/29/07  M. Dilworth  Video Design Software
// Modified to summport unified engine
////////////////////////////////////////////////////////////////////////////////
// V1.3.1  09/29/07  M. Dilworth  Video Design Software
// Modified to fix bug with addition of sponsor logos to playlist (caused engine
// to hang)
////////////////////////////////////////////////////////////////////////////////
// V1.3.2  10/06/07  M. Dilworth  Video Design Software
// Modified to include editor for NCAA conference chips
////////////////////////////////////////////////////////////////////////////////
// V1.3.4  10/25/07  M. Dilworth  Video Design Software
// Modified to include support for CFB stats
////////////////////////////////////////////////////////////////////////////////
// V1.4.0  11/24/07  M. Dilworth  Video Design Software
// Added support for College Basketball; misc. mods for CFB
////////////////////////////////////////////////////////////////////////////////
// V1.4.0.1  11/27/07  M. Dilworth  Video Design Software
// Added support for College Basketball stats
////////////////////////////////////////////////////////////////////////////////
// V1.4.0.2  12/01/07  M. Dilworth  Video Design Software
// Fixed bug where CSS did not work as backup source for CFB.
////////////////////////////////////////////////////////////////////////////////
// V1.4.0.3  12/04/07  M. Dilworth  Video Design Software
// Interim release
////////////////////////////////////////////////////////////////////////////////
// V1.4.0.4  12/05/07  M. Dilworth  Video Design Software
// Added support for specifying sponsors by region ID
////////////////////////////////////////////////////////////////////////////////
// V1.4.0.6  01/05/08  M. Dilworth  Video Design Software
// Added support for displaying league code in CSS games grid
////////////////////////////////////////////////////////////////////////////////
// V1.4.0.8  02/21/08  M. Dilworth  Video Design Software
// Modified display of day of week abbreviation for final games not from
// current day.
////////////////////////////////////////////////////////////////////////////////
// V1.5.0.0  03/13/08  M. Dilworth  Video Design Software
// Modified to work with live clocks from CSS for NCAA tournament.
////////////////////////////////////////////////////////////////////////////////
// V1.6.0.0  02/13/09  M. Dilworth  Video Design Software
// Modified to game exclusion for CFB & CBB games.
////////////////////////////////////////////////////////////////////////////////
// V1.6.1.0  08/19/08  M. Dilworth  Video Design Software
// Modified to use standard TClientSocket component to prevent hang if engine
// not running when Playout is launched.
////////////////////////////////////////////////////////////////////////////////
// V1.6.2.0  10/08/09  M. Dilworth  Video Design Software
// Modified to support Fantasy Stats
////////////////////////////////////////////////////////////////////////////////
// V1.6.3.0  11/27/09  M. Dilworth  Video Design Software
// Fixed bug where excluded games were not showing up.
////////////////////////////////////////////////////////////////////////////////
// V1.7.0.0  09/08/10  M. Dilworth  Video Design Software
// Modified to support new engine/look for NFL, 2010.
////////////////////////////////////////////////////////////////////////////////
// V1.7.1.0  09/17/10  M. Dilworth  Video Design Software
// Modified to support new engine/look for CFB/CBB, 2010.
////////////////////////////////////////////////////////////////////////////////
// V1.7.2.0  09/18/10  M. Dilworth  Video Design Software
// Modified to support new sponsor logos approach for CFB & CBB.
////////////////////////////////////////////////////////////////////////////////
// V1.7.3.0  12/04/10  M. Dilworth  Video Design Software
// Modified to allow combining CBB & CFB in CSS Scoreboard based playlists.
////////////////////////////////////////////////////////////////////////////////
// V1.7.4.0  03/26/11  M. Dilworth  Video Design Software
// Modified to support NCAA Seed Selection for NCAA Tournament
////////////////////////////////////////////////////////////////////////////////
// V2.0.0  09/02/11  M. Dilworth  Video Design Software
// Modified to support Twitter messages in ticker.
////////////////////////////////////////////////////////////////////////////////
// V2.0.1  09/11/11  M. Dilworth  Video Design Software
// Fixed minor bugs with Twitter implementation.
////////////////////////////////////////////////////////////////////////////////
// V2.0.2  09/17/11  M. Dilworth  Video Design Software
// Added additional filtering options to Twitter Message select dialog.
////////////////////////////////////////////////////////////////////////////////
// V2.0.3  09/28/11  M. Dilworth  Video Design Software
// Fixed bug that caused Twitter text to be cleared if editing data. Added
// support for previewing Score Alert animation and Score Alert Play.
////////////////////////////////////////////////////////////////////////////////
// V2.1.0  03/13/12  M. Dilworth  Video Design Software
// Modified to support INI-based selection of NCAA Tournament Headers for game
// template data entry.
////////////////////////////////////////////////////////////////////////////////
// V2.2.0  08/24/12  M. Dilworth  Video Design Software
// Modified to fix issues where data grids lost connection to data sources.
////////////////////////////////////////////////////////////////////////////////
// V2.3.0  10/11/12  M. Dilworth  Video Design Software
// Added support for CBS logos in league region (lower-left).
////////////////////////////////////////////////////////////////////////////////
// V2.4.0  10/12/12  M. Dilworth  Video Design Software
// Added support for CBS logos in conference chips.
////////////////////////////////////////////////////////////////////////////////
// V2.5.0  11/08/12  M. Dilworth  Video Design Software
// Added support for team records in NCAA games.
////////////////////////////////////////////////////////////////////////////////
// V2.6.0  03/14/13  M. Dilworth  Video Design Software
// Added support for team records in NCAA Tournament games, added support for
// editing NCAA Tournament headers for Gametrax Ticker.
////////////////////////////////////////////////////////////////////////////////
// V3.0.0  09/07/13  M. Dilworth  Video Design Software
// Modified to support new look for 2013 Backup Eyebar.
////////////////////////////////////////////////////////////////////////////////
// V3.0.1  09/13/13  M. Dilworth  Video Design Software
// Modified to support college templates and remove color/font formatting.
////////////////////////////////////////////////////////////////////////////////
// V3.0.3  09/26/13  M. Dilworth  Video Design Software
// Added support for editing default template dwell times.
////////////////////////////////////////////////////////////////////////////////
// V3.0.4  09/21/14  M. Dilworth  Video Design Software
// Added "GameKey" (NFL Game ID) column to CSS games grid for diagnostic
// purposes
////////////////////////////////////////////////////////////////////////////////
// V3.0.5  03/06/15  M. Dilworth  Video Design Software
// Modified to support persistent sponsor logos.
////////////////////////////////////////////////////////////////////////////////
// V3.0.6  03/14/15  M. Dilworth  Video Design Software
// Modified to support persistent sponsor logos.
////////////////////////////////////////////////////////////////////////////////
// V3.0.7  03/14/15  M. Dilworth  Video Design Software
// Modified to support display of Tournament notes template in preview mode.
////////////////////////////////////////////////////////////////////////////////
// Ticker Record types
// 0	- Mini-playlist
// 1	- Sponsor
// 2	- Promo
// 3	- News
// 4	- Program alert
// 5	- Opening odds
// 6	- Current odds
// 7	- Game preview
// 8	- Live game score
// 9	- Scoring update
// 10 - Final score
// 11 - Final score with notes
// 12 - Weather
// 13 - Futures

// Bug Record types
// 1 - Opening odds
// 2 - Current odds
// 3 - Live game score
// 4 - Final score

unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Spin, Buttons, ExtCtrls, Menus, Mask, ComCtrls, StBase,
  TSImageList, Grids_ts, TSGrid, TSDBGrid, DBCtrls, JPEG, StColl, MPlayer, OleCtrls,
  OleServer, Excel97, Grids, PBJustOne, ad3SpellBase, ad3Spell, AdStatLt, ClipBrd, TSMask,
  DBGrids, OoMisc, {AdStatLt,} Globals, INIFiles, Variants;

{H+} //ANSI strings

//Record type defs
type
  //Main progam object def
  TMainForm = class(TForm)
    ProgramModePageControl: TPageControl;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    Prefs1: TMenuItem;
    SetPrefs1: TMenuItem;
    Help1: TMenuItem;
    About1: TMenuItem;
    PlaylistDataEntry: TTabSheet;
    tsImageList: TtsImageList;
    tsImageList1: TtsImageList;
    tsImageList2: TtsImageList;
    DatabaseMaintenance: TTabSheet;
    DBMaintPanel: TPanel;
    PBJustOne1: TPBJustOne;
    Database1: TMenuItem;
    RepopulateTeamsCollection1: TMenuItem;
    TimeOfDayTimer: TTimer;
    ScheduleTab: TTabSheet;
    SchedulePanel: TPanel;
    N3: TMenuItem;
    PurgeScheduledEvents1DayOld1: TMenuItem;
    N4: TMenuItem;
    tsMaskDefs1: TtsMaskDefs;
    Panel29: TPanel;
    Label4: TLabel;
    BitBtn6: TBitBtn;
    BitBtn24: TBitBtn;
    Edit1: TEdit;
    ExpandButton: TBitBtn;
    PlaylistSelectTabControl: TTabControl;
    Panel9: TPanel;
    Label34: TLabel;
    Label69: TLabel;
    NumEntries: TLabel;
    BitBtn20: TBitBtn;
    BitBtn18: TBitBtn;
    Panel4: TPanel;
    Label38: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    EntryEnable: TCheckBox;
    EntryNote: TEdit;
    EntryStartEnableDate: TDateTimePicker;
    EntryStartEnableTime: TDateTimePicker;
    EntryEndEnableDate: TDateTimePicker;
    EntryEndEnableTime: TDateTimePicker;
    Panel16: TPanel;
    Label14: TLabel;
    TemplateDescription: TLabel;
    EntryFieldsGrid: TtsGrid;
    PlaylistGrid: TtsGrid;
    PlaylistPopupMenu: TPopupMenu;
    Copy1: TMenuItem;
    Paste1: TMenuItem;
    Duplicate1: TMenuItem;
    Delete1: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    Cut1: TMenuItem;
    Enable1: TMenuItem;
    Disable1: TMenuItem;
    N7: TMenuItem;
    AppendtoEnd1: TMenuItem;
    DuplicateandAppendtoEnd1: TMenuItem;
    N8: TMenuItem;
    EditStartEndEnableTimes1: TMenuItem;
    Panel27: TPanel;
    Label13: TLabel;
    Label15: TLabel;
    Label19: TLabel;
    Label37: TLabel;
    Label68: TLabel;
    ScheduleEntryStartDate: TDateTimePicker;
    ScheduleEntryEndDate: TDateTimePicker;
    Label24: TLabel;
    AvailablePlaylistsGrid: TtsDBGrid;
    Label25: TLabel;
    ScheduledPlaylistsGrid: TtsDBGrid;
    Panel26: TPanel;
    Label36: TLabel;
    BitBtn44: TBitBtn;
    BitBtn43: TBitBtn;
    Panel19: TPanel;
    Label40: TLabel;
    DatabaseMaintPlaylistGrid: TtsDBGrid;
    DBNavigator1: TDBNavigator;
    BitBtn32: TBitBtn;
    BitBtn34: TBitBtn;
    Panel1: TPanel;
    PlaylistModeLabel: TLabel;
    DataEntryPanel: TPanel;
    Panel5: TPanel;
    Label1: TLabel;
    Label11: TLabel;
    LeagueTab: TTabControl;
    SIGamesDBGrid: TtsDBGrid;
    SponsorsDBGrid: TtsDBGrid;
    AvailableTemplatesDBGrid: TtsDBGrid;
    AddToPlaylistBtn: TBitBtn;
    InsertIntoPlaylistBtn: TBitBtn;
    SponsorDwellPanel: TPanel;
    Label5: TLabel;
    SponsorLogoDwell: TSpinEdit;
    N9: TMenuItem;
    SelectAll1: TMenuItem;
    N10: TMenuItem;
    DeSelectAll1: TMenuItem;
    Panel2: TPanel;
    ApdStatusLight1: TApdStatusLight;
    Label6: TLabel;
    ApdStatusLight2: TApdStatusLight;
    ReconnecttoGraphicsEngine1: TMenuItem;
    ScheduleEntryStartTime: TDateTimePicker;
    ScheduleEntryEndTime: TDateTimePicker;
    Label2: TLabel;
    BitBtn42: TBitBtn;
    PromptForData: TCheckBox;
    Label7: TLabel;
    EntryDwellTime: TSpinEdit;
    Panel3: TPanel;
    DateTimeLabel: TLabel;
    PlaylistNameLabel: TLabel;
    DBMaintPopupMenu: TPopupMenu;
    BitBtn1: TBitBtn;
    Label8: TLabel;
    LastSaveTimeLabel: TLabel;
    ManualLeaguePanel: TPanel;
    Label3: TLabel;
    ManualLeagueSelect: TComboBox;
    Image1: TImage;
    ManualOverridePanel: TPanel;
    ManualOverrideBtn: TBitBtn;
    N12: TMenuItem;
    ClearGraphicsOutput1: TMenuItem;
    N13: TMenuItem;
    ClearGraphicsOutput2: TMenuItem;
    N14: TMenuItem;
    EditSponsorLogoDefinitions1: TMenuItem;
    EditTeamInformation1: TMenuItem;
    EditGamePhaseData1: TMenuItem;
    EditCustomHeaderCategories1: TMenuItem;
    GameTraxSetup: TTabSheet;
    Panel6: TPanel;
    Label17: TLabel;
    Panel7: TPanel;
    RegionID: TSpinEdit;
    Label10: TLabel;
    Label12: TLabel;
    BitBtn2: TBitBtn;
    Label26: TLabel;
    Panel8: TPanel;
    ExcludedGamesGrid: TtsDBGrid;
    Label9: TLabel;
    DBNavigator2: TDBNavigator;
    Label18: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    CSSGamesDBGrid: TtsDBGrid;
    NFLDMGamesDBGrid: TtsDBGrid;
    DataSourceLabel: TLabel;
    ExclusionGamesGrid: TtsGrid;
    ClearAllExcludedGames: TBitBtn;
    ScoreUpdatePanel: TPanel;
    NFLDMGamesDBGrid2: TtsDBGrid;
    Label16: TLabel;
    CSSGamesDBGrid2: TtsDBGrid;
    SIGamesDBGrid2: TtsDBGrid;
    CSSGameNotePanel: TPanel;
    BitBtn4: TBitBtn;
    BitBtn7: TBitBtn;
    N1: TMenuItem;
    ClearCFBTeamRanings1: TMenuItem;
    Label29: TLabel;
    SponsorLogoRegion: TSpinEdit;
    ApdStatusLight3: TApdStatusLight;
    FantasyUpdatePanel: TPanel;
    DisplayFantasyStatsBtn: TBitBtn;
    TemplateTimeEditPopupMenu: TPopupMenu;
    EditDefaultTemplateStartEndEnableTimes1: TMenuItem;
    Panel10: TPanel;
    EnablePassingStats: TCheckBox;
    EnableRushingStats: TCheckBox;
    EnableReceivingStats: TCheckBox;
    DeletePlaylist1: TMenuItem;
    GameOverridePanel: TPanel;
    Label30: TLabel;
    OverrideGamesGrid: TtsDBGrid;
    BitBtn3: TBitBtn;
    GSISOverridePopupMenu: TPopupMenu;
    AddGametoGSISOverrideList1: TMenuItem;
    Panel11: TPanel;
    EnableRegion1Fantasy: TCheckBox;
    EnableRegion2Fantasy: TCheckBox;
    EnableRegion3Fantasy: TCheckBox;
    EnableRegion4Fantasy: TCheckBox;
    EnableRegion5Fantasy: TCheckBox;
    EnableRegion6Fantasy: TCheckBox;
    EnableRegion7Fantasy: TCheckBox;
    EnableRegion8Fantasy: TCheckBox;
    NewsPromoUpdatePanel: TPanel;
    DisplayNewsPromoUpdateBtn: TBitBtn;
    Panel14: TPanel;
    EnableRegion1NewsPromos: TCheckBox;
    EnableRegion2NewsPromos: TCheckBox;
    EnableRegion3NewsPromos: TCheckBox;
    EnableRegion4NewsPromos: TCheckBox;
    EnableRegion5NewsPromos: TCheckBox;
    EnableRegion6NewsPromos: TCheckBox;
    EnableRegion7NewsPromos: TCheckBox;
    EnableRegion8NewsPromos: TCheckBox;
    NewsPromoUpdateGrid: TtsGrid;
    DeleteNewsPromoUpdateItem: TBitBtn;
    N2: TMenuItem;
    AddSelectedNotetoNewsPromoUpdateList1: TMenuItem;
    Options1: TMenuItem;
    UseNCAARank: TMenuItem;
    UseNCAASeed: TMenuItem;
    TwitterSetup: TTabSheet;
    Panel12: TPanel;
    Label31: TLabel;
    Panel13: TPanel;
    Label32: TLabel;
    Label33: TLabel;
    Label35: TLabel;
    Label39: TLabel;
    AddGameAssignmentBtn: TBitBtn;
    TwitterGamesGrid: TtsGrid;
    Panel15: TPanel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    TwitterGameAssignmentsGrid: TtsDBGrid;
    DBNavigator3: TDBNavigator;
    ClearAllGameAssignments: TBitBtn;
    TwitterTalentGrid: TtsGrid;
    Label51: TLabel;
    Panel17: TPanel;
    Label58: TLabel;
    ViewTwitterMessagesButton: TBitBtn;
    EditTalentTwitterInfo1: TMenuItem;
    DeleteTwitterMessagesButton: TBitBtn;
    Label52: TLabel;
    AddTwitterMessagesBtn: TBitBtn;
    InsertTwitterMessagesBtn: TBitBtn;
    TestScoreAlertAnimationBtn: TBitBtn;
    TestScoreAlertPlayBtn: TBitBtn;
    EditNCAATournamentRoundInfo: TMenuItem;
    EditCollegeTeamLogos: TMenuItem;
    E1: TMenuItem;
    PersistentSponsorCheckbox: TCheckBox;
    //General program functions
    procedure FormActivate(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    //Dialog functions
    procedure About1Click(Sender: TObject);
    procedure SetPrefs1Click(Sender: TObject);
    procedure StorePrefs1Click(Sender: TObject);
    procedure LoadPrefs;
    //Handlers for UI objects
    procedure ProgramModePageControlChange(Sender: TObject);
    procedure ScheduledPlaylistsGridDblClick(Sender: TObject);
    procedure PlaylistGridDblClick(Sender: TObject);
    procedure PurgeScheduledEvents1DayOld1Click(Sender: TObject);
    procedure LeagueTabChange(Sender: TObject);
    procedure ExpandButtonClick(Sender: TObject);
    procedure AddToPlaylistBtnClick(Sender: TObject);
    procedure InsertIntoPlaylistBtnClick(Sender: TObject);
    procedure AvailableTemplatesDBGridDblClick(Sender: TObject);
    procedure PlaylistGridRowChanged(Sender: TObject; OldRow, NewRow: Integer);
    procedure ScheduleSelectTabChange(Sender: TObject);
    procedure DatabaseMaintSelectTabChange(Sender: TObject);
    procedure PlaylistSelectTabControlChange(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn24Click(Sender: TObject);
    procedure BitBtn32Click(Sender: TObject);
    procedure BitBtn34Click(Sender: TObject);
    procedure BitBtn42Click(Sender: TObject);
    procedure BitBtn44Click(Sender: TObject);
    procedure BitBtn43Click(Sender: TObject);
    //Playlist operations
    procedure LoadZipperCollection;
    procedure RefreshEntryFieldsGrid(CurrentTickerMode: SmallInt);
    procedure RefreshPlaylistGrid;
    procedure DeleteGraphicFromZipperPlaylist(Confirm: Boolean);
    procedure DeleteZipperPlaylist (PlaylistType: SmallInt; Playlist_ID: Double; Playlist_Description: String);
    procedure AddPlaylistEntry;
    procedure InsertPlaylistEntry;
    function SaveTickerPlaylist(PlaylistType: SmallInt; ClearCollection: Boolean): Boolean;
    procedure DeletePlaylistFromSchedule(PlaylistType: SmallInt; Start_Enable_Time: Double);
    procedure RefreshScheduleGrid;
    procedure EditCurrentScheduleEntry;
    procedure AddScheduleEntry;
    //Utility functions
    procedure ReloadDataFromDatabase;
    function GetPlaylistCount(PlaylistType: SmallInt; SearchStr: String): SmallInt;
    procedure EditZipperPlaylistEntry;
    procedure LoadPlaylistCollection(PlaylistType: SmallInt; AppendMode: SmallInt; PlaylistID: Double);
    procedure RepopulateTeamsCollection1Click(Sender: TObject);
    procedure ReloadGamesfromGametrak1Click(Sender: TObject);
    function GetSTGameTimeString(GTimeStr: String): String;
    function GetStatInfo (StoredProcedureName: String): StatRec;
    function GetAutomatedLeagueDisplayMnemonic (League: String): String;
    function GetSTGameState(GTIME: String; GPHASE: SmallInt): Integer;
    function GetSportGamesTableName(SportMnemonic: String): String;
    function GetTemplateFieldsCount(Template_ID: SmallInt): SmallInt;
    function GetRecordTypeFromTemplateID(Template_ID: SmallInt): SmallInt;
    function GetTemplateInformation(TemplateID: SmallInt): TemplateDefsRec;
    procedure Delete1Click(Sender: TObject);
    procedure Enable1Click(Sender: TObject);
    procedure Disable1Click(Sender: TObject);
    procedure Copy1Click(Sender: TObject);
    procedure SelectForCopyOrCut;
    procedure Cut1Click(Sender: TObject);
    procedure Paste1Click(Sender: TObject);
    procedure AppendtoEnd1Click(Sender: TObject);
    procedure PasteEntries(AppendToEnd: Boolean);
    procedure DuplicateEntries(AppendToEnd: Boolean; DuplicationCount: SmallInt);
    procedure EditStartEndEnableTimes1Click(Sender: TObject);
    procedure Duplicate1Click(Sender: TObject);
    procedure DuplicateandAppendtoEnd1Click(Sender: TObject);
    function GetRecordTypeDescription (Playlist_Type: SmallInt; Record_Type: SmallInt): String;
    procedure TimeOfDayTimerTimer(Sender: TObject);
    //Procedures for hotkey operations
    procedure Cut;
    procedure Copy;
    procedure Paste;
    procedure PasteEnd;
    procedure Duplicate;
    procedure PlaylistGridKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SelectAll1Click(Sender: TObject);
    procedure DeSelectAll1Click(Sender: TObject);
    procedure SelectAll;
    procedure DeSelectAll;
    procedure MoveRecord(Direction: SmallInt);
    procedure PlaylistGridClick(Sender: TObject);
    procedure ReconnecttoGraphicsEngine1Click(Sender: TObject);
    procedure PlaylistGridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure AvailableTemplatesDBGridRowChanged(Sender: TObject; OldRow, NewRow: Variant);
    procedure PreviewCurrentPlaylistRecord;
    procedure BitBtn1Click(Sender: TObject);
    procedure ManualGameOverride;
    function ScrubText (InText: String):String;
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ShowOddsOnlyClick(Sender: TObject);
    procedure ClearGraphicsOutput1Click(Sender: TObject);
    procedure ClearGraphicsOutput2Click(Sender: TObject);
    procedure EditDefaultTemplateStartEndEnableTimes1Click(
      Sender: TObject);
    procedure EditSponsorLogoDefinitions1Click(Sender: TObject);
    procedure EditTeamInformation1Click(Sender: TObject);
    procedure EditGamePhaseData1Click(Sender: TObject);
    procedure EditPromoLogoInformation1Click(Sender: TObject);
    procedure EditCustomHeaderCategories1Click(Sender: TObject);
    procedure StorePrefs;
    procedure RefreshExclusionGamesGrid;
    procedure BitBtn2Click(Sender: TObject);
    procedure ClearAllExcludedGamesClick(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure ClearCFBTeamRanings1Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    function CheckForMultipleCollegeLeagues: Boolean;
    procedure DisplayFantasyStatsBtnClick(Sender: TObject);
    procedure DeletePlaylist1Click(Sender: TObject);
    procedure DeleteSelectedPlaylist;
    procedure BitBtn3Click(Sender: TObject);
    procedure CSSGamesDBGridDblClick(Sender: TObject);
    procedure AddGametoGSISOverrideList1Click(Sender: TObject);
    procedure AddCurrentCSSGameToOverrideList;

    //For News/Promos Updates
    procedure AddSelectedNotetoNewsPromoUpdateList1Click(Sender: TObject);
    procedure DeleteNewsPromoUpdateItemClick(Sender: TObject);
    procedure DisplayNewsPromoUpdateBtnClick(Sender: TObject);
    procedure RefreshNewsPromoUpdateGrid;
    //General procedure to add a News/Promo Update to the database
    procedure SaveNewsPromoUpdateToDatabase(RegionID: SmallInt);
    procedure UseNCAARankClick(Sender: TObject);
    procedure UseNCAASeedClick(Sender: TObject);

    //For Twitter support
    procedure RefreshTwitterTalentGrid;
    procedure RefreshTwitterGamesGrid;
    procedure ClearAllGameAssignmentsClick(Sender: TObject);
    procedure AddGameAssignmentBtnClick(Sender: TObject);
    procedure ViewTwitterMessagesButtonClick(Sender: TObject);
    procedure EditTalentTwitterInfo1Click(Sender: TObject);
    procedure DeleteTwitterMessagesButtonClick(Sender: TObject);
    procedure AddTwitterMessagesBtnClick(Sender: TObject);
    procedure InsertTwitterMessagesBtnClick(Sender: TObject);
    procedure TestScoreAlertAnimationBtnClick(Sender: TObject);
    procedure TestScoreAlertPlayBtnClick(Sender: TObject);
    procedure EditNCAATournamentRoundInfoClick(Sender: TObject);
    procedure EditCollegeTeamLogosClick(Sender: TObject);
    procedure E1Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

uses Preferences,     //Prefs dialog
     AboutBox,        //About box
     DataModule,      //Data module
     SearchDataEntry, //Dialog used to enter text to search for
     GeneralFunctions,//General purpose functions
     PlaylistSelect,  //Dialog for selecting playlist to load
     PlaylistGraphicsViewer,  //Viewer for Zipper playlists
     ZipperEntryEditor,  //Editor dialog for playlist entries
     ScheduleEntryTimeEditor, //Editor for schedule entry times
     NoteEntryEditor, //Dialog for game note entry & editing
     NCAAManualGameEntry, //Dialog for manual NCAA entries
     ZipperEntry, //Dialog for basic playlist element entry
     DuplicationCountSelect, //Dialog for selecting the number of duplicate records to generate
     EnableDateTimeEditor, //Dialog for editing start/end enable time for a playlist entry
     EngineConnectionPreferences, //Graphics engine connection preferences dialog
     EngineIntf, //Module for communication with graphics engine
     ManualGameEditor, //Dialog for manual game editing
     SponsorLogoEditor, // Dialog for editing sponsor logos
     DatabaseEditor, //General DB editor
     GamePhaseEditor, //Dialog for editing game phase codes
     CustomHeaderEditor, //Dialog for editing custom headers
     CSSGameNoteViewer, //CSS Game Note viewer
     ConferenceChipEditor, TwitterMessageEditor, TwitterTalentEditor,
  NCAATournamentChipEditor, CollegeTeamLogoEditor,
  DefaultTemplateDwellTimeEditor; //Conference chip editor

{$R *.DFM}
////////////////////////////////////////////////////////////////////////////////
//Procedures to delete data pointers in each collection node
////////////////////////////////////////////////////////////////////////////////
procedure Ticker_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TickerRec));
end;
procedure Bug_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(BugRec));
end;
procedure GameTrax_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(GameTraxRec));
end;
procedure Temp_Ticker_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TickerRec));
end;
procedure Temp_Bug_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(BugRec));
end;
procedure Temp_GameTrax_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(GameTraxRec));
end;
procedure Team_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TeamRec));
end;
procedure Ticker_Playout_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TickerRec));
end;
procedure SponsorLogo_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(SponsorLogoRec));
end;
procedure PromoLogo_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(PromoLogoRec));
end;
procedure Stat_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(StatRec));
end;
procedure Temp_Stat_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(StatRec));
end;
procedure Game_Phase_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(GamePhaseRec));
end;
procedure Template_Defs_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TemplateDefsRec));
end;
procedure Child_Template_ID_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(ChildTemplateIDRec));
end;
procedure Temporary_Fields_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TemplateFieldsRec));
end;
procedure Template_Fields_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TemplateFieldsRec));
end;
procedure Categories_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(CategoryRec));
end;
procedure Category_Templates_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(CategoryTemplatesRec));
end;
procedure Sports_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(SportRec));
end;
procedure RecordType_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(RecordTypeRec));
end;
procedure StyleChip_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(StyleChipRec));
end;
procedure CustomNewsHeader_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(CustomNewsHeaderRec));
end;
procedure Automated_League_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(AutomatedLeagueRec));
end;
procedure Weather_Icon_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(WeatherIconRec));
end;
procedure Schedule_Game_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(ScheduleGameRec));
end;
procedure Conference_Chip_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(ConferenceChipRec));
end;
procedure NewsPromo_Update_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(NewsPromoUpdateRec));
end;
procedure Twitter_Talent_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TwitterTalentRec));
end;

////////////////////////////////////////////////////////////////////////////////
// GENERAL PROGRAM PROCEDURES, FUNCTIONS AND HANDLERS
////////////////////////////////////////////////////////////////////////////////
//Handler for form activiation - inits done here
procedure TMainForm.FormActivate(Sender: TObject);
var
  Year, Month, Day: Word;
  SeasonStr: String;
  SQLText: String;
  CategoryRecPtr: ^CategoryRec;
begin
  MainForm.Width := 1299;
  MainForm.Height := 1024;

  //Set defaults
  LastPageIndex := 0;
  DBConnectionString := 'Provider=SQLOLEDB.1;Persist Security Info=False;' +
                        'User ID=sa;Data Source=TSTN';
  ProgramModePageControl.ActivePageIndex := 0;
  LeagueTab.TabIndex := 0;
  //Init for standard mode ticker
  CurrentTickerDisplayMode := -1;

  //Set data dictionary directory
  SpellCheckerDictionaryDir := 'C:\Program Files\VDS\TSTN\Dictionaries';

  //Set default engine prefs
  with EngineParameters do
  begin
    Enabled := FALSE;
    IPAddress := '127.0.0.1';
    Port := '7795';
  end;

  //Load pereferences file
  LoadPrefs;

  //Set checkmarks for NCAA seeding
  if (UseNCAASeeding) then
  begin
    UseNCAARank.Checked := FALSE;
    UseNCAASeed.Checked := TRUE;
  end
  else begin
    UseNCAARank.Checked := TRUE;
    UseNCAASeed.Checked := FALSE;
  end;

  //Create collections
  StudioTicker_Collection := TStCollection.Create(1500);
  StudioBug_Collection := TStCollection.Create(500);
  GameTrax_Collection := TStCollection.Create(500);
  Temp_StudioTicker_Collection := TStCollection.Create(1500);
  Temp_StudioBug_Collection := TStCollection.Create(500);
  Temp_GameTrax_Collection := TStCollection.Create(500);
  Team_Collection := TStCollection.Create(5000);
  Ticker_Playout_Collection := TStCollection.Create(1500);
  Overlay_Collection := TStCollection.Create(250);
  SponsorLogo_Collection := TStCollection.Create(100);
  PromoLogo_Collection := TStCollection.Create(100);
  Stat_Collection := TStCollection.Create(250);
  Temp_Stat_Collection := TStCollection.Create(250);
  Game_Phase_Collection := TStCollection.Create(500);
  Template_Defs_Collection := TStCollection.Create(250);
  Child_Template_ID_Collection := TStCollection.Create(50);
  Template_Fields_Collection := TStCollection.Create(5000);
  Temporary_Fields_Collection := TStCollection.Create(100);
  Categories_Collection := TStCollection.Create(50);
  Category_Templates_Collection := TStCollection.Create(500);
  Sports_Collection := TStCollection.Create(50);
  RecordType_Collection := TStCollection.Create(250);
  StyleChip_Collection := TStCollection.Create(100);
  CustomNewsHeader_Collection := TStCollection.Create(250);
  Automated_League_Collection := TStCollection.Create(50);
  Weather_Icon_Collection := TStCollection.Create(20);
  Schedule_Game_Collection := TStCollection.Create(250);
  Conference_Chip_Collection := TStCollection.Create(200);
  NewsPromo_Update_Collection := TStCollection.Create(20);
  Twitter_Talent_Collection := TStCollection.Create(100);

  StudioTicker_Collection.DisposeData := Ticker_Collection_DisposeData;
  StudioBug_Collection.DisposeData := Bug_Collection_DisposeData;
  GameTrax_Collection.DisposeData := GameTrax_Collection_DisposeData;
  Temp_StudioTicker_Collection.DisposeData := Temp_Ticker_Collection_DisposeData;
  Temp_StudioBug_Collection.DisposeData := Temp_Bug_Collection_DisposeData;
  Temp_GameTrax_Collection.DisposeData := Temp_GameTrax_Collection_DisposeData;
  Team_Collection.DisposeData := Team_Collection_DisposeData;
  Ticker_Playout_Collection.DisposeData := Ticker_Playout_Collection_DisposeData;
  SponsorLogo_Collection.DisposeData := SponsorLogo_Collection_DisposeData;
  PromoLogo_Collection.DisposeData := PromoLogo_Collection_DisposeData;
  Stat_Collection.DisposeData := Stat_Collection_DisposeData;
  Temp_Stat_Collection.DisposeData := Temp_Stat_Collection_DisposeData;
  Game_Phase_Collection.DisposeData := Game_Phase_Collection_DisposeData;
  Template_Defs_Collection.DisposeData := Template_Defs_Collection_DisposeData;
  Child_Template_ID_Collection.DisposeData := Child_Template_ID_Collection_DisposeData;
  Template_Fields_Collection.DisposeData := Template_Fields_Collection_DisposeData;
  Temporary_Fields_Collection.DisposeData := Temporary_Fields_Collection_DisposeData;
  Categories_Collection.DisposeData := Categories_Collection_DisposeData;
  Category_Templates_Collection.DisposeData := Category_Templates_Collection_DisposeData;
  Sports_Collection.DisposeData := Sports_Collection_DisposeData;
  RecordType_Collection.DisposeData := RecordType_Collection_DisposeData;
  StyleChip_Collection.DisposeData := StyleChip_Collection_DisposeData;
  CustomNewsHeader_Collection.DisposeData := CustomNewsHeader_Collection_DisposeData;
  Automated_League_Collection.DisposeData := Automated_League_Collection_DisposeData;
  Weather_Icon_Collection.DisposeData := Weather_Icon_Collection_DisposeData;
  Schedule_Game_Collection.DisposeData := Schedule_Game_Collection_DisposeData;
  Conference_Chip_Collection.DisposeData := Conference_Chip_Collection_DisposeData;
  NewsPromo_Update_Collection := TStCollection.Create(20);
  Twitter_Talent_Collection.DisposeData := Twitter_Talent_Collection_DisposeData;

  try
    //Activate database & tables
    With dmMain do
    begin
      dbCBS.Connected := FALSE;
      dbCBS.ConnectionString := DBConnectionString;
      dbCBS.Connected := TRUE;
      tblTicker_Groups.Active := TRUE;
      tblTicker_Elements.Active := TRUE;
      tblScheduled_Ticker_Groups.Active := TRUE;
      tblBug_Groups.Active := TRUE;
      tblBug_Elements.Active := TRUE;
      tblScheduled_Bug_Groups.Active := TRUE;
      tblGameTrax_Groups.Active := TRUE;
      tblGameTrax_Elements.Active := TRUE;
      tblScheduled_GameTrax_Groups.Active := TRUE;
      tblSponsor_Logos.Active := TRUE;
      dbSportBase.Connected := FALSE;
      dbSportBase.ConnectionString := DBConnectionString2;
      dbSportBase.Connected := TRUE;
      dbCSS.Connected := FALSE;
      dbCSS.ConnectionString := DBConnectionString3;
      dbCSS.Connected := TRUE;
      dbNFLDatamart.Connected := FALSE;
      dbNFLDatamart.ConnectionString := DBConnectionString4;
      dbNFLDatamart.Connected := TRUE;
      dbVDSSocialMediaGateway.ConnectionString := VDSSocialMediaDBConnectionString;
      dbVDSSocialMediaGateway.Connected := TRUE;
    end;

    //Call procedure to load data from database
    ReloadDataFromDatabase;

    //Check which data source to use
    dmMain.Query3.Active := FALSE;
    dmMain.Query3.SQL.Clear;
    dmMain.Query3.SQL.Add('SELECT * FROM Flags');
    dmMain.Query3.Active := TRUE;
    UseBackupDataSource := FALSE;
    if (dmMain.Query3.RecordCount > 0) then
    begin
      UsebackupDataSource := dmMain.Query3.FieldByName('UsebackupDataSource').AsBoolean;
      DisplayFeatureFlags.EnablePossessionIndicator :=
        dmMain.Query3.FieldByName('EnablePossessionIndicator').AsBoolean;
      DisplayFeatureFlags.EnableDownYardage :=
        dmMain.Query3.FieldByName('EnableDownYardage').AsBoolean;
      DisplayFeatureFlags.EnableFieldPosition :=
        dmMain.Query3.FieldByName('EnableFieldPosition').AsBoolean;
      DisplayFeatureFlags.EnableLiveUpdates :=
        dmMain.Query3.FieldByName('EnableLiveUpdates').AsBoolean;
      DisplayFeatureFlags.EnableWinnerIndicator :=
        dmMain.Query3.FieldByName('EnableWinnerIndicator').AsBoolean;
    end;
    dmMain.Query3.Active := FALSE;

    //Check to see if category tab is a sport; if so, show and populate games grid
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);

    //Get scoreboard info from CSS if it's the source; overwrites settings from INI file
    if (NFLSeasonWeekInfoSource = 0) then
    begin
      dmMain.CSSQuery.Active := FALSE;
      dmMain.CSSQuery.SQL.Clear;
      SQLText := '/* */vds_GetScoreboardInfo';
      dmMain.CSSQuery.SQL.Add(SQLText);
      //Activate query
      dmMain.CSSQuery.Active := TRUE;
      //Set values
      if (dmMain.CSSQuery.RecordCount > 0) then
      begin
        NFLWeekInfo.SeasonYear := dmMain.CSSQuery.FieldByName('season').AsInteger;
        NFLWeekInfo.Week := dmMain.CSSQuery.FieldByName('weekOfSeason').AsInteger;
        if (dmMain.CSSQuery.FieldByName('weekOfSeason').AsString = 'PRE') then
          NFLWeekInfo.SeasonType := 0
        else if (dmMain.CSSQuery.FieldByName('weekOfSeason').AsString = 'REG') then
          NFLWeekInfo.SeasonType := 1
        else if (dmMain.CSSQuery.FieldByName('weekOfSeason').AsString = 'POST') then
          NFLWeekInfo.SeasonType := 2;
      end;
      dmMain.CSSQuery.Active := FALSE;
    end;

    //Do initial load of data from Datamart
    dmMain.DatamartQuery.Active := FALSE;
    dmMain.DatamartQuery.SQL.Clear;
    //Set season type
    Case NFLWeekInfo.SeasonType of
      0: SeasonStr := 'PRE';
      1: SeasonStr := 'REG';
      2: SeasonStr := 'POST';
    end;
    SQLText := '/* */p_GetWeekSchedule ' + IntToStr(NFLWeekInfo.SeasonYear) + ', ' +
           QuotedStr(SeasonStr) + ', ' + IntToStr(NFLWeekInfo.Week);
    dmMain.DatamartQuery.SQL.Add(SQLText);
    //Activate query
    dmMain.DatamartQuery.Active := TRUE;

    //Do initial load of data from CSS if active
    //if (UseBackupDataSource = TRUE) then
    if (ConnectToCSS = TRUE) then
    begin
      //Close & setup query
      dmMain.CSSQuery.Active := FALSE;
      dmMain.CSSQuery.SQL.Clear;
      //SQLText := '/* */vds_GetGamesByLeague ' +  CategoryRecPtr^.Category_Sport + ',2';
      SQLText := '/* */vds_GetGames';
      dmMain.CSSQuery.SQL.Add(SQLText);
      //Activate query
      dmMain.CSSQuery.Active := TRUE;
      //Set current data source
      //CurrentDataSource := 'CBSCSS';
    end;
  except
    EngineInterface.WriteToErrorLog('Error occurred loading initial data from database');
  end;

  //Init date/time pickers & combo boxes
  ScheduleEntryStartDate.Date := Now;
  ScheduleEntryEndDate.Date := Now+1;
  ScheduleEntryStartTime.Time := 0;
  ScheduleEntryEndTime.Time := 0;
  EntryStartEnableDate.Date := Now;
  EntryStartEnableTime.Time := 0;
  DecodeDate(Now, Year, Month, Day);
  EntryEndEnableDate.Date := EncodeDate(Year+10, Month, Day);
  EntryEndEnableTime.Time := 1-(1/(24*60));

  //Force update of games grid
  LeagueTab.OnChange(self);

  //Set playlist type tab names - Extra line temporarily disabled
  PlaylistSelectTabControl.Tabs.Clear;
  PlaylistSelectTabControl.Tabs.Add('N/A');
  PlaylistSelectTabControl.Tabs.Add('N/A');
  PlaylistSelectTabControl.Tabs.Add('GameTrax');
  PlaylistSelectTabControl.Tabs.Add('Score Updates');
  PlaylistSelectTabControl.Tabs.Add('Fantasy Update');
  PlaylistSelectTabControl.Tabs.Add('News/Promo Update');
  PlaylistSelectTabControl.Tabs.Add('NFL GSIS Game Override');
  PlaylistSelectTabControl.TabIndex := 2;
  PlaylistSelectTabControlChange(Self);

  //Connect to the graphics engine if it's enabled
  if (EngineParameters.Enabled = TRUE) then
  begin
    try
      EngineInterface.EnginePort.Address := EngineParameters.IPAddress;
      EngineInterface.EnginePort.Port := StrToInt(EngineParameters.Port);
      EngineInterface.EnginePort.Open;
    except
      if (ErrorLoggingEnabled = True) then
      begin
        Error_Condition := True;
        //Label16.Caption := 'ERROR';
        //WriteToErrorLog
        EngineInterface.WriteToErrorLog('Error occurred while trying connect to database engine');
      end;
    end;
  end;
end;

//Handler for program exit from main menu
procedure TMainForm.Exit1Click(Sender: TObject);
begin
  Close;
end;

//Handler for main program form close
procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
var
  OKToClose: Boolean;
begin
  //Init
  OkToClose := TRUE;
  //Confirm with operator}
  if (MessageDlg('Are you sure you want to exit the CBS Ticker Authoring application?',
                  mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
  begin
    //Prompt for save of playlist before exiting
    if (StudioTicker_Collection.Count > 0) then
    begin
      if (MessageDlg('Do you wish to save the contents of the current Studio Ticker playlist before exiting?',
                      mtWarning, [mbYes, mbNo], 0) = mrYes) then
      begin
        //Save the playlist
        if (SaveTickerPlaylist(TICKER, TRUE) = FALSE) then OKToCLose := FALSE;
      end;
    end;
    //Prompt for save of playlist before exiting
    if (StudioBug_Collection.Count > 0) AND (OKToClose = TRUE) then
    begin
      if (MessageDlg('Do you wish to save the contents of the current Studio Bug playlist before exiting?',
                      mtWarning, [mbYes, mbNo], 0) = mrYes) then
      begin
        //Save the playlist
        if (SaveTickerPlaylist(BUG, TRUE) = FALSE) then OKToCLose := FALSE;
      end;
    end;
    //Prompt for save of playlist before exiting
    if (GameTrax_Collection.Count > 0) AND (OKToClose = TRUE) then
    begin
      if (MessageDlg('Do you wish to save the contents of the current GameTrax playlist before exiting?',
                      mtWarning, [mbYes, mbNo], 0) = mrYes) then
      begin
        //Save the playlist
        if (SaveTickerPlaylist(GameTrax, TRUE) = FALSE) then OKToCLose := FALSE;
      end;
    end;
    //Set action for main form
    if (OKToClose) then
    begin
      //Close database connections
      dmMain.dbCBS.Connected := FALSE;
      dmMain.dbSportBase.Connected := FALSE;
      Action := caFree;
    end
    else Action := caNone;
  end
  else
     Action := caNone;
end;

//Procedure to load user preferences
procedure TMainForm.LoadPrefs;
var
  PrefsINI : TINIFile;
  SeasonTypeStr: String;
begin
  try
    //Create the INI file
    PrefsINI := TINIFile.Create(ExtractFilePath(Application.EXEName) + 'CBS_Authoring.ini');
    //Save the settings
    StationID := PrefsINI.ReadInteger('General Settings', 'Authoring Station ID', 1);
    SpellCheckerDictionaryDir := PrefsINI.ReadString('General Settings', 'Spellchecker Dictionary Directory',
      'C:\VDS_Projects\CBS\Source\Authoring\Dictionaries');
    EngineParameters.Enabled := PrefsINI.ReadBool('General Settings', 'Graphics Generator Enable', FALSE);
    EngineParameters.IPAddress := PrefsINI.ReadString('General Settings', 'Graphics Generator IP Address', '127.0.0.1');
    AllowDataSourceSelection := PrefsINI.ReadBool('General Settings', 'Allow Data Source Selection', FALSE);
    Force16x9Mode := PrefsINI.ReadBool('General Settings', 'Force 16 x 9 Mode', FALSE);
    ConnectToCSS := PrefsINI.ReadBool('General Settings', 'ConnectToCSS', TRUE);
    UseNCAASeeding := PrefsINI.ReadBool('General Settings', 'Use NCAA Seeding', FALSE);
    ForceUpperCaseScoringPlayDescription := PrefsINI.ReadBool('General Settings', 'Force Upper-Case Scoring Play Description', FALSE);
    UseNCAATournamentHeaders := PrefsINI.ReadBool('General Settings', 'Use NCAA Tournament Headers', FALSE);
    EnableTeamRecordsForNCAATournament := PrefsINI.ReadBool('General Settings', 'Enable Team Records for NCAA Tournament', TRUE);

    NCAATournamentLookNotesTemplateID := PrefsINI.ReadInteger('General Settings', 'NCAA Tournament Look Notes Template ID', 48);

    DBConnectionString := PrefsINI.ReadString('Database Connections', 'Ticker Database Connection String',
      'Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Initial Catalog=TSTN;Data Source=(local)');
    DBConnectionString2 := PrefsINI.ReadString('Database Connections', 'Sportbase Database Connection String',
      'Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Initial Catalog=SportBase;Data Source=(local)');
    DBConnectionString3 := PrefsINI.ReadString('Database Connections', 'CSS Database Connection String',
      'Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Initial Catalog=CompScoreSys;Data Source=(local)');
    DBConnectionString4 := PrefsINI.ReadString('Database Connections', 'NFL Datamart Connection String',
      'Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Initial Catalog=NFLDatamart;Data Source=(local)');
    VDSSocialMediaDBConnectionString := PrefsINI.ReadString('Database Connections', 'VDS Social Media Database Connection String',
      'Provider=SQLOLEDB.1;Persist Security Info=True;User ID=sa;Password=Vds@dmin1; Initial Catalog=VDSSocialMediaGateway;Data Source=OWNER-PC\SQLEXPRESS');
    NFLSeasonWeekInfoSource := PrefsINI.ReadInteger('NFL Season/Week Information', 'Data Source', 0);
    NFLWeekInfo.SeasonYear := PrefsINI.ReadInteger('NFL Season/Week Information', 'Season Year', 2007);
    SeasonTypeStr := PrefsINI.ReadString('NFL Season/Week Information', 'Season Type', 'REG');
    if (SeasonTypeStr = 'PRE') then NFLWeekInfo.SeasonType := 0
    else if (SeasonTypeStr = 'REG') then NFLWeekInfo.SeasonType := 1
    else if (SeasonTypeStr = 'POST') then NFLWeekInfo.SeasonType := 2
    else NFLWeekInfo.SeasonType := 1;
    NFLWeekInfo.Week := PrefsINI.ReadInteger('NFL Season/Week Information', 'Week', 1);

    //For Twitter
    Twitter_Manual_Template_NFL := PrefsINI.ReadInteger('Twitter Setup Information', 'Manual Template ID (NFL)', 52);
    Twitter_Manual_Template_NFL_No_Subheader := PrefsINI.ReadInteger('Twitter Setup Information', 'Manual Template ID (NFL, No Sub-Header)', 53);
    Twitter_Manual_Template_Non_NFL := PrefsINI.ReadInteger('Twitter Setup Information', 'Manual Template ID (Non-NFL)', 54);
    Twitter_Manual_Template_Non_NFL_No_Subheader := PrefsINI.ReadInteger('Twitter Setup Information', 'Manual Template ID (Non-NFL, No Sub-Header)', 55);
  finally
    //Free up the object
    PrefsINI.Free;
  end;
end;

//Procedure to store user preferences
procedure TMainForm.StorePrefs1Click(Sender: TObject);
begin
  StorePrefs;
end;
procedure TMainForm.StorePrefs;
var
  PrefsINI : TINIFile;
  SeasonTypeStr: String;
begin
  try
    //Create the INI file
    PrefsINI := TINIFile.Create(ExtractFilePath(Application.EXEName) + 'CBS_Authoring.ini');
    //Save the settings
    PrefsINI.WriteInteger('General Settings', 'Authoring Station ID', StationID);
    PrefsINI.WriteString('General Settings', 'Spellchecker Dictionary Directory', SpellCheckerDictionaryDir);
    PrefsINI.WriteBool('General Settings', 'Graphics Generator Enable', EngineParameters.Enabled);
    PrefsINI.WriteString('General Settings', 'Graphics Generator IP Address', EngineParameters.IPAddress);
    PrefsINI.WriteBool('General Settings', 'Allow Data Source Selection', AllowDataSourceSelection);
    PrefsINI.WriteBool('General Settings', 'ConnectToCSS', ConnectToCSS);
    PrefsINI.WriteBool('General Settings', 'Force 16 x 9 Mode', Force16x9Mode);
    PrefsINI.WriteBool('General Settings', 'Use NCAA Seeding', UseNCAASeeding);
    PrefsINI.WriteBool('General Settings', 'Force Upper-Case Scoring Play Description', ForceUpperCaseScoringPlayDescription);
    PrefsINI.WriteBool('General Settings', 'Use NCAA Tournament Headers', UseNCAATournamentHeaders);
    PrefsINI.WriteBool('General Settings', 'Enable Team Records for NCAA Tournament', EnableTeamRecordsForNCAATournament);

    PrefsINI.WriteInteger('General Settings', 'NCAA Tournament Look Notes Template ID', NCAATournamentLookNotesTemplateID);

    PrefsINI.WriteString('Database Connections', 'Ticker Database Connection String', DBConnectionString);
    PrefsINI.WriteString('Database Connections', 'Sportbase Database Connection String', DBConnectionString2);
    PrefsINI.WriteString('Database Connections', 'CSS Database Connection String', DBConnectionString3);
    PrefsINI.WriteString('Database Connections', 'NFL Datamart Connection String', DBConnectionString4);
    PrefsINI.WriteString('Database Connections', 'VDS Social Media Database Connection String', VDSSocialMediaDBConnectionString);
    PrefsINI.WriteInteger('NFL Season/Week Information', 'Data Source', NFLSeasonWeekInfoSource);
    PrefsINI.WriteInteger('NFL Season/Week Information', 'Season Year', NFLWeekInfo.SeasonYear);
    Case NFLWeekInfo.SeasonType of
      0: SeasonTypeStr := 'PRE';
      1: SeasonTypeStr := 'REG';
      2: SeasonTypeStr := 'POST';
    end;
    PrefsINI.WriteString('NFL Season/Week Information', 'Season Type', SeasonTypeStr);
    PrefsINI.WriteInteger('NFL Season/Week Information', 'Week', NFLWeekInfo.Week);
    //For Twitter
    PrefsINI.WriteInteger('Twitter Setup Information', 'Manual Template ID (NFL)', Twitter_Manual_Template_NFL);
    PrefsINI.WriteInteger('Twitter Setup Information', 'Manual Template ID (NFL, No Sub-Header)', Twitter_Manual_Template_NFL_No_Subheader);
    PrefsINI.WriteInteger('Twitter Setup Information', 'Manual Template ID (Non-NFL)', Twitter_Manual_Template_Non_NFL);
    PrefsINI.WriteInteger('Twitter Setup Information', 'Manual Template ID (Non-NFL, No Sub-Header)', Twitter_Manual_Template_Non_NFL_No_Subheader);
  finally
    //Free up the object
    PrefsINI.Free;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES FOR LAUNCHING VARIOUS PROGRAM DIALOGS
////////////////////////////////////////////////////////////////////////////////
//Display about box
procedure TMainForm.About1Click(Sender: TObject);
var
  Modal: TAbout;
begin
  Modal := TAbout.Create(Application);
  try
    Modal.ShowModal;
  finally
    Modal.Free
  end;
end;

//Handler to display dialog for setting user preferences
procedure TMainForm.SetPrefs1Click(Sender: TObject);
var
  Modal: TPrefs;
  Control: Word;
  SQLText: String;
  SeasonStr: String;
  CategoryRecPtr: ^CategoryRec;
begin
  Modal := TPrefs.Create(Application);
  try
    Modal.SpinEdit1.Value := StationID;
    Modal.Edit1.Text := SpellCheckerDictionaryDir;
    Modal.Edit2.Text := DBConnectionString;
    Modal.Edit3.Text := DBConnectionString2;
    Modal.Edit4.Text := DBConnectionString3;
    Modal.Edit5.Text := DBConnectionString4;
    Modal.Edit6.Text := VDSSocialMediaDBConnectionString;
    Modal.CheckBox1.Checked := EngineParameters.Enabled;
    if (AllowDataSourceSelection) then
    begin
      Modal.RadioGroup2.Enabled := TRUE;
      Modal.RadioGroup3.Enabled := TRUE;
      Modal.RadioGroup4.Enabled := TRUE;
      Modal.CheckBox2.Enabled := TRUE;
      Modal.CheckBox3.Enabled := TRUE;
      Modal.CheckBox4.Enabled := TRUE;
      Modal.CheckBox5.Enabled := TRUE;
    end
    else begin
      Modal.RadioGroup2.Enabled := FALSE;
      Modal.RadioGroup3.Enabled := FALSE;
      Modal.RadioGroup4.Enabled := FALSE;
      Modal.CheckBox2.Enabled := FALSE;
      Modal.CheckBox3.Enabled := FALSE;
      Modal.CheckBox4.Enabled := FALSE;
      Modal.CheckBox5.Enabled := FALSE;
    end;
    if (ConnectToCSS) then Modal.RadioGroup4.ItemIndex := 1
    else Modal.RadioGroup4.ItemIndex := 0;
    if (UseBackupDataSource) then Modal.RadioGroup2.ItemIndex := 1
    else Modal.RadioGroup2.ItemIndex := 0;
    Modal.RadioGroup3.ItemIndex := NFLSeasonWeekInfoSource;

    //Set controls for ticker display features
    Modal.CheckBox2.Checked := DisplayFeatureFlags.EnablePossessionIndicator;
    Modal.CheckBox3.Checked := DisplayFeatureFlags.EnableDownYardage;
    Modal.CheckBox4.Checked := DisplayFeatureFlags.EnableFieldPosition;
    Modal.CheckBox5.Checked := DisplayFeatureFlags.EnableLiveUpdates;
    Modal.CheckBox6.Checked := DisplayFeatureFlags.EnableWinnerIndicator;

    //Set NFL season info
    Modal.RadioGroup1.ItemIndex := NFLWeekInfo.SeasonType;
    Modal.SpinEdit3.Value := NFLWeekInfo.SeasonYear;
    Modal.SpinEdit2.Value := NFLWeekInfo.Week;
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    //Set new values if user didn't cancel out
    if (Control = mrOK) then
    begin
      StationID := Modal.SpinEdit1.Value;
      DBConnectionString := Modal.Edit2.Text;
      DBConnectionString2 := Modal.Edit3.Text;
      DBConnectionString3 := Modal.Edit4.Text;
      DBConnectionString4 := Modal.Edit5.Text;
      VDSSocialMediaDBConnectionString := Modal.Edit6.Text;
      SpellCheckerDictionaryDir := Modal.Edit1.Text;
      EngineParameters.Enabled := Modal.CheckBox1.Checked;
      Case Modal.RadioGroup2.ItemIndex of
        0: UseBackupDataSource := FALSE;
        1: UseBackupDataSource := TRUE;
      end;
      Case Modal.RadioGroup4.ItemIndex of
        0: ConnectToCSS := FALSE;
        1: ConnectToCSS := TRUE;
      end;
      //Set controls for ticker display features
      DisplayFeatureFlags.EnablePossessionIndicator := Modal.CheckBox2.Checked;
      DisplayFeatureFlags.EnableDownYardage := Modal.CheckBox3.Checked;
      DisplayFeatureFlags.EnableFieldPosition := Modal.CheckBox4.Checked;
      DisplayFeatureFlags.EnableLiveUpdates := Modal.CheckBox5.Checked;
      DisplayFeatureFlags.EnableWinnerIndicator := Modal.CheckBox6.Checked;
      NFLSeasonWeekInfoSource := Modal.RadioGroup3.ItemIndex;

      //Only overwrite week settings if manual mode enabled
      if (NFLSeasonWeekInfoSource = 1) then
      begin
        NFLWeekInfo.SeasonType := Modal.RadioGroup1.ItemIndex;
        NFLWeekInfo.SeasonYear := Modal.SpinEdit3.Value;
        NFLWeekInfo.Week := Modal.SpinEdit2.Value;
      end
      else begin
        //Get scoreboard info from CSS if it's the source; overwrites settings from INI file
        if (NFLSeasonWeekInfoSource = 0) then
        begin
          dmMain.CSSQuery.Active := FALSE;
          dmMain.CSSQuery.SQL.Clear;
          SQLText := '/* */vds_GetScoreboardInfo';
          dmMain.CSSQuery.SQL.Add(SQLText);
          //Activate query
          dmMain.CSSQuery.Active := TRUE;
          //Set values
          if (dmMain.CSSQuery.RecordCount > 0) then
          begin
            NFLWeekInfo.SeasonYear := dmMain.CSSQuery.FieldByName('season').AsInteger;
            NFLWeekInfo.Week := dmMain.CSSQuery.FieldByName('weekOfSeason').AsInteger;
            if (dmMain.CSSQuery.FieldByName('weekOfSeason').AsString = 'PRE') then
              NFLWeekInfo.SeasonType := 0
            else if (dmMain.CSSQuery.FieldByName('weekOfSeason').AsString = 'REG') then
              NFLWeekInfo.SeasonType := 1
            else if (dmMain.CSSQuery.FieldByName('weekOfSeason').AsString = 'POST') then
              NFLWeekInfo.SeasonType := 2;
          end;
          dmMain.CSSQuery.Active := FALSE;
        end;
      end;

      //Set new values in Flags database
      dmMain.tblFlags.Active := TRUE;
      dmMain.tblFlags.Edit;
      if (AllowDataSourceSelection) then
        dmMain.tblFlags.FieldByName('UseBackupDataSource').AsBoolean := UseBackupDataSource;
      dmMain.tblFlags.FieldByName('EnablePossessionIndicator').AsBoolean :=
        DisplayFeatureFlags.EnablePossessionIndicator;
      dmMain.tblFlags.FieldByName('EnableDownYardage').AsBoolean :=
        DisplayFeatureFlags.EnableDownYardage;
      dmMain.tblFlags.FieldByName('EnableFieldPosition').AsBoolean :=
        DisplayFeatureFlags.EnableFieldPosition;
      dmMain.tblFlags.FieldByName('EnableLiveUpdates').AsBoolean :=
        DisplayFeatureFlags.EnableLiveUpdates;
      dmMain.tblFlags.FieldByName('EnableWinnerIndicator').AsBoolean :=
        DisplayFeatureFlags.EnableWinnerIndicator;
      dmMain.tblFlags.Post;
      dmMain.tblFlags.Active := FALSE;

      //Activate database & tables
      try
        With dmMain do
        begin
          dbCBS.Connected := FALSE;
          dbCBS.ConnectionString := DBConnectionString;
          dbCBS.Connected := TRUE;
          tblTicker_Elements.Active := TRUE;
          tblTicker_Groups.Active := TRUE;
          tblScheduled_Ticker_Groups.Active := TRUE;
          tblBug_Elements.Active := TRUE;
          tblBug_Groups.Active := TRUE;
          tblScheduled_Bug_Groups.Active := TRUE;
          tblGameTrax_Elements.Active := TRUE;
          tblGameTrax_Groups.Active := TRUE;
          tblScheduled_GameTrax_Groups.Active := TRUE;
          tblSponsor_Logos.Active := TRUE;
          dbSportBase.Connected := FALSE;
          dbSportBase.ConnectionString := DBConnectionString2;
          dbSportBase.Connected := TRUE;
          dbCSS.Connected := FALSE;
          dbCSS.ConnectionString := DBConnectionString3;
          dbCSS.Connected := TRUE;
          dbNFLDatamart.Connected := FALSE;
          dbNFLDatamart.ConnectionString := DBConnectionString4;
          dbNFLDatamart.Connected := TRUE;
          dbVDSSocialMediaGateway.Connected := FALSE;
          dbVDSSocialMediaGateway.ConnectionString := VDSSocialMediaDBConnectionString;
          dbVDSSocialMediaGateway.Connected := TRUE;
        end;
        //Get scoreboard info from CSS if it's the source
        if (NFLSeasonWeekInfoSource = 0) then
        begin
          dmMain.CSSQuery.Active := FALSE;
          dmMain.CSSQuery.SQL.Clear;
          SQLText := '/* */vds_GetScoreboardInfo';
          dmMain.CSSQuery.SQL.Add(SQLText);
          //Activate query
          dmMain.CSSQuery.Active := TRUE;
          //Set values
          if (dmMain.CSSQuery.RecordCount > 0) then
          begin
            NFLWeekInfo.SeasonYear := dmMain.CSSQuery.FieldByName('season').AsInteger;
            NFLWeekInfo.Week := dmMain.CSSQuery.FieldByName('weekOfSeason').AsInteger;
            if (dmMain.CSSQuery.FieldByName('seasonType').AsString = 'PRE') then
              NFLWeekInfo.SeasonType := 0
            else if (dmMain.CSSQuery.FieldByName('seasonType').AsString = 'REG') then
              NFLWeekInfo.SeasonType := 1
            else if (dmMain.CSSQuery.FieldByName('seasonType').AsString = 'POST') then
              NFLWeekInfo.SeasonType := 2;
          end;
          dmMain.CSSQuery.Active := FALSE;
        end;

        //Do initial load of data from Datamart
        dmMain.DatamartQuery.Active := FALSE;
        dmMain.DatamartQuery.SQL.Clear;
        //Set season type
        Case NFLWeekInfo.SeasonType of
          0: SeasonStr := 'PRE';
          1: SeasonStr := 'REG';
          2: SeasonStr := 'POST';
        end;
        SQLText := '/* */p_GetWeekSchedule ' + IntToStr(NFLWeekInfo.SeasonYear) + ', ' +
               QuotedStr(SeasonStr) + ', ' + IntToStr(NFLWeekInfo.Week);
        dmMain.DatamartQuery.SQL.Add(SQLText);
        //Activate query
        dmMain.DatamartQuery.Active := TRUE;

        //Do initial load of data from CSS if active
        //if (UseBackupDataSource = TRUE) then
        if (ConnectToCSS = TRUE) then
        begin
          //Close & setup query
          dmMain.CSSQuery.Active := FALSE;
          dmMain.CSSQuery.SQL.Clear;
          //SQLText := '/* */vds_GetGamesByLeague ' +  CategoryRecPtr^.Category_Sport + ',2';
          SQLText := '/* */vds_GetGames';
          dmMain.CSSQuery.SQL.Add(SQLText);
          //Activate query
          dmMain.CSSQuery.Active := TRUE;
          //Set current data source
          //CurrentDataSource := 'CBSCSS';
        end;
      except
        EngineInterface.WriteToErrorLog('Error occurred while trying to connect to database');
      end;
      //Store the preferences
      StorePrefs;
    end;
    Modal.Free
  end;
  //Now, populate the templates grid by querying the database
  try
    //Check to see if category tab is a sport; if so, show and populate games grid
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    dmMain.Query2.Active := FALSE;
    dmMain.Query2.SQL.Clear;
    dmMain.Query2.Filtered := TRUE;
    //Filter for templates applicable to current playlist type
    dmMain.Query2.Filter := 'Template_Type = ' + IntToStr(PlaylistSelectTabControl.TabIndex+1);
    //Add filter to query to ensure that only applicable templates are displayed (odds only)
    dmMain.Query2.SQL.Add ('SELECT * FROM Category_Templates_2010 WHERE ' +
      'Category_ID = ' + IntToStr(CategoryRecPtr^.Category_ID) + ' AND Template_IsOddsOnly = ' + IntToStr(0));
    dmMain.Query2.Active := TRUE;
  except
    //WriteToErrorLog
    EngineInterface.WriteToErrorLog('Error occurred while trying to retrieve template info for category');
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// UTILITY FUNCTIONS
// Various functions used for collection lookup, data processing, etc.
////////////////////////////////////////////////////////////////////////////////
function TMainForm.ScrubText (InText: String) : String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := '';
  for i := 1 to Length(InText) do
    //Filter out control characters, etc.
    if (Ord(InText[i]) >= 32) AND (Ord(InText[i]) <= 126) then OutStr := OutStr + InText[i];
  //Remove leading and trailing blanks from story
  ScrubText := OutStr;
end;

//Handler to reload collections
procedure TMainForm.RepopulateTeamsCollection1Click(Sender: TObject);
begin
  ReloadDataFromDatabase;
end;
//General procedure to cache in data from database into collections
procedure TMainForm.ReloadDataFromDatabase;
var
  TeamPtr: ^TeamRec; //Pointer to team collection object
  SponsorLogoPtr: ^SponsorLogoRec; //Pointer to sponsor logo object
  StatPtr: ^StatRec; //Pointer to stat object
  GamePhasePtr: ^GamePhaseRec;
  TemplateDefRecPtr: ^TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  ChildTemplateIDRecPtr: ^ChildTemplateIDRec;
  CategoryRecPtr: ^CategoryRec;
  CategoryTemplatesRecPtr: ^CategoryTemplatesRec;
  SportRecPtr: ^SportRec;
  RecordTypeRecPtr: ^RecordTypeRec;
  StyleChipRecPtr: ^StyleChipRec;
  CustomNewsHeaderRecPtr: ^CustomNewsHeaderRec;
  AutomatedLeagueRecPtr: ^AutomatedLeagueRec;
  WeatherIconRecPtr: ^WeatherIconRec;
  ConferenceChipRecPtr: ^ConferenceChipRec;
  TwitterTalentRecPtr: ^TwitterTalentRec;
begin
  //Load collections for teams (from Sportbase)
  Team_Collection.Clear;
  Team_Collection.Pack;
  try
    //Load in teams information
    dmMain.SportbaseQuery.SQL.Clear;
    dmMain.SportbaseQuery.SQL.Add('SELECT * FROM Teams');
    dmMain.SportbaseQuery.Open;
    if (dmMain.SportbaseQuery.RecordCount > 0) then
    begin
      dmMain.SportbaseQuery.First;
      repeat
        //Add item to teams collection
        GetMem (TeamPtr, SizeOf(TeamRec));
        With TeamPtr^ do
        begin
          ID := dmMain.SportbaseQuery.FieldByName('ID').AsFloat;
          League := dmMain.SportbaseQuery.FieldByName('League').AsString;
          LeagueID := dmMain.SportbaseQuery.FieldByName('LeagueID').AsFloat;
          City := dmMain.SportbaseQuery.FieldByName('City').AsString;
          Team := dmMain.SportbaseQuery.FieldByName('Team').AsString;
          FullName := dmMain.SportbaseQuery.FieldByName('FullName').AsString;
          SportstickerMnemonic := dmMain.SportbaseQuery.FieldByName('SportstickerMnemonic').AsString;;
          StatsIncId := dmMain.SportbaseQuery.FieldByName('StatsIncId').AsFloat;
          StatsIncAlias := dmMain.SportbaseQuery.FieldByName('StatsIncAlias').AsString;
          OddsId := dmMain.SportbaseQuery.FieldByName('OddsId').AsFloat;
          ShortDisplayName1 := dmMain.SportbaseQuery.FieldByName('ShortDisplayName1').AsString;
          ShortDisplayName2 := dmMain.SportbaseQuery.FieldByName('ShortDisplayName2').AsString;
          MediumDisplayName1 := dmMain.SportbaseQuery.FieldByName('MediumDisplayName1').AsString;
          MediumDisplayName2 := dmMain.SportbaseQuery.FieldByName('MediumDisplayName2').AsString;
          LongDisplayName1 := dmMain.SportbaseQuery.FieldByName('LongDisplayName1').AsString;
          LongDisplayName2 := dmMain.SportbaseQuery.FieldByName('LongDisplayName2').AsString;
          Division := dmMain.SportbaseQuery.FieldByName('Division').AsString;
          Conference := dmMain.SportbaseQuery.FieldByName('Conference').AsString;
          TeamLogoName := dmMain.SportbaseQuery.FieldByName('TeamLogoName').AsString;
          If (Team_Collection.Count <= 5000) then
          begin
            Team_Collection.Insert(TeamPtr);
            Team_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.SportbaseQuery.Next;
      until (dmMain.SportbaseQuery.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.SportbaseQuery.Active := FALSE;

    //Load in the data from the game phase codes table; iterate for all records in table
    Game_Phase_Collection.Clear;
    Game_Phase_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Game_Phase_Codes_2010');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (GamePhasePtr, SizeOf(GamePhaseRec));
        With GamePhasePtr^ do
        begin
          League := dmMain.Query1.FieldByName('League').AsString;
          ST_Phase_Code := dmMain.Query1.FieldByName('ST_Phase_Code').AsInteger;
          SI_Phase_Code := dmMain.Query1.FieldByName('SI_Phase_Code').AsInteger;
          NFLDM_Phase_Code := dmMain.Query1.FieldByName('NFLDM_Phase_Code').AsInteger;
          Label_Period := dmMain.Query1.FieldByName('Label_Period').AsString;
          Display_Period := dmMain.Query1.FieldByName('Display_Period').AsString;
          End_Is_Half := dmMain.Query1.FieldByName('End_Is_Half').AsBoolean;
          Display_Half_Short := dmMain.Query1.FieldByName('Display_Half_Short').AsString;
          Display_Half := dmMain.Query1.FieldByName('Display_Half').AsString;
          Display_Final_Short := dmMain.Query1.FieldByName('Display_Final_Short').AsString;
          Display_Final := dmMain.Query1.FieldByName('Display_Final').AsString;
          Display_Extended1 := dmMain.Query1.FieldByName('Display_Extended1').AsString;
          Display_Extended2 := dmMain.Query1.FieldByName('Display_Extended2').AsString;
          If (Game_Phase_Collection.Count <= 500) then
          begin
            //Add to collection
            Game_Phase_Collection.Insert(GamePhasePtr);
            Game_Phase_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the stats info table; iterate for all records in table
    Stat_Collection.Clear;
    Stat_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Stat_Definitions');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (StatPtr, SizeOf(StatRec));
        With StatPtr^ do
        begin
          StatLeague := dmMain.Query1.FieldByName('StatLeague').AsString;
          StatType := dmMain.Query1.FieldByName('StatType').AsInteger;
          StatDescription := dmMain.Query1.FieldByName('StatDescription').AsString;
          StatHeader := dmMain.Query1.FieldByName('StatHeader').AsString;
          StatStoredProcedure := dmMain.Query1.FieldByName('StatStoredProcedure').AsString;
          StatDataField := dmMain.Query1.FieldByName('StatDataField').AsString;
          StatAbbreviation := dmMain.Query1.FieldByName('StatAbbreviation').AsString;
          UseStatQualifier := dmMain.Query1.FieldByName('UseStatQualifier').AsBoolean;
          StatQualifier := dmMain.Query1.FieldByName('StatQualifier').AsString;
          StatQualifierValue := dmMain.Query1.FieldByName('StatQualifierValue').AsInteger;
          If (Stat_Collection.Count <= 100) then
          begin
            //Add to collection
            Stat_Collection.Insert(StatPtr);
            Stat_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the sponsor logos table; iterate for all records in table
    SponsorLogo_Collection.Clear;
    SponsorLogo_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Sponsor_Logos');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (SponsorLogoPtr, SizeOf(SponsorLogoRec));
        With SponsorLogoPtr^ do
        begin
          SponsorLogoIndex := dmMain.Query1.FieldByName('LogoIndex').AsInteger;
          SponsorLogoName := dmMain.Query1.FieldByName('LogoName').AsString;
          SponsorLogoFilename := dmMain.Query1.FieldByName('LogoFilename').AsString;
          If (SponsorLogo_Collection.Count <= 100) then
          begin
            //Add to collection
            SponsorLogo_Collection.Insert(SponsorLogoPtr);
            SponsorLogo_Collection.Pack;
            //Add to comboboxes
            //ComboBox4.Items.Add(SponsorLogoName);
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the template definitions table; iterate for all records in table
    Template_Defs_Collection.Clear;
    Template_Defs_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Template_Defs_2010');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (TemplateDefRecPtr, SizeOf(TemplateDefsRec));
        With TemplateDefRecPtr^ do
        begin
          Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
          Template_Type := dmMain.Query1.FieldByName('Template_Type').AsInteger;
          Template_Description := dmMain.Query1.FieldByName('Template_Description').AsString;
          Template_Has_Children := dmMain.Query1.FieldByName('Template_Has_Children').AsBoolean;
          Template_Is_Child := dmMain.Query1.FieldByName('Template_Is_Child').AsBoolean;
          Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
          Engine_Template_ID := dmMain.Query1.FieldByName('Engine_Template_ID').AsInteger;
          Default_Dwell := dmMain.Query1.FieldByName('Default_Dwell').AsInteger;
          ManualLeague := dmMain.Query1.FieldByName('ManualLeague').AsBoolean;
          EnableForOddsOnly := dmMain.Query1.FieldByName('EnableForOddsOnly').AsBoolean;
          UsesGameData := dmMain.Query1.FieldByName('UsesGameData').AsBoolean;
          RequiredGameState := dmMain.Query1.FieldByName('RequiredGameState').AsInteger;
          StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
          EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
          If (Template_Defs_Collection.Count <= 250) then
          begin
            //Add to collection
            Template_Defs_Collection.Insert(TemplateDefRecPtr);
            Template_Defs_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the child template definitions table; iterate for all records in table
    Child_Template_ID_Collection.Clear;
    Child_Template_ID_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Child_Template_IDs_2010');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (ChildTemplateIDRecPtr, SizeOf(ChildTemplateIDRec));
        With ChildTemplateIDRecPtr^ do
        begin
          Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
          Child_Template_ID := dmMain.Query1.FieldByName('Child_Template_ID').AsInteger;
          Child_Default_Enable_State := dmMain.Query1.FieldByName('Child_Default_Enable_State').AsBoolean;
          If (Child_Template_ID_Collection.Count <= 50) then
          begin
            //Add to collection
            Child_Template_ID_Collection.Insert(ChildTemplateIDRecPtr);
            Child_Template_ID_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the template fieldss table; iterate for all records in table
    Template_Fields_Collection.Clear;
    Template_Fields_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Template_Fields_2010');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (TemplateFieldsRecPtr, SizeOf(TemplateFieldsRec));
        With TemplateFieldsRecPtr^ do
        begin
          Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
          Field_ID := dmMain.Query1.FieldByName('Field_ID').AsInteger;
          Field_Type := dmMain.Query1.FieldByName('Field_Type').AsInteger;
          Field_Is_Manual := dmMain.Query1.FieldByName('Field_Is_Manual').AsBoolean;
          Field_Label := dmMain.Query1.FieldByName('Field_Label').AsString;
          Field_Prefix := dmMain.Query1.FieldByName('Field_Prefix').AsString;
          Field_Contents := dmMain.Query1.FieldByName('Field_Contents').AsString;
          Field_Suffix := dmMain.Query1.FieldByName('Field_Suffix').AsString;
          Field_Max_Length := dmMain.Query1.FieldByName('Field_Max_Length').AsInteger;
          Engine_Field_ID := dmMain.Query1.FieldByName('Engine_Field_ID').AsInteger;
          If (Template_Fields_Collection.Count <= 5000) then
          begin
            //Add to collection
            Template_Fields_Collection.Insert(TemplateFieldsRecPtr);
            Template_Fields_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the categories table; iterate for all records in table
    Categories_Collection.Clear;
    Categories_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Categories');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      //Clear the categories tab control string list
      LeagueTab.Tabs.Clear;
      repeat
        //Add item to scripts collection
        GetMem (CategoryRecPtr, SizeOf(CategoryRec));
        With CategoryRecPtr^ do
        begin
          Category_ID := dmMain.Query1.FieldByName('Category_ID').AsInteger;
          Category_Name := dmMain.Query1.FieldByName('Category_Name').AsString;
          Category_Label := dmMain.Query1.FieldByName('Category_Label').AsString;
          Category_Description := dmMain.Query1.FieldByName('Category_Description').AsString;
          Category_Is_Sport := dmMain.Query1.FieldByName('Category_Is_Sport').AsBoolean;
          Category_Sport := dmMain.Query1.FieldByName('Category_Sport').AsString;
          Primary_Data_Source := dmMain.Query1.FieldByName('Primary_Data_Source').AsString;
          Backup_Data_Source := dmMain.Query1.FieldByName('Backup_Data_Source').AsString;
          If (Categories_Collection.Count <= 50) then
          begin
            //Add to collection
            Categories_Collection.Insert(CategoryRecPtr);
            Categories_Collection.Pack;
            //Add the category name to the tab list
            LeagueTab.Tabs.Add (Category_Name);
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the categories table; iterate for all records in table
    Category_Templates_Collection.Clear;
    Category_Templates_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Category_Templates_2010');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (CategoryTemplatesRecPtr, SizeOf(CategoryTemplatesRec));
        With CategoryTemplatesRecPtr^ do
        begin
          Category_ID := dmMain.Query1.FieldByName('Category_ID').AsInteger;
          Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
          Template_Type := dmMain.Query1.FieldByName('Template_Type').AsInteger;
          Template_Description := dmMain.Query1.FieldByName('Template_Description').AsString;
          Template_IsOddsOnly := dmMain.Query1.FieldByName('Template_IsOddsOnly').AsBoolean;
          If (Category_Templates_Collection.Count <= 500) then
          begin
            //Add to collection
            Category_Templates_Collection.Insert(CategoryTemplatesRecPtr);
            Category_Templates_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the Sports table; iterate for all records in table
    Sports_Collection.Clear;
    Sports_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Sports');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (SportRecPtr, SizeOf(SportRec));
        With SportRecPtr^ do
        begin
          Sport_Mnemonic := dmMain.Query1.FieldByName('Sport_Mnemonic').AsString;
          Sport_Description := dmMain.Query1.FieldByName('Sport_Description').AsString;
          Sport_Games_Table_Name := dmMain.Query1.FieldByName('Sport_Games_Table_Name').AsString;
          Sport_Stats_Query := dmMain.Query1.FieldByName('Sport_Stats_Query').AsString;
          If (Sports_Collection.Count <= 50) then
          begin
            //Add to collection
            Sports_Collection.Insert(SportRecPtr);
            Sports_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the record types table; iterate for all records in table
    RecordType_Collection.Clear;
    RecordType_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Record_Types_2010');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (RecordTypeRecPtr, SizeOf(RecordTypeRec));
        With RecordTypeRecPtr^ do
        begin
          Playlist_Type := dmMain.Query1.FieldByName('Playlist_Type').AsInteger;
          Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
          Record_Description := dmMain.Query1.FieldByName('Record_Description').AsString;
          If (RecordType_Collection.Count <= 250) then
          begin
            //Add to collection
            RecordType_Collection.Insert(RecordTypeRecPtr);
            RecordType_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the style chips table; iterate for all records in table
    StyleChip_Collection.Clear;
    StyleChip_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Style_Chips');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to style chip collection
        GetMem (StyleChipRecPtr, SizeOf(StyleChipRec));
        With StyleChipRecPtr^ do
        begin
          StyleChip_Index := dmMain.Query1.FieldByName('StyleChip_Index').AsInteger;
          StyleChip_Description := dmMain.Query1.FieldByName('StyleChip_Description').AsString;
          StyleChip_Code := dmMain.Query1.FieldByName('StyleChip_Code').AsString;
          StyleChip_Type := dmMain.Query1.FieldByName('StyleChip_Type').AsInteger;
          StyleChip_String := dmMain.Query1.FieldByName('StyleChip_String').AsString;
          StyleChip_FontCode := dmMain.Query1.FieldByName('StyleChip_FontCode').AsInteger;
          StyleChip_CharacterCode := dmMain.Query1.FieldByName('StyleChip_CharacterCode').AsInteger;
          If (StyleChip_Collection.Count <= 100) then
          begin
            //Add to collection
            StyleChip_Collection.Insert(StyleChipRecPtr);
            StyleChip_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the custom news header table
    CustomNewsHeader_Collection.Clear;
    CustomNewsHeader_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Custom_News_Headers');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      //Clear the league select combo box entries
      ManualLeagueSelect.Items.Clear;
      dmMain.Query1.First;
      repeat
        //Add item to league codes collection
        GetMem (CustomNewsHeaderRecPtr, SizeOf(CustomNewsHeaderRec));
        With CustomNewsHeaderRecPtr^ do
        begin
          HeaderText := dmMain.Query1.FieldByName('HeadingText').AsString;
          If (CustomNewsHeader_Collection.Count <= 250) then
          begin
            //Add to collection
            CustomNewsHeader_Collection.Insert(CustomNewsHeaderRecPtr);
            CustomNewsHeader_Collection.Pack;
            //Add to manual league selection combo box
            ManualLeagueSelect.Items.Add(CustomNewsHeaderRecPtr^.HeaderText);
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;
    //Set top of combo box
    ManualLeagueSelect.ItemIndex := 0;

    //Load in the data from the weather icons; iterate for all records in table
    Weather_Icon_Collection.Clear;
    Weather_Icon_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Weather_Icons');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to league codes collection
        GetMem (WeatherIconRecPtr, SizeOf(WeatherIconRec));
        With WeatherIconRecPtr^ do
        begin
          Icon_Description := dmMain.Query1.FieldByName('Icon_Description').AsString;
          Icon_Name := dmMain.Query1.FieldByName('Icon_Name').AsString;
          If (Weather_Icon_Collection.Count <= 20) then
          begin
            //Add to collection
            Weather_Icon_Collection.Insert(WeatherIconRecPtr);
            Weather_Icon_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the conference chip text table; iterate for all records in table
    Conference_Chip_Collection.Clear;
    Conference_Chip_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    if (UseNCAATournamentHeaders) then
    begin
      dmMain.Query1.SQL.Add('SELECT * FROM Tournament_Headers');
      dmMain.Query1.Open;
      if (dmMain.Query1.RecordCount > 0) then
      begin
        dmMain.Query1.First;
        repeat
          //Add item to CFB conference chips collection
          GetMem (ConferenceChipRecPtr, SizeOf(ConferenceChipRec));
          With ConferenceChipRecPtr^ do
          begin
            Conference_Name := dmMain.Query1.FieldByName('headerText').AsString;
            Conference_Chip_Character_Value := 0;
            Conference_Chip_text := dmMain.Query1.FieldByName('headerText').AsString;
            If (Conference_Chip_Collection.Count <= 200) then
            begin
              //Add to collection
              Conference_Chip_Collection.Insert(ConferenceChipRecPtr);
              Conference_Chip_Collection.Pack;
            end;
          end;
          //Got to next record
          dmMain.Query1.Next;
        until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
      end;
    end
    else begin
      dmMain.Query1.SQL.Add('SELECT * FROM CFB_Conference_Chips_2010');
      dmMain.Query1.Open;
      if (dmMain.Query1.RecordCount > 0) then
      begin
        dmMain.Query1.First;
        repeat
          //Add item to CFB conference chips collection
          GetMem (ConferenceChipRecPtr, SizeOf(ConferenceChipRec));
          With ConferenceChipRecPtr^ do
          begin
            Conference_Name := dmMain.Query1.FieldByName('Conference_Name').AsString;
            Conference_Chip_Character_Value := dmMain.Query1.FieldByName('Conference_Chip_Character_Value').AsInteger;
            Conference_Chip_text := dmMain.Query1.FieldByName('Conference_Chip_Text').AsString;
            If (Conference_Chip_Collection.Count <= 200) then
            begin
              //Add to collection
              Conference_Chip_Collection.Insert(ConferenceChipRecPtr);
              Conference_Chip_Collection.Pack;
            end;
          end;
          //Got to next record
          dmMain.Query1.Next;
        until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
      end;
    end;
    //Close query
    dmMain.Query1.Active := FALSE;
  except
    //WriteToErrorLog
    EngineInterface.WriteToErrorLog('Error occurred while trying cache data from database');
  end;
end;

//Handler to request complete reload of games from Database
procedure TMainForm.ReloadGamesfromGametrak1Click(Sender: TObject);
begin
  try
    //Refresh games tables
    dmMain.SportbaseQuery.Active := FALSE;
    dmMain.SportbaseQuery.Active := TRUE;
    if (ConnectToCSS = TRUE) then
    begin
      //Close & setup query
      dmMain.CSSQuery.Active := FALSE;
      dmMain.CSSQuery.Active := TRUE;
    end;
  except
    //WriteToErrorLog
    EngineInterface.WriteToErrorLog('Error occurred while trying to refresh games data');
  end;
end;

//Function to return number of zipper playlists in the database corresponding to
//the specified Cart ID
function TMainForm.GetPlaylistCount(PlaylistType: SmallInt; SearchStr: String): SmallInt;
var
  OutCount: SmallInt;
begin
  try
    dmMain.Query1.Close;
    dmMain.Query1.SQL.Clear;
    Case PlaylistType of
     0: dmMain.Query1.SQL.Add('SELECT * FROM Ticker_Groups WHERE Playlist_Description ' +
                          'LIKE ' + QuotedStr('%' + SearchStr + '%'));
     1: dmMain.Query1.SQL.Add('SELECT * FROM Bug_Groups WHERE Playlist_Description ' +
                          'LIKE ' + QuotedStr('%' + SearchStr + '%'));
     2: dmMain.Query1.SQL.Add('SELECT * FROM GameTrax_Groups WHERE Playlist_Description ' +
                          'LIKE ' + QuotedStr('%' + SearchStr + '%'));
    end;
    dmMain.Query1.Open;
    //Get the count
    OutCount := dmMain.Query1.RecordCount;
    //Close the query and return
    dmMain.Query1.Close;
    GetPlaylistCount := OutCount;
  except
    //WriteToErrorLog
    EngineInterface.WriteToErrorLog('Error occurred while trying to run GetPlaylistCount function');
  end;
end;

//Function to take a template ID & return its description
function TMainForm.GetTemplateInformation(TemplateID: SmallInt): TemplateDefsRec;
var
  i: SmallInt;
  TemplateDefsRecPtr: ^TemplateDefsRec;
  OutRec: TemplateDefsRec;
begin
  if (Template_Defs_Collection.Count > 0) then
  begin
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_ID = TemplateID) then
      with OutRec do
      begin
        Template_ID := TemplateDefsRecPtr^.Template_ID;
        Template_Type := TemplateDefsRecPtr^.Template_Type;
        Template_Description := TemplateDefsRecPtr^.Template_Description;
        Record_Type := TemplateDefsRecPtr^.Record_Type;
        Engine_Template_ID := TemplateDefsRecPtr^.Engine_Template_ID;
        Default_Dwell := TemplateDefsRecPtr^.Default_Dwell;
        ManualLeague := TemplateDefsRecPtr^.ManualLeague;
        EnableForOddsOnly := TemplateDefsRecPtr^.EnableForOddsOnly;
        UsesGameData := TemplateDefsRecPtr^.UsesGameData;
        Template_Has_Children := TemplateDefsRecPtr^.Template_Has_Children;
        Template_Is_Child := TemplateDefsRecPtr^.Template_Is_Child;
        StartEnableDateTime := TemplateDefsRecPtr^.StartEnableDateTime;
        EndEnableDateTime := TemplateDefsRecPtr^.EndEnableDateTime;
      end;
    end
  end;
  GetTemplateInformation := OutRec;
end;

//Function to get game time string from GT Server time value
function TMainForm.GetSTGameTimeString(GTimeStr: String): String;
var
  Min, Sec: String;
begin
  //Blank out case where time = 9999 - for MLB
  if (GTimeStr = '9999') then Result := ' '
  //Actual clock time
  else if (StrToIntDef(GTimeStr, -1) <> -1) AND (Length(GTimeStr) = 4) then
  begin
    Min := GTimeStr[1] + GTimeStr[2];
    Sec := GTimeStr[3] + GTimeStr[4];
    Min := IntToStr(StrToInt(GTimeStr[1]+GTimeStr[2]));
    Result := Min + ':' + Sec;
  end
  else if (GTimeStr = 'END-') then Result := 'End'
  else if (GTimeStr = 'POST') then Result := 'PPD'
  else if (GTimeStr = 'SUSP') then Result := 'PPD'
  else if (GTimeStr = 'DELA') then Result := 'DLY'
  else if (GTimeStr = 'RAIN') then Result := 'RD'
  else if (GTimeStr = 'CANC') then Result := 'PPD'
  else Result := ' ';
end;

//Function to take the stat stored procedure name and return the stat information record
function TMainForm.GetStatInfo (StoredProcedureName: String): StatRec;
var
  i: SmallInt;
  StatRecPtr: ^StatRec; //Pointer to stat record type
  OutRec: StatRec;
begin
  OutRec.StatLeague := ' ';
  OutRec.StatType := 0;
  OutRec.StatDescription := ' ';
  OutRec.StatHeader := ' ';
  OutRec.StatStoredProcedure := ' ';
  OutRec.StatDataField := ' ';
  OutRec.StatAbbreviation := ' ';
  OutRec.UseStatQualifier := FALSE;
  OutRec.StatQualifier := ' ';
  OutRec.StatQualifierValue := 0;
  if (Stat_Collection.Count > 0) then
  begin
    for i := 0 to Stat_Collection.Count-1 do
    begin
      StatRecPtr := Stat_Collection.At(i);
      if (Trim(StatRecPtr^.StatStoredProcedure) = Trim(StoredProcedureName)) then
      begin
        OutRec.StatLeague := StatRecPtr^.StatLeague;
        OutRec.StatType := StatRecPtr^.StatType;
        OutRec.StatDescription := StatRecPtr^.StatDescription;
        OutRec.StatHeader := StatRecPtr^.StatHeader;
        OutRec.StatStoredProcedure := StatRecPtr^.StatStoredProcedure;
        OutRec.StatDataField := StatRecPtr^.StatDataField;
        OutRec.StatAbbreviation := StatRecPtr^.StatAbbreviation;
        OutRec.UseStatQualifier := StatRecPtr^.UseStatQualifier;
        OutRec.StatQualifier := StatRecPtr^.StatQualifier;
        OutRec.StatQualifierValue := StatRecPtr^.StatQualifierValue;
      end;
    end;
  end;
  GetStatInfo := OutRec;
end;

//Function to take the Stats Inc. league code and return the display mnemonic
function TMainForm.GetAutomatedLeagueDisplayMnemonic (League: String): String;
var
  i: SmallInt;
  AutomatedLeagueRecPtr: ^AutomatedLeagueRec; //Pointer to team record type
  OutStr: String;
  FoundRecord: Boolean;
begin
  //Init to passed in variable; will return if no substitution found
  OutStr := League;
  if (Automated_League_Collection.Count > 0) then
  begin
    FoundRecord := FALSE;
    i := 0;
    Repeat
      AutomatedLeagueRecPtr := Automated_League_Collection.At(i);
      if (Trim(AutomatedLeagueRecPtr^.SI_LeagueCode) = League) then
      begin
        OutStr := AutomatedLeagueRecPtr^.Display_Mnemonic;
      end;
      Inc(i);
    Until (FoundRecord = TRUE) OR (i = Automated_League_Collection.Count);
  end;
  GetAutomatedLeagueDisplayMnemonic := OutStr;
end;

//Function to get game state
function TMainForm.GetSTGameState(GTIME: String; GPHASE: SmallInt): Integer;
begin
  if (GTIME = '9999') OR (GPHASE = 0) then
    //Game not started
    Result := 0
  else if (GTIME = 'FINA') OR (GTIME = 'SUM-') then
    //Game is final
    Result := 3
  else if (GTIME = 'END-') then
    //End of period/quarter
    Result := 2
  else if (GPHASE <> 0) AND (StrToIntDef(GTIME, -1) <> -1) then
    //Game in progress
    Result := 1
  else
    //All other conditions
    Result := -1; // Game is suspended due to rain or cancelled etc. (GTIME = 'RAIN' , GTIME = 'SUSP', GTIME = 'CANC')
end;

//Function to get the number of fields in a template
function TMainForm.GetTemplateFieldsCount(Template_ID: SmallInt): SmallInt;
var
  i: SmallInt;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  OutVal: SmallInt;
begin
  OutVal := 0;
  If (Template_Fields_Collection.Count > 0) then
  begin
    for i := 0 to Template_Fields_Collection.Count-1 do
    begin
      TemplateFieldsRecPtr := Template_Fields_Collection.At(i);
      if (TemplateFieldsRecPtr^.Template_ID = Template_ID) {AND
         (TemplateFieldsRecPtr^.Field_Label <> 'Unused')} then Inc(OutVal);
    end;
  end;
  GetTemplateFieldsCount := OutVal;
end;

//Function to get the record type based on the template ID
function TMainForm.GetRecordTypeFromTemplateID(Template_ID: SmallInt): SmallInt;
var
  i: SmallInt;
  TemplateDefsRecPtr: ^TemplateDefsRec;
  OutVal: SmallInt;
begin
  OutVal := 0;
  if (Template_Defs_Collection.Count > 0) then
  begin
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_ID = Template_ID) then
        OutVal := TemplateDefsRecPtr^.Record_Type;
    end;
  end;
  GetRecordTypeFromTemplateID := OutVal;
end;

//Handler for change of league tab selection
function TMainForm.GetSportGamesTableName (SportMnemonic: String): String;
var
  i: SmallInt;
  SportRecPtr: ^SportRec;
  OutStr: String;
begin
  OutStr := '';
  if (Sports_Collection.Count > 1) then
  begin
    for i := 0 to Sports_Collection.Count-1 do
    begin
      SportRecPtr := Sports_Collection.At(i);
      if (SportRecPtr^.Sport_Mnemonic = SportMnemonic) then
        OutStr := SportRecPtr^.Sport_Games_Table_Name;
    end;
  end;
  GetSportGamesTableName := OutStr;
end;

//Function to return a record type description base on playlist type and record type code
function TMainForm.GetRecordTypeDescription (Playlist_Type: SmallInt; Record_Type: SmallInt): String;
var
  i: SmallInt;
  RecordTypeRecPtr: ^RecordTypeRec;
  OutStr: String;
begin
  OutStr := '';
  if (RecordType_Collection.Count > 1) then
  begin
    for i := 0 to RecordType_Collection.Count-1 do
    begin
      RecordTypeRecPtr := RecordType_Collection.At(i);
      if (RecordTypeRecPtr^.Playlist_Type = Playlist_Type) AND (RecordTypeRecPtr^.Record_Type = Record_Type) then
        OutStr := RecordTypeRecPtr^.Record_Description;
    end;
  end;
  GetRecordTypeDescription := OutStr;
end;

////////////////////////////////////////////////////////////////////////////////
// HANDLERS FOR GENERAL PROGRAM OPERATIONS
////////////////////////////////////////////////////////////////////////////////
//Event handler for page control change of selection
procedure TMainForm.ProgramModePageControlChange(Sender: TObject);
var
  i: SmallInt;
  CategoryRecPtr: ^CategoryRec;
  ScheduleGameRecPtr: ^ScheduleGameRec;
  NFLSeasonTypeStr: String;
begin
  //Hide manual override panel
  ManualOverridePanel.Visible := FALSE;
  //Handle page change
  Case ProgramModePageControl.ActivePageIndex of
    //Playlist scheduling mode
    0: Begin
         Case PlaylistSelectTabControl.TabIndex of
             //Ticker
          0: begin
               dmMain.tblTicker_Groups.Active := FALSE;
               dmMain.tblTicker_Groups.Active := TRUE;
               dmMain.tblScheduled_Ticker_Groups.Active := FALSE;
               dmMain.tblScheduled_Ticker_Groups.Active := TRUE;
               AvailablePlaylistsGrid.DataSource := dmMain.dsTicker_Groups;
               ScheduledPlaylistsGrid.DataSource := dmMain.dsScheduled_Ticker_Groups;
             end;
             //Bug
          1: begin
               dmMain.tblBug_Groups.Active := FALSE;
               dmMain.tblBug_Groups.Active := TRUE;
               dmMain.tblScheduled_Bug_Groups.Active := FALSE;
               dmMain.tblScheduled_Bug_Groups.Active := TRUE;
               AvailablePlaylistsGrid.DataSource := dmMain.dsBug_Groups;
               ScheduledPlaylistsGrid.DataSource := dmMain.dsScheduled_Bug_Groups;
             end;
             //Gametrax
          2: begin
               dmMain.tblGameTrax_Groups.Active := FALSE;
               dmMain.tblGameTrax_Groups.Active := TRUE;
               dmMain.tblScheduled_GameTrax_Groups.Active := FALSE;
               dmMain.tblScheduled_GameTrax_Groups.Active := TRUE;
               AvailablePlaylistsGrid.DataSource := dmMain.dsGameTrax_Groups;
               ScheduledPlaylistsGrid.DataSource := dmMain.dsScheduled_GameTrax_Groups;
             end;
         end;
       end;
    //Ticker authoring mode
    1: Begin
         //Check to see if category tab is a sport; if so, show and populate games grid
         CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
         //Now, populate the templates grid by querying the database
         dmMain.Query2.Active := FALSE;
         dmMain.Query2.SQL.Clear;
         dmMain.Query2.Filtered := TRUE;
         //Filter for templates applicable to current playlist type
         dmMain.Query2.Filter := 'Template_Type = ' + IntToStr(PlaylistSelectTabControl.TabIndex+1);
         dmMain.Query2.SQL.Add ('SELECT * FROM Category_Templates_2010 WHERE ' +
           'Category_ID = ' + IntToStr(CategoryRecPtr^.Category_ID));
         dmMain.Query2.Active := TRUE;
       end;
    //DB Maint
    2: Begin
         Case PlaylistSelectTabControl.TabIndex of
             //Ticker
          0: begin
               dmMain.tblTicker_Groups.Active := FALSE;
               dmMain.tblTicker_Groups.Active := TRUE;
               DatabaseMaintPlaylistGrid.DataSource := dmMain.dsTicker_Groups;
             end;
             //Bug
          1: begin
               dmMain.tblBug_Groups.Active := FALSE;
               dmMain.tblBug_Groups.Active := TRUE;
               DatabaseMaintPlaylistGrid.DataSource := dmMain.dsBug_Groups;
             end;
             //Gametrax
          2: begin
               dmMain.tblGameTrax_Groups.Active := FALSE;
               dmMain.tblGameTrax_Groups.Active := TRUE;
               DatabaseMaintPlaylistGrid.DataSource := dmMain.dsGameTrax_Groups;
             end;
         end;
       end;
        //GameTrax Setup
    3:  Begin
          try
            //Clear the collection
            Schedule_Game_Collection.Clear;
            Schedule_Game_Collection.Pack;
            //Show game exclusions
            dmMain.tblGameTrax_Regional_Game_Exclusions.Active := TRUE;
            CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
            //Populate the collection of scheduled games - use CSS unless it's disabled
            if (ConnectToCSS) then
            begin
              if (dmMain.CSSQuery.RecordCount > 0) then
              begin
                dmMain.CSSQuery.First;
                for i := 0 to dmMain.CSSQuery.RecordCount-1 do
                begin
                  GetMem(ScheduleGameRecPtr, SizeOf(ScheduleGameRec));
                  With ScheduleGameRecPtr^ do
                  begin
                    CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                    NFLDM_GameID := dmMain.CSSQuery.FieldByName('GameKey').AsInteger; //Set Game Key
                    SI_GameID := '0';
                    ST_GameID := '0';
                    Description := dmMain.CSSQuery.FieldByname('awayName').AsString + ' @ ' +
                      dmMain.CSSQuery.FieldByname('homeName').AsString;
                  end;
                  Schedule_Game_Collection.Insert(ScheduleGameRecPtr);
                  Schedule_Game_Collection.Pack;
                  dmMain.CSSQuery.Next;
                end;
              end;
            end
            else if (ConnectToCSS = FALSE) AND (CategoryRecPtr^.Category_Sport='NFL') then
            begin
              if (dmMain.DatamartQuery.RecordCount > 0) then
              begin
                dmMain.DatamartQuery.First;
                for i := 0 to dmMain.DatamartQuery.RecordCount-1 do
                begin
                  GetMem(ScheduleGameRecPtr, SizeOf(ScheduleGameRec));
                  With ScheduleGameRecPtr^ do
                  begin
                    CSS_GameID := 0;
                    NFLDM_GameID := dmMain.DatamartQuery.FieldByname('GameKey').AsInteger;
                    SI_GameID := '0';
                    ST_GameID := '0';
                    Description := dmMain.DatamartQuery.FieldByname('VClubCityName').AsString + ' @ ' +
                      dmMain.DatamartQuery.FieldByname('HClubCityName').AsString;
                  end;
                  Schedule_Game_Collection.Insert(ScheduleGameRecPtr);
                  Schedule_Game_Collection.Pack;
                  dmMain.DatamartQuery.Next;
                end;
              end;
            end
            else if (UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC') OR
                    (UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC')then
            begin
              if (dmMain.SportbaseQuery.RecordCount > 0) then
              begin
                dmMain.SportbaseQuery.First;
                for i := 0 to dmMain.SportbaseQuery.RecordCount-1 do
                begin
                  GetMem(ScheduleGameRecPtr, SizeOf(ScheduleGameRec));
                  With ScheduleGameRecPtr^ do
                  begin
                    CSS_GameID := 0;
                    NFLDM_GameID := -1; //Default to blank for now
                    SI_GameID := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                    ST_GameID := '0';
                    Description := dmMain.SportbaseQuery.FieldByname('VCity').AsString + ' @ ' +
                      dmMain.SportbaseQuery.FieldByname('HCity').AsString;
                  end;
                  Schedule_Game_Collection.Insert(ScheduleGameRecPtr);
                  Schedule_Game_Collection.Pack;
                  dmMain.SportbaseQuery.Next;
                end;
              end;
            end;
          except
            //WriteToErrorLog
            EngineInterface.WriteToErrorLog('Error occurred while trying to setup Gametrax Game Exclusions panel');
          end;
          //Refresh the games grid
          RefreshExclusionGamesGrid;
        end;
        //Twitter Setup
    4:  Begin
          try
            //Clear the collection
            Schedule_Game_Collection.Clear;
            Schedule_Game_Collection.Pack;
            //Show Twitter talent/game assignments
            dmMain.tblTwitterGameAssignments.Active := FALSE;
            dmMain.tblTwitterGameAssignments.Active := TRUE;
            CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
            //Populate the collection of scheduled games - use CSS unless it's disabled
            if (ConnectToCSS) then
            begin
              if (dmMain.CSSQuery.RecordCount > 0) then
              begin
                dmMain.CSSQuery.First;
                for i := 0 to dmMain.CSSQuery.RecordCount-1 do
                begin
                  GetMem(ScheduleGameRecPtr, SizeOf(ScheduleGameRec));
                  With ScheduleGameRecPtr^ do
                  begin
                    CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                    NFLDM_GameID := dmMain.CSSQuery.FieldByName('GameKey').AsInteger; //Set Game Key
                    SI_GameID := '0';
                    ST_GameID := '0';
                    Description := dmMain.CSSQuery.FieldByname('awayName').AsString + ' @ ' +
                      dmMain.CSSQuery.FieldByname('homeName').AsString;
                  end;
                  Schedule_Game_Collection.Insert(ScheduleGameRecPtr);
                  Schedule_Game_Collection.Pack;
                  dmMain.CSSQuery.Next;
                end;
              end;
            end
            else if (ConnectToCSS = FALSE) AND (CategoryRecPtr^.Category_Sport='NFL') then
            begin
              if (dmMain.DatamartQuery.RecordCount > 0) then
              begin
                dmMain.DatamartQuery.First;
                for i := 0 to dmMain.DatamartQuery.RecordCount-1 do
                begin
                  GetMem(ScheduleGameRecPtr, SizeOf(ScheduleGameRec));
                  With ScheduleGameRecPtr^ do
                  begin
                    CSS_GameID := 0;
                    NFLDM_GameID := dmMain.DatamartQuery.FieldByname('GameKey').AsInteger;
                    SI_GameID := '0';
                    ST_GameID := '0';
                    Description := dmMain.DatamartQuery.FieldByname('VClubCityName').AsString + ' @ ' +
                      dmMain.DatamartQuery.FieldByname('HClubCityName').AsString;
                  end;
                  Schedule_Game_Collection.Insert(ScheduleGameRecPtr);
                  Schedule_Game_Collection.Pack;
                  dmMain.DatamartQuery.Next;
                end;
              end;
            end
            else if (UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC') OR
                    (UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC')then
            begin
              if (dmMain.SportbaseQuery.RecordCount > 0) then
              begin
                dmMain.SportbaseQuery.First;
                for i := 0 to dmMain.SportbaseQuery.RecordCount-1 do
                begin
                  GetMem(ScheduleGameRecPtr, SizeOf(ScheduleGameRec));
                  With ScheduleGameRecPtr^ do
                  begin
                    CSS_GameID := 0;
                    NFLDM_GameID := -1; //Default to blank for now
                    SI_GameID := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                    ST_GameID := '0';
                    Description := dmMain.SportbaseQuery.FieldByname('VCity').AsString + ' @ ' +
                      dmMain.SportbaseQuery.FieldByname('HCity').AsString;
                  end;
                  Schedule_Game_Collection.Insert(ScheduleGameRecPtr);
                  Schedule_Game_Collection.Pack;
                  dmMain.SportbaseQuery.Next;
                end;
              end;
            end;
          except
            //WriteToErrorLog
            EngineInterface.WriteToErrorLog('Error occurred while trying to setup Gametrax Game Exclusions panel');
          end;
          //Refreah the Talent grid
          //Refresh the games grid
          RefreshTwitterTalentGrid;
          RefreshTwitterGamesGrid;
        end;
  end;
  //Set new page index
  LastPageIndex := ProgramModePageControl.ActivePageIndex;
end;

//Handler for PlaylistSelectTabControl tab change
procedure TMainForm.PlaylistSelectTabControlChange(Sender: TObject);
var
  CategoryRecPtr: ^CategoryRec;
  SeasonStr: String;
  QueryString: String;
  SQLText: String;
  Now, Year, Month, Day: Word;
  TodayStr, YesterdayStr: String;
  TableName: String;
begin
  Case PlaylistSelectTabControl.TabIndex of
    //Ticker schedule
   0: begin
        DatabaseMaintPlaylistGrid.DataSource := dmMain.dsTicker_Groups;
        AvailablePlaylistsGrid.DataSource := dmMain.dsTicker_Groups;
        ScheduledPlaylistsGrid.DataSource := dmMain.dsScheduled_Ticker_Groups;
        LastSaveTimeLabel.Caption :=
          PlaylistInfo[PlaylistSelectTabControl.TabIndex+1].LastPlaylistSaveTimeString;
        //Hide score update panel
        ScoreUpdatePanel.Visible := FALSE;
        FantasyUpdatePanel.Visible := FALSE;
        NewsPromoUpdatePanel.Visible := FALSE;
        GameOverridePanel.Visible := FALSE;
      end;
      //Bug schedule
   1: begin
        DatabaseMaintPlaylistGrid.DataSource := dmMain.dsBug_Groups;
        AvailablePlaylistsGrid.DataSource := dmMain.dsBug_Groups;
        ScheduledPlaylistsGrid.DataSource := dmMain.dsScheduled_Bug_Groups;
        //Hide score update panel
        ScoreUpdatePanel.Visible := FALSE;
        FantasyUpdatePanel.Visible := FALSE;
        NewsPromoUpdatePanel.Visible := FALSE;
        GameOverridePanel.Visible := FALSE;
      end;
      //GameTrax schedule
   2: begin
        DatabaseMaintPlaylistGrid.DataSource := dmMain.dsGameTrax_Groups;
        AvailablePlaylistsGrid.DataSource := dmMain.dsGameTrax_Groups;
        ScheduledPlaylistsGrid.DataSource := dmMain.dsScheduled_GameTrax_Groups;
        //Hide score update panel
        ScoreUpdatePanel.Visible := FALSE;
        FantasyUpdatePanel.Visible := FALSE;
        NewsPromoUpdatePanel.Visible := FALSE;
        GameOverridePanel.Visible := FALSE;
      end;
      //Score Updates
   3: begin
        ScoreUpdatePanel.Visible := TRUE;
        FantasyUpdatePanel.Visible := FALSE;
        NewsPromoUpdatePanel.Visible := FALSE;
        GameOverridePanel.Visible := FALSE;
        //If it's the NFL, check to see that Datamart is the source
        if (CurrentDataSource = 'NFLDM') then
        begin
          //Set visible grid
          SIGamesDBGrid2.Visible := FALSE;
          if (ConnectToCSS) then
          begin
            CSSGamesDBGrid2.Visible := TRUE;
            NFLDMGamesDBGrid2.Visible := FALSE;
            //Close & setup query
            try
              CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
              dmMain.CSSQuery.Active := FALSE;
              dmMain.CSSQuery.SQL.Clear;
              SQLText := '/* */vds_GetGames';
              dmMain.CSSQuery.SQL.Add(SQLText);
              //Activate query
              if (Trim(CategoryRecPtr^.Category_Sport) <> '') then
                dmMain.CSSQuery.Active := TRUE;
            except
              //WriteToErrorLog
              EngineInterface.WriteToErrorLog('Error occurred while trying to refresh list for live game updates');
            end;
          end
          else begin
            CSSGamesDBGrid2.Visible := FALSE;
            NFLDMGamesDBGrid2.Visible := TRUE;
            //Close & setup query
            try
              dmMain.DatamartQuery.Active := FALSE;
              dmMain.DatamartQuery.SQL.Clear;
              //Set season type
              Case NFLWeekInfo.SeasonType of
                0: SeasonStr := 'PRE';
                1: SeasonStr := 'REG';
                2: SeasonStr := 'POST';
              end;
              SQLText := '/* */p_GetWeekSchedule ' + IntToStr(NFLWeekInfo.SeasonYear) + ', ' +
                     QuotedStr(SeasonStr) + ', ' + IntToStr(NFLWeekInfo.Week);
              dmMain.DatamartQuery.SQL.Add(SQLText);
              //Activate query
              dmMain.DatamartQuery.Active := TRUE;
            except
              //WriteToErrorLog
              EngineInterface.WriteToErrorLog('Error occurred while trying to refresh list for live game updates');
            end;
          end;
        end
        //Not the Datamart, check for use of Stats Inc.
        else if (CurrentDataSource = 'STATSINC') then
        begin
          //Set visible grid
          SIGamesDBGrid2.Visible := TRUE;
          CSSGamesDBGrid2.Visible := FALSE;
          NFLDMGamesDBGrid2.Visible := FALSE;
          //Setup query
          try
            dmMain.SportbaseQuery.Active := FALSE;
            dmMain.SportbaseQuery.SQL.Clear;
            //Now, add so that only today's and yesterday's games are shown
            DecodeDate(Now, Year, Month, Day);
            TodayStr := IntToStr(Month) + '/' + IntToStr(Day) + '/' + IntToStr(Year);
            DecodeDate(Now-1, Year, Month, Day);
            YesterdayStr := IntToStr(Month) + '/' + IntToStr(Day) + '/' + IntToStr(Year);
            CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
            TableName := GetSportGamesTableName(CategoryRecPtr^.Category_Sport);
            QueryString := 'SELECT * FROM ' + TableName;
            QueryString := QueryString + ' WHERE ((GDate = ' + QuotedStr(TodayStr) +
              ') OR (GDate = ' + QuotedStr(YesterdayStr) + '))';
            //Set to order by date and time
            QueryString := QueryString + ' ORDER BY GDATE ASC, CAST(START AS DATETIME) ASC';
            dmMain.SportbaseQuery.SQL.Add(QueryString);
            dmMain.SportbaseQuery.Active := TRUE;
          except
            //WriteToErrorLog
            EngineInterface.WriteToErrorLog('Error occurred while trying to refresh list for live game updates');
          end;
        end
        //Setup query and open based on sport
        else if (CurrentDataSource = 'CBSCSS') then
        begin
          CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
          SIGamesDBGrid2.Visible := FALSE;
          CSSGamesDBGrid2.Visible := TRUE;
          NFLDMGamesDBGrid2.Visible := FALSE;
          //Close & setup query
          try
            dmMain.CSSQuery.Active := FALSE;
            dmMain.CSSQuery.SQL.Clear;
            SQLText := '/* */vds_GetGames';
            dmMain.CSSQuery.SQL.Add(SQLText);
            //Activate query
            if (Trim(CategoryRecPtr^.Category_Sport) <> '') then
              dmMain.CSSQuery.Active := TRUE;
          except
            //WriteToErrorLog
            EngineInterface.WriteToErrorLog('Error occurred while trying to refresh list for live game updates');
          end;
        end;
      end;
      //Fantasy updates
   4: begin
        FantasyUpdatePanel.Visible := TRUE;
        NewsPromoUpdatePanel.Visible := FALSE;
        ScoreUpdatePanel.Visible := FALSE;
        GameOverridePanel.Visible := FALSE;
      end;
      //News/Promo Updates
   5: begin
        GameOverridePanel.Visible := FALSE;
        ScoreUpdatePanel.Visible := FALSE;
        FantasyUpdatePanel.Visible := FALSE;
        NewsPromoUpdatePanel.Visible := TRUE;
      end;
      //NFL GSIS Game Overrides
   6: begin
        dmMain.tblNFL_GSIS_Overrides.Active := TRUE;
        GameOverridePanel.Visible := TRUE;
        ScoreUpdatePanel.Visible := FALSE;
        FantasyUpdatePanel.Visible := FALSE;
        NewsPromoUpdatePanel.Visible := FALSE;
      end;
  end;

  //Set playlist name
  Edit1.Text := PlaylistInfo[PlaylistSelectTabControl.TabIndex+1].PlaylistName;
  PlaylistNameLabel.Caption := PlaylistInfo[PlaylistSelectTabControl.TabIndex+1].PlaylistName;
  LastSaveTimeLabel.Caption :=
    PlaylistInfo[PlaylistSelectTabControl.TabIndex+1].LastPlaylistSaveTimeString;

  //Set labels and control attributes
  Case PlaylistSelectTabControl.TabIndex of
      //Studio Ticker
   0: begin
        SchedulePanel.Color := clSilver;
        DataEntryPanel.Color := clSilver;
        DBMaintPanel.Color := clSilver;
        PlaylistModeLabel.Color := clSilver;
        PlaylistModeLabel.Caption := 'STUDIO TICKER PLAYLIST MODE';
      end;
      //Studio Bug
   1: begin
        SchedulePanel.Color := clSkyBlue;
        DataEntryPanel.Color := clSkyBlue;
        DBMaintPanel.Color := clSkyBLue;
        PlaylistModeLabel.Color := clSkyBlue;
        PlaylistModeLabel.Caption := 'STUDIO BUG PLAYLIST MODE';
      end;
      //Gametrax
   2: begin
        SchedulePanel.Color := clGray;
        DataEntryPanel.Color := clGray;
        DBMaintPanel.Color := clGray;
        PlaylistModeLabel.Color := clGray;
        PlaylistModeLabel.Caption := 'GAMETRAX TICKER MODE';
      end;
  end;

  //Check to see if category tab is a sport; if so, show and populate games grid
  CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);

  //Now, populate the templates grid by querying the database
  try
    dmMain.Query2.Active := FALSE;
    dmMain.Query2.SQL.Clear;
    dmMain.Query2.Filtered := TRUE;
    //Filter for templates applicable to current playlist type
    dmMain.Query2.Filter := 'Template_Type = ' + IntToStr(PlaylistSelectTabControl.TabIndex+1);
    dmMain.Query2.SQL.Add ('SELECT * FROM Category_Templates_2010 WHERE ' +
      'Category_ID = ' + IntToStr(CategoryRecPtr^.Category_ID));
    dmMain.Query2.Active := TRUE;
  except
    //WriteToErrorLog
    EngineInterface.WriteToErrorLog('Error occurred while trying to load applicable templates');
  end;
  //Refresh the grids
  RefreshPlaylistGrid;
  RefreshEntryFieldsGrid(CurrentTickerDisplayMode);
  //Force refresh of available templates
  LeagueTabChange(Self);
end;

//Handler for change in schedule selection
procedure TMainForm.ScheduleSelectTabChange(Sender: TObject);
begin
end;

//Handler for change in database maintenance selection
procedure TMainForm.DatabaseMaintSelectTabChange(Sender: TObject);
begin
end;

//Handler for check/uncheck of odds only checkbox
procedure TMainForm.ShowOddsOnlyClick(Sender: TObject);
begin
  ManualOverridePanel.Color := clBtnFace;
  LeagueTabChange(Self);
end;

//Handler for a change in the league select tab
procedure TMainForm.LeagueTabChange(Sender: TObject);
var
  CategoryRecPtr: ^CategoryRec;
  Day, Month, Year: Word;
  TodayStr, YesterdayStr: String;
  QueryString: String;
  TableName: String;
  OddsOnly: SmallInt;
  SQLText: String;
  SeasonStr: String;
begin
  //Hide control panel for launching CSS game note viewer by default
  CSSGameNotePanel.Visible := FALSE;
  //Check to see if category tab is a sport; if so, show and populate games grid
  CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
  //First, check to see if sponsors. If so, show sponsor grid
  if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
  begin
    SponsorsDBGrid.Visible := TRUE;
    dmMain.tblSponsor_Logos.Active := TRUE;
    SponsorsDBGrid.DataSource := dmMain.dsSponsor_Logos;
    SponsorDwellPanel.Visible := TRUE;
    SIGamesDBGrid.Visible := FALSE;
    CSSGamesDBGrid.Visible := FALSE;
    NFLDMGamesDBGrid.Visible := FALSE;
    ManualOverridePanel.Visible := FALSE;
    ManualLeaguePanel.Visible := FALSE;
  end
  //Next, check if it's the promo logos
  else if (UpperCase(CategoryRecPtr^.Category_Name) = 'PROMOS') then
  begin
    SponsorsDBGrid.Visible := TRUE;
    dmMain.tblPromo_Logos.Active := TRUE;
    SponsorsDBGrid.DataSource := dmMain.dsPromo_Logos;
    SponsorDwellPanel.Visible := FALSE;
    SIGamesDBGrid.Visible := FALSE;
    CSSGamesDBGrid.Visible := FALSE;
    NFLDMGamesDBGrid.Visible := FALSE;
    ManualOverridePanel.Visible := FALSE;
    ManualLeaguePanel.Visible := TRUE;
  end
  //Work done here for getting schedules for automated sports
  else if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
  begin
    //Get the table name in case it's Stats Inc.
    TableName := GetSportGamesTableName(CategoryRecPtr^.Category_Sport);

    SponsorsDBGrid.Visible := FALSE;
    SponsorDwellPanel.Visible := FALSE;
    ManualOverridePanel.Visible := FALSE;
    ManualLeaguePanel.Visible := TRUE;

    //Check which data source to use
    try
      dmMain.Query3.Active := FALSE;
      dmMain.Query3.SQL.Clear;
      dmMain.Query3.SQL.Add('SELECT * FROM Flags');
      dmMain.Query3.Active := TRUE;
      UseBackupDataSource := dmMain.Query3.FieldByName('UseBackupDataSource').AsBoolean;
      if (dmMain.Query3.RecordCount > 0) then
      begin
        UsebackupDataSource := dmMain.Query3.FieldByName('UsebackupDataSource').AsBoolean;
      end;
      //Check to see if VDS is set to be the primary data source
      if (UseBackupDataSource) then
      begin
        //Set data source label
        DataSourceLabel.Caption := 'Data Source: Backup';
        //Setup query and open based on sport
        //If it's the NFL, check to see that Datamart is the source
        if (CategoryRecPtr^.Backup_Data_Source = 'NFLDM') then
        begin
          if (ConnectToCSS) then CSSGameNotePanel.Visible := TRUE;
          //Set visible grid
          SIGamesDBGrid.Visible := FALSE;
          //Changed
          if (ConnectToCSS) then
          begin
            //Use CSS as source for data since CSS is enabled
            CSSGamesDBGrid.Visible := TRUE;
            NFLDMGamesDBGrid.Visible := FALSE;
            //Set current data source
            //Close & setup query
            dmMain.CSSQuery.Active := FALSE;
            dmMain.CSSQuery.SQL.Clear;
            SQLText := '/* */vds_GetGames';
            dmMain.CSSQuery.SQL.Add(SQLText);
            //Activate query
            dmMain.CSSQuery.Active := TRUE;
          end
          else begin
            //Don't CSS as source for data since CSS is disabled
            CSSGamesDBGrid.Visible := FALSE;
            NFLDMGamesDBGrid.Visible := TRUE;
            //Close & setup query
            dmMain.DatamartQuery.Active := FALSE;
            dmMain.DatamartQuery.SQL.Clear;

            //Set season type
            Case NFLWeekInfo.SeasonType of
              0: SeasonStr := 'PRE';
              1: SeasonStr := 'REG';
              2: SeasonStr := 'POST';
            end;
            SQLText := '/* */p_GetWeekSchedule ' + IntToStr(NFLWeekInfo.SeasonYear) + ', ' +
                   QuotedStr(SeasonStr) + ', ' + IntToStr(NFLWeekInfo.Week);
            dmMain.DatamartQuery.SQL.Add(SQLText);
            //Activate query
            dmMain.DatamartQuery.Active := TRUE;
          end;
          CurrentDataSource := 'NFLDM';
        end
        //Not the Datamart, check for use of Stats Inc.
        else if (CategoryRecPtr^.Backup_Data_Source = 'STATSINC') AND (Trim(TableName) <> '') then
        begin
          //Set visible grid
          SIGamesDBGrid.Visible := TRUE;
          CSSGamesDBGrid.Visible := FALSE;
          NFLDMGamesDBGrid.Visible := FALSE;
          //Setup query
          dmMain.SportbaseQuery.Active := FALSE;
          dmMain.SportbaseQuery.SQL.Clear;
          QueryString := 'SELECT * FROM ' + TableName;
          //Now, add so that only today's and yesterday's games are shown
          DecodeDate(Now, Year, Month, Day);
          TodayStr := IntToStr(Month) + '/' + IntToStr(Day) + '/' + IntToStr(Year);
          DecodeDate(Now-1, Year, Month, Day);
          YesterdayStr := IntToStr(Month) + '/' + IntToStr(Day) + '/' + IntToStr(Year);
          QueryString := QueryString + ' WHERE ((GDate = ' + QuotedStr(TodayStr) +
            ') OR (GDate = ' + QuotedStr(YesterdayStr) + '))';
          //Set to order by date and time
          QueryString := QueryString + ' ORDER BY GDATE ASC, CAST(START AS DATETIME) ASC';
          dmMain.SportbaseQuery.SQL.Add(QueryString);
          dmMain.SportbaseQuery.Active := TRUE;
          //Set current data source
          CurrentDataSource := 'STATSINC';
        end
        //Setup query and open based on sport
        else if (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS') AND (Trim(TableName) <> '') then
        begin
          //Set visible grid
          SIGamesDBGrid.Visible := FALSE;
          CSSGamesDBGrid.Visible := TRUE;
          NFLDMGamesDBGrid.Visible := FALSE;
          //Close & setup query
          dmMain.CSSQuery.Active := FALSE;
          dmMain.CSSQuery.SQL.Clear;
          SQLText := '/* */vds_GetGames';
          dmMain.CSSQuery.SQL.Add(SQLText);
          //Activate query
          dmMain.CSSQuery.Active := TRUE;
          //Set current data source
          CurrentDataSource := 'CBSCSS';
        end
        //Manual templates
        else begin
          SponsorsDBGrid.Visible := FALSE;
          SponsorDwellPanel.Visible := FALSE;
          SIGamesDBGrid.Visible := FALSE;
          CSSGamesDBGrid.Visible := FALSE;
          NFLDMGamesDBGrid.Visible := FALSE;
          ManualOverridePanel.Visible := FALSE;
          //Refresh the league comabo box
          ManualLeaguePanel.Visible := TRUE;
        end;
      end
      //Primary is VDS
      else begin
        //Hide control panel for launching CSS game note viewer
        if (ConnectToCSS) then
          CSSGameNotePanel.Visible := TRUE;
        //Set data source label
        DataSourceLabel.Caption := 'Data Source: Primary';
        //Set visible grid
        SIGamesDBGrid.Visible := FALSE;
        if (ConnectToCSS) then
        begin
          CSSGamesDBGrid.Visible := TRUE;
          NFLDMGamesDBGrid.Visible := FALSE;
        end
        else begin
          CSSGamesDBGrid.Visible := FALSE;
          NFLDMGamesDBGrid.Visible := TRUE;
        end;
        //Setup query and open based on sport
        if (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS') AND (Trim(TableName) <> '') then
        begin
          //Close & setup query
          dmMain.CSSQuery.Active := FALSE;
          dmMain.CSSQuery.SQL.Clear;
          SQLText := '/* */vds_GetGames';
          dmMain.CSSQuery.SQL.Add(SQLText);
          //Activate query
          dmMain.CSSQuery.Active := TRUE;
          //Set current data source
          CurrentDataSource := 'CBSCSS';
        end
        else if (CategoryRecPtr^.Primary_Data_Source = 'NFLDM') then
        begin
          //Set visible grid
          //Change
          SIGamesDBGrid.Visible := FALSE;
          if (ConnectToCSS) then
          begin
            //Close & setup query
            dmMain.CSSQuery.Active := FALSE;
            dmMain.CSSQuery.SQL.Clear;
            SQLText := '/* */vds_GetGames';
            dmMain.CSSQuery.SQL.Add(SQLText);
            //Activate query
            dmMain.CSSQuery.Active := TRUE;
            CSSGamesDBGrid.Visible := TRUE;
            NFLDMGamesDBGrid.Visible := FALSE;
          end
          else begin
            //Close & setup query
            dmMain.DatamartQuery.Active := FALSE;
            dmMain.DatamartQuery.SQL.Clear;

            //Set season type
            Case NFLWeekInfo.SeasonType of
              0: SeasonStr := 'PRE';
              1: SeasonStr := 'REG';
              2: SeasonStr := 'POST';
            end;
            SQLText := '/* */p_GetWeekSchedule ' + IntToStr(NFLWeekInfo.SeasonYear) + ', ' +
                   QuotedStr(SeasonStr) + ', ' + IntToStr(NFLWeekInfo.Week);
            dmMain.DatamartQuery.SQL.Add(SQLText);
            //Activate query
            dmMain.DatamartQuery.Active := TRUE;
            CSSGamesDBGrid.Visible := FALSE;
            NFLDMGamesDBGrid.Visible := TRUE;
          end;
          //Set current data source
          CurrentDataSource := 'NFLDM';
        end
        //Not the Datamart, check for use of Stats Inc.
        else if (CategoryRecPtr^.Primary_Data_Source = 'STATSINC') AND (Trim(TableName) <> '') then
        begin
          //Set visible grid
          SIGamesDBGrid.Visible := TRUE;
          CSSGamesDBGrid.Visible := FALSE;
          NFLDMGamesDBGrid.Visible := FALSE;
          //Setup query
          dmMain.SportbaseQuery.Active := FALSE;
          dmMain.SportbaseQuery.SQL.Clear;
          QueryString := 'SELECT * FROM ' + TableName;
          //Now, add so that only today's and yesterday's games are shown
          DecodeDate(Now, Year, Month, Day);
          TodayStr := IntToStr(Month) + '/' + IntToStr(Day) + '/' + IntToStr(Year);
          DecodeDate(Now-1, Year, Month, Day);
          YesterdayStr := IntToStr(Month) + '/' + IntToStr(Day) + '/' + IntToStr(Year);
          QueryString := QueryString + ' WHERE ((GDate = ' + QuotedStr(TodayStr) +
            ') OR (GDate = ' + QuotedStr(YesterdayStr) + '))';
          //Set to order by date and time
          QueryString := QueryString + ' ORDER BY GDATE ASC, CAST(START AS DATETIME) ASC';
          dmMain.SportbaseQuery.SQL.Add(QueryString);
          dmMain.SportbaseQuery.Active := TRUE;
          //Set current data source
          CurrentDataSource := 'STATSINC';
        end
        //Manual templates
        else begin
          SponsorsDBGrid.Visible := FALSE;
          SponsorDwellPanel.Visible := FALSE;
          SIGamesDBGrid.Visible := FALSE;
          CSSGamesDBGrid.Visible := FALSE;
          NFLDMGamesDBGrid.Visible := FALSE;
          ManualOverridePanel.Visible := FALSE;
          //Refresh the league comabo box
          ManualLeaguePanel.Visible := TRUE;
        end;
      end;
    except
      //WriteToErrorLog
      EngineInterface.WriteToErrorLog('Error occurred while trying to process change in league');
    end;
  end;
  //Now, populate the templates grid by querying the database
  try
    dmMain.Query2.Active := FALSE;
    dmMain.Query2.SQL.Clear;
    dmMain.Query2.Filtered := TRUE;
    //Filter for templates applicable to current playlist type
    dmMain.Query2.Filter := 'Template_Type = ' + IntToStr(PlaylistSelectTabControl.TabIndex+1);
    //Add filter to query to ensure that only applicable templates are displayed (odds only)
    OddsOnly := 0;
    dmMain.Query2.SQL.Add ('SELECT * FROM Category_Templates_2010 WHERE ' +
      'Category_ID = ' + IntToStr(CategoryRecPtr^.Category_ID) + ' AND Template_IsOddsOnly = ' + IntToStr(OddsOnly));
    dmMain.Query2.Active := TRUE;
  except
    //WriteToErrorLog
    EngineInterface.WriteToErrorLog('Error occurred while trying to retrieve template info for category');
  end;
end;

//Handler for playlist collapse/expand button
procedure TMainForm.ExpandButtonClick(Sender: TObject);
begin
  if (ExpandButton.Caption = 'Expand Playlist') then
  begin
    ExpandButton.Caption := 'Collapse Playlist';
    PlaylistGrid.Top := 43;
    PlaylistGrid.Height := 734;
  end
  else begin
    ExpandButton.Caption := 'Expand Playlist';
    PlaylistGrid.Top := 43;
    PlaylistGrid.Height := 460;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES AND HANDLERS FOR ZIPPER PLAYLIST CREATION AND EDITING
////////////////////////////////////////////////////////////////////////////////
//General procedure to clear and refresh the trial playlist grid
procedure TMainForm.RefreshPlaylistGrid;
var
  i: SmallInt;
  TickerPtr: ^TickerRec;
  BugPtr: ^BugRec;
  GameTraxPtr: ^GameTraxRec;
  CollectionCount: SmallInt;
  AutoLeagueMnemonic: String;
  CurrentRow, CurrentTopRow: SmallInt;
begin
  //Get current row
  CurrentRow := PlaylistGrid.CurrentDataRow;
  CurrentTopRow := PlaylistGrid.TopRow;
  //Clear grid values
  if (PlaylistGrid.Rows > 0) then
    PlaylistGrid.DeleteRows (1, PlaylistGrid.Rows);
  //Init values
  Case PlaylistSelectTabControl.TabIndex of
   0: CollectionCount := StudioTicker_Collection.Count;
   1: CollectionCount := StudioBug_Collection.Count;
   2: CollectionCount := GameTrax_Collection.Count;
  end;
  if (CollectionCount > 0) then
  begin
    PlaylistGrid.StoreData := TRUE;
    PlaylistGrid.Cols := 8;
    PlaylistGrid.Rows := CollectionCount;
    //Populate the grid
    for i := 0 to CollectionCount-1 do
    begin
      Case PlaylistSelectTabControl.TabIndex of
          //Ticker
       0: begin
            TickerPtr := StudioTicker_Collection.At(i);
            PlaylistGrid.Cell[1,i+1] := IntToStr(i+1); //Index
            //Check if automated league; if so, lookup display mnemonic
            PlaylistGrid.Cell[2,i+1] := TickerPtr^.CSS_GameID; //CSS Game ID
            PlaylistGrid.Cell[3,i+1] := TickerPtr^.Subleague_Mnemonic_Standard; //Subleague name
            PlaylistGrid.Cell[4,i+1] := IntToStr(TickerPtr^.Event_GUID.D2); //League name
            PlaylistGrid.Cell[5,i+1] := TickerPtr^.Enabled; //Record enable
            PlaylistGrid.Cell[6,i+1] :=
              GetRecordTypeDescription(PlaylistSelectTabControl.TabIndex+1, TickerPtr^.Record_Type);
            //Sponsor
            if (TickerPtr^.Record_Type = 1) then
              PlaylistGrid.Cell[7,i+1] := TickerPtr^.SponsorLogo_Name
            //Promo
            else if (TickerPtr^.Record_Type = 2) then
              PlaylistGrid.Cell[7,i+1] := TickerPtr^.SponsorLogo_Name
            else if (TickerPtr^.Record_Type = 3) then
              PlaylistGrid.Cell[7,i+1] := TickerPtr^.Comments
            else if (GetTemplateInformation(TickerPtr^.Template_ID).UsesGameData = TRUE) then
              PlaylistGrid.Cell[7,i+1] := TickerPtr^.Description
            else
              PlaylistGrid.Cell[7,i+1] := TickerPtr^.UserData[1];
            PlaylistGrid.Cell[8,i+1] := TickerPtr^.Comments; //Record enable
            //By default, show item over white background except if child template and added as group
            if (GetTemplateInformation(TickerPtr^.Template_ID).Template_Is_Child = TRUE) AND
               (TickerPtr^.Is_Child = TRUE) then
              PlaylistGrid.RowColor[i+1] := clGray
            else
              PlaylistGrid.RowColor[i+1] := clWindow;
            //Update number fo entries label
            NumEntries.Caption := IntToStr(StudioTicker_Collection.Count);
          end;
          //Bug
       1: begin
            BugPtr := StudioBug_Collection.At(i);
            PlaylistGrid.Cell[1,i+1] := IntToStr(i+1); //Index
            PlaylistGrid.Cell[2,i+1] := BugPtr^.CSS_GameID; //CSS Game ID
            PlaylistGrid.Cell[3,i+1] := BugPtr^.League; //League name
            PlaylistGrid.Cell[4,i+1] := IntToStr(BugPtr^.Event_GUID.D2); //League name
            PlaylistGrid.Cell[5,i+1] := BugPtr^.Enabled; //Record enable
            PlaylistGrid.Cell[6,i+1] :=
              GetRecordTypeDescription(PlaylistSelectTabControl.TabIndex+1, BugPtr^.Record_Type);
            PlaylistGrid.Cell[7,i+1] := BugPtr^.Description;
            PlaylistGrid.Cell[8,i+1] := BugPtr^.Comments; //Record enable
            //By default, show item over white background except if mini-playlist
            if (GetTemplateInformation(BugPtr^.Template_ID).Template_Is_Child = TRUE) AND
               (BugPtr^.Is_Child = TRUE) then
              PlaylistGrid.RowColor[i+1] := clGray
            else
              PlaylistGrid.RowColor[i+1] := clWindow;
            //Update number fo entries label
            NumEntries.Caption := IntToStr(StudioBug_Collection.Count);
          end;
          //Gametrax
       2: begin
            GameTraxPtr := GameTrax_Collection.At(i);
            PlaylistGrid.Cell[1,i+1] := IntToStr(i+1); //Index
            PlaylistGrid.Cell[2,i+1] := GameTraxPtr^.CSS_GameID; //CSS Game ID
            PlaylistGrid.Cell[3,i+1] := GameTraxPtr^.League; //League name
            PlaylistGrid.Cell[4,i+1] := IntToStr(GameTraxPtr^.Event_GUID.D2); //League name
            PlaylistGrid.Cell[5,i+1] := GameTraxPtr^.Enabled; //Record enable
            PlaylistGrid.Cell[6,i+1] :=
              GetRecordTypeDescription(PlaylistSelectTabControl.TabIndex+1, GameTraxPtr^.Record_Type);
            //Set Additional Info field
            //Sponsor
            if (GametraxPtr^.Record_Type = 1000) then
            begin
              if (GametraxPtr^.SponsorLogo_Region = 0) then
                PlaylistGrid.Cell[7,i+1] := GametraxPtr^.SponsorLogo_Name + '   Region ID: ALL'
              else
                PlaylistGrid.Cell[7,i+1] := GametraxPtr^.SponsorLogo_Name + '   Region ID: ' +
                  IntToStr(GametraxPtr^.SponsorLogo_Region);
            end
            else if (GetTemplateInformation(GameTraxPtr^.Template_ID).UsesGameData = TRUE) then
              PlaylistGrid.Cell[7,i+1] := GameTraxPtr^.Description
            else begin
              PlaylistGrid.Cell[7,i+1] := GameTraxPtr^.UserData[1];
              if (Trim(GameTraxPtr^.UserData[2]) <> '') then
                PlaylistGrid.Cell[7,i+1] := trim(GameTraxPtr^.UserData[1]) + '|' + Trim(GameTraxPtr^.UserData[2]);
            end;
            PlaylistGrid.Cell[8,i+1] := GameTraxPtr^.Comments; //Record enable
            //By default, show item over white background except if mini-playlist
            if (GetTemplateInformation(GameTraxPtr^.Template_ID).Template_Is_Child = TRUE) AND
               (GameTraxPtr^.Is_Child = TRUE) then
              PlaylistGrid.RowColor[i+1] := clGray
            else
              PlaylistGrid.RowColor[i+1] := clWindow;
            //Update number fo entries label
            NumEntries.Caption := IntToStr(GameTrax_Collection.Count);
          end;
      end;
    end;
  end
  else begin
    NumEntries.Caption := IntToStr(CollectionCount);
  end;
  //Refresh grid
  if (CurrentTopRow > 0) AND (CollectionCount > 0) then
  begin
    if (CurrentTopRow > CollectionCount) then CurrentTopRow := CollectionCount;
    PlaylistGrid.TopRow := CurrentTopRow;
  end;
  if (CurrentRow > 0) AND (CollectionCount > 0) then
  begin
    if (CurrentRow > CollectionCount) then CurrentRow := CollectionCount;
    PlaylistGrid.CurrentDataRow := CurrentRow;
    PlaylistGrid.RowSelected[CurrentRow] := TRUE;
  end;
end;

//Handler for append entry to playlist button
procedure TMainForm.AddToPlaylistBtnClick(Sender: TObject);
begin
  AddPlaylistEntry;
end;
//Handler for double-click on template select grid
procedure TMainForm.AvailableTemplatesDBGridDblClick(Sender: TObject);
begin
  AddPlaylistEntry;
end;
//General procedure for appending an entry to the main ticker playlist
procedure TMainForm.AddPlaylistEntry;
var
  i,j,k,m: SmallInt;
  TickerRecPtr: ^TickerRec;
  BugRecPtr: ^BugRec;
  GameTraxRecPtr: ^GameTraxRec;
  //SaveRow: SmallInt;
  SaveCurrentListRow, SaveTopListRow: Variant;
  SaveCurrentGamesRow, SaveTopGamesRow: Variant;
  Control: Word;
  Modal: TZipperEntryDlg;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CurrentTemplateDefs: TemplateDefsRec;
  ChildTemplateIDRecPtr: ^ChildTemplateIDRec;
  NumTemplateFields: SmallInt;
  CurrentTemplateID: SmallInt;
  User_Data: Array[1..25] of String[255];
  CollectionCount: SmallInt;
  CategoryRecPtr: ^CategoryRec;
  EventGUID: TGUID;
  OKToGo: Boolean;
  CurrentGameData: GameRec;
  NumGamesSelected: SmallInt;
  StyleChipRecPtr: ^StyleChipRec;
  CustomNewsHeaderRecPtr: ^CustomNewsHeaderRec;
  WeatherIconRecPtr: ^WeatherIconRec;
  RowSelected: Boolean;
  RecordCount: SmallInt;
  NFLSeasonTypeStr: String;
  ConferenceChipRecPtr: ^ConferenceChipRec;
begin
  //Init
  OKToGo := TRUE;

  try
    //Save row in playlist to restore later
    SaveCurrentListRow := PlaylistGrid.CurrentDataRow;
    SaveTopListRow := PlaylistGrid.TopRow ;

    //Check to see if category tab is a sport; if so, show and populate games grid
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    //First, check to see if sponsors. If so, show sponsor grid
    if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
    begin
      //Get number of games selected
      if (CurrentDataSource = 'CBSCSS') AND (dmMain.CSSQuery.RecordCount > 0) then
      begin
        NumGamesSelected := CSSGamesDBGrid.SelectedRows.Count;
        if (NumGamesSelected = 0) then
        begin
          CSSGamesDBGrid.SelectRows(CSSGamesDBGrid.CurrentDataRow, CSSGamesDBGrid.CurrentDataRow, TRUE);
          NumGamesSelected := 1;
        end;
        //Set top & current row
        SaveCurrentGamesRow := CSSGamesDBGrid.CurrentDataRow;
        SaveTopGamesRow := CSSGamesDBGrid.TopRow;
      end
      else if (CurrentDataSource = 'STATSINC') AND (dmMain.SportbaseQuery.RecordCount > 0) then
      begin
        NumGamesSelected := SIGamesDBGrid.SelectedRows.Count;
        if (NumGamesSelected = 0) then
        begin
          SIGamesDBGrid.SelectRows(SIGamesDBGrid.CurrentDataRow, SIGamesDBGrid.CurrentDataRow, TRUE);
          NumGamesSelected := 1;
        end;
        //Set top & current row
        SaveCurrentGamesRow := SIGamesDBGrid.CurrentDataRow;
        SaveTopGamesRow := SIGamesDBGrid.TopRow;
      end
      else if (CurrentDataSource = 'NFLDM') AND (dmMain.DatamartQuery.RecordCount > 0) then
      begin
        if (ConnectToCSS) then
        begin
          NumGamesSelected := CSSGamesDBGrid.SelectedRows.Count;
          if (NumGamesSelected = 0) then
          begin
            CSSGamesDBGrid.SelectRows(CSSGamesDBGrid.CurrentDataRow, CSSGamesDBGrid.CurrentDataRow, TRUE);
            NumGamesSelected := 1;
          end;
          //Set top & current row
          SaveCurrentGamesRow := CSSGamesDBGrid.CurrentDataRow;
          SaveTopGamesRow := CSSGamesDBGrid.TopRow;
        end
        else begin
          NumGamesSelected := NFLDMGamesDBGrid.SelectedRows.Count;
          if (NumGamesSelected = 0) then
          begin
            NFLDMGamesDBGrid.SelectRows(NFLDMGamesDBGrid.CurrentDataRow, NFLDMGamesDBGrid.CurrentDataRow, TRUE);
            NumGamesSelected := 1;
          end;
          //Set top & current row
          SaveCurrentGamesRow := NFLDMGamesDBGrid.CurrentDataRow;
          SaveTopGamesRow := NFLDMGamesDBGrid.TopRow;
        end;
      end
      else NumGamesSelected := 0;
    end
    else NumGamesSelected := 0;

    //Init fields
    for i := 1 to 25 do User_Data[i] := ' ';

    //Get collection count
    Case PlaylistSelectTabControl.TabIndex of
         //Ticker
      0: CollectionCount := StudioTicker_Collection.Count;
         //Bug
      1: CollectionCount := StudioBug_Collection.Count;
         //Gametrax
      2: CollectionCount := GameTrax_Collection.Count;
    end;
    if (CollectionCount < 1500) then
    begin
      CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
      CurrentTemplateID := dmMain.Query2.FieldByName('Template_ID').AsInteger;
      NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);
      //First, check to make sure that there are games listed if the template requires one
      CurrentTemplateDefs := GetTemplateInformation(dmMain.Query2.FieldByName('Template_ID').AsInteger);
      if (CurrentTemplateDefs.Record_Type = 1) AND (CurrentTemplateDefs.UsesGameData = TRUE) AND
        (CategoryRecPtr^.Category_Is_Sport = TRUE) AND
        (NumGamesSelected = 0) then
      begin
        MessageDlg('There are no games available or a game has not been selected. The record ' +
                   'cannot be added to the playlist.', mtWarning, [mbOK], 0);
      end
      //OK to proceed
      else begin
        //Only proceed if fields defined, prompt for data enabled and NOT a parent template; if more than one
        //game selected, don't prompt operator
        if (NumTemplateFields > 0) AND (PromptForData.Checked) AND
           (CurrentTemplateDefs.Template_Has_Children = FALSE) AND
           (((NumGamesSelected = 1) AND (GetTemplateInformation(CurrentTemplateID).UsesGameData=TRUE) {AND
             (dmMain.SportbaseQuery.RecordCount > 0)}) OR (GetTemplateInformation(CurrentTemplateID).UsesGameData=FALSE)) then
        begin
          try
            Modal := TZipperEntryDlg.Create(Application);
            //Add style chips
            if (StyleChip_Collection.Count > 0) then
            begin
              Modal.StyleChipsGrid.StoreData := TRUE;
              Modal.StyleChipsGrid.Cols := 2;
              Modal.StyleChipsGrid.Rows := StyleChip_Collection.Count;
              for i := 0 to StyleChip_Collection.Count-1 do
              begin
                StyleChipRecPtr := StyleChip_Collection.At(i);
                Modal.StyleChipsGrid.Cell[1,i+1] := StyleChipRecPtr^.StyleChip_Code;
                Modal.StyleChipsGrid.Cell[2,i+1] := StyleChipRecPtr^.StyleChip_Description;
              end;
            end;
            //Set initial values for grid
            //Clear grid values
            if (Modal.RecordGrid.Rows > 0) then
              Modal.RecordGrid.DeleteRows (1, Modal.RecordGrid.Rows);
            Modal.RecordGrid.StoreData := TRUE;
            Modal.RecordGrid.Cols := 4;
            Modal.RecordGrid.Rows := NumTemplateFields;
            //Populate the grid
            i := 0;
            //Get the game data
            if (GetTemplateInformation(CurrentTemplateID).UsesGameData) then
            begin
              //Query for Game data
              if ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS')) OR
                 ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS')) then
              begin
                CurrentGameData := EngineInterface.GetGameData(CategoryRecPtr^.Category_Sport,
                  '0', //Dummy Stats Inc. code
                  '0', //Dummy Sportsticker ID
                  dmMain.CSSQuery.FieldByname('game').AsInteger, //CSS ID
                  dmMain.CSSQuery.FieldByname('GameKey').AsInteger, //NFL ID
                  CategoryRecPtr^.Primary_Data_Source,
                  CategoryRecPtr^.Backup_Data_Source);
              end
              else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC')) OR
                      ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC')) then
              begin
                CurrentGameData := EngineInterface.GetGameData(CategoryRecPtr^.Category_Sport,
                  dmMain.SportbaseQuery.FieldByname('GCode').AsString,
                  '0', //Dummy SPortsticker ID
                  0, //Dummy CSS ID
                  0, // Dummy NFLDM GameID
                  CategoryRecPtr^.Primary_Data_Source,
                  CategoryRecPtr^.Backup_Data_Source);
              end
              else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'NFLDM')) OR
                      ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'NFLDM')) then
              begin
                if (ConnectToCSS) then
                begin
                  CurrentGameData := EngineInterface.GetGameData(CategoryRecPtr^.Category_Sport,
                    '0', //Dummy Stats Inc. code
                    '0', //Dummy Sportsticker ID
                    dmMain.CSSQuery.FieldByname('game').AsInteger, //CSS ID
                    dmMain.CSSQuery.FieldByname('GameKey').AsInteger, //NFL ID
                    CategoryRecPtr^.Primary_Data_Source,
                    CategoryRecPtr^.Backup_Data_Source);
                end
                else begin
                  CurrentGameData := EngineInterface.GetGameData(CategoryRecPtr^.Category_Sport,
                    '0', //Dummy Stats Inc. code
                    '0', //Dummy Sportsticker ID
                    0, //CSS ID
                    dmMain.DatamartQuery.FieldByname('GameKey').AsInteger, //NFL ID
                    CategoryRecPtr^.Primary_Data_Source,
                    CategoryRecPtr^.Backup_Data_Source);
                end;
              end;
            end;
            //Iterate through the fields
            for j := 0 to Template_Fields_Collection.Count-1 do
            begin
              TemplateFieldsRecPtr := Template_Fields_Collection.At(j);
              if (TemplateFieldsRecPtr^.Template_ID = CurrentTemplateID) then
              begin
                Modal.RecordGrid.Cell[1,i+1] := TemplateFieldsRecPtr^.Field_ID; //Index
                Modal.RecordGrid.Cell[2,i+1] := TemplateFieldsRecPtr^.Field_Label; //Description
                //Set color & read only property
                Modal.RecordGrid.CellReadOnly[1,i+1] := roOn;
                Modal.RecordGrid.CellReadOnly[2,i+1] := roOn;
                Modal.RecordGrid.Cell[4,i+1] := TemplateFieldsRecPtr^.Field_Type;
                Modal.RecordGrid.CellReadOnly[4,i+1] := roOn;
                if (TemplateFieldsRecPtr^.Field_Is_Manual = TRUE) then
                begin
                  //First check to see if it's a manual game winner indicator or animation
                  if (TemplateFieldsRecPtr^.Field_Type = 5) OR (TemplateFieldsRecPtr^.Field_Type = 6) then
                  begin
                    Modal.RecordGrid.CellControlType[3,i+1] := ctCheck;
                    Modal.RecordGrid.CellCheckBoxState[3,i+1] := cbUnchecked;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  //Next, check to see if it's a manual next page icon animation
                  else if (TemplateFieldsRecPtr^.Field_Type = 9) then
                  begin
                    Modal.RecordGrid.CellControlType[3,i+1] := ctCheck;
                    Modal.RecordGrid.CellCheckBoxState[3,i+1] := cbUnchecked;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  //Next, check to see if it's a weather icon; if so, display flipbooks in dropdown
                  else if (TemplateFieldsRecPtr^.Field_Type = 7) then
                  begin
                    Modal.RecordGrid.AssignCellCombo(3,i+1);
                    Modal.RecordGrid.CellButtonType[3,i+1] := btCombo;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                    //Init first combo box entry
                    WeatherIconRecPtr := Weather_Icon_Collection.At(0);
                    Modal.RecordGrid.Cell[3, i+1] := WeatherIconRecPtr^.Icon_Name;
                  end
                  //Next, check to see if it's a conference chip rec; if so, display conference chips in dropdown
                  else if (TemplateFieldsRecPtr^.Field_Type = 10) then
                  begin
                    Modal.RecordGrid.AssignCellCombo(3,i+1);
                    Modal.RecordGrid.CellButtonType[3,i+1] := btCombo;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                    //Init first combo box entry
                    ConferenceChipRecPtr := Conference_Chip_Collection.At(0);
                    Modal.RecordGrid.Cell[3, i+1] := ConferenceChipRecPtr^.Conference_Name;
                  end
                  //Next, check to see if it's the manual league entry; if so, display choices in dropdown
                  else if (TemplateFieldsRecPtr^.Field_Type = 8) then
                  begin
                    //Init combo box entry
                    CustomNewsHeaderRecPtr := CustomNewsHeader_Collection.At(ManualLeagueSelect.ItemIndex);
                    Modal.RecordGrid.Cell[3,i+1] :=  CustomNewsHeaderRecPtr^.HeaderText;
                  end
                  else begin
                    Modal.RecordGrid.RowColor[i+1] := clWindow;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end;
                end
                //Not a manual field, so populate automatically
                else begin
                  //Set cell value
                  if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) <> 0) then
                  begin
                    //Check if manual template; if so put league into first field
                    if (TemplateFieldsRecPtr^.Field_Contents = '$League') then
                    begin
                      CustomNewsHeaderRecPtr := CustomNewsHeader_Collection.At(ManualLeagueSelect.ItemIndex);
                      Modal.RecordGrid.Cell[3,i+1] := CustomNewsHeaderRecPtr^.HeaderText;
                    end
                    else
                      Modal.RecordGrid.Cell[3,i+1] := EngineInterface.GetValueOfSymbol(PlaylistSelectTabControl.TabIndex+1,
                        TemplateFieldsRecPtr^.Field_Contents, TemplateFieldsRecPtr^.Field_Type, NOENTRYINDEX, CurrentGameData, 0);
                  end
                  else
                    Modal.RecordGrid.Cell[3,i+1] := TemplateFieldsRecPtr^.Field_Contents;
                  Modal.RecordGrid.RowColor[i+1] := clAqua;
                  Modal.RecordGrid.CellReadOnly[3,i+1] := roOn;
                end;
                //Increment the template fields match counter
                Inc (i);
              end;
            end;
            Modal.TemplateName.Caption := dmMain.Query2.FieldByName('Template_Description').AsString;

            //Show the dialog
            Control := Modal.ShowModal;
          finally
            //Set new values if user didn't cancel out
            if (Control = mrOK) then OKToGo := TRUE
            else OKToGo := FALSE;
            //Set values based on data in grid
            if (Modal.RecordGrid.Rows > 0) then
            begin
              i := 1;
              for j := 1 to Modal.RecordGrid.Rows do
              begin
                if (Modal.RecordGrid.CellReadOnly[3,j] = roOff) then
                begin
                  //Check for visitor manual winner indication record type
                  if (Modal.RecordGrid.Cell[4,j] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                  begin
                    User_Data[24] := 'TRUE';
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                  begin
                    User_Data[24] := 'FALSE';
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                  begin
                    User_Data[25] := 'TRUE';
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                  begin
                    User_Data[25] := 'FALSE';
                    Dec(i);
                  end
                  //Check for next page animated icon
                  else if (Modal.RecordGrid.Cell[4,j] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                  begin
                    User_Data[23] := 'TRUE';
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                  begin
                    User_Data[23] := 'FALSE';
                    Dec(i);
                  end
                  else begin
                    //Set user value text and increment
                    User_Data[i] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                  end;
                  Inc(i);
                end;
              end;
            end;
            //Save current grid row
            SaveCurrentListRow := PlaylistGrid.CurrentDataRow;
            Modal.Free;
          end;
        end
        else begin
          //Init fields
          for i := 1 to 25 do User_Data[i] := ' ';
        end;
        //Append record; falls through to here whether or not user dialog is displayed
        if (OKToGo) then
        begin
          ////////////////////////////////////////////////////////////////////////
          // THIS BLOCK EXECUTED FOR CATEGORIES THAT ARE AUTOMATED SPORTS AND FOR
          // TEMPLATES THAT REQUIRE GAME DATA
          ////////////////////////////////////////////////////////////////////////
          //Check to see if at least one game in sportbase database
          if (CategoryRecPtr^.Category_Is_Sport = TRUE) AND
             {(dmMain.SportbaseQuery.RecordCount > 0) AND}
             (CurrentTemplateDefs.UsesGameData = TRUE) then
          begin
            //Loop through for all selected games
            //Go to first record in dataset
            if (CurrentDataSource = 'CBSCSS') then
            begin
              dmMain.CSSQuery.First;
              RecordCount := dmMain.CSSQuery.RecordCount;
            end
            else if (CurrentDataSource = 'NFLDM') then
            begin
              if (ConnectToCSS) then
              begin
                dmMain.CSSQuery.First;
                RecordCount := dmMain.CSSQuery.RecordCount;
              end
              else begin
                dmMain.DatamartQuery.First;
                RecordCount := dmMain.DatamartQuery.RecordCount;
              end;
            end
            else if (CurrentDataSource = 'STATSINC') then
            begin
              dmMain.SportbaseQuery.First;
              RecordCount := dmMain.SportbaseQuery.RecordCount;
            end;
            for m := 1 to RecordCount do
            begin
              //Check to see if game is selected
              if (CurrentDataSource = 'CBSCSS') then
                RowSelected := CSSGamesDBGrid.RowSelected[CSSGamesDBGrid.CurrentDataRow]
              else if (CurrentDataSource = 'NFLDM') then
              begin
                if (ConnectToCSS) then
                  RowSelected := CSSGamesDBGrid.RowSelected[CSSGamesDBGrid.CurrentDataRow]
                else
                RowSelected := NFLDMGamesDBGrid.RowSelected[NFLDMGamesDBGrid.CurrentDataRow];
              end
              else if (CurrentDataSource = 'STATSINC') then
                RowSelected := SIGamesDBGrid.RowSelected[SIGamesDBGrid.CurrentDataRow];
              if (RowSelected = TRUE) then
              begin
                //Query for Game data
                if ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS')) OR
                   ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS')) then
                begin
                  CurrentGameData := EngineInterface.GetGameData(CategoryRecPtr^.Category_Sport,
                    '0', //Dummy Stats Inc. code
                    '0', //Dummy Sportsticker ID
                    dmMain.CSSQuery.FieldByname('game').AsInteger, //CSS ID
                    dmMain.CSSQuery.FieldByname('GameKey').AsInteger, //NFL ID
                    CategoryRecPtr^.Primary_Data_Source,
                    CategoryRecPtr^.Backup_Data_Source);
                end
                else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC')) OR
                        ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC')) then
                begin
                  CurrentGameData := EngineInterface.GetGameData(CategoryRecPtr^.Category_Sport,
                    dmMain.SportbaseQuery.FieldByname('GCode').AsString,
                    '0', //Dummy SPortsticker ID
                    0, //Dummy CSS ID
                    0, // Dummy NFLDM GameID
                    CategoryRecPtr^.Primary_Data_Source,
                    CategoryRecPtr^.Backup_Data_Source);
                end
                else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'NFLDM')) OR
                        ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'NFLDM')) then
                begin
                  if (ConnectToCSS) then
                  begin
                    CurrentGameData := EngineInterface.GetGameData(CategoryRecPtr^.Category_Sport,
                      '0', //Dummy Stats Inc. code
                      '0', //Dummy Sportsticker ID
                      dmMain.CSSQuery.FieldByname('game').AsInteger, //CSS ID
                      dmMain.CSSQuery.FieldByname('GameKey').AsInteger, //NFL ID
                      CategoryRecPtr^.Primary_Data_Source,
                      CategoryRecPtr^.Backup_Data_Source);
                  end
                  else begin
                    CurrentGameData := EngineInterface.GetGameData(CategoryRecPtr^.Category_Sport,
                      '0', //Dummy Stats Inc. code
                      '0', //Dummy Sportsticker ID
                      0, //CSS ID
                      dmMain.DatamartQuery.FieldByname('GameKey').AsInteger, //NFL ID
                      CategoryRecPtr^.Primary_Data_Source,
                      CategoryRecPtr^.Backup_Data_Source);
                  end;
                end;
                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR PARENT TEMPLATES - ADDS CHILDREN
                ////////////////////////////////////////////////////////////////////////
                //Check to see if template is a "parent" template; if so, add all child templates
                if (CurrentTemplateDefs.Template_Has_Children) then
                begin
                  if (Child_Template_ID_Collection.Count > 0) then
                  begin
                    for i := 0 to Child_Template_ID_Collection.Count-1 do
                    begin
                      ChildTemplateIDRecPtr := Child_Template_ID_Collection.At(i);
                      //Check for child, and append if match
                      if (ChildTemplateIDRecPtr^.Template_ID = CurrentTemplateID) then
                      begin
                        //Allocate memory for the object
                        Case PlaylistSelectTabControl.TabIndex of
                             //Ticker
                         0: begin
                              GetMem (TickerRecPtr, SizeOf(TickerRec));
                              With TickerRecPtr^ do
                              begin
                                //Set values for collection record
                                Event_Index := StudioTicker_Collection.Count+1;
                                CreateGUID(EventGUID);
                                Event_GUID := EventGUID;
                                Enabled := ChildTemplateIDRecPtr^.Child_Default_Enable_State;
                                Template_ID := ChildTemplateIDRecPtr^.Child_Template_ID;
                                Record_Type := GetRecordTypeFromTemplateID(Template_ID);
                                Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
                                Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
                                //Set league ID
                                if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                                begin
                                  League := CategoryRecPtr^.Category_Sport;
                                  Subleague_Mnemonic_Standard := GetAutomatedLeagueDisplayMnemonic(League);
                                  Mnemonic_LiveEvent := GetAutomatedLeagueDisplayMnemonic(League);
                                  Description := CurrentGameData.Description;
                                  //Set data source information
                                  if ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS')) OR
                                     ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS')) then
                                  begin
                                    CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                                    NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                                    SI_GCode := '0';
                                    ST_Game_ID := '0'; //Default to blank for now
                                  end
                                  else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC')) OR
                                          ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC')) then
                                  begin
                                    CSS_GameID := 0;
                                    SI_GCode := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                                    NFLDM_GameID := 0; //Default to blank for now
                                    ST_Game_ID := '0'; //Default to blank for now
                                  end
                                  else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'NFLDM')) OR
                                          ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'NFLDM')) then
                                  begin
                                    if (ConnectToCSS) then
                                    begin
                                      CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                                      NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                                    end
                                    else begin
                                      CSS_GameID := 0;
                                      NFLDM_GameID := dmMain.DatamartQuery.FieldByname('GameKey').AsInteger;
                                    end;
                                    SI_GCode := '0';
                                    ST_Game_ID := '0'; //Default to blank for now
                                  end;
                                end
                                else begin
                                  League := 'NONE';
                                  Subleague_Mnemonic_Standard := 'NONE';
                                  Mnemonic_LiveEvent := 'NONE';
                                  SI_GCode := '0';
                                  ST_Game_ID := ' '; //Default to blank for now
                                  CSS_GameID := 0;
                                  NFLDM_GameID := 0; //Default to blank for now
                                  Description := '';
                                end;
                                Selected := FALSE;
                                //Check to see if sponsor logo
                                CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                                if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                                begin
                                  SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                                  SponsorLogo_Dwell := SponsorLogoDwell.Value;
                                  SponsorLogo_Region := SponsorLogoRegion.Value;
                                end
                                else begin
                                  SponsorLogo_Name := ' ';
                                  SponsorLogo_Dwell := 0;
                                  SponsorLogo_Region := 0;
                                end;
                                //Set Is Child bit
                                Is_Child := TRUE;
                                //For stats
                                StatStoredProcedure := ' ';
                                StatType := 0;;
                                StatTeam := ' ';
                                StatLeague := ' ';
                                StatRecordNumber := 0;
                                for k := 1 to 25 do UserData[k] := User_Data[k];
                                //Set the start/end enable times to the template defaults
                                StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                                EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                                DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                                Comments := ' ';
                                //Insert object to collection
                                StudioTicker_Collection.Insert(TickerRecPtr);
                                StudioTicker_Collection.Pack;
                              end;
                            end;
                             //Bug
                         1: begin
                              GetMem (BugRecPtr, SizeOf(BugRec));
                              With BugRecPtr^ do
                              begin
                                //Set values for collection record
                                Event_Index := StudioBug_Collection.Count+1;
                                CreateGUID(EventGUID);
                                Event_GUID := EventGUID;
                                Enabled := ChildTemplateIDRecPtr^.Child_Default_Enable_State;
                                Template_ID := ChildTemplateIDRecPtr^.Child_Template_ID;
                                Record_Type := GetRecordTypeFromTemplateID(Template_ID);
                                Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
                                Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
                                //Set league ID
                                if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                                begin
                                  League := CategoryRecPtr^.Category_Sport;
                                  //Set data source information
                                  if ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS')) OR
                                     ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS')) then
                                  begin
                                    CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                                    NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                                    SI_GCode := '0';
                                    ST_Game_ID := '0'; //Default to blank for now
                                  end
                                  else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC')) OR
                                          ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC')) then
                                  begin
                                    CSS_GameID := 0;
                                    SI_GCode := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                                    NFLDM_GameID := 0; //Default to blank for now
                                    ST_Game_ID := '0'; //Default to blank for now
                                  end
                                  else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'NFLDM')) OR
                                          ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'NFLDM')) then
                                  begin
                                    if (ConnectToCSS) then
                                    begin
                                      CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                                      NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                                    end
                                    else begin
                                      CSS_GameID := 0;
                                      NFLDM_GameID := dmMain.DatamartQuery.FieldByname('GameKey').AsInteger;
                                    end;
                                    SI_GCode := '0';
                                    ST_Game_ID := '0'; //Default to blank for now
                                  end;
                                  Description := CurrentGameData.Description;
                                end
                                else begin
                                  League := 'NONE';
                                  SI_GCode := '0';
                                  ST_Game_ID := '0'; //Default to blank for now
                                  NFLDM_GameID := 0; //Default to blank for now
                                  CSS_GameID := 0;
                                  Description := '';
                                end;
                                Selected := FALSE;
                                //Set Is Child bit
                                Is_Child := TRUE;
                                for k := 1 to 25 do UserData[k] := User_Data[k];
                                //Set the start/end enable times to the template defaults
                                StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                                EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                                DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                                Comments := ' ';
                                //Insert object to collection
                                StudioBug_Collection.Insert(BugRecPtr);
                                StudioBug_Collection.Pack;
                              end;
                            end;
                             //GameTrax
                         2: begin
                              GetMem (GameTraxRecPtr, SizeOf(GameTraxRec));
                              With GameTraxRecPtr^ do
                              begin
                                //Set values for collection record
                                Event_Index := GameTrax_Collection.Count+1;
                                CreateGUID(EventGUID);
                                Event_GUID := EventGUID;
                                Enabled := ChildTemplateIDRecPtr^.Child_Default_Enable_State;
                                Template_ID := ChildTemplateIDRecPtr^.Child_Template_ID;
                                Record_Type := GetRecordTypeFromTemplateID(Template_ID);
                                Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
                                Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
                                if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                                begin
                                  League := CategoryRecPtr^.Category_Sport;
                                  //Check if odds only
                                  if ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS')) OR
                                     ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS')) then
                                  begin
                                    CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                                    NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                                    SI_GCode := '0';
                                    ST_Game_ID := '0'; //Default to blank for now
                                  end
                                  else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC')) OR
                                          ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC')) then
                                  begin
                                    CSS_GameID := 0;
                                    SI_GCode := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                                    NFLDM_GameID := 0; //Default to blank for now
                                    ST_Game_ID := '0'; //Default to blank for now
                                  end
                                  else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'NFLDM')) OR
                                          ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'NFLDM')) then
                                  begin
                                    if (ConnectToCSS) then
                                    begin
                                      CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                                      NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                                    end
                                    else begin
                                      CSS_GameID := 0;
                                      NFLDM_GameID := dmMain.DatamartQuery.FieldByname('GameKey').AsInteger;
                                    end;
                                    SI_GCode := '0';
                                    ST_Game_ID := '0'; //Default to blank for now
                                  end;
                                  Description := CurrentGameData.Description;
                                end
                                else begin
                                  League := 'NONE';
                                  SI_GCode := '0';
                                  ST_Game_ID := '0'; //Default to blank for now
                                  CSS_GameID := 0;
                                  NFLDM_GameID := 0; //Default to blank for now
                                  Description := '';
                                end;
                                //Add sponsor logo information
                                if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                                begin
                                  SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                                  SponsorLogo_Dwell := SponsorLogoDwell.Value;
                                  SponsorLogo_Region := SponsorLogoRegion.Value;
                                end
                                else begin
                                  SponsorLogo_Name := ' ';
                                  SponsorLogo_Dwell := 0;
                                  SponsorLogo_Region := 0;
                                end;
                                //Set Is Child bit
                                Is_Child := TRUE;
                                for k := 1 to 25 do UserData[k] := User_Data[k];
                                //Set the start/end enable times to the template defaults
                                StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                                EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                                DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                                Comments := ' ';
                                Selected := FALSE;
                                //Insert object to collection
                                GameTrax_Collection.Insert(GameTraxRecPtr);
                                GameTrax_Collection.Pack;
                              end;
                            end;
                        end;
                      end;
                    end;
                  end;
                end
                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR NON-PARENT TEMPLATES IN SPORTS CATEGORIES
                ////////////////////////////////////////////////////////////////////////
                //Not a parent template, so just append record for selected record type
                else begin
                  //Allocate memory for the object
                  Case PlaylistSelectTabControl.TabIndex of
                       //Ticker
                   0: begin
                        GetMem (TickerRecPtr, SizeOf(TickerRec));
                        With TickerRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := StudioTicker_Collection.Count+1;
                          CreateGUID(EventGUID);
                          Event_GUID := EventGUID;
                          Enabled := EntryEnable.Checked;
                          Template_ID := CurrentTemplateID;
                          Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                          Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
                          Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
                          //Set league ID
                          if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                          begin
                            League := CategoryRecPtr^.Category_Sport;
                            Subleague_Mnemonic_Standard := GetAutomatedLeagueDisplayMnemonic(League);
                            if ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS')) OR
                               ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS')) then
                            begin
                              CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                              NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                              SI_GCode := '0';
                              ST_Game_ID := '0'; //Default to blank for now
                            end
                            else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC')) OR
                                    ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC')) then
                            begin
                              CSS_GameID := 0;
                              SI_GCode := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                              NFLDM_GameID := 0; //Default to blank for now
                              ST_Game_ID := '0'; //Default to blank for now
                            end
                            else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'NFLDM')) OR
                                    ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'NFLDM')) then
                            begin
                              if (ConnectToCSS) then
                              begin
                                CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                                NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                              end
                              else begin
                                CSS_GameID := 0;
                                NFLDM_GameID := dmMain.DatamartQuery.FieldByname('GameKey').AsInteger;
                              end;
                              SI_GCode := '0';
                              ST_Game_ID := '0'; //Default to blank for now
                            end;
                            Description := CurrentGameData.Description;
                            Mnemonic_LiveEvent := GetAutomatedLeagueDisplayMnemonic(League);
                          end
                          else begin
                            League := 'NONE';
                            Subleague_Mnemonic_Standard := 'NONE';
                            Mnemonic_LiveEvent := 'NONE';
                            SI_GCode := '0';
                            ST_Game_ID := '0'; //Default to blank for now
                            CSS_GameID := 0;
                            NFLDM_GameID := 0; //Default to blank for now
                            Description := '';
                          end;
                          //Check to see if sponsor logo
                          CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                          SponsorLogo_Name := ' ';
                          SponsorLogo_Dwell := 0;
                          SponsorLogo_Region := 0;
                          //Clear Is Child bit
                          Is_Child := FALSE;
                          Selected := FALSE;
                          //For stats
                          StatStoredProcedure := '';
                          StatType := 0;;
                          StatTeam := ' ';
                          StatLeague := ' ';
                          StatRecordNumber := 0;
                          for k := 1 to 25 do UserData[k] := User_Data[k];
                          //Set the start/end enable times to the template defaults
                          StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                          EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                          DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                          Comments := ' ';
                          //Insert object to collection
                          StudioTicker_Collection.Insert(TickerRecPtr);
                          StudioTicker_Collection.Pack;
                        end;
                      end;
                       //Bug
                   1: begin
                        GetMem (BugRecPtr, SizeOf(BugRec));
                        With BugRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := StudioBug_Collection.Count+1;
                          CreateGUID(EventGUID);
                          Event_GUID := EventGUID;
                          Enabled := EntryEnable.Checked;
                          Template_ID := CurrentTemplateID;
                          Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                          Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
                          Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
                          if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                          begin
                            League := CategoryRecPtr^.Category_Sport;
                            if ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS')) OR
                               ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS')) then
                            begin
                              CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                              NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                              SI_GCode := '0';
                              ST_Game_ID := '0'; //Default to blank for now
                            end
                            else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC')) OR
                                    ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC')) then
                            begin
                              CSS_GameID := 0;
                              SI_GCode := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                              NFLDM_GameID := 0; //Default to blank for now
                              ST_Game_ID := '0'; //Default to blank for now
                            end
                            else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'NFLDM')) OR
                                    ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'NFLDM')) then
                            begin
                              if (ConnectToCSS) then
                              begin
                                CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                                NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                              end
                              else begin
                                CSS_GameID := 0;
                                NFLDM_GameID := dmMain.DatamartQuery.FieldByname('GameKey').AsInteger;
                              end;
                              SI_GCode := '0';
                              ST_Game_ID := '0'; //Default to blank for now
                            end;
                            Description := CurrentGameData.Description;
                          end
                          else begin
                            League := 'NONE';
                            SI_GCode := '0';
                            ST_Game_ID := '0'; //Default to blank for now
                            CSS_GameID := 0;
                            NFLDM_GameID := 0; //Default to blank for now
                            Description := '';
                          end;
                          //Clear Is Child bit
                          Is_Child := FALSE;
                          for k := 1 to 25 do UserData[k] := User_Data[k];
                          //Set the start/end enable times to the template defaults
                          StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                          EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                          DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                          Selected := FALSE;
                          Comments := ' ';
                          //Insert object to collection
                          StudioBug_Collection.Insert(BugRecPtr);
                          StudioBug_Collection.Pack;
                        end;
                      end;
                       //GameTrax
                   2: begin
                        GetMem (GameTraxRecPtr, SizeOf(GameTraxRec));
                        With GameTraxRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := GameTrax_Collection.Count+1;
                          CreateGUID(EventGUID);
                          Event_GUID := EventGUID;
                          Enabled := EntryEnable.Checked;
                          Template_ID := CurrentTemplateID;
                          Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                          Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
                          Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
                          if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                          begin
                            League := CategoryRecPtr^.Category_Sport;
                            if ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS')) OR
                               ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS')) then
                            begin
                              CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                              NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                              SI_GCode := '0';
                              ST_Game_ID := '0'; //Default to blank for now
                            end
                            else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC')) OR
                                    ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC')) then
                            begin
                              CSS_GameID := 0;
                              SI_GCode := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                              NFLDM_GameID := 0; //Default to blank for now
                              ST_Game_ID := '0'; //Default to blank for now
                            end
                            else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'NFLDM')) OR
                                    ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'NFLDM')) then
                            begin
                              if (ConnectToCSS) then
                              begin
                                CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                                NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                              end
                              else begin
                                CSS_GameID := 0;
                                NFLDM_GameID := dmMain.DatamartQuery.FieldByname('GameKey').AsInteger;
                              end;
                              SI_GCode := '0';
                              ST_Game_ID := '0'; //Default to blank for now
                            end;
                            Description := CurrentGameData.Description;
                          end
                          else begin
                            League := 'NONE';
                            SI_GCode := '0';
                            ST_Game_ID := '0'; //Default to blank for now
                            CSS_GameID := 0;
                            NFLDM_GameID := 0; //Default to blank for now
                            Description := '';
                          end;
                          //Add sponsor logo information
                          if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                          begin
                            SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                            //Modified for Version 3.0.5
                            if (PersistentSponsorCheckbox.Checked) then
                              SponsorLogo_Dwell := -1
                            else
                              SponsorLogo_Dwell := SponsorLogoDwell.Value;
                            SponsorLogo_Region := SponsorLogoRegion.Value;
                          end
                          else begin
                            SponsorLogo_Name := ' ';
                            SponsorLogo_Dwell := 0;
                            SponsorLogo_Region := 0;
                          end;
                          //Clear Is Child bit
                          Is_Child := FALSE;
                          for k := 1 to 25 do UserData[k] := User_Data[k];
                          //Set the start/end enable times to the template defaults
                          StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                          EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                          DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                          Selected := FALSE;
                          Comments := ' ';
                          //Insert object to collection
                          GameTrax_Collection.Insert(GameTraxRecPtr);
                          GameTrax_Collection.Pack;
                        end;
                      end;
                  end;
                end; //Single template type (NOT a parent record)
              end;
              //Go to next record
              if (CurrentDataSource = 'CBSCSS') then
                dmMain.CSSQuery.Next
              else if (CurrentDataSource = 'STATSINC') then
                dmMain.SportbaseQuery.Next
              else if (CurrentDataSource = 'NFLDM') then
              begin
                if (ConnectToCSS) then
                  dmMain.CSSQuery.Next
                else
                  dmMain.DatamartQuery.Next;
              end;
            end; //Loop for selected games
          end
          ////////////////////////////////////////////////////////////////////////
          // THIS BLOCK EXECUTED FOR TEMPLATES THAT DO NOT REQUIRE GAME DATA
          ////////////////////////////////////////////////////////////////////////
          //No games in Sportbase query - if not a game specific record type, just add one record
          else if (CurrentTemplateDefs.UsesGameData = FALSE) then
          begin
            //Allocate memory for the object
            Case PlaylistSelectTabControl.TabIndex of
                 //Ticker
             0: begin
                  GetMem (TickerRecPtr, SizeOf(TickerRec));
                  With TickerRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := StudioTicker_Collection.Count+1;
                    CreateGUID(EventGUID);
                    Event_GUID := EventGUID;
                    Enabled := EntryEnable.Checked;
                    Template_ID := CurrentTemplateID;
                    Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                    //Get category & league information
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    //Check if manual league; if so, first user text field will be league
                    if (CurrentTemplateDefs.ManualLeague) then
                    begin
                      CustomNewsHeaderRecPtr := CustomNewsHeader_Collection.At(ManualLeagueSelect.ItemIndex);
                      League := CustomNewsHeaderRecPtr^.HeaderText;
                      Subleague_Mnemonic_Standard := 'NONE';
                      Mnemonic_LiveEvent := 'NONE';
                   end
                    else if (Trim(CategoryRecPtr^.Category_Sport) <> '') then
                    begin
                      League := CategoryRecPtr^.Category_Sport;
                      Subleague_Mnemonic_Standard := 'NONE';
                      Mnemonic_LiveEvent := 'NONE';
                    end
                    else begin
                      League := 'NONE';
                      Subleague_Mnemonic_Standard := 'NONE';
                      Mnemonic_LiveEvent := 'NONE';
                    end;
                    Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
                    Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
                    SI_GCode := '0';
                    ST_Game_ID := '0'; //Default to blank for now
                    CSS_GameID := 0;
                    NFLDM_GameID := 0; //Default to blank for now
                    Description := '';
                    //Check to see if sponsor logo
                    if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                    begin
                      SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                      SponsorLogo_Dwell := SponsorLogoDwell.Value;
                      SponsorLogo_Region := SponsorLogoRegion.Value;
                    end
                    //Check if promo
                    else if (UpperCase(CategoryRecPtr^.Category_Name) = 'PROMOS') then
                    begin
                      SponsorLogo_Name := dmMain.tblPromo_Logos.FieldByName('LogoFilename').AsString;
                      SponsorLogo_Dwell := 0;
                      SponsorLogo_Region := 0;
                    end
                    else begin
                      SponsorLogo_Name := ' ';
                      SponsorLogo_Dwell := 0;
                      SponsorLogo_Region := 0;
                    end;
                    //Clear Is Child bit
                    Is_Child := FALSE;
                    //For stats
                    StatStoredProcedure := '';
                    StatType := 0;;
                    StatTeam := '';
                    StatLeague := '';
                    StatRecordNumber := 0;
                    for k := 1 to 25 do UserData[k] := User_Data[k];
                    //Set the start/end enable times to the template defaults
                    StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                    EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                    DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                    Selected := FALSE;
                    Comments := ' ';
                    //Insert object to collection
                    StudioTicker_Collection.Insert(TickerRecPtr);
                    StudioTicker_Collection.Pack;
                  end;
                end;
                 //Bug
             1: begin
                  GetMem (BugRecPtr, SizeOf(BugRec));
                  With BugRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := StudioBug_Collection.Count+1;
                    CreateGUID(EventGUID);
                    Event_GUID := EventGUID;
                    Enabled := EntryEnable.Checked;
                    Template_ID := CurrentTemplateID;
                    Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                    //Get category & league information
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    //Check if manual league; if so, first user text field will be league
                    if (CurrentTemplateDefs.ManualLeague) then
                    begin
                      CustomNewsHeaderRecPtr := CustomNewsHeader_Collection.At(ManualLeagueSelect.ItemIndex);
                      League := CustomNewsHeaderRecPtr^.HeaderText;
                    end
                    else if (Trim(CategoryRecPtr^.Category_Sport) <> '') then
                      League := CategoryRecPtr^.Category_Sport
                    else League := 'NONE';
                    //Clear Is Child bit
                    Is_Child := FALSE;
                    Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
                    Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
                    SI_GCode := '0';
                    ST_Game_ID := '0'; //Default to blank for now
                    CSS_GameID := 0;
                    NFLDM_GameID := 0; //Default to blank for now
                    Description := '';
                    for k := 1 to 25 do UserData[k] := User_Data[k];
                    //Set the start/end enable times to the template defaults
                    StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                    EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                    DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                    Selected := FALSE;
                    Comments := ' ';
                    //Insert object to collection
                    StudioBug_Collection.Insert(BugRecPtr);
                    StudioBug_Collection.Pack;
                  end;
                end;
                 //Gametrax
             2: begin
                  GetMem (GameTraxRecPtr, SizeOf(GameTraxRec));
                  With GameTraxRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := GameTrax_Collection.Count+1;
                    CreateGUID(EventGUID);
                    Event_GUID := EventGUID;
                    Enabled := EntryEnable.Checked;
                    Template_ID := CurrentTemplateID;
                    Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                    //Get category & league information
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    //Check if manual league; if so, first user text field will be league
                    if (CurrentTemplateDefs.ManualLeague) then
                    begin
                      CustomNewsHeaderRecPtr := CustomNewsHeader_Collection.At(ManualLeagueSelect.ItemIndex);
                      League := CustomNewsHeaderRecPtr^.HeaderText;
                    end
                    else if (Trim(CategoryRecPtr^.Category_Sport) <> '') then
                      League := CategoryRecPtr^.Category_Sport
                    else League := 'NONE';
                    Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
                    Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
                    SI_GCode := '0';
                    ST_Game_ID := '0'; //Default to blank for now
                    CSS_GameID := 0;
                    NFLDM_GameID := 0; //Default to blank for now
                    Description := '';
                    //Clear Is Child bit
                    Is_Child := FALSE;
                    //Add sponsor logo information
                    if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                    begin
                      SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                      //Modified for Version 3.0.5
                      if (PersistentSponsorCheckbox.Checked) then
                        SponsorLogo_Dwell := -1
                      else
                        SponsorLogo_Dwell := SponsorLogoDwell.Value;
                      SponsorLogo_Region := SponsorLogoRegion.Value;
                    end
                    else begin
                      SponsorLogo_Name := ' ';
                      SponsorLogo_Dwell := 0;
                      SponsorLogo_Region := 0;
                    end;
                    //Set league ID
                    for k := 1 to 25 do UserData[k] := User_Data[k];
                    //Set the start/end enable times to the template defaults
                    StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                    EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                    DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                    Selected := FALSE;
                    Comments := ' ';
                    //Insert object to collection
                    GameTrax_Collection.Insert(GameTraxRecPtr);
                    GameTrax_Collection.Pack;
                  end;
                end;
            end; //Single template type (NOT a parent record)
          end;
        end;
        //Refresh grid
        RefreshPlaylistGrid;
        //Refresh the fields grid
        RefreshEntryFieldsGrid(CurrentTickerDisplayMode);
        //Restore current grid row
        if (SaveCurrentListRow < 1) then SaveCurrentListRow := 1;
        PlaylistGrid.CurrentDataRow := SaveCurrentListRow;
        PlaylistGrid.TopRow := SaveTopListRow;
      end;
    end;
    //Restore grid positions
    if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
    begin
      //Get number of games selected
      if (CurrentDataSource = 'CBSCSS') AND (dmMain.CSSQuery.RecordCount > 0) then
      begin
        //Set top & current row
        CSSGamesDBGrid.CurrentDataRow := SaveCurrentGamesRow;
        CSSGamesDBGrid.TopRow := SaveTopGamesRow;
      end
      else if (CurrentDataSource = 'STATSINC') AND (dmMain.SportbaseQuery.RecordCount > 0) then
      begin
        //Set top & current row
        SIGamesDBGrid.CurrentDataRow := SaveCurrentGamesRow;
        SIGamesDBGrid.TopRow := SaveTopGamesRow;
      end
      else if (CurrentDataSource = 'NFLDM') AND (dmMain.DatamartQuery.RecordCount > 0) then
      begin
        if (ConnectToCSS) then
        begin
          //Set top & current row
          CSSGamesDBGrid.CurrentDataRow := SaveCurrentGamesRow;
          CSSGamesDBGrid.TopRow := SaveTopGamesRow;
        end
        else begin
          //Set top & current row
          NFLDMGamesDBGrid.CurrentDataRow := SaveCurrentGamesRow;
          NFLDMGamesDBGrid.TopRow := SaveTopGamesRow;
        end;
      end;
    end;
  except
    EngineInterface.WriteToErrorLog('Error occurred while trying to add playlist entry');
  end;
end;

//Handler for insert entry into playlist button
procedure TMainForm.InsertIntoPlaylistBtnClick(Sender: TObject);
begin
  InsertPlaylistEntry;
end;
//General procedure to insert an entry into the main ticker playlist
procedure TMainForm.InsertPlaylistEntry;
var
  OKToGo: Boolean;
  SaveCurrentListRow, SaveTopListRow: SmallInt;
  SaveCurrentGamesRow, SaveTopGamesRow: Variant;
  i,j,k,m: SmallInt;
  TickerRecPtr: ^TickerRec;
  BugRecPtr: ^BugRec;
  GameTraxRecPtr: ^GameTraxRec;
  Control: Word;
  Modal: TZipperEntryDlg;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  ChildTemplateIDRecPtr: ^ChildTemplateIDRec;
  NumTemplateFields: SmallInt;
  CurrentTemplateID: SmallInt;
  CurrentTemplateDefs: TemplateDefsRec;
  User_Data: Array[1..25] of String[255];
  CollectionCount: SmallInt;
  CategoryRecPtr: ^CategoryRec;
  CurrentGameData: GameRec;
  EventGUID: TGUID;
  NumGamesSelected: SmallInt;
  StyleChipRecPtr: ^StyleChipRec;
  CustomNewsHeaderRecPtr: ^CustomNewsHeaderRec;
  WeatherIconRecPtr: ^WeatherIconRec;
  RowSelected: Boolean;
  RecordCount: SmallInt;
  NFLSeasonTypeStr: String;
  ConferenceChipRecPtr: ^ConferenceChipRec;
begin
  //Init
  OKToGo := TRUE;
  try
     //Save row in playlist to restore later
     SaveCurrentListRow := PlaylistGrid.CurrentDataRow;
     SaveTopListRow := PlaylistGrid.TopRow;

    //Check to see if category tab is a sport; if so, show and populate games grid
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    //First, check to see if sponsors. If so, show sponsor grid
    if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
    begin
      //Get number of games selected
      if (CurrentDataSource = 'CBSCSS') AND (dmMain.CSSQuery.RecordCount > 0) then
      begin
        NumGamesSelected := CSSGamesDBGrid.SelectedRows.Count;
        if (NumGamesSelected = 0) then
        begin
          CSSGamesDBGrid.SelectRows(CSSGamesDBGrid.CurrentDataRow, CSSGamesDBGrid.CurrentDataRow, TRUE);
          NumGamesSelected := 1;
        end;
        //Set top & current row
        SaveCurrentGamesRow := CSSGamesDBGrid.CurrentDataRow;
        SaveTopGamesRow := CSSGamesDBGrid.TopRow;
      end
      else if (CurrentDataSource = 'STATSINC') AND (dmMain.SportbaseQuery.RecordCount > 0) then
      begin
        NumGamesSelected := SIGamesDBGrid.SelectedRows.Count;
        if (NumGamesSelected = 0) then
        begin
          SIGamesDBGrid.SelectRows(SIGamesDBGrid.CurrentDataRow, SIGamesDBGrid.CurrentDataRow, TRUE);
          NumGamesSelected := 1;
        end;
        //Set top & current row
        SaveCurrentGamesRow := SIGamesDBGrid.CurrentDataRow;
        SaveTopGamesRow := SIGamesDBGrid.TopRow;
      end
      else if (CurrentDataSource = 'NFLDM') AND (dmMain.DatamartQuery.RecordCount > 0) then
      begin
        if (ConnectToCSS) then
        begin
          NumGamesSelected := CSSGamesDBGrid.SelectedRows.Count;
          if (NumGamesSelected = 0) then
          begin
            CSSGamesDBGrid.SelectRows(CSSGamesDBGrid.CurrentDataRow, CSSGamesDBGrid.CurrentDataRow, TRUE);
            NumGamesSelected := 1;
          end;
          //Set top & current row
          SaveCurrentGamesRow := CSSGamesDBGrid.CurrentDataRow;
          SaveTopGamesRow := CSSGamesDBGrid.TopRow;
        end
        else begin
          NumGamesSelected := NFLDMGamesDBGrid.SelectedRows.Count;
          if (NumGamesSelected = 0) then
          begin
            NFLDMGamesDBGrid.SelectRows(NFLDMGamesDBGrid.CurrentDataRow, NFLDMGamesDBGrid.CurrentDataRow, TRUE);
            NumGamesSelected := 1;
          end;
          //Set top & current row
          SaveCurrentGamesRow := NFLDMGamesDBGrid.CurrentDataRow;
          SaveTopGamesRow := NFLDMGamesDBGrid.TopRow;
        end;
      end
      else NumGamesSelected := 0;
    end
    else NumGamesSelected := 0;

    //Init fields
    for i := 1 to 25 do User_Data[i] := ' ';

    //Get collection count
    Case PlaylistSelectTabControl.TabIndex of
         //Ticker
      0: CollectionCount := StudioTicker_Collection.Count;
         //Bug
      1: CollectionCount := StudioBug_Collection.Count;
         //Extra line
      2: CollectionCount := GameTrax_Collection.Count;
    end;
    if (CollectionCount < 1500) then
    begin
      CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
      CurrentTemplateID := dmMain.Query2.FieldByName('Template_ID').AsInteger;
      NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);
      //First, check to make sure that there are games listed if the template requires one
      CurrentTemplateDefs := GetTemplateInformation(dmMain.Query2.FieldByName('Template_ID').AsInteger);
      if (CurrentTemplateDefs.Record_Type = 1) AND (CurrentTemplateDefs.UsesGameData = TRUE) AND
        (CategoryRecPtr^.Category_Is_Sport = TRUE) AND
        (NumGamesSelected = 0) then
      begin
        MessageDlg('There are no games available or a game has not been selected. The record ' +
                   'cannot be added to the playlist.', mtWarning, [mbOK], 0);
      end
      //OK to proceed
      else begin
        //Only proceed if fields defined, prompt for data enabled and NOT a parent template; if more than one
        //game selected, don't prompt operator
        if (NumTemplateFields > 0) AND (PromptForData.Checked) AND
           (CurrentTemplateDefs.Template_Has_Children = FALSE) AND
           (((NumGamesSelected = 1) AND (GetTemplateInformation(CurrentTemplateID).UsesGameData=TRUE) {AND
             (dmMain.SportbaseQuery.RecordCount > 0)}) OR (GetTemplateInformation(CurrentTemplateID).UsesGameData=FALSE)) then
        begin
          try
            Modal := TZipperEntryDlg.Create(Application);
            //Set initial values for grid
            //Add style chips
            if (StyleChip_Collection.Count > 0) then
            begin
              Modal.StyleChipsGrid.StoreData := TRUE;
              Modal.StyleChipsGrid.Cols := 2;
              Modal.StyleChipsGrid.Rows := StyleChip_Collection.Count;
              for i := 0 to StyleChip_Collection.Count-1 do
              begin
                StyleChipRecPtr := StyleChip_Collection.At(i);
                Modal.StyleChipsGrid.Cell[1,i+1] := StyleChipRecPtr^.StyleChip_Code;
                Modal.StyleChipsGrid.Cell[2,i+1] := StyleChipRecPtr^.StyleChip_Description;
              end;
            end;
            //Clear grid values
            if (Modal.RecordGrid.Rows > 0) then
              Modal.RecordGrid.DeleteRows (1, Modal.RecordGrid.Rows);
            Modal.RecordGrid.StoreData := TRUE;
            Modal.RecordGrid.Cols := 4;
            Modal.RecordGrid.Rows := NumTemplateFields;
            //Populate the grid
            i := 0;
            //Get the game data
            if (GetTemplateInformation(CurrentTemplateID).UsesGameData) then
            begin
              if ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS')) OR
                 ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS')) then
              begin
                CurrentGameData := EngineInterface.GetGameData(CategoryRecPtr^.Category_Sport,
                  '0', //Dummy Stats Inc. code
                  '0', //Dummy Sportsticker ID
                  dmMain.CSSQuery.FieldByname('game').AsInteger, //CSS ID
                  dmMain.CSSQuery.FieldByname('GameKey').AsInteger, //NFL ID
                  CategoryRecPtr^.Primary_Data_Source,
                  CategoryRecPtr^.Backup_Data_Source);
              end
              else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC')) OR
                      ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC')) then
              begin
                CurrentGameData := EngineInterface.GetGameData(CategoryRecPtr^.Category_Sport,
                  dmMain.SportbaseQuery.FieldByname('GCode').AsString,
                  '0', //Dummy SPortsticker ID
                  0, //Dummy CSS ID
                  0, // Dummy NFLDM GameID
                  CategoryRecPtr^.Primary_Data_Source,
                  CategoryRecPtr^.Backup_Data_Source);
              end
              else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'NFLDM')) OR
                      ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'NFLDM')) then
              begin
                if (ConnectToCSS) then
                begin
                  CurrentGameData := EngineInterface.GetGameData(CategoryRecPtr^.Category_Sport,
                    '0', //Dummy Stats Inc. code
                    '0', //Dummy Sportsticker ID
                    dmMain.CSSQuery.FieldByname('game').AsInteger, //CSS ID
                    dmMain.CSSQuery.FieldByname('GameKey').AsInteger, //NFL ID
                    CategoryRecPtr^.Primary_Data_Source,
                    CategoryRecPtr^.Backup_Data_Source);
                end
                else begin
                  CurrentGameData := EngineInterface.GetGameData(CategoryRecPtr^.Category_Sport,
                    '0', //Dummy Stats Inc. code
                    '0', //Dummy Sportsticker ID
                    0, //CSS ID
                    dmMain.DatamartQuery.FieldByname('GameKey').AsInteger, //NFL ID
                    CategoryRecPtr^.Primary_Data_Source,
                    CategoryRecPtr^.Backup_Data_Source);
                end;
              end;
            end;
            //Iterate through the fields
            for j := 0 to Template_Fields_Collection.Count-1 do
            begin
              TemplateFieldsRecPtr := Template_Fields_Collection.At(j);
              if (TemplateFieldsRecPtr^.Template_ID = CurrentTemplateID) then
              begin
                Modal.RecordGrid.Cell[1,i+1] := TemplateFieldsRecPtr^.Field_ID; //Index
                Modal.RecordGrid.Cell[2,i+1] := TemplateFieldsRecPtr^.Field_Label; //Description
                Modal.RecordGrid.Cell[4,i+1] := TemplateFieldsRecPtr^.Field_Type;
                //Set color & read only property
                Modal.RecordGrid.CellReadOnly[1,i+1] := roOn;
                Modal.RecordGrid.CellReadOnly[2,i+1] := roOn;
                Modal.RecordGrid.CellReadOnly[4,i+1] := roOn;
                if (TemplateFieldsRecPtr^.Field_Is_Manual = TRUE) then
                begin
                  //Firt check to see if it's a manual game winner indicator or animation
                  if (TemplateFieldsRecPtr^.Field_Type = 5) OR (TemplateFieldsRecPtr^.Field_Type = 6) then
                  begin
                    Modal.RecordGrid.CellControlType[3,i+1] := ctCheck;
                    Modal.RecordGrid.CellCheckBoxState[3,i+1] := cbUnchecked;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  //Next, check to see if it's a manual next page icon animation
                  else if (TemplateFieldsRecPtr^.Field_Type = 9) then
                  begin
                    Modal.RecordGrid.CellControlType[3,i+1] := ctCheck;
                    Modal.RecordGrid.CellCheckBoxState[3,i+1] := cbUnchecked;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  //Next, check to see if it's a weather icon; if so, display flipbooks in dropdown
                  else if (TemplateFieldsRecPtr^.Field_Type = 7) then
                  begin
                    Modal.RecordGrid.AssignCellCombo(3,i+1);
                    Modal.RecordGrid.CellButtonType[3,i+1] := btCombo;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                    //Init first combo box entry
                    WeatherIconRecPtr := Weather_Icon_Collection.At(0);
                    Modal.RecordGrid.Cell[3, i+1] := WeatherIconRecPtr^.Icon_Name;
                  end
                  //Next, check to see if it's a conference chip rec; if so, display conference chips in dropdown
                  else if (TemplateFieldsRecPtr^.Field_Type = 10) then
                  begin
                    Modal.RecordGrid.AssignCellCombo(3,i+1);
                    Modal.RecordGrid.CellButtonType[3,i+1] := btCombo;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                    //Init first combo box entry
                    ConferenceChipRecPtr := Conference_Chip_Collection.At(0);
                    Modal.RecordGrid.Cell[3, i+1] := ConferenceChipRecPtr^.Conference_Name;
                  end
                  //Next, check to see if it's the manual league entry; if so, display choices in dropdown
                  else if (TemplateFieldsRecPtr^.Field_Type = 8) then
                  begin
                    //Init combo box entry
                    CustomNewsHeaderRecPtr := CustomNewsHeader_Collection.At(ManualLeagueSelect.ItemIndex);
                    Modal.RecordGrid.Cell[3,i+1] :=  CustomNewsHeaderRecPtr^.HeaderText;
                  end
                  else begin
                    Modal.RecordGrid.RowColor[i+1] := clWindow;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end;
                end
                else begin
                  //Set cell value
                  Modal.RecordGrid.Cell[3,i+1] := EngineInterface.GetValueOfSymbol(PlaylistSelectTabControl.TabIndex+1,
                    TemplateFieldsRecPtr^.Field_Contents, TemplateFieldsRecPtr^.Field_Type, NOENTRYINDEX, CurrentGameData, 0);
                  Modal.RecordGrid.RowColor[i+1] := clAqua;
                  Modal.RecordGrid.CellReadOnly[3,i+1] := roOn;
                end;
                //Increment the template fields match counter
                Inc (i);
              end;
            end;
            Modal.TemplateName.Caption := dmMain.Query2.FieldByName('Template_Description').AsString;
            //Show the dialog
            Control := Modal.ShowModal;
          finally
            //Set new values if user didn't cancel out
            //Set new values if user didn't cancel out
            if (Control = mrOK) then OKToGo := TRUE
            else OKToGo := FALSE;
            //Set values based on data in grid
            if (Modal.RecordGrid.Rows > 0) then
            begin
              i := 1;
              for j := 1 to Modal.RecordGrid.Rows do
              begin
                if (Modal.RecordGrid.CellReadOnly[3,j] = roOff) then
                begin
                  //Check for visitor manual winner indication record type
                  if (Modal.RecordGrid.Cell[4,i] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,i] = cbChecked) then
                  begin
                    User_Data[24] := 'TRUE';
                  end
                  else if (Modal.RecordGrid.Cell[4,i] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,i] = cbUnChecked) then
                  begin
                    User_Data[24] := 'FALSE';
                  end
                  else if (Modal.RecordGrid.Cell[4,i] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,i] = cbChecked) then
                  begin
                    User_Data[25] := 'TRUE';
                  end
                  else if (Modal.RecordGrid.Cell[4,i] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,i] = cbUnChecked) then
                  begin
                    User_Data[25] := 'FALSE';
                  end
                  //Check for next page animated icon
                  else if (Modal.RecordGrid.Cell[4,i] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,i] = cbChecked) then
                  begin
                    User_Data[23] := 'TRUE';
                  end
                  else if (Modal.RecordGrid.Cell[4,i] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,i] = cbUnChecked) then
                  begin
                    User_Data[23] := 'FALSE';
                  end
                  else begin
                    //Set user value text and increment
                    User_Data[i] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                  end;
                  Inc(i);
                end;
              end;
            end;
            //Save current grid row
            SaveCurrentListRow := PlaylistGrid.CurrentDataRow;
            Modal.Free;
          end;
        end
        else begin
          //Init fields
          for i := 1 to 25 do User_Data[i] := ' ';
        end;
        if (OKToGo) then
        begin
          ////////////////////////////////////////////////////////////////////////
          // THIS BLOCK EXECUTED FOR CATEGORIES THAT ARE AUTOMATED SPORTS AND FOR
          // TEMPLATES THAT REQUIRED GAME DATA
          ////////////////////////////////////////////////////////////////////////
          //Check to see if at least one game in sportbase database
          if (CategoryRecPtr^.Category_Is_Sport = TRUE) AND
             {(dmMain.SportbaseQuery.RecordCount > 0) AND}
             (CurrentTemplateDefs.UsesGameData = TRUE) then
          begin
            //Go to first record in dataset
            if (CurrentDataSource = 'CBSCSS') then
            begin
              dmMain.CSSQuery.First;
              RecordCount := dmMain.CSSQuery.RecordCount;
            end
            else if (CurrentDataSource = 'NFLDM') then
            begin
              if (ConnectToCSS) then
              begin
                dmMain.CSSQuery.First;
                RecordCount := dmMain.CSSQuery.RecordCount;
              end
              else begin
                dmMain.DatamartQuery.First;
                RecordCount := dmMain.DatamartQuery.RecordCount;
              end;
            end
            else if (CurrentDataSource = 'STATSINC') then
            begin
              dmMain.SportbaseQuery.First;
              RecordCount := dmMain.SportbaseQuery.RecordCount;
            end;
            //Loop through for all selected games
            for m := 1 to RecordCount do
            begin
              //Check to see if game is selected
              if (CurrentDataSource = 'CBSCSS') then
                RowSelected := CSSGamesDBGrid.RowSelected[CSSGamesDBGrid.CurrentDataRow]
              else if (CurrentDataSource = 'NFLDM') then
              begin
                if (ConnectToCSS) then
                  RowSelected := CSSGamesDBGrid.RowSelected[CSSGamesDBGrid.CurrentDataRow]
                else
                RowSelected := NFLDMGamesDBGrid.RowSelected[NFLDMGamesDBGrid.CurrentDataRow];
              end
              else if (CurrentDataSource = 'STATSINC') then
                RowSelected := SIGamesDBGrid.RowSelected[SIGamesDBGrid.CurrentDataRow];
              if (RowSelected = TRUE) then
              begin
                //Query for Game data
                if ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS')) OR
                   ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS')) then
                begin
                  CurrentGameData := EngineInterface.GetGameData(CategoryRecPtr^.Category_Sport,
                    '0', //Dummy Stats Inc. code
                    '0', //Dummy Sportsticker ID
                    dmMain.CSSQuery.FieldByname('game').AsInteger, //CSS ID
                    dmMain.CSSQuery.FieldByname('GameKey').AsInteger, //NFL ID
                    CategoryRecPtr^.Primary_Data_Source,
                    CategoryRecPtr^.Backup_Data_Source);
                end
                else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC')) OR
                        ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC')) then
                begin
                  CurrentGameData := EngineInterface.GetGameData(CategoryRecPtr^.Category_Sport,
                    dmMain.SportbaseQuery.FieldByname('GCode').AsString,
                    '0', //Dummy SPortsticker ID
                    0, //Dummy CSS ID
                    0, // Dummy NFLDM GameID
                    CategoryRecPtr^.Primary_Data_Source,
                    CategoryRecPtr^.Backup_Data_Source);
                end
                else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'NFLDM')) OR
                        ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'NFLDM')) then
                begin
                  if (ConnectToCSS) then
                  begin
                    CurrentGameData := EngineInterface.GetGameData(CategoryRecPtr^.Category_Sport,
                      '0', //Dummy Stats Inc. code
                      '0', //Dummy Sportsticker ID
                      dmMain.CSSQuery.FieldByname('game').AsInteger, //CSS ID
                      dmMain.CSSQuery.FieldByname('GameKey').AsInteger, //NFL ID
                      CategoryRecPtr^.Primary_Data_Source,
                      CategoryRecPtr^.Backup_Data_Source);
                  end
                  else begin
                    CurrentGameData := EngineInterface.GetGameData(CategoryRecPtr^.Category_Sport,
                      '0', //Dummy Stats Inc. code
                      '0', //Dummy Sportsticker ID
                      0, //CSS ID
                      dmMain.DatamartQuery.FieldByname('GameKey').AsInteger, //NFL ID
                      CategoryRecPtr^.Primary_Data_Source,
                      CategoryRecPtr^.Backup_Data_Source);
                  end;
                end;
                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR PARENT TEMPLATES - ADDS CHILDREN
                ////////////////////////////////////////////////////////////////////////
                //Check to see if template is a "parent" template; if so, add all child templates
                if (CurrentTemplateDefs.Template_Has_Children) then
                begin
                  if (Child_Template_ID_Collection.Count > 0) then
                  begin
                    for i := Child_Template_ID_Collection.Count-1 downto 0 do
                    begin
                      //Need to go backwards for insert to make it work for record ordering
                      ChildTemplateIDRecPtr := Child_Template_ID_Collection.At(i);
                      //Check for child, and append if match
                      if (ChildTemplateIDRecPtr^.Template_ID = CurrentTemplateID) then
                      begin
                        //Allocate memory for the object
                        Case PlaylistSelectTabControl.TabIndex of
                            //Ticker
                         0: begin
                              //Allocate memory for the object
                              GetMem (TickerRecPtr, SizeOf(TickerRec));
                              With TickerRecPtr^ do
                              begin
                                //Set values for collection record
                                Event_Index := StudioTicker_Collection.Count+1;
                                CreateGUID(EventGUID);
                                Event_GUID := EventGUID;
                                Enabled := ChildTemplateIDRecPtr^.Child_Default_Enable_State;
                                Template_ID := ChildTemplateIDRecPtr^.Child_Template_ID;
                                Record_Type := GetRecordTypeFromTemplateID(Template_ID);
                                Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
                                Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
                                //Set league ID
                                if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                                begin
                                  League := CategoryRecPtr^.Category_Sport;
                                  Subleague_Mnemonic_Standard := GetAutomatedLeagueDisplayMnemonic(League);
                                  //Set data source information
                                  if ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS')) OR
                                     ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS')) then
                                  begin
                                    CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                                    NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                                    SI_GCode := '0';
                                    ST_Game_ID := '0'; //Default to blank for now
                                  end
                                  else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC')) OR
                                          ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC')) then
                                  begin
                                    CSS_GameID := 0;
                                    SI_GCode := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                                    NFLDM_GameID := 0; //Default to blank for now
                                    ST_Game_ID := '0'; //Default to blank for now
                                  end
                                  else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'NFLDM')) OR
                                          ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'NFLDM')) then
                                  begin
                                    if (ConnectToCSS) then
                                    begin
                                      CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                                      NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                                    end
                                    else begin
                                      CSS_GameID := 0;
                                      NFLDM_GameID := dmMain.DatamartQuery.FieldByname('GameKey').AsInteger;
                                    end;
                                    SI_GCode := '0';
                                    ST_Game_ID := '0'; //Default to blank for now
                                  end;
                                  Description := CurrentGameData.Description;
                                end
                                else begin
                                  League := 'NONE';
                                  Subleague_Mnemonic_Standard := 'NONE';
                                  Mnemonic_LiveEvent := 'NONE';
                                  SI_GCode := '0';
                                  ST_Game_ID := '0'; //Default to blank for now
                                  NFLDM_GameID := 0;
                                  CSS_GameID := 0;
                                  Description := '';
                                end;
                                if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                                begin
                                  SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                                  SponsorLogo_Dwell := SponsorLogoDwell.Value;
                                  SponsorLogo_Region := SponsorLogoRegion.Value;
                                end
                                else begin
                                  SponsorLogo_Name := '';
                                  SponsorLogo_Dwell := 0;
                                  SponsorLogo_Region := 0;
                                end;
                                //Set Is Child bit
                                Is_Child := TRUE;
                                //For stats
                                StatStoredProcedure := '';
                                StatType := 0;;
                                StatTeam := '';
                                StatLeague := '';
                                StatRecordNumber := 0;
                                for k := 1 to 25 do UserData[k] := User_Data[k];
                                //Set the start/end enable times to the template defaults
                                StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                                EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                                DwellTime := EntryDwellTime.Value*1000;
                                Selected := FALSE;
                                Comments := ' ';
                                //Insert object to collection
                                if (PlaylistGrid.CurrentDataRow > 0) then
                                begin
                                  StudioTicker_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, TickerRecPtr);
                                end
                                else
                                  StudioTicker_Collection.Insert(TickerRecPtr);
                                StudioTicker_Collection.Pack;
                              end;
                            end;
                            //Bug
                         1: begin
                              //Allocate memory for the object
                              GetMem (BugRecPtr, SizeOf(BugRec));
                              With BugRecPtr^ do
                              begin
                                //Set values for collection record
                                Event_Index := StudioBug_Collection.Count+1;
                                CreateGUID(EventGUID);
                                Event_GUID := EventGUID;
                                Enabled := ChildTemplateIDRecPtr^.Child_Default_Enable_State;
                                Template_ID := ChildTemplateIDRecPtr^.Child_Template_ID;
                                Record_Type := GetRecordTypeFromTemplateID(Template_ID);
                                Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
                                Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
                                //Set league ID
                                if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                                begin
                                  League := CategoryRecPtr^.Category_Sport;
                                  //Set data source information
                                  if ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS')) OR
                                     ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS')) then
                                  begin
                                    CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                                    NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                                    SI_GCode := '0';
                                    ST_Game_ID := '0'; //Default to blank for now
                                  end
                                  else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC')) OR
                                          ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC')) then
                                  begin
                                    CSS_GameID := 0;
                                    SI_GCode := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                                    NFLDM_GameID := 0; //Default to blank for now
                                    ST_Game_ID := '0'; //Default to blank for now
                                  end
                                  else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'NFLDM')) OR
                                          ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'NFLDM')) then
                                  begin
                                    if (ConnectToCSS) then
                                    begin
                                      CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                                      NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                                    end
                                    else begin
                                      CSS_GameID := 0;
                                      NFLDM_GameID := dmMain.DatamartQuery.FieldByname('GameKey').AsInteger;
                                    end;
                                    SI_GCode := '0';
                                    ST_Game_ID := '0'; //Default to blank for now
                                  end;
                                  Description := CurrentGameData.Description;
                                end
                                else begin
                                  League := 'N/A';
                                  SI_GCode := '0';
                                  ST_Game_ID := '0'; //Default to blank for now
                                  CSS_GameID := 0;
                                  NFLDM_GameID := 0;
                                  Description := '';
                                end;
                                //Set Is Child bit
                                Is_Child := TRUE;
                                for k := 1 to 25 do UserData[k] := User_Data[k];
                                //Set the start/end enable times to the template defaults
                                StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                                EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                                DwellTime := EntryDwellTime.Value*1000;
                                Selected := FALSE;
                                Comments := ' ';
                                //Insert object to collection
                                if (PlaylistGrid.CurrentDataRow > 0) then
                                  StudioBug_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, BugRecPtr)
                                else
                                  StudioBug_Collection.Insert(BugRecPtr);
                                StudioBug_Collection.Pack;
                              end;
                            end;
                            //Gametrax
                         2: begin
                              //Allocate memory for the object
                              GetMem (GameTraxRecPtr, SizeOf(GameTraxRec));
                              With GameTraxRecPtr^ do
                              begin
                                //Set values for collection record
                                Event_Index := GameTrax_Collection.Count+1;
                                CreateGUID(EventGUID);
                                Event_GUID := EventGUID;
                                Enabled := ChildTemplateIDRecPtr^.Child_Default_Enable_State;
                                Template_ID := ChildTemplateIDRecPtr^.Child_Template_ID;
                                Record_Type := GetRecordTypeFromTemplateID(Template_ID);
                                Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
                                Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
                                //Set league ID
                                if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                                begin
                                  League := CategoryRecPtr^.Category_Sport;
                                  //Set data source information
                                  if ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS')) OR
                                     ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS')) then
                                  begin
                                    CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                                    NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                                    SI_GCode := '0';
                                    ST_Game_ID := '0'; //Default to blank for now
                                  end
                                  else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC')) OR
                                          ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC')) then
                                  begin
                                    CSS_GameID := 0;
                                    SI_GCode := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                                    NFLDM_GameID := 0; //Default to blank for now
                                    ST_Game_ID := '0'; //Default to blank for now
                                  end
                                  else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'NFLDM')) OR
                                          ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'NFLDM')) then
                                  begin
                                    if (ConnectToCSS) then
                                    begin
                                      CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                                      NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                                    end
                                    else begin
                                      CSS_GameID := 0;
                                      NFLDM_GameID := dmMain.DatamartQuery.FieldByname('GameKey').AsInteger;
                                    end;
                                    SI_GCode := '0';
                                    ST_Game_ID := '0'; //Default to blank for now
                                  end;
                                  Description := CurrentGameData.Description;
                                end
                                else begin
                                  League := 'N/A';
                                  SI_GCode := '0';
                                  ST_Game_ID := '0'; //Default to blank for now
                                  CSS_GameID := 0;
                                  NFLDM_GameID := 0;
                                  Description := '';
                                end;
                                //Add sponsor logo information
                                if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                                begin
                                  SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                                  //Modified for Version 3.0.5
                                  if (PersistentSponsorCheckbox.Checked) then
                                    SponsorLogo_Dwell := -1
                                  else
                                    SponsorLogo_Dwell := SponsorLogoDwell.Value;
                                  SponsorLogo_Region := SponsorLogoRegion.Value;
                                end
                                else begin
                                  SponsorLogo_Name := ' ';
                                  SponsorLogo_Dwell := 0;
                                  SponsorLogo_Region := 0;
                                end;
                                //Set Is Child bit
                                Is_Child := TRUE;
                                for k := 1 to 25 do UserData[k] := User_Data[k];
                                //Set the start/end enable times to the template defaults
                                StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                                EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                                DwellTime := EntryDwellTime.Value*1000;
                                Selected := FALSE;
                                Comments := ' ';
                                //Insert object to collection
                                if (PlaylistGrid.CurrentDataRow > 0) then
                                  GameTrax_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, GameTraxRecPtr)
                                else
                                  GameTrax_Collection.Insert(GameTraxRecPtr);
                                GameTrax_Collection.Pack;
                              end;
                            end;
                        end;
                      end;
                    end;
                  end;
                end
                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR NON-PARENT TEMPLATES IN SPORTS CATEGORIES
                ////////////////////////////////////////////////////////////////////////
                //Not a parent template, so just append record for selected record type
                else begin
                  Case PlaylistSelectTabControl.TabIndex of
                      //Ticker
                   0: begin
                        //Allocate memory for the object
                        GetMem (TickerRecPtr, SizeOf(TickerRec));
                        With TickerRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := StudioTicker_Collection.Count+1;
                          CreateGUID(EventGUID);
                          Event_GUID := EventGUID;
                          Enabled := EntryEnable.Checked;
                          Template_ID := CurrentTemplateID;
                          Record_Type := GetRecordTypeFromTemplateID(dmMain.Query2.FieldByName('Template_ID').AsInteger);
                          Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
                          Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
                          //Set league ID
                          if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                          begin
                            League := CategoryRecPtr^.Category_Sport;
                            Subleague_Mnemonic_Standard := GetAutomatedLeagueDisplayMnemonic(League);
                            Mnemonic_LiveEvent := GetAutomatedLeagueDisplayMnemonic(League);
                            //Set data source information
                            if ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS')) OR
                               ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS')) then
                            begin
                              CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                              NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                              SI_GCode := '0';
                              ST_Game_ID := '0'; //Default to blank for now
                            end
                            else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC')) OR
                                    ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC')) then
                            begin
                              CSS_GameID := 0;
                              SI_GCode := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                              NFLDM_GameID := 0; //Default to blank for now
                              ST_Game_ID := '0'; //Default to blank for now
                            end
                            else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'NFLDM')) OR
                                    ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'NFLDM')) then
                            begin
                              if (ConnectToCSS) then
                              begin
                                CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                                NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                              end
                              else begin
                                CSS_GameID := 0;
                                NFLDM_GameID := dmMain.DatamartQuery.FieldByname('GameKey').AsInteger;
                              end;
                              SI_GCode := '0';
                              ST_Game_ID := '0'; //Default to blank for now
                            end;
                            Description := CurrentGameData.Description;
                          end
                          else begin
                            League := 'NONE';
                            Subleague_Mnemonic_Standard := 'NONE';
                            Mnemonic_LiveEvent := 'NONE';
                            SI_GCode := '0';
                            ST_Game_ID := '0'; //Default to blank for now
                            CSS_GameID := 0;
                            NFLDM_GameID := 0;
                            Description := '';
                          end;
                          if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                          begin
                            SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                            SponsorLogo_Dwell := SponsorLogoDwell.Value;
                            SponsorLogo_Region := SponsorLogoRegion.Value;
                          end
                          else begin
                            SponsorLogo_Name := '';
                            SponsorLogo_Dwell := 0;
                            SponsorLogo_Region := 0;
                          end;
                          //Clear Is Child bit
                          Is_Child := FALSE;
                          //For stats
                          StatStoredProcedure := '';
                          StatType := 0;;
                          StatTeam := '';
                          StatLeague := '';
                          StatRecordNumber := 0;
                          for k := 1 to 25 do UserData[k] := User_Data[k];
                          //Set the start/end enable times to the template defaults
                          StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                          EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                          DwellTime := EntryDwellTime.Value*1000;
                          Selected := FALSE;
                          Comments := ' ';
                          //Insert object to collection
                          if (PlaylistGrid.CurrentDataRow > 0) then
                            StudioTicker_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, TickerRecPtr)
                          else
                            StudioTicker_Collection.Insert(TickerRecPtr);
                          StudioTicker_Collection.Pack;
                        end;
                      end;
                      //Bug
                   1: begin
                        //Allocate memory for the object
                        GetMem (BugRecPtr, SizeOf(BugRec));
                        With BugRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := StudioBug_Collection.Count+1;
                          CreateGUID(EventGUID);
                          Event_GUID := EventGUID;
                          Enabled := EntryEnable.Checked;
                          Template_ID := CurrentTemplateID;
                          Record_Type := GetRecordTypeFromTemplateID(dmMain.Query2.FieldByName('Template_ID').AsInteger);
                          Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
                          Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
                          //Set league ID
                          if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                          begin
                            League := dmMain.SportbaseQuery.FieldByname('League').AsString;
                            //Set data source information
                            if ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS')) OR
                               ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS')) then
                            begin
                              CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                              NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                              SI_GCode := '0';
                              ST_Game_ID := '0'; //Default to blank for now
                            end
                            else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC')) OR
                                    ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC')) then
                            begin
                              CSS_GameID := 0;
                              SI_GCode := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                              NFLDM_GameID := 0; //Default to blank for now
                              ST_Game_ID := '0'; //Default to blank for now
                            end
                            else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'NFLDM')) OR
                                    ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'NFLDM')) then
                            begin
                              if (ConnectToCSS) then
                              begin
                                CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                                NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                              end
                              else begin
                                CSS_GameID := 0;
                                NFLDM_GameID := dmMain.DatamartQuery.FieldByname('GameKey').AsInteger;
                              end;
                              SI_GCode := '0';
                              ST_Game_ID := '0'; //Default to blank for now
                            end;
                            Description := CurrentGameData.Description;
                          end
                          else begin
                            League := 'N/A';
                            SI_GCode := '0';
                            ST_Game_ID := '0'; //Default to blank for now
                            CSS_GameID := 0;
                            NFLDM_GameID := 0;
                            Description := '';
                          end;
                          //Clear Is Child bit
                          Is_Child := FALSE;
                          for k := 1 to 25 do UserData[k] := User_Data[k];
                          //Set the start/end enable times to the template defaults
                          StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                          EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                          DwellTime := EntryDwellTime.Value*1000;
                          Selected := FALSE;
                          Comments := ' ';
                          //Insert object to collection
                          if (PlaylistGrid.CurrentDataRow > 0) then
                            StudioBug_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, BugRecPtr)
                          else
                            StudioBug_Collection.Insert(BugRecPtr);
                          StudioBug_Collection.Pack;
                        end;
                      end;
                      //Gametrax
                   2: begin
                        //Allocate memory for the object
                        GetMem (GameTraxRecPtr, SizeOf(GameTraxRec));
                        With GameTraxRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := GameTrax_Collection.Count+1;
                          CreateGUID(EventGUID);
                          Event_GUID := EventGUID;
                          Enabled := EntryEnable.Checked;
                          Template_ID := CurrentTemplateID;
                          Record_Type := GetRecordTypeFromTemplateID(dmMain.Query2.FieldByName('Template_ID').AsInteger);
                          Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
                          Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
                          //Set league ID
                          if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                          begin
                            League := CategoryRecPtr^.Category_Sport;
                            //Set data source information
                            if ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS')) OR
                               ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS')) then
                            begin
                              CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                              NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                              SI_GCode := '0';
                              ST_Game_ID := '0'; //Default to blank for now
                            end
                            else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC')) OR
                                    ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC')) then
                            begin
                              CSS_GameID := 0;
                              SI_GCode := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                              NFLDM_GameID := 0; //Default to blank for now
                              ST_Game_ID := '0'; //Default to blank for now
                            end
                            else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'NFLDM')) OR
                                    ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'NFLDM')) then
                            begin
                              if (ConnectToCSS) then
                              begin
                                CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
                                NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
                              end
                              else begin
                                CSS_GameID := 0;
                                NFLDM_GameID := dmMain.DatamartQuery.FieldByname('GameKey').AsInteger;
                              end;
                              SI_GCode := '0';
                              ST_Game_ID := '0'; //Default to blank for now
                            end;
                            Description := CurrentGameData.Description;
                          end
                          else begin
                            League := 'N/A';
                            SI_GCode := '0';
                            ST_Game_ID := '0'; //Default to blank for now
                            CSS_GameID := 0;
                            NFLDM_GameID := 0;
                            Description := '';
                          end;
                          //Add sponsor logo information
                          if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                          begin
                            SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                            //Modified for Version 3.0.5
                            if (PersistentSponsorCheckbox.Checked) then
                              SponsorLogo_Dwell := -1
                            else
                              SponsorLogo_Dwell := SponsorLogoDwell.Value;
                            SponsorLogo_Region := SponsorLogoRegion.Value;
                          end
                          else begin
                            SponsorLogo_Name := ' ';
                            SponsorLogo_Dwell := 0;
                            SponsorLogo_Region := 0;
                          end;
                          //Clear Is Child bit
                          Is_Child := FALSE;
                          for k := 1 to 25 do UserData[k] := User_Data[k];
                          //Set the start/end enable times to the template defaults
                          StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                          EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                          DwellTime := EntryDwellTime.Value*1000;
                          Selected := FALSE;
                          Comments := ' ';
                          //Insert object to collection
                          if (PlaylistGrid.CurrentDataRow > 0) then
                            GameTrax_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, GameTraxRecPtr)
                          else
                            GameTrax_Collection.Insert(GameTraxRecPtr);
                          GameTrax_Collection.Pack;
                        end;
                      end;
                  end;
                end;
              end;
              //Go to next record
              if (CurrentDataSource = 'CBSCSS') then
                dmMain.CSSQuery.Prior
              else if (CurrentDataSource = 'STATSINC') then
                dmMain.SportbaseQuery.Prior
              else if (CurrentDataSource = 'NFLDM') then
              begin
                if (ConnectToCSS) then
                  dmMain.CSSQuery.Next
                else
                  dmMain.DatamartQuery.Next;
              end;
            end;
          end
          ////////////////////////////////////////////////////////////////////////
          // THIS BLOCK EXECUTED FOR TRMPLATES THAT DO NOT REQUIRE GAME DATA
          ////////////////////////////////////////////////////////////////////////
          //No games in Sportbase query - if not a game specific record type, just add one record
          else if (CurrentTemplateDefs.UsesGameData = FALSE) then
          begin
            //Allocate memory for the object
            Case PlaylistSelectTabControl.TabIndex of
                 //Ticker
             0: begin
                  GetMem (TickerRecPtr, SizeOf(TickerRec));
                  With TickerRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := StudioTicker_Collection.Count+1;
                    CreateGUID(EventGUID);
                    Event_GUID := EventGUID;
                    Enabled := EntryEnable.Checked;
                    Template_ID := CurrentTemplateID;
                    Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                    //Get category & league information
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    //Check if manual league; if so, first user text field will be league
                    if (CurrentTemplateDefs.ManualLeague) then
                    begin
                      CustomNewsHeaderRecPtr := CustomNewsHeader_Collection.At(ManualLeagueSelect.ItemIndex);
                      League := CustomNewsHeaderRecPtr^.HeaderText;
                      Subleague_Mnemonic_Standard := 'NONE';
                      Mnemonic_LiveEvent := 'NONE';
                    end
                    else if (Trim(CategoryRecPtr^.Category_Sport) <> '') then
                    begin
                      League := CategoryRecPtr^.Category_Sport;
                      Subleague_Mnemonic_Standard := 'NONE';
                      Mnemonic_LiveEvent := 'NONE';
                    end
                    else begin
                      League := 'NONE';
                      Subleague_Mnemonic_Standard := 'NONE';
                      Mnemonic_LiveEvent := 'NONE';
                    end;
                    Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
                    Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
                    SI_GCode := '0';
                    ST_Game_ID := '0'; //Default to blank for now
                    CSS_GameID := 0;
                    NFLDM_GameID := 0; //Default to blank for now
                    Description := '';
                    //Check to see if sponsor logo
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                    begin
                      SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                      SponsorLogo_Dwell := SponsorLogoDwell.Value;
                      SponsorLogo_Region := SponsorLogoRegion.Value;
                    end
                    //Check if promo
                    else if (UpperCase(CategoryRecPtr^.Category_Name) = 'PROMOS') then
                    begin
                      SponsorLogo_Name := dmMain.tblPromo_Logos.FieldByName('LogoFilename').AsString;
                      SponsorLogo_Dwell := 0;
                      SponsorLogo_Region := 0;
                    end
                    else begin
                      SponsorLogo_Name := '';
                      SponsorLogo_Dwell := 0;
                      SponsorLogo_Region := 0;
                    end;
                    //Clear Is Child bit
                    Is_Child := FALSE;
                    //For stats
                    StatStoredProcedure := '';
                    StatType := 0;;
                    StatTeam := '';
                    StatLeague := '';
                    StatRecordNumber := 0;
                    for k := 1 to 25 do UserData[k] := User_Data[k];
                    //Set the start/end enable times to the template defaults
                    StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                    EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                    DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                    Selected := FALSE;
                    Comments := ' ';
                    //Insert object to collection
                    if (PlaylistGrid.CurrentDataRow > 0) then
                      StudioTicker_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, TickerRecPtr)
                    else
                      StudioTicker_Collection.Insert(TickerRecPtr);
                    StudioTicker_Collection.Pack;
                  end;
                end;
                 //Bug
             1: begin
                  GetMem (BugRecPtr, SizeOf(BugRec));
                  With BugRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := StudioBug_Collection.Count+1;
                    CreateGUID(EventGUID);
                    Event_GUID := EventGUID;
                    Enabled := EntryEnable.Checked;
                    Template_ID := CurrentTemplateID;
                    Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                    //Get category & league information
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    //Check if manual league; if so, first user text field will be league
                    if (CurrentTemplateDefs.ManualLeague) then
                    begin
                      CustomNewsHeaderRecPtr := CustomNewsHeader_Collection.At(ManualLeagueSelect.ItemIndex);
                      League := CustomNewsHeaderRecPtr^.HeaderText;
                    end
                    else if (Trim(CategoryRecPtr^.Category_Sport) <> '') then
                    begin
                      League := CategoryRecPtr^.Category_Sport;
                    end
                    else begin
                      League := 'NONE';
                    end;
                    //Clear Is Child bit
                    Is_Child := FALSE;
                    Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
                    Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
                    SI_GCode := '0';
                    ST_Game_ID := '0'; //Default to blank for now
                    CSS_GameID := 0;
                    NFLDM_GameID := 0; //Default to blank for now
                    Description := '';
                    for k := 1 to 25 do UserData[k] := User_Data[k];
                    //Set the start/end enable times to the template defaults
                    StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                    EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                    DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                    Selected := FALSE;
                    Comments := ' ';
                    //Insert object to collection
                    if (PlaylistGrid.CurrentDataRow > 0) then
                      StudioBug_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, BugRecPtr)
                    else
                      StudioBug_Collection.Insert(BugRecPtr);
                    StudioBug_Collection.Pack;
                  end;
                end;
                 //Gametrax
             2: begin
                  GetMem (GameTraxRecPtr, SizeOf(GameTraxRec));
                  With GameTraxRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := GameTrax_Collection.Count+1;
                    CreateGUID(EventGUID);
                    Event_GUID := EventGUID;
                    Enabled := EntryEnable.Checked;
                    Template_ID := CurrentTemplateID;
                    Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                    Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
                    Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
                    SI_GCode := '0';
                    ST_Game_ID := '0'; //Default to blank for now
                    CSS_GameID := 0;
                    NFLDM_GameID := 0; //Default to blank for now
                    Description := '';
                    Selected := FALSE;
                    //Get category & league information
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    //Check if manual league; if so, first user text field will be league
                    if (CurrentTemplateDefs.ManualLeague) then
                    begin
                      CustomNewsHeaderRecPtr := CustomNewsHeader_Collection.At(ManualLeagueSelect.ItemIndex);
                      League := CustomNewsHeaderRecPtr^.HeaderText;
                    end
                    else if (Trim(CategoryRecPtr^.Category_Sport) <> '') then
                    begin
                      League := CategoryRecPtr^.Category_Sport;
                    end
                    else begin
                      League := 'NONE';
                    end;
                    //Add sponsor logo information
                    if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                    begin
                      SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                      //Modified for Version 3.0.5
                      if (PersistentSponsorCheckbox.Checked) then
                        SponsorLogo_Dwell := -1
                      else
                        SponsorLogo_Dwell := SponsorLogoDwell.Value;
                      SponsorLogo_Region := SponsorLogoRegion.Value;
                    end
                    else begin
                      SponsorLogo_Name := ' ';
                      SponsorLogo_Dwell := 0;
                      SponsorLogo_Region := 0;
                    end;
                    //Clear Is Child bit
                    Is_Child := FALSE;
                    for k := 1 to 25 do UserData[k] := User_Data[k];
                    //Set the start/end enable times to the template defaults
                    StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                    EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                    DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                    Comments := ' ';
                    //Insert object to collection
                    if (PlaylistGrid.CurrentDataRow > 0) then
                      GameTrax_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, GameTraxRecPtr)
                    else
                      GameTrax_Collection.Insert(GameTraxRecPtr);
                    GameTrax_Collection.Pack;
                  end;
                end;
            end; //Single template type (NOT a parent record)
          end;
        end;
        //Refresh grid
        RefreshPlaylistGrid;
        //Refresh the fields grid
        RefreshEntryFieldsGrid(CurrentTickerDisplayMode);
        //Restore current grid rows
        PlaylistGrid.CurrentDataRow := SaveCurrentListRow;
        PlaylistGrid.TopRow := SaveTopListRow;
      end;
    end;
    //Restore grid positions
    if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
    begin
      //Get number of games selected
      if (CurrentDataSource = 'CBSCSS') AND (dmMain.CSSQuery.RecordCount > 0) then
      begin
        //Set top & current row
        CSSGamesDBGrid.CurrentDataRow := SaveCurrentGamesRow;
        CSSGamesDBGrid.TopRow := SaveTopGamesRow;
      end
      else if (CurrentDataSource = 'STATSINC') AND (dmMain.SportbaseQuery.RecordCount > 0) then
      begin
        //Set top & current row
        SIGamesDBGrid.CurrentDataRow := SaveCurrentGamesRow;
        SIGamesDBGrid.TopRow := SaveTopGamesRow;
      end
      else if (CurrentDataSource = 'NFLDM') AND (dmMain.DatamartQuery.RecordCount > 0) then
      begin
        if (ConnectToCSS) then
        begin
          //Set top & current row
          CSSGamesDBGrid.CurrentDataRow := SaveCurrentGamesRow;
          CSSGamesDBGrid.TopRow := SaveTopGamesRow;
        end
        else begin
          //Set top & current row
          NFLDMGamesDBGrid.CurrentDataRow := SaveCurrentGamesRow;
          NFLDMGamesDBGrid.TopRow := SaveTopGamesRow;
        end;
      end;
    end;
  except
    EngineInterface.WriteToErrorLog('Error occurred while trying to insert playlist entry');
  end;
end;

//Handler for Enable selected playlist records
procedure TMainForm.Enable1Click(Sender: TObject);
var
  TickerRecPtr, TickerRecPtr2: ^TickerRec;
  BugRecPtr, BugRecPtr2: ^BugRec;
  GameTraxRecPtr, GameTraxRecPtr2: ^GameTraxRec;
  i,j: SmallInt;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Studio Ticker
   0: begin
        if (StudioTicker_Collection.Count > 0) then
          for i := 1 to StudioTicker_Collection.Count do
          begin
            TickerRecPtr := StudioTicker_Collection.At(i-1);
            if (PlaylistGrid.RowSelected[i] = TRUE) then
              TickerRecPtr^.Enabled := TRUE;
            //Now, perform same operation on all copies of pages
            for j := 1 to StudioTicker_Collection.Count do
            begin
              TickerRecPtr2 := StudioTicker_Collection.At(j-1);
              if (IsEqualGUID(TickerRecPtr^.Event_GUID, TickerRecPtr2^.Event_GUID)) then
              begin
                if (PlaylistGrid.RowSelected[i] = TRUE) then
                  TickerRecPtr2^.Enabled := TRUE;
              end;
            end;
          end;
      end;
      //Studio Bug
   1: begin
        if (StudioBug_Collection.Count > 0) then
          for i := 1 to StudioBug_Collection.Count do
          begin
            BugRecPtr := StudioBug_Collection.At(i-1);
            if (PlaylistGrid.RowSelected[i] = TRUE) then
              BugRecPtr^.Enabled := TRUE;
            //Now, perform same operation on all copies of pages
            for j := 1 to StudioBug_Collection.Count do
            begin
              BugRecPtr2 := StudioBug_Collection.At(j-1);
              if (IsEqualGUID(BugRecPtr^.Event_GUID, BugRecPtr2^.Event_GUID)) then
              begin
                if (PlaylistGrid.RowSelected[i] = TRUE) then
                  BugRecPtr2^.Enabled := TRUE;
              end;
            end;
          end;
      end;
      //Gametrax
   2: begin
        if (GameTrax_Collection.Count > 0) then
          for i := 1 to GameTrax_Collection.Count do
          begin
            GameTraxRecPtr := GameTrax_Collection.At(i-1);
            if (PlaylistGrid.RowSelected[i] = TRUE) then
              GameTraxRecPtr^.Enabled := TRUE;
            //Now, perform same operation on all copies of pages
            for j := 1 to GameTrax_Collection.Count do
            begin
              GameTraxRecPtr2 := GameTrax_Collection.At(j-1);
              if (IsEqualGUID(GameTraxRecPtr^.Event_GUID, GameTraxRecPtr2^.Event_GUID)) then
              begin
                if (PlaylistGrid.RowSelected[i] = TRUE) then
                  GameTraxRecPtr2^.Enabled := TRUE;
              end;
            end;
          end;
      end;
  end;
  //Refresh the grid
  RefreshPlaylistGrid;
end;

//Handler for Disable selected playlist records
procedure TMainForm.Disable1Click(Sender: TObject);
var
  TickerRecPtr, TickerRecPtr2: ^TickerRec;
  BugRecPtr, BugRecPtr2: ^BugRec;
  GameTraxRecPtr, GameTraxRecPtr2: ^GameTraxRec;
  i,j: SmallInt;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Studio Ticker
   0: begin
        if (StudioTicker_Collection.Count > 0) then
          for i := 1 to StudioTicker_Collection.Count do
          begin
            TickerRecPtr := StudioTicker_Collection.At(i-1);
            if (PlaylistGrid.RowSelected[i] = TRUE) then
              TickerRecPtr^.Enabled := FALSE;
            //Now, perform same operation on all copies of pages
            for j := 1 to StudioTicker_Collection.Count do
            begin
              TickerRecPtr2 := StudioTicker_Collection.At(j-1);
              if (IsEqualGUID(TickerRecPtr^.Event_GUID, TickerRecPtr2^.Event_GUID)) then
              begin
                if (PlaylistGrid.RowSelected[i] = TRUE) then
                  TickerRecPtr2^.Enabled := FALSE;
              end;
            end;
          end;
      end;
      //Studio Bug
   1: begin
        if (StudioBug_Collection.Count > 0) then
          for i := 1 to StudioBug_Collection.Count do
          begin
            BugRecPtr := StudioBug_Collection.At(i-1);
            if (PlaylistGrid.RowSelected[i] = TRUE) then
              BugRecPtr^.Enabled := FALSE;
            //Now, perform same operation on all copies of pages
            for j := 1 to StudioBug_Collection.Count do
            begin
              BugRecPtr2 := StudioBug_Collection.At(j-1);
              if (IsEqualGUID(BugRecPtr^.Event_GUID, BugRecPtr2^.Event_GUID)) then
              begin
                if (PlaylistGrid.RowSelected[i] = TRUE) then
                  BugRecPtr2^.Enabled := FALSE;
              end;
            end;
          end;
      end;
      //Gametrax
   2: begin
        if (GameTrax_Collection.Count > 0) then
          for i := 1 to GameTrax_Collection.Count do
          begin
            GameTraxRecPtr := GameTrax_Collection.At(i-1);
            if (PlaylistGrid.RowSelected[i] = TRUE) then
              GameTraxRecPtr^.Enabled := FALSE;
            //Now, perform same operation on all copies of pages
            for j := 1 to GameTrax_Collection.Count do
            begin
              GameTraxRecPtr2 := GameTrax_Collection.At(j-1);
              if (IsEqualGUID(GameTraxRecPtr^.Event_GUID, GameTraxRecPtr2^.Event_GUID)) then
              begin
                if (PlaylistGrid.RowSelected[i] = TRUE) then
                  GameTraxRecPtr2^.Enabled := FALSE;
              end;
            end;
          end;
      end;
  end;
  //Refresh the grid
  RefreshPlaylistGrid;
end;

//Handler for popup menu entry to allow editing of start/end enable times
procedure TMainForm.EditStartEndEnableTimes1Click(Sender: TObject);
var
  i,j,k,m: SmallInt;
  CurrentRow, CurrentTopRow: SmallInt;
  Modal: TEnableDateTimeEditorDlg;
  Control: Word;
  TickerRecPtr, TickerRecPtr2: ^TickerRec;
  BugRecPtr, BugRecPtr2: ^BugRec;
  GameTraxRecPtr, GameTraxRecPtr2: ^GameTraxRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  User_Data: Array[1..25] of String[255];
  CurrentTemplateID: SmallInt;
  NumTemplateFields: SmallInt;
  CollectionCount: SmallInt;
  EventGUID: TGUID;
begin
  Case PlaylistSelectTabControl.TabIndex of
       //Studio Ticker
    0: CollectionCount := StudioTicker_Collection.Count;
       //Studio Bug
    1: CollectionCount := StudioBug_Collection.Count;
       //Gametrax
    2: CollectionCount := GameTrax_Collection.Count;
  end;
  If ((CollectionCount > 0) AND (PlaylistGrid.Rows >= 0)) then
  begin
    //Get current row
    CurrentRow := PlaylistGrid.CurrentDataRow;
    CurrentTopRow := PlaylistGrid.TopRow;
    //Only proceed if a row was selected
    if (CurrentRow <> -1) then
    begin
      Case PlaylistSelectTabControl.TabIndex of
          //Ticker
       0: begin
            TickerRecPtr := StudioTicker_Collection.At(CurrentRow-1);
            //Setup and launch editing dialog if applicable
            try
              Modal := TEnableDateTimeEditorDlg.Create(Application);
              Modal.EntryNote.Text := TickerRecPtr^.Comments;
              Modal.EntryStartEnableDate.Date := TickerRecPtr^.StartEnableDateTime;
              Modal.EntryStartEnableTime.Time := TickerRecPtr^.StartEnableDateTime;
              Modal.EntryEndEnableDate.Date := TickerRecPtr^.EndEnableDateTime;
              Modal.EntryEndEnableTime.Time := TickerRecPtr^.EndEnableDateTime;
              //If a sponsor dwell was specified, it's a sponsor logo
              if (TickerRecPtr^.SponsorLogo_Dwell > 0) then
                Modal.EntryDwellTime.Value := Trunc(TickerRecPtr^.SponsorLogo_Dwell)
              else
                Modal.EntryDwellTime.Value := Trunc(TickerRecPtr^.DwellTime/1000);

              //Show the dialog
              Control := Modal.ShowModal;
            finally
              //Set new values if user didn't cancel out
              if (Control = mrOK) then
              begin
                //Edit all matching objects using comparsion of GUIDs
                for j := 0 to StudioTicker_Collection.Count-1 do
                begin
                  if (PlaylistGrid.RowSelected[j+1] = TRUE) then
                  begin
                    TickerRecPtr := StudioTicker_Collection.At(j);
                    EventGUID := TickerRecPtr^.Event_GUID;
                    for m := 0 to StudioTicker_Collection.Count-1 do
                    begin
                      TickerRecPtr2 := StudioTicker_Collection.At(m);
                      if (IsEqualGUID(TickerRecPtr2^.Event_GUID, EventGUID)) then
                      With TickerRecPtr2^ do
                      begin
                        StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                            (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                        EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                            (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                        if (Modal.NoteChangeEnable.Checked) then
                          Comments := Modal.EntryNote.Text;
                        //If a sponsor dwell was specified, it's a sponsor logo
                        if (TickerRecPtr^.SponsorLogo_Dwell > 0) then
                          SponsorLogo_Dwell := Modal.EntryDwellTime.Value
                        else
                          DwellTime := Modal.EntryDwellTime.Value*1000;
                      end;
                    end;
                  end;
                end;
              end;
              Modal.Free;
            end;
          end;
          //Bug
       1: begin
            BugRecPtr := StudioBug_Collection.At(CurrentRow-1);
            //Setup and launch editing dialog if applicable
            try
              Modal := TEnableDateTimeEditorDlg.Create(Application);
              Modal.EntryNote.Text := BugRecPtr^.Comments;
              Modal.EntryStartEnableDate.Date := BugRecPtr^.StartEnableDateTime;
              Modal.EntryStartEnableTime.Time := BugRecPtr^.StartEnableDateTime;
              Modal.EntryEndEnableDate.Date := BugRecPtr^.EndEnableDateTime;
              Modal.EntryEndEnableTime.Time := BugRecPtr^.EndEnableDateTime;
              Modal.EntryDwellTime.Value := Trunc(BugRecPtr^.DwellTime/1000);
              //Show the dialog
              Control := Modal.ShowModal;
            finally
              //Set new values if user didn't cancel out
              if (Control = mrOK) then
              begin
                //Edit all matching objects using comparsion of GUIDs
                for j := 0 to StudioBug_Collection.Count-1 do
                begin
                  if (PlaylistGrid.RowSelected[j+1] = TRUE) then
                  begin
                    BugRecPtr := StudioBug_Collection.At(j);
                    EventGUID := BugRecPtr^.Event_GUID;
                    for m := 0 to StudioBug_Collection.Count-1 do
                    begin
                      BugRecPtr2 := StudioBug_Collection.At(m);
                      if (IsEqualGUID(BugRecPtr2^.Event_GUID, EventGUID)) then
                      With BugRecPtr2^ do
                      begin
                        StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                            (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                        EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                            (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                        if (Modal.NoteChangeEnable.Checked) then
                          Comments := Modal.EntryNote.Text;
                        //if (Modal.DwellTimeEnable.Checked) then
                        DwellTime := Modal.EntryDwellTime.Value*1000;
                      end;
                    end;
                  end;
                end;
              end;
              Modal.Free;
            end;
          end;
          //Gametrax
       2: begin
            GameTraxRecPtr := GameTrax_Collection.At(CurrentRow-1);
            //Setup and launch editing dialog if applicable
            try
              Modal := TEnableDateTimeEditorDlg.Create(Application);
              Modal.EntryNote.Text := GameTraxRecPtr^.Comments;
              Modal.EntryStartEnableDate.Date := GameTraxRecPtr^.StartEnableDateTime;
              Modal.EntryStartEnableTime.Time := GameTraxRecPtr^.StartEnableDateTime;
              Modal.EntryEndEnableDate.Date := GameTraxRecPtr^.EndEnableDateTime;
              Modal.EntryEndEnableTime.Time := GameTraxRecPtr^.EndEnableDateTime;
              Modal.EntryDwellTime.Value := Trunc(GameTraxRecPtr^.DwellTime/1000);
              //Show the dialog
              Control := Modal.ShowModal;
            finally
              //Set new values if user didn't cancel out
              if (Control = mrOK) then
              begin
                //Edit all matching objects using comparsion of GUIDs
                for j := 0 to GameTrax_Collection.Count-1 do
                begin
                  if (PlaylistGrid.RowSelected[j+1] = TRUE) then
                  begin
                    GameTraxRecPtr := GameTrax_Collection.At(j);
                    EventGUID := GameTraxRecPtr^.Event_GUID;
                    for m := 0 to GameTrax_Collection.Count-1 do
                    begin
                      GameTraxRecPtr2 := GameTrax_Collection.At(m);
                      if (IsEqualGUID(GameTraxRecPtr2^.Event_GUID, EventGUID)) then
                      With GameTraxRecPtr2^ do
                      begin
                        StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                            (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                        EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                            (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                        if (Modal.NoteChangeEnable.Checked) then
                          Comments := Modal.EntryNote.Text;
                        //if (Modal.DwellTimeEnable.Checked) then
                        DwellTime := Modal.EntryDwellTime.Value*1000;
                      end;
                    end;
                  end;
                end;
              end;
              Modal.Free;
            end;
          end;
      end;
    end;
    //Refresh grid
    RefreshPlaylistGrid;
    PlaylistGrid.CurrentDataRow := CurrentRow;
    PlaylistGrid.TopRow := CurrentTopRow;
    PlaylistGrid.RowSelected[CurrentRow] := TRUE;
  end;
end;

//Handler for cut playlist entry
procedure TMainForm.Cut1Click(Sender: TObject);
begin
  Cut;
end;
procedure TMainForm.Cut;
begin
  //Copy records to temp collection
  SelectForCopyOrCut;
  //Set flag
  SelectForCut := TRUE;
  //Delete records from main collection
  DeleteGraphicFromZipperPlaylist(FALSE);
end;

//Handler for copy playlist entry
procedure TMainForm.Copy1Click(Sender: TObject);
begin
  Copy;
end;
procedure TMainForm.Copy;
begin
  SelectForCopyOrCut;
  SelectForCut := FALSE;
end;

//General procedure to set the record to be copied or cut
procedure TMainForm.SelectForCopyOrCut;
var
  i,j: SmallInt;
  CutCopyRecordCount: SmallInt;
  SelectedRecordFound: Boolean;
  TickerRecPtr, TempTickerRecPtr: ^TickerRec;
  BugRecPtr, TempBugRecPtr: ^BugRec;
  GameTraxRecPtr, TempGameTraxRecPtr: ^GameTraxRec;
  Save_Cursor: TCursor;
begin
  //Change cursor
  Save_Cursor := Screen.Cursor;
  //Show hourglass cursor  Screen.Cursor := crHourGlass;
  SelectedRecordFound := FALSE;
  CutCopyRecordCount := 0;
  Case PlaylistSelectTabControl.TabIndex of
      //Studio Ticker
   0: begin
        if (StudioTicker_Collection.Count > 0) then
        begin
          //Clear the temp collection
          Temp_StudioTicker_Collection.Clear;
          Temp_StudioTicker_Collection.Pack;
          for i := 0 to StudioTicker_Collection.Count-1 do
          begin
            TickerRecPtr := StudioTicker_Collection.At(i);
            if (PlaylistGrid.RowSelected[i+1] = TRUE) then
            begin
              //Set selected flag to true
              TickerRecPtr^.Selected := TRUE;
              //Inset record into temporary collection - will be used for copy/paste operation
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //GetMem (TempTickerRecPtr, SizeOf(TickerRec));
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //With TempTickerRecPtr^ do
              With TemporaryTickerDataArray[CutCopyRecordCount+1] do
              begin
                Event_Index := TickerRecPtr^.Event_Index;
                Event_GUID := TickerRecPtr^.Event_GUID;
                Is_Child := TickerRecPtr^.Is_Child;
                Enabled := TickerRecPtr^.Enabled;
                League := TickerRecPtr^.League;
                Subleague_Mnemonic_Standard := TickerRecPtr^.Subleague_Mnemonic_Standard;
                Mnemonic_LiveEvent := TickerRecPtr^.Mnemonic_LiveEvent;
                Template_ID := TickerRecPtr^.Template_ID;
                Record_Type := TickerRecPtr^.Record_Type;
                SI_GCode := TickerRecPtr^.SI_GCode;
                ST_Game_ID := TickerRecPtr^.ST_Game_ID;
                NFLDM_GameID := TickerRecPtr^.NFLDM_GameID;
                CSS_GameID := TickerRecPtr^.CSS_GameID;
                Primary_Data_Source := TickerRecPtr^.Primary_Data_Source;
                Backup_Data_Source := TickerRecPtr^.Backup_Data_Source;
                SponsorLogo_Name := TickerRecPtr^.SponsorLogo_Name;
                SponsorLogo_Dwell := TickerRecPtr^.SponsorLogo_Dwell;
                SponsorLogo_Region := TickerRecPtr^.SponsorLogo_Region;
                StatStoredProcedure := TickerRecPtr^.StatStoredProcedure;
                StatType := TickerRecPtr^.StatType;
                StatTeam := TickerRecPtr^.StatTeam;
                StatLeague := TickerRecPtr^.StatLeague;
                StatRecordNumber := TickerRecPtr^.StatRecordNumber;
                for j := 1 to 25 do UserData[j] := TickerRecPtr^.UserData[j];
                StartEnableDateTime := TickerRecPtr^.StartEnableDateTime;
                EndEnableDateTime := TickerRecPtr^.EndEnableDateTime;
                Description := TickerRecPtr^.Description;
                Comments := TickerRecPtr^.Comments;
                Selected := TickerRecPtr^.Selected;
                DwellTime := TickerRecPtr^.DwellTime;
              end;
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //Insert the new record into the collection
              //Temp_Ticker_Collection.Insert(TempTickerRecPtr);
              //Temp_Ticker_Collection.Pack;
              Inc(CutCopyRecordCount);
            end
            else TickerRecPtr^.Selected := FALSE;
          end;
          TemporaryTickerDataCount := CutCopyRecordCount;
        end;
      end;
      //Bug
   1: begin
        if (StudioBug_Collection.Count > 0) then
        begin
          //Clear the temp collection
          Temp_StudioBug_Collection.Clear;
          Temp_StudioBug_Collection.Pack;
          for i := 0 to StudioBug_Collection.Count-1 do
          begin
            BugRecPtr := StudioBug_Collection.At(i);
            if (PlaylistGrid.RowSelected[i+1] = TRUE) then
            begin
              BugRecPtr^.Selected := TRUE;
              //Insert record into temporary collection - will be used for copy/paste operation
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //GetMem (TempBugRecPtr, SizeOf(BugRec));
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //With TempBugRecPtr^ do
              With TemporaryBugDataArray[CutCopyRecordCount+1] do
              begin
                Event_Index := BugRecPtr^.Event_Index;
                Event_GUID := BugRecPtr^.Event_GUID;
                Is_Child := BugRecPtr^.Is_Child;
                Enabled := BugRecPtr^.Enabled;
                League := BugRecPtr^.League;
                Template_ID := BugRecPtr^.Template_ID;
                Record_Type := BugRecPtr^.Record_Type;
                SI_GCode := BugRecPtr^.SI_GCode;
                ST_Game_ID := BugRecPtr^.ST_Game_ID;
                NFLDM_GameID := BugRecPtr^.NFLDM_GameID;
                CSS_GameID := BugRecPtr^.CSS_GameID;
                Primary_Data_Source := BugRecPtr^.Primary_Data_Source;
                Backup_Data_Source := BugRecPtr^.Backup_Data_Source;
                for j := 1 to 25 do UserData[j] := BugRecPtr^.UserData[j];
                StartEnableDateTime := BugRecPtr^.StartEnableDateTime;
                EndEnableDateTime := BugRecPtr^.EndEnableDateTime;
                Description := BugRecPtr^.Description;
                Comments := BugRecPtr^.Comments;
                Selected := BugRecPtr^.Selected;
                DwellTime := BugRecPtr^.DwellTime;
              end;
              //Insert the new record into the collection
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //Insert the new record into the collection
              //Temp_Bug_Collection.Insert(TempBugRecPtr);
              //Temp_Bug_Collection.Pack;
              Inc(CutCopyRecordCount);
            end
            else BugRecPtr^.Selected := FALSE;
          end;
          TemporaryBugDataCount := CutCopyRecordCount;
        end;
      end;
      //Extra line
   2: begin
        if (GameTrax_Collection.Count > 0) then
        begin
          //Clear the temp collection
          Temp_GameTrax_Collection.Clear;
          Temp_GameTrax_Collection.Pack;
          for i := 0 to GameTrax_Collection.Count-1 do
          begin
            GameTraxRecPtr := GameTrax_Collection.At(i);
            if (PlaylistGrid.RowSelected[i+1] = TRUE) then
            begin
              GameTraxRecPtr^.Selected := TRUE;
              //Inset record into temporary collection - will be used for copy/paste operation
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //GetMem (TempGameTraxRecPtr, SizeOf(GameTraxRec));
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //With TempGameTraxRecPtr^ do
              With TemporaryGameTraxDataArray[CutCopyRecordCount+1] do
              begin
                Event_Index := GameTraxRecPtr^.Event_Index;
                Event_GUID := GameTraxRecPtr^.Event_GUID;
                Enabled := GameTraxRecPtr^.Enabled;
                Is_Child := GameTraxRecPtr^.Is_Child;
                League := GameTraxRecPtr^.League;
                Template_ID := GameTraxRecPtr^.Template_ID;
                Record_Type := GameTraxRecPtr^.Record_Type;
                SI_GCode := GametraxRecPtr^.SI_GCode;
                ST_Game_ID := GametraxRecPtr^.ST_Game_ID;
                NFLDM_GameID := GametraxRecPtr^.NFLDM_GameID;
                CSS_GameID := GametraxRecPtr^.CSS_GameID;
                SponsorLogo_Name := GametraxRecPtr^.SponsorLogo_Name;
                SponsorLogo_Dwell := GametraxRecPtr^.SponsorLogo_Dwell;
                SponsorLogo_Region := GametraxRecPtr^.SponsorLogo_Region;
                Primary_Data_Source := GametraxRecPtr^.Primary_Data_Source;
                Backup_Data_Source := GametraxRecPtr^.Backup_Data_Source;
                for j := 1 to 25 do UserData[j] := GameTraxRecPtr^.UserData[j];
                StartEnableDateTime := GameTraxRecPtr^.StartEnableDateTime;
                EndEnableDateTime := GameTraxRecPtr^.EndEnableDateTime;
                Description := GameTraxRecPtr^.Description;
                Comments := GameTraxRecPtr^.Comments;
                Selected := GameTraxRecPtr^.Selected;
                DwellTime := GameTraxRecPtr^.DwellTime;
              end;
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //Insert the new record into the collection
              //Temp_GameTrax_Collection.Insert(TempGameTraxRecPtr);
              //Temp_GameTrax_Collection.Pack;
              Inc(CutCopyRecordCount);
            end
            else GameTraxRecPtr^.Selected := FALSE;
          end;
          TemporaryGameTraxDataCount := CutCopyRecordCount;
        end;
      end;
  end;
  //Restore cursor
  Screen.Cursor := Save_Cursor;
end;

////////////////////////////////////////////////////////////////////////////////
//General procedure for moving a record up or down in the playlist
procedure TMainForm.MoveRecord(Direction: SmallInt);
var
  i,j: SmallInt;
  CutCopyRecordCount: SmallInt;
  SelectedRecordFound: Boolean;
  TickerRecPtr, TempTickerRecPtr: ^TickerRec;
  BugRecPtr, TempBugRecPtr: ^BugRec;
  GameTraxRecPtr, TempGameTraxRecPtr: ^GameTraxRec;
  Save_Cursor: TCursor;
  FoundRecord: Boolean;
  SaveGridPosition: SmallInt;
  SaveDataRow: SmallInt;
  SaveTopRow: SmallInt;
begin
  //Change cursor
  Save_Cursor := Screen.Cursor;
  //Show hourglass cursor
  Screen.Cursor := crHourGlass;
  SelectedRecordFound := FALSE;
  CutCopyRecordCount := 0;
  SaveDataRow := PlaylistGrid.CurrentDataRow;
  SaveTopRow := PlaylistGrid.TopRow;
  Case PlaylistSelectTabControl.TabIndex of
      //Studio Ticker
   0: begin
        if (StudioTicker_Collection.Count > 0) then
        begin
          //Clear the temp collection
          for i := 0 to StudioTicker_Collection.Count-1 do
          begin
            TickerRecPtr := StudioTicker_Collection.At(i);
            if (PlaylistGrid.CurrentDataRow-1 = i) AND (SelectedRecordFound = FALSE) then
            begin
              //Set selected flag to true
              SelectedRecordFound := TRUE;
              //Allocate memory for the record
              GetMem (TempTickerRecPtr, SizeOf(TickerRec));
              //Copy the data to the temporary array
              With TempTickerRecPtr^ do
              begin
                Event_Index := TickerRecPtr^.Event_Index;
                Event_GUID := TickerRecPtr^.Event_GUID;
                Is_Child := TickerRecPtr^.Is_Child;
                Enabled := TickerRecPtr^.Enabled;
                League := TickerRecPtr^.League;
                Subleague_Mnemonic_Standard := TickerRecPtr^.Subleague_Mnemonic_Standard;
                Mnemonic_LiveEvent := TickerRecPtr^.Mnemonic_LiveEvent;
                Template_ID := TickerRecPtr^.Template_ID;
                Record_Type := TickerRecPtr^.Record_Type;
                SI_GCode := TickerRecPtr^.SI_GCode;
                ST_Game_ID := TickerRecPtr^.ST_Game_ID;
                NFLDM_GameID := TickerRecPtr^.NFLDM_GameID;
                CSS_GameID := TickerRecPtr^.CSS_GameID;
                Primary_Data_Source := TickerRecPtr^.Primary_Data_Source;
                Backup_Data_Source := TickerRecPtr^.Backup_Data_Source;
                SponsorLogo_Name := TickerRecPtr^.SponsorLogo_Name;
                SponsorLogo_Dwell := TickerRecPtr^.SponsorLogo_Dwell;
                SponsorLogo_Region := TickerRecPtr^.SponsorLogo_Region;
                StatStoredProcedure := TickerRecPtr^.StatStoredProcedure;
                StatType := TickerRecPtr^.StatType;
                StatTeam := TickerRecPtr^.StatTeam;
                StatLeague := TickerRecPtr^.StatLeague;
                StatRecordNumber := TickerRecPtr^.StatRecordNumber;
                for j := 1 to 25 do UserData[j] := TickerRecPtr^.UserData[j];
                StartEnableDateTime := TickerRecPtr^.StartEnableDateTime;
                EndEnableDateTime := TickerRecPtr^.EndEnableDateTime;
                Description := TickerRecPtr^.Description;
                Comments := TickerRecPtr^.Comments;
                Selected := TickerRecPtr^.Selected;
                DwellTime := TickerRecPtr^.DwellTime;
              end;
              //Delete the current record
              StudioTicker_Collection.AtDelete(i);
              StudioTicker_Collection.Pack;
              //Insert the record at the next position up in the grid
              if (i > 0) AND (Direction = UP) then
              begin
                StudioTicker_Collection.AtInsert(i-1, TempTickerRecPtr);
                Dec(SaveDataRow);
                if (SaveTopRow > 1) then Dec(SaveTopRow);
              end
              else if (i = 0) AND (Direction = UP) then
              begin
                StudioTicker_Collection.AtInsert(0, TempTickerRecPtr);
                SaveDataRow := 1;
                SaveTopRow := 1;
              end
              else if (i < PlaylistGrid.Rows-2) AND (Direction = DOWN) then
              begin
                StudioTicker_Collection.AtInsert(i+1, TempTickerRecPtr);
                Inc(SaveDataRow);
                if (SaveTopRow < PlaylistGrid.Rows) then Inc(SaveTopRow);
              end
              else if ((i = PlaylistGrid.Rows-2) OR (i = PlaylistGrid.Rows-1)) AND (Direction = DOWN) then
              begin
                StudioTicker_Collection.Insert(TempTickerRecPtr);
                SaveDataRow := PlaylistGrid.Rows;
              end;
            end;
          end;
        end;
      end;
      //Studio Bug
   1: begin
        if (StudioBug_Collection.Count > 0) then
        begin
          //Clear the temp collection
          for i := 0 to StudioBug_Collection.Count-1 do
          begin
            BugRecPtr := StudioBug_Collection.At(i);
            if (PlaylistGrid.CurrentDataRow-1 = i) AND (SelectedRecordFound = FALSE) then
            begin
              //Set selected flag to true
              SelectedRecordFound := TRUE;
              //Allocate memory for the record
              GetMem (TempBugRecPtr, SizeOf(BugRec));
              //Copy the data to the temporary array
              With TempBugRecPtr^ do
              begin
                Event_Index := BugRecPtr^.Event_Index;
                Event_GUID := BugRecPtr^.Event_GUID;
                Is_Child := BugRecPtr^.Is_Child;
                Enabled := BugRecPtr^.Enabled;
                League := BugRecPtr^.League;
                Template_ID := BugRecPtr^.Template_ID;
                Record_Type := BugRecPtr^.Record_Type;
                SI_GCode := BugRecPtr^.SI_GCode;
                ST_Game_ID := BugRecPtr^.ST_Game_ID;
                NFLDM_GameID := BugRecPtr^.NFLDM_GameID;
                CSS_GameID := BugRecPtr^.CSS_GameID;
                Primary_Data_Source := BugRecPtr^.Primary_Data_Source;
                Backup_Data_Source := BugRecPtr^.Backup_Data_Source;
                for j := 1 to 25 do UserData[j] := BugRecPtr^.UserData[j];
                StartEnableDateTime := BugRecPtr^.StartEnableDateTime;
                EndEnableDateTime := BugRecPtr^.EndEnableDateTime;
                Description := BugRecPtr^.Description;
                Comments := BugRecPtr^.Comments;
                Selected := BugRecPtr^.Selected;
                DwellTime := BugRecPtr^.DwellTime;
              end;
              //Delete the current record
              StudioBug_Collection.AtDelete(i);
              StudioBug_Collection.Pack;
              //Insert the record at the next position up in the grid
              if (i > 0) AND (Direction = UP) then
              begin
                StudioBug_Collection.AtInsert(i-1, TempBugRecPtr);
                Dec(SaveDataRow);
              end
              else if (i = 0) AND (Direction = UP) then
              begin
                StudioBug_Collection.AtInsert(0, TempBugRecPtr);
                SaveDataRow := 1;
              end
              else if (i < PlaylistGrid.Rows-2) AND (Direction = DOWN) then
              begin
                StudioBug_Collection.AtInsert(i+1, TempBugRecPtr);
                Inc(SaveDataRow);
              end
              else if ((i = PlaylistGrid.Rows-2) OR (i = PlaylistGrid.Rows-1)) AND (Direction = DOWN) then
              begin
                StudioBug_Collection.Insert(TempBugRecPtr);
                SaveDataRow := PlaylistGrid.Rows;
              end;
            end;
          end;
        end;
      end;
      //GameTrax
   2: begin
        if (GameTrax_Collection.Count > 0) then
        begin
          //Clear the temp collection
          for i := 0 to GameTrax_Collection.Count-1 do
          begin
            GameTraxRecPtr := GameTrax_Collection.At(i);
            if (PlaylistGrid.CurrentDataRow-1 = i) AND (SelectedRecordFound = FALSE) then
            begin
              //Set selected flag to true
              SelectedRecordFound := TRUE;
              //Allocate memory for the record
              GetMem (TempGameTraxRecPtr, SizeOf(GameTraxRec));
              //Copy the data to the temporary array
              With TempGameTraxRecPtr^ do
              begin
                Event_Index := GameTraxRecPtr^.Event_Index;
                Event_GUID := GameTraxRecPtr^.Event_GUID;
                Is_Child := GameTraxRecPtr^.Is_Child;
                Enabled := GameTraxRecPtr^.Enabled;
                League := GameTraxRecPtr^.League;
                Template_ID := GameTraxRecPtr^.Template_ID;
                Record_Type := GameTraxRecPtr^.Record_Type;
                SI_GCode := GametraxRecPtr^.SI_GCode;
                ST_Game_ID := GametraxRecPtr^.ST_Game_ID;
                NFLDM_GameID := GametraxRecPtr^.NFLDM_GameID;
                CSS_GameID := GametraxRecPtr^.CSS_GameID;
                SponsorLogo_Name := GametraxRecPtr^.SponsorLogo_Name;
                SponsorLogo_Dwell := GametraxRecPtr^.SponsorLogo_Dwell;
                SponsorLogo_Region := GametraxRecPtr^.SponsorLogo_Region;
                Primary_Data_Source := GametraxRecPtr^.Primary_Data_Source;
                Backup_Data_Source := GametraxRecPtr^.Backup_Data_Source;
                for j := 1 to 25 do UserData[j] := GameTraxRecPtr^.UserData[j];
                StartEnableDateTime := GameTraxRecPtr^.StartEnableDateTime;
                EndEnableDateTime := GameTraxRecPtr^.EndEnableDateTime;
                Description := GameTraxRecPtr^.Description;
                Comments := GameTraxRecPtr^.Comments;
                Selected := GameTraxRecPtr^.Selected;
                DwellTime := GameTraxRecPtr^.DwellTime;
              end;
              //Delete the current record
              GameTrax_Collection.AtDelete(i);
              GameTrax_Collection.Pack;
              //Insert the record at the next position up in the grid
              if (i > 0) AND (Direction = UP) then
              begin
                GameTrax_Collection.AtInsert(i-1, TempGameTraxRecPtr);
                Dec(SaveDataRow);
              end
              else if (i = 0) AND (Direction = UP) then
              begin
                GameTrax_Collection.AtInsert(0, TempGameTraxRecPtr);
                SaveDataRow := 1;
              end
              else if (i < PlaylistGrid.Rows-2) AND (Direction = DOWN) then
              begin
                GameTrax_Collection.AtInsert(i+1, TempGameTraxRecPtr);
                Inc(SaveDataRow);
              end
              else if ((i = PlaylistGrid.Rows-2) OR (i = PlaylistGrid.Rows-1)) AND (Direction = DOWN) then
              begin
                GameTrax_Collection.Insert(TempGameTraxRecPtr);
                SaveDataRow := PlaylistGrid.Rows;
              end;
            end;
          end;
        end;
      end;
  end;
  //Refresh the grid
  RefreshPlaylistGrid;
  //Restore cursor & selected record
  Screen.Cursor := Save_Cursor;
  PlaylistGrid.CurrentDataRow := SaveDataRow;
  PlaylistGrid.SelectRows(1, PlaylistGrid.Rows, FALSE);
  PlaylistGrid.SelectRows(PlaylistGrid.CurrentDataRow, PlaylistGrid.CurrentDataRow, TRUE);
  PlaylistGrid.TopRow := SaveTopRow;
end;

////////////////////////////////////////////////////////////////////////////////

//Handler for paste selected record(s) at specified record location (not last)
procedure TMainForm.Paste1Click(Sender: TObject);
begin
  Paste;
end;
procedure TMainForm.Paste;
begin
  //Call paste entries procedure WITHOUT append and one copy
  PasteEntries(FALSE);
end;
//Handler for paste selected record(s) at end of playlist
procedure TMainForm.AppendtoEnd1Click(Sender: TObject);
begin
  PasteEnd;
end;
procedure TMainForm.PasteEnd;
begin
  //Call paste entries procedure WITH append and one copy
  PasteEntries(TRUE);
end;
//Handler for duplicate selected record at specified record location (not last)
procedure TMainForm.Duplicate1Click(Sender: TObject);
begin
  Duplicate;
end;
procedure TMainForm.Duplicate;
var
  DuplicationCount: SmallInt;
  Modal: TDuplicationCountSelectDlg;
  Control: Word;
begin
  try
    Modal := TDuplicationCountSelectDlg.Create(Application);
    Modal.DuplicationCount.Value := LastDuplicationCount;
    Control := Modal.ShowModal;
  finally
    if (Control = mrOK) then
    begin
      DuplicationCount := Modal.DuplicationCount.Value;
      LastDuplicationCount := Modal.DuplicationCount.Value;
      //Call paste entries procedure WITHOUT append and required number of copies
      DuplicateEntries(FALSE, DuplicationCount);
    end;
    Modal.Free;
  end;
end;
//Handler for duplicate selected record at end of playlist
procedure TMainForm.DuplicateandAppendtoEnd1Click(Sender: TObject);
var
  DuplicationCount: SmallInt;
  Modal: TDuplicationCountSelectDlg;
  Control: Word;
begin
  try
    Modal := TDuplicationCountSelectDlg.Create(Application);
    Modal.DuplicationCount.Value := LastDuplicationCount;
    Control := Modal.ShowModal;
  finally
    if (Control = mrOK) then
    begin
      DuplicationCount := Modal.DuplicationCount.Value;
      LastDuplicationCount := Modal.DuplicationCount.Value;
      //Call paste entries procedure WITH append and required number of copies
      DuplicateEntries(TRUE, DuplicationCount);
    end;
    Modal.Free;
  end;
end;

//General procedure for pasting selected records
procedure TMainForm.PasteEntries(AppendToEnd: Boolean);
var
  i,j,k, m: SmallInt;
  InsertPoint: SmallInt;
  ExistingTickerRecPtr, TickerRecPtr, NewTickerRecPtr: ^TickerRec;
  ExistingBugRecPtr, BugRecPtr, NewBugRecPtr: ^BugRec;
  ExistingGameTraxRecPtr, GameTraxRecPtr, NewGameTraxRecPtr: ^GameTraxRec;
  EventGUID: TGUID;
  UpdateOnlyMode: Boolean;
  Control: Word;
  CurrentEventGUID: TGUID;
  Save_Cursor: TCursor;
  ArrayIndex: SmallInt;
begin
  //Change cursor
  Save_Cursor := Screen.Cursor;
  //Show hourglass cursor
  Screen.Cursor := crHourGlass;
  //Init
  UpdateOnlyMode := FALSE;

  //Prompt for data update only mode
  Control := MessageDlg('Do you wish to OVERWRITE the data for matching records (' +
      'Selecting NO will paste new records)?', mtWarning, [mbYes, mbNo], 0);
  if (Control = mrYes) then UpDateOnlyMode := TRUE;

  Case PlaylistSelectTabControl.TabIndex of
      //Studio Ticker
   0: begin
        //Set the paste/insertion point
        InsertPoint := PlaylistGrid.CurrentDataRow-1;
        if (InsertPoint < 0) then InsertPoint := 0;
        //Iterate through temporary collection starting at last element and working to
        //top to keep in correct order
        if (TemporaryTickerDataCount > 0) then
        begin
          for i := 0 to TemporaryTickerDataCount-1 do
          begin
            if (AppendToEnd) then
              ArrayIndex := i+1
            else
              ArrayIndex := TemporaryTickerDataCount-i;
            CurrentEventGUID := TemporaryTickerDataArray[ArrayIndex].Event_GUID;
            //Edit all matching objects using comparsion of GUIDs
            for j := 0 to StudioTicker_Collection.Count-1 do
            begin
              ExistingTickerRecPtr := StudioTicker_Collection.At(j);
              if (IsEqualGUID(ExistingTickerRecPtr^.Event_GUID, CurrentEventGUID)) then
              begin
                With ExistingTickerRecPtr^ do
                begin
                  //Set values for collection record
                  for m := 1 to 25 do ExistingTickerRecPtr^.UserData[m] := TemporaryTickerDataArray[ArrayIndex].UserData[m];
                  ExistingTickerRecPtr^.StartEnableDateTime := TemporaryTickerDataArray[ArrayIndex].StartEnableDateTime;
                  ExistingTickerRecPtr^.EndEnableDateTime := TemporaryTickerDataArray[ArrayIndex].EndEnableDateTime;
                  ExistingTickerRecPtr^.Enabled := TemporaryTickerDataArray[ArrayIndex].Enabled;
                  ExistingTickerRecPtr^.Description := TemporaryTickerDataArray[ArrayIndex].Description;
                  ExistingTickerRecPtr^.Comments := TemporaryTickerDataArray[ArrayIndex].Comments;
                  ExistingTickerRecPtr^.DwellTime := TemporaryTickerDataArray[ArrayIndex].DwellTime;
                end;
              end;
            end;
            //Append the new record if not data update only mode
            if (UpdateOnlyMode = FALSE) then
            begin
              GetMem (NewTickerRecPtr, SizeOf(TickerRec));
              With NewTickerRecPtr^ do
              begin
                Event_Index := TemporaryTickerDataArray[ArrayIndex].Event_Index;
                Event_GUID := TemporaryTickerDataArray[ArrayIndex].Event_GUID;
                Is_Child := TemporaryTickerDataArray[ArrayIndex].Is_Child;
                Enabled := TemporaryTickerDataArray[ArrayIndex].Enabled;
                League := TemporaryTickerDataArray[ArrayIndex].League;
                Subleague_Mnemonic_Standard := TemporaryTickerDataArray[ArrayIndex].Subleague_Mnemonic_Standard;
                Mnemonic_LiveEvent := TemporaryTickerDataArray[ArrayIndex].Mnemonic_LiveEvent;
                Template_ID := TemporaryTickerDataArray[ArrayIndex].Template_ID;
                Record_Type := TemporaryTickerDataArray[ArrayIndex].Record_Type;
                SI_GCode := TemporaryTickerDataArray[ArrayIndex].SI_GCode;
                ST_Game_ID := TemporaryTickerDataArray[ArrayIndex].ST_Game_ID;
                NFLDM_GameID := TemporaryTickerDataArray[ArrayIndex].NFLDM_GameID;
                CSS_GameID := TemporaryTickerDataArray[ArrayIndex].CSS_GameID;
                Primary_Data_Source := TemporaryTickerDataArray[ArrayIndex].Primary_Data_Source;
                Backup_Data_Source := TemporaryTickerDataArray[ArrayIndex].Backup_Data_Source;
                SponsorLogo_Name := TemporaryTickerDataArray[ArrayIndex].SponsorLogo_Name;
                SponsorLogo_Dwell := TemporaryTickerDataArray[ArrayIndex].SponsorLogo_Dwell;
                SponsorLogo_Region := TemporaryTickerDataArray[ArrayIndex].SponsorLogo_Region;
                StatStoredProcedure := TemporaryTickerDataArray[ArrayIndex].StatStoredProcedure;
                StatType := TemporaryTickerDataArray[ArrayIndex].StatType;
                StatTeam := TemporaryTickerDataArray[ArrayIndex].StatTeam;
                StatLeague := TemporaryTickerDataArray[ArrayIndex].StatLeague;
                StatRecordNumber := TemporaryTickerDataArray[ArrayIndex].StatRecordNumber;
                for j := 1 to 25 do UserData[j] := TemporaryTickerDataArray[ArrayIndex].UserData[j];
                StartEnableDateTime := TemporaryTickerDataArray[ArrayIndex].StartEnableDateTime;
                EndEnableDateTime := TemporaryTickerDataArray[ArrayIndex].EndEnableDateTime;
                Description := TemporaryTickerDataArray[ArrayIndex].Description;
                Comments := TemporaryTickerDataArray[ArrayIndex].Comments;
                Selected := TemporaryTickerDataArray[ArrayIndex].Selected;
                DwellTime := TemporaryTickerDataArray[ArrayIndex].DwellTime;
              end;
              //Insert the new record into the collection
              if (AppendToEnd) then
                StudioTicker_Collection.Insert(NewTickerRecPtr)
              else
                StudioTicker_Collection.AtInsert(InsertPoint, NewTickerRecPtr);
            end;
          end;
        end;
        RefreshPlaylistGrid;
      end;
      //Studio Bug
   1: begin
        //Set the paste/insertion point
        InsertPoint := PlaylistGrid.CurrentDataRow-1;
        if (InsertPoint < 0) then InsertPoint := 0;
        //Iterate through temporary collection starting at last element and working to
        //top to keep in correct order
        if (TemporaryBugDataCount > 0) then
        begin
          for i := 0 to TemporaryBugDataCount-1 do
          begin
            if (AppendToEnd) then
              ArrayIndex := i+1
            else
              ArrayIndex := TemporaryBugDataCount-i;
            //Append the new record
            GetMem (NewBugRecPtr, SizeOf(BugRec));
            With NewBugRecPtr^ do
            begin
              Event_Index := TemporaryBugDataArray[ArrayIndex].Event_Index;
              Event_GUID := TemporaryBugDataArray[ArrayIndex].Event_GUID;
              Is_Child := TemporaryBugDataArray[ArrayIndex].Is_Child;
              Enabled := TemporaryBugDataArray[ArrayIndex].Enabled;
              League := TemporaryBugDataArray[ArrayIndex].League;
              Template_ID := TemporaryBugDataArray[ArrayIndex].Template_ID;
              Record_Type := TemporaryBugDataArray[ArrayIndex].Record_Type;
              SI_GCode := TemporaryBugDataArray[ArrayIndex].SI_GCode;
              ST_Game_ID := TemporaryBugDataArray[ArrayIndex].ST_Game_ID;
              NFLDM_GameID := TemporaryBugDataArray[ArrayIndex].NFLDM_GameID;
              CSS_GameID := TemporaryBugDataArray[ArrayIndex].CSS_GameID;
              Primary_Data_Source := TemporaryBugDataArray[ArrayIndex].Primary_Data_Source;
              Backup_Data_Source := TemporaryBugDataArray[ArrayIndex].Backup_Data_Source;
              for j := 1 to 25 do UserData[j] := TemporaryBugDataArray[ArrayIndex].UserData[j];
              StartEnableDateTime := TemporaryBugDataArray[ArrayIndex].StartEnableDateTime;
              EndEnableDateTime := TemporaryBugDataArray[ArrayIndex].EndEnableDateTime;
              Description := TemporaryBugDataArray[ArrayIndex].Description;
              Comments := TemporaryBugDataArray[ArrayIndex].Comments;
              Selected := TemporaryBugDataArray[ArrayIndex].Selected;
              DwellTime := TemporaryBugDataArray[ArrayIndex].DwellTime;
            end;
            //Insert the new record into the collection
            if (AppendToEnd) then
              StudioBug_Collection.Insert(NewBugRecPtr)
            else
              StudioBug_Collection.AtInsert(InsertPoint, NewBugRecPtr)
          end;
        end;
        RefreshPlaylistGrid;
      end;
      //Gsmetrax
   2: begin
        //Set the paste/insertion point
        InsertPoint := PlaylistGrid.CurrentDataRow-1;
        if (InsertPoint < 0) then InsertPoint := 0;
        //Iterate through temporary collection starting at last element and working to
        //top to keep in correct order
        if (TemporaryGameTraxDataCount > 0) then
        begin
          for i := 0 to TemporaryGameTraxDataCount-1 do
          begin
            if (AppendToEnd) then
              ArrayIndex := i+1
            else
              ArrayIndex := TemporaryGameTraxDataCount-i;
            CurrentEventGUID := TemporaryGameTraxDataArray[ArrayIndex].Event_GUID;

            //Edit all matching objects using comparsion of GUIDs
            for j := 0 to GameTrax_Collection.Count-1 do
            begin
              ExistingGameTraxRecPtr := GameTrax_Collection.At(j);
              if (IsEqualGUID(ExistingGameTraxRecPtr^.Event_GUID, CurrentEventGUID)) then
              begin
                With ExistingGameTraxRecPtr^ do
                begin
                  //Set values for collection record
                  for m := 1 to 25 do ExistingGameTraxRecPtr^.UserData[m] := TemporaryGameTraxDataArray[ArrayIndex].UserData[m];
                  ExistingGameTraxRecPtr^.StartEnableDateTime := TemporaryGameTraxDataArray[ArrayIndex].StartEnableDateTime;
                  ExistingGameTraxRecPtr^.EndEnableDateTime := TemporaryGameTraxDataArray[ArrayIndex].EndEnableDateTime;
                  ExistingGameTraxRecPtr^.Enabled := TemporaryGameTraxDataArray[ArrayIndex].Enabled;
                  ExistingGameTraxRecPtr^.Description := TemporaryGameTraxDataArray[ArrayIndex].Description;
                  ExistingGameTraxRecPtr^.Comments := TemporaryGameTraxDataArray[ArrayIndex].Comments;
                  ExistingGameTraxRecPtr^.DwellTime := TemporaryGameTraxDataArray[ArrayIndex].DwellTime;
                end;
              end;
            end;

            //Append the new record if not data update only mode
            if (UpdateOnlyMode = FALSE) then
            begin
              GetMem (NewGameTraxRecPtr, SizeOf(GameTraxRec));
              With NewGameTraxRecPtr^ do
              begin
                Event_Index := TemporaryGameTraxDataArray[ArrayIndex].Event_Index;
                Event_GUID := TemporaryGameTraxDataArray[ArrayIndex].Event_GUID;
                Is_Child := TemporaryGameTraxDataArray[ArrayIndex].Is_Child;
                Enabled := TemporaryGameTraxDataArray[ArrayIndex].Enabled;
                League := TemporaryGameTraxDataArray[ArrayIndex].League;
                Template_ID := TemporaryGameTraxDataArray[ArrayIndex].Template_ID;
                Record_Type := TemporaryGameTraxDataArray[ArrayIndex].Record_Type;
                SI_GCode := TemporaryGametraxDataArray[ArrayIndex].SI_GCode;
                ST_Game_ID := TemporaryGametraxDataArray[ArrayIndex].ST_Game_ID;
                NFLDM_GameID := TemporaryGametraxDataArray[ArrayIndex].NFLDM_GameID;
                CSS_GameID := TemporaryGametraxDataArray[ArrayIndex].CSS_GameID;
                Primary_Data_Source := TemporaryGametraxDataArray[ArrayIndex].Primary_Data_Source;
                Backup_Data_Source := TemporaryGametraxDataArray[ArrayIndex].Backup_Data_Source;
                SponsorLogo_Name := TemporaryGametraxDataArray[ArrayIndex].SponsorLogo_Name;
                SponsorLogo_Dwell := TemporaryGametraxDataArray[ArrayIndex].SponsorLogo_Dwell;
                SponsorLogo_Region := TemporaryGametraxDataArray[ArrayIndex].SponsorLogo_Region;
                for j := 1 to 25 do UserData[j] := TemporaryGameTraxDataArray[ArrayIndex].UserData[j];
                StartEnableDateTime := TemporaryGameTraxDataArray[ArrayIndex].StartEnableDateTime;
                EndEnableDateTime := TemporaryGameTraxDataArray[ArrayIndex].EndEnableDateTime;
                Description := TemporaryGameTraxDataArray[ArrayIndex].Description;
                Comments := TemporaryGameTraxDataArray[ArrayIndex].Comments;
                Selected := TemporaryGameTraxDataArray[ArrayIndex].Selected;
                DwellTime := TemporaryGameTraxDataArray[ArrayIndex].DwellTime;
              end;
              //Insert the new record into the collection
              if (AppendToEnd) then
                GameTrax_Collection.Insert(NewGameTraxRecPtr)
              else
                GameTrax_Collection.AtInsert(InsertPoint, NewGameTraxRecPtr);
            end;
          end;
        end;
        RefreshPlaylistGrid;
      end;
  end;
  //Restore cursor
  Screen.Cursor := Save_Cursor;
end;

//General procedure for pasting selected records
procedure TMainForm.DuplicateEntries(AppendToEnd: Boolean; DuplicationCount: SmallInt);
var
  i,j,k, m: SmallInt;
  InsertPoint: SmallInt;
  ExistingTickerRecPtr, TickerRecPtr, NewTickerRecPtr: ^TickerRec;
  ExistingBugRecPtr, BugRecPtr, NewBugRecPtr: ^BugRec;
  ExistingGameTraxRecPtr, GameTraxRecPtr, NewGameTraxRecPtr: ^GameTraxRec;
  EventGUID: TGUID;
  UpdateOnlyMode: Boolean;
  Control: Word;
  CurrentEventGUID: TGUID;
  Save_Cursor: TCursor;
  ArrayIndex: SmallInt;
  SaveCurrentDataRow, SaveTopRow: SmallInt;
begin
  //Change cursor
  Save_Cursor := Screen.Cursor;
  //Show hourglass cursor
  Screen.Cursor := crHourGlass;
  //Init
  UpdateOnlyMode := FALSE;
  //Save grid position
  SaveCurrentDataRow := PlaylistGrid.CurrentDataRow;
  SaveTopRow := PlaylistGrid.TopRow;

  Case PlaylistSelectTabControl.TabIndex of
      //Studio Ticker
   0: begin
        //Set the paste/insertion point
        InsertPoint := PlaylistGrid.CurrentDataRow-1;
        if (InsertPoint < 0) then InsertPoint := 0;
        //Iterate through temporary collection starting at last element and working to
        //top to keep in correct order
        TickerRecPtr := StudioTicker_Collection.At(InsertPoint);
        for k := 1 to DuplicationCount do
        begin
          GetMem (NewTickerRecPtr, SizeOf(TickerRec));
          With NewTickerRecPtr^ do
          begin
            Event_Index := TickerRecPtr.Event_Index;
            CreateGUID(EventGUID);
            Event_GUID := EventGUID;
            Is_Child := TickerRecPtr.Is_Child;
            Enabled := TickerRecPtr.Enabled;
            League := TickerRecPtr.League;
            Subleague_Mnemonic_Standard := TickerRecPtr.Subleague_Mnemonic_Standard;
            Mnemonic_LiveEvent := TickerRecPtr.Mnemonic_LiveEvent;
            Template_ID := TickerRecPtr.Template_ID;
            Record_Type := TickerRecPtr.Record_Type;
            SI_GCode := TickerRecPtr^.SI_GCode;
            ST_Game_ID := TickerRecPtr^.ST_Game_ID;
            NFLDM_GameID := TickerRecPtr^.NFLDM_GameID;
            CSS_GameID := TickerRecPtr^.CSS_GameID;
            Primary_Data_Source := TickerRecPtr^.Primary_Data_Source;
            Backup_Data_Source := TickerRecPtr^.Backup_Data_Source;
            SponsorLogo_Name := TickerRecPtr.SponsorLogo_Name;
            SponsorLogo_Dwell := TickerRecPtr.SponsorLogo_Dwell;
            SponsorLogo_Region := TickerRecPtr.SponsorLogo_Region;
            StatStoredProcedure := TickerRecPtr.StatStoredProcedure;
            StatType := TickerRecPtr.StatType;
            StatTeam := TickerRecPtr.StatTeam;
            StatLeague := TickerRecPtr.StatLeague;
            StatRecordNumber := TickerRecPtr.StatRecordNumber;
            for j := 1 to 25 do UserData[j] := TickerRecPtr.UserData[j];
            StartEnableDateTime := TickerRecPtr.StartEnableDateTime;
            EndEnableDateTime := TickerRecPtr.EndEnableDateTime;
            Description := TickerRecPtr^.Description;
            Comments := TickerRecPtr.Comments;
            Selected := TickerRecPtr.Selected;
            DwellTime := TickerRecPtr.DwellTime;
          end;
          //Insert the new record into the collection
          if (AppendToEnd) then
            StudioTicker_Collection.Insert(NewTickerRecPtr)
          else
            StudioTicker_Collection.AtInsert(InsertPoint, NewTickerRecPtr);
        end;
        RefreshPlaylistGrid;
      end;
      //Studio Bug
   1: begin
        //Set the paste/insertion point
        InsertPoint := PlaylistGrid.CurrentDataRow-1;
        if (InsertPoint < 0) then InsertPoint := 0;
        //Iterate through temporary collection starting at last element and working to
        //top to keep in correct order
        BugRecPtr := StudioBug_Collection.At(InsertPoint);
        for k := 1 to DuplicationCount do
        begin
          GetMem (NewBugRecPtr, SizeOf(BugRec));
          With NewBugRecPtr^ do
          begin
            Event_Index := BugRecPtr.Event_Index;
            CreateGUID(EventGUID);
            Event_GUID := EventGUID;
            Is_Child := BugRecPtr.Is_Child;
            Enabled := BugRecPtr.Enabled;
            League := BugRecPtr.League;
            Template_ID := BugRecPtr.Template_ID;
            Record_Type := BugRecPtr.Record_Type;
            SI_GCode := BugRecPtr^.SI_GCode;
            ST_Game_ID := BugRecPtr^.ST_Game_ID;
            NFLDM_GameID := BugRecPtr^.NFLDM_GameID;
            CSS_GameID := BugRecPtr^.CSS_GameID;
            Primary_Data_Source := BugRecPtr^.Primary_Data_Source;
            Backup_Data_Source := BugRecPtr^.Backup_Data_Source;
            for j := 1 to 25 do UserData[j] := BugRecPtr.UserData[j];
            StartEnableDateTime := BugRecPtr.StartEnableDateTime;
            EndEnableDateTime := BugRecPtr.EndEnableDateTime;
            Description := BugRecPtr.Description;
            Comments := BugRecPtr.Comments;
            Selected := BugRecPtr.Selected;
            DwellTime := BugRecPtr.DwellTime;
          end;
          //Insert the new record into the collection
          if (AppendToEnd) then
            StudioBug_Collection.Insert(NewBugRecPtr)
          else
            StudioBug_Collection.AtInsert(InsertPoint, NewBugRecPtr);
        end;
        RefreshPlaylistGrid;
      end;
      //GameTrax
   2: begin
        //Set the paste/insertion point
        InsertPoint := PlaylistGrid.CurrentDataRow-1;
        if (InsertPoint < 0) then InsertPoint := 0;
        //Iterate through temporary collection starting at last element and working to
        //top to keep in correct order
        GameTraxRecPtr := GameTrax_Collection.At(InsertPoint);
        for k := 1 to DuplicationCount do
        begin
          GetMem (NewGameTraxRecPtr, SizeOf(GameTraxRec));
          With NewGameTraxRecPtr^ do
          begin
            Event_Index := GameTraxRecPtr.Event_Index;
            CreateGUID(EventGUID);
            Event_GUID := EventGUID;
            Enabled := GameTraxRecPtr.Enabled;
            Is_Child := GameTraxRecPtr.Is_Child;
            League := GameTraxRecPtr.League;
            Template_ID := GameTraxRecPtr.Template_ID;
            Record_Type := GameTraxRecPtr.Record_Type;
            SI_GCode := GametraxRecPtr^.SI_GCode;
            ST_Game_ID := GametraxRecPtr^.ST_Game_ID;
            NFLDM_GameID := GametraxRecPtr^.NFLDM_GameID;
            CSS_GameID := GametraxRecPtr^.CSS_GameID;
            Primary_Data_Source := GametraxRecPtr^.Primary_Data_Source;
            Backup_Data_Source := GametraxRecPtr^.Backup_Data_Source;
            SponsorLogo_Name := GametraxRecPtr^.SponsorLogo_Name;
            SponsorLogo_Dwell := GametraxRecPtr^.SponsorLogo_Dwell;
            SponsorLogo_Region := GametraxRecPtr^.SponsorLogo_Region;
            for j := 1 to 25 do UserData[j] := GameTraxRecPtr.UserData[j];
            StartEnableDateTime := GameTraxRecPtr.StartEnableDateTime;
            EndEnableDateTime := GameTraxRecPtr.EndEnableDateTime;
            Description := GameTraxRecPtr.Description;
            Comments := GameTraxRecPtr.Comments;
            Selected := GameTraxRecPtr.Selected;
            DwellTime := GameTraxRecPtr.DwellTime;
          end;
          //Insert the new record into the collection
          if (AppendToEnd) then
            GameTrax_Collection.Insert(NewGameTraxRecPtr)
          else
            GameTrax_Collection.AtInsert(InsertPoint, NewGameTraxRecPtr);
        end;
        RefreshPlaylistGrid;
      end;
  end;
  //Set current row to bottom duplication row
  PlaylistGrid.CurrentDataRow := SaveCurrentDataRow + DuplicationCount;
  PlaylistGrid.TopRow := SaveTopRow + DuplicationCount;
  PlaylistGrid.RowSelected[SaveCurrentDataRow] := FALSE;
  PlaylistGrid.RowSelected[SaveCurrentDataRow+DuplicationCount] := TRUE;
  //Restore cursor
  Screen.Cursor := Save_Cursor;
end;

//Handler for delete from pop-up menu
procedure TMainForm.Delete1Click(Sender: TObject);
begin
  DeleteGraphicFromZipperPlaylist(TRUE);
end;
//General procedure to delete an element from the zipper playlist
procedure TMainForm.DeleteGraphicFromZipperPlaylist(Confirm: Boolean);
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  BugRecPtr: ^BugRec;
  GameTraxRecPtr: ^GameTraxRec;
  CollectionCount: SmallInt;
  Control: Word;
begin
  //Alert operator
  if (Confirm) then
    Control := MessageDlg('Are you sure you want to delete the selected records?', mtConfirmation, [mbYes, mbNo], 0)
  else
    Control := mrYes;
  if (Control = mrYes) then
  begin
    Case PlaylistSelectTabControl.TabIndex of
         //Studio Ticker
      0: CollectionCount := StudioTicker_Collection.Count;
         //Studio Bug
      1: CollectionCount := StudioBug_Collection.Count;
         //GameTrax
      2: CollectionCount := GameTrax_Collection.Count;
    end;
    If ((CollectionCount > 0) AND (PlaylistGrid.Rows >= 0)) then
    begin
      //Point to current record & get game id
      //Delete the item from the stack
      Case PlaylistSelectTabControl.TabIndex of
          //Studio Ticker
       0: begin
            for i := 0 to StudioTicker_Collection.Count-1 do
            begin
              TickerRecPtr := StudioTicker_Collection.At(i);
              if (PlaylistGrid.RowSelected[i+1] = TRUE) then TickerRecPtr^.Selected := TRUE
              else TickerRecPtr^.Selected := FALSE;
            end;
            i := 0;
            Repeat
              TickerRecPtr := StudioTicker_Collection.At(i);
              if (TickerRecPtr^.Selected = TRUE) then
              begin
                StudioTicker_Collection.AtDelete(i);
                StudioTicker_Collection.Pack;
              end
              else Inc(i);
            Until (i = StudioTicker_Collection.Count);
          end;
          //Studio Bug
       1: begin
            for i := 0 to StudioBug_Collection.Count-1 do
            begin
              BugRecPtr := StudioBug_Collection.At(i);
              if (PlaylistGrid.RowSelected[i+1] = TRUE) then BugRecPtr^.Selected := TRUE
              else BugRecPtr^.Selected := FALSE;
            end;
            i := 0;
            Repeat
              BugRecPtr := StudioBug_Collection.At(i);
              if (BugRecPtr^.Selected = TRUE) then
              begin
                StudioBug_Collection.AtDelete(i);
                StudioBug_Collection.Pack;
              end
              else Inc(i);
            Until (i = StudioBug_Collection.Count);
          end;
          //GameTrax
       2: begin
            for i := 0 to GameTrax_Collection.Count-1 do
            begin
              GameTraxRecPtr := GameTrax_Collection.At(i);
              if (PlaylistGrid.RowSelected[i+1] = TRUE) then GameTraxRecPtr^.Selected := TRUE
              else GameTraxRecPtr^.Selected := FALSE;
            end;
            i := 0;
            Repeat
              GameTraxRecPtr := GameTrax_Collection.At(i);
              if (GameTraxRecPtr^.Selected = TRUE) then
              begin
                GameTrax_Collection.AtDelete(i);
                GameTrax_Collection.Pack;
              end
              else Inc(i);
            Until (i = GameTrax_Collection.Count);
          end;
      end;
      //Refresh the grid
      RefreshPlaylistGrid;
    end;
  end;
end;

//Handler for double-click on zipper entry in grid - edit entry
procedure TMainForm.PlaylistGridDblClick(Sender: TObject);
begin
  EditZipperPlaylistEntry;
  //Preview the edited record
  PreviewCurrentPlaylistRecord;
end;

//Procedure to launch dialog for editing currently selected zipper entry
procedure TMainForm.EditZipperPlaylistEntry;
var
  i,j,k: SmallInt;
  CurrentRow: SmallInt;
  Modal: TZipperEntryEditorDlg;
  Control: Word;
  TickerRecPtr: ^TickerRec;
  BugRecPtr: ^BugRec;
  GameTraxRecPtr: ^GameTraxRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  User_Data: Array[1..25] of String[255];
  CurrentTemplateID: SmallInt;
  NumTemplateFields: SmallInt;
  CollectionCount: SmallInt;
  CurrentEventGUID: TGUID;
  CurrentGameData: GameRec;
  StyleChipRecPtr: ^StyleChipRec;
  CurrentTemplateDefs: TemplateDefsRec;
  SaveTopRow: SmallInt;
begin
  Case PlaylistSelectTabControl.TabIndex of
       //Studio Ticker
    0: CollectionCount := StudioTicker_Collection.Count;
       //Studio Bug
    1: CollectionCount := StudioBug_Collection.Count;
       //GameTrax
    2: CollectionCount := GameTrax_Collection.Count;
  end;

  //Init fields
  for i := 1 to 25 do User_Data[i] := '';

  If ((CollectionCount > 0) AND (PlaylistGrid.Rows >= 0)) then
  begin
    //Get current row
    CurrentRow := PlaylistGrid.CurrentDataRow;
    SaveTopRow := PlaylistGrid.TopRow;
    //Only proceed if a row was selected
    if (CurrentRow <> -1) then
    begin
      Case PlaylistSelectTabControl.TabIndex of
          //Studio Ticker
       0: begin
            TickerRecPtr := StudioTicker_Collection.At(CurrentRow-1);
            CurrentEventGUID := TickerRecPtr^.Event_GUID;
            //Setup and launch editing dialog if applicable
            try
              Modal := TZipperEntryEditorDlg.Create(Application);
              //Add style chips
              if (StyleChip_Collection.Count > 0) then
              begin
                Modal.StyleChipsGrid.StoreData := TRUE;
                Modal.StyleChipsGrid.Cols := 2;
                Modal.StyleChipsGrid.Rows := StyleChip_Collection.Count;
                for i := 0 to StyleChip_Collection.Count-1 do
                begin
                  StyleChipRecPtr := StyleChip_Collection.At(i);
                  Modal.StyleChipsGrid.Cell[1,i+1] := StyleChipRecPtr^.StyleChip_Code;
                  Modal.StyleChipsGrid.Cell[2,i+1] := StyleChipRecPtr^.StyleChip_Description;
                end;
              end;
              Modal.EntryEnable.Checked := TickerRecPtr^.Enabled;
              Modal.EntryNote.Text := TickerRecPtr^.Comments;
              Modal.EntryStartEnableDate.Date := TickerRecPtr^.StartEnableDateTime;
              Modal.EntryStartEnableTime.Time := TickerRecPtr^.StartEnableDateTime;
              Modal.EntryEndEnableDate.Date := TickerRecPtr^.EndEnableDateTime;
              Modal.EntryEndEnableTime.Time := TickerRecPtr^.EndEnableDateTime;
              //If a sponsor dwell was specified, it's a sponsor logo
              if (TickerRecPtr^.SponsorLogo_Dwell > 0) then
              begin
                Modal.EntryDwellTime.Value := Trunc(TickerRecPtr^.SponsorLogo_Dwell);
                Modal.SponsorLogoRegionLabel.Visible := TRUE;
                Modal.SponsorLogoRegionLabel2.Visible := TRUE;
                Modal.SponsorLogoRegion.Visible := TRUE;
                Modal.SponsorLogoRegion.Value := TickerRecPtr^.SponsorLogo_Region;
              end
              else begin
                Modal.EntryDwellTime.Value := Trunc(TickerRecPtr^.DwellTime/1000);
                Modal.SponsorLogoRegionLabel.Visible := FALSE;
                Modal.SponsorLogoRegionLabel2.Visible := FALSE;
                Modal.SponsorLogoRegion.Visible := FALSE;
              end;
              Modal.BitBtn4.Caption := 'Replace Existing Entry';
              //Set data mode label
              if (CurrentTickerDisplayMode = -1) then Modal.DataModeLabel.Caption := 'STANDARD MODE'
              else if (CurrentTickerDisplayMode = 1) then Modal.DataModeLabel.Caption := 'LIVE EVENT MODE';
              //Set initial values for grid
              CurrentTemplateID := TickerRecPtr^.Template_ID;
              NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);
              CurrentTemplateDefs := GetTemplateInformation(CurrentTemplateID);
              Modal.TemplateName.Caption := CurrentTemplateDefs.Template_Description;
              if (NumTemplateFields > 0) then
              begin
                //Clear grid values
                if (Modal.RecordGrid.Rows > 0) then
                  Modal.RecordGrid.DeleteRows (1, Modal.RecordGrid.Rows);
                Modal.RecordGrid.StoreData := TRUE;
                Modal.RecordGrid.Cols := 4;
                Modal.RecordGrid.Rows := NumTemplateFields;
                Modal.HasWeather := FALSE;
                Modal.HasConferenceChips := FALSE;
                Modal.HasManualLeague := FALSE;
                //Populate the grid
                i := 1; k := 1;
                //Check if manual league; if not, get the game data
                if (CurrentTemplateDefs.ManualLeague = FALSE) then
                begin
                  CurrentGameData := EngineInterface.GetGameData(TickerRecPtr^.League, TickerRecPtr^.SI_GCode,
                    TickerRecPtr^.ST_Game_ID, TickerRecPtr^.CSS_GameID, TickerRecPtr^.NFLDM_GameID,
                    TickerRecPtr^.Primary_Data_Source, TickerRecPtr^.Backup_Data_Source);
                end;
                for j := 0 to Template_Fields_Collection.Count-1 do
                begin
                  TemplateFieldsRecPtr := Template_Fields_Collection.At(j);
                  if (TemplateFieldsRecPtr^.Template_ID = CurrentTemplateID) then
                  begin
                    Modal.RecordGrid.Cell[1,i] := TemplateFieldsRecPtr^.Field_ID; //Index
                    Modal.RecordGrid.Cell[2,i] := TemplateFieldsRecPtr^.Field_Label; //Description
                    Modal.RecordGrid.Cell[4,i] := TemplateFieldsRecPtr^.Field_Type;
                    //Set color & read only property
                    Modal.RecordGrid.CellReadOnly[1,i] := roOn;
                    Modal.RecordGrid.CellReadOnly[2,i] := roOn;
                    Modal.RecordGrid.CellReadOnly[4,i] := roOn;
                    if (TemplateFieldsRecPtr^.Field_Is_Manual = TRUE) then
                    begin
                      //Firt check to see if it's a manual game winner indicator or animation
                      if (TemplateFieldsRecPtr^.Field_Type = 5) then
                      begin
                        Modal.RecordGrid.CellControlType[3,i] := ctCheck;
                        if (TickerRecPtr^.UserData[24] = 'TRUE') then
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbChecked
                        else
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbUnchecked;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      else if (TemplateFieldsRecPtr^.Field_Type = 6) then
                      begin
                        Modal.RecordGrid.CellControlType[3,i] := ctCheck;
                        if (TickerRecPtr^.UserData[25] = 'TRUE') then
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbChecked
                        else
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbUnchecked;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      //Next, check to see if it's a manual next page icon animation
                      else if (TemplateFieldsRecPtr^.Field_Type = 9) then
                      begin
                        Modal.RecordGrid.CellControlType[3,i] := ctCheck;
                        if (TickerRecPtr^.UserData[23] = 'TRUE') then
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbChecked
                        else
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbUnchecked;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      //Next, check to see if it's a weather icon; if so, display flipbooks in dropdown
                      else if (TemplateFieldsRecPtr^.Field_Type = 7) then
                      begin
                        Modal.RecordGrid.AssignCellCombo(3,i);
                        Modal.RecordGrid.CellButtonType[3,i] := btCombo;

                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Init first combo box entry
                        Modal.RecordGrid.Cell[3,i] := TickerRecPtr^.UserData[k];
                        //Set flag
                        Modal.HasWeather := TRUE;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      //Next, check to see if it's a conference chip rec; if so, display conference chips in dropdown
                      else if (TemplateFieldsRecPtr^.Field_Type = 10) then
                      begin
                        Modal.RecordGrid.AssignCellCombo(3,i);
                        Modal.RecordGrid.CellButtonType[3,i] := btCombo;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Init first combo box entry
                        Modal.RecordGrid.Cell[3,i] := TickerRecPtr^.UserData[k];
                        //Set flag
                        Modal.HasConferenceChips := TRUE;
                      end
                      else if (TemplateFieldsRecPtr^.Field_Type = 8) then
                      begin
                        Modal.RecordGrid.AssignCellCombo(3,i);
                        Modal.RecordGrid.CellButtonType[3,i] := btCombo;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Init combo box entry
                        Modal.RecordGrid.Cell[3,i] := TickerRecPtr^.League + '/' +
                          TickerRecPtr^.Subleague_Mnemonic_Standard;
                        //Set flag
                        Modal.HasManualLeague := TRUE;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      else begin
                        Modal.RecordGrid.RowColor[i] := clWindow;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Check if standard mode, or live event mode
                        //Live Event mode
                        if (CurrentTickerDisplayMode = 1) then
                          Modal.RecordGrid.Cell[3,i] := Trim(TickerRecPtr^.UserData[25+k])
                        //Live event - apply bias of 25
                        else
                          Modal.RecordGrid.Cell[3,i] := Trim(TickerRecPtr^.UserData[k]);
                      end;
                      Inc(k);
                    end
                    else begin
                      //Set cell value
                      Modal.RecordGrid.Cell[3,i] := EngineInterface.GetValueOfSymbol(PlaylistSelectTabControl.TabIndex+1,
                        TemplateFieldsRecPtr^.Field_Contents, TemplateFieldsRecPtr^.Field_Type, CurrentRow-1, CurrentGameData, CurrentTickerDisplayMode);
                      Modal.RecordGrid.RowColor[i] := clAqua;
                      Modal.RecordGrid.CellReadOnly[3,i] := roOn;
                    end;
                    //Increment the template fields match counter
                    Inc (i);
                  end;
                end;
              end;
              //Show the dialog
              Control := Modal.ShowModal;
            finally
              //Set new values if user didn't cancel out
              if (Control = mrOK) then
              begin
                //Set values based on data in grid
                if (Modal.RecordGrid.Rows > 0) then
                begin
                  i := 1;
                  for j := 1 to Modal.RecordGrid.Rows do
                  begin
                    if (Modal.RecordGrid.CellReadOnly[3,j] = roOff) then
                    begin
                      //Check for visitor manual winner indication record type
                      if (Modal.RecordGrid.Cell[4,j] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                      begin
                        User_Data[24] := 'TRUE';
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                      begin
                        User_Data[24] := 'FALSE';
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                      begin
                        User_Data[25] := 'TRUE';
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                      begin
                        User_Data[25] := 'FALSE';
                        Dec(i);
                      end
                      //Check for next page icon
                      else if (Modal.RecordGrid.Cell[4,j] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                      begin
                        User_Data[23] := 'TRUE';
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                      begin
                        User_Data[23] := 'FALSE';
                        Dec(i);
                      end
                      //Check for manual league
                      else if (Modal.RecordGrid.Cell[4,j] = 8) then
                      begin
                        //If User changed the entry via dropdown, get the new values
                        if (Modal.CustomHeader <> '') then
                        begin
                          TickerRecPtr^.League := Modal.CustomHeader;
                          TickerRecPtr^.Subleague_Mnemonic_Standard := '';;
                          TickerRecPtr^.Mnemonic_LiveEvent := '';
                        end;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(i);
                      end
                      else begin
                        //If live event mode, change only live event mode text values
                        if (CurrentTickerDisplayMode = 1) then
                        begin
                          //Set default live mode values - can be edited later
                          User_Data[i] := TickerRecPtr^.UserData[i];
                        end
                        //Standard mode, so change all text values
                        else if (CurrentTickerDisplayMode = -1) then
                        begin
                          //Set user value text and increment
                          User_Data[i] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                        end;
                      end;
                      Inc(i);
                    end;
                  end;
                end;
                //Set other values
                TickerRecPtr^.StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                    (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                TickerRecPtr^.EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                    (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                TickerRecPtr^.Enabled := Modal.EntryEnable.Checked;
                TickerRecPtr^.SponsorLogo_Region := Modal.SponsorLogoRegion.Value;

                //If a sponsor logo dwell was specified, it's a sponsor logo
                //Bug fixed for Version 3.0.6
                if (TickerRecPtr^.SponsorLogo_Dwell > 0) then
                  TickerRecPtr^.SponsorLogo_Dwell := Modal.EntryDwellTime.Value
                else
                  TickerRecPtr^.DwellTime := Modal.EntryDwellTime.Value*1000;

                TickerRecPtr^.Comments := Modal.EntryNote.Text;
                //Edit all matching objects using comparsion of GUIDs
                for j := 0 to StudioTicker_Collection.Count-1 do
                begin
                  TickerRecPtr := StudioTicker_Collection.At(j);
                  if (IsEqualGUID(TickerRecPtr^.Event_GUID, CurrentEventGUID)) then
                  begin
                    With TickerRecPtr^ do
                    begin
                      //Set values for collection record
                      for i := 1 to 25 do UserData[i] := User_Data[i];
                      TickerRecPtr^.StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                          (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                      TickerRecPtr^.EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                          (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                      TickerRecPtr^.Enabled := Modal.EntryEnable.Checked;
                      //If a sponsor logo dwell was specified, it's a sponsor logo
                      if (TickerRecPtr^.SponsorLogo_Dwell > 0) then
                        TickerRecPtr^.SponsorLogo_Dwell := Modal.EntryDwellTime.Value
                      else
                        TickerRecPtr^.DwellTime := Modal.EntryDwellTime.Value*1000;
                      TickerRecPtr^.Comments := Modal.EntryNote.Text;
                      //Re-eanble to change league for all matching GUIDs
                      if (Modal.CustomHeader <> '') then
                      begin
                        TickerRecPtr^.League := Modal.CustomHeader;
                        TickerRecPtr^.Subleague_Mnemonic_Standard := '';
                        TickerRecPtr^.Mnemonic_LiveEvent := '';
                      end;
                    end;
                  end;
                end;
              end;
              Modal.Free;
              //Refresh grid
              RefreshPlaylistGrid;
              //Refresh the fields grid
              RefreshEntryFieldsGrid(CurrentTickerDisplayMode);
            end;
          end;
          //Studio Bug
       1: begin
            BugRecPtr := StudioBug_Collection.At(CurrentRow-1);
            CurrentEventGUID := BugRecPtr^.Event_GUID;
            //Setup and launch editing dialog if applicable
            try
              Modal := TZipperEntryEditorDlg.Create(Application);
              //Add style chips
              if (StyleChip_Collection.Count > 0) then
              begin
                Modal.StyleChipsGrid.StoreData := TRUE;
                Modal.StyleChipsGrid.Cols := 2;
                Modal.StyleChipsGrid.Rows := StyleChip_Collection.Count;
                for i := 0 to StyleChip_Collection.Count-1 do
                begin
                  StyleChipRecPtr := StyleChip_Collection.At(i);
                  Modal.StyleChipsGrid.Cell[1,i+1] := StyleChipRecPtr^.StyleChip_Code;
                  Modal.StyleChipsGrid.Cell[2,i+1] := StyleChipRecPtr^.StyleChip_Description;
                end;
              end;
              Modal.EntryEnable.Checked := BugRecPtr^.Enabled;
              Modal.EntryNote.Text := BugRecPtr^.Comments;
              Modal.EntryStartEnableDate.Date := BugRecPtr^.StartEnableDateTime;
              Modal.EntryStartEnableTime.Time := BugRecPtr^.StartEnableDateTime;
              Modal.EntryEndEnableDate.Date := BugRecPtr^.EndEnableDateTime;
              Modal.EntryEndEnableTime.Time := BugRecPtr^.EndEnableDateTime;
              Modal.EntryDwellTime.Value := Trunc(BugRecPtr^.DwellTime/1000);
              Modal.BitBtn4.Caption := 'Replace Existing Entry';
              //Set initial values for grid
              CurrentTemplateID := BugRecPtr^.Template_ID;
              NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);
              CurrentTemplateDefs := GetTemplateInformation(CurrentTemplateID);
              Modal.TemplateName.Caption := CurrentTemplateDefs.Template_Description;
              if (NumTemplateFields > 0) then
              begin
                //Clear grid values
                if (Modal.RecordGrid.Rows > 0) then
                  Modal.RecordGrid.DeleteRows (1, Modal.RecordGrid.Rows);
                Modal.RecordGrid.StoreData := TRUE;
                Modal.RecordGrid.Cols := 4;
                Modal.RecordGrid.Rows := NumTemplateFields;
                //Populate the grid
                i := 1; k := 1;
                //Check if manual league; if not, get game data
                if (CurrentTemplateDefs.ManualLeague = FALSE) then
                begin
                  CurrentGameData := EngineInterface.GetGameData(BugRecPtr^.League, BugRecPtr^.SI_GCode,
                    BugRecPtr^.ST_Game_ID, BugRecPtr^.CSS_GameID, BugRecPtr^.NFLDM_GameID,
                    BugRecPtr^.Primary_Data_Source, BugRecPtr^.Backup_Data_Source);
                end;
                for j := 0 to Template_Fields_Collection.Count-1 do
                begin
                  TemplateFieldsRecPtr := Template_Fields_Collection.At(j);
                  if (TemplateFieldsRecPtr^.Template_ID = CurrentTemplateID) then
                  begin
                    Modal.RecordGrid.Cell[1,i] := TemplateFieldsRecPtr^.Field_ID; //Index
                    Modal.RecordGrid.Cell[2,i] := TemplateFieldsRecPtr^.Field_Label; //Description
                    Modal.RecordGrid.Cell[4,i] := TemplateFieldsRecPtr^.Field_Type;
                    //Set color & read only property
                    Modal.RecordGrid.CellReadOnly[1,i] := roOn;
                    Modal.RecordGrid.CellReadOnly[2,i] := roOn;
                    Modal.RecordGrid.CellReadOnly[4,i] := roOn;
                    if (TemplateFieldsRecPtr^.Field_Is_Manual = TRUE) then
                    begin
                      //Firt check to see if it's a manual game winner indicator or animation
                      if (TemplateFieldsRecPtr^.Field_Type = 5) then
                      begin
                        Modal.RecordGrid.CellControlType[3,i] := ctCheck;
                        if (BugRecPtr^.UserData[24] = 'TRUE') then
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbChecked
                        else
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbUnchecked;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      else if (TemplateFieldsRecPtr^.Field_Type = 6) then
                      begin
                        Modal.RecordGrid.CellControlType[3,i] := ctCheck;
                        if (BugRecPtr^.UserData[25] = 'TRUE') then
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbChecked
                        else
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbUnchecked;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      //Next, check to see if it's the manual league entry; if so, display choices in dropdown
                      else if (TemplateFieldsRecPtr^.Field_Type = 8) then
                      begin
                        Modal.RecordGrid.AssignCellCombo(3,i);
                        Modal.RecordGrid.CellButtonType[3,i] := btCombo;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Init combo box entry
                        Modal.RecordGrid.Cell[3,i] := BugRecPtr^.League;
                        //Set flag
                        Modal.HasManualLeague := TRUE;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      else begin
                        Modal.RecordGrid.RowColor[i] := clWindow;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        Modal.RecordGrid.Cell[3,i] := Trim(BugRecPtr^.UserData[k]);
                      end;
                      Inc(k);
                    end
                    else begin
                      //Set cell value
                      Modal.RecordGrid.Cell[3,i] := EngineInterface.GetValueOfSymbol(PlaylistSelectTabControl.TabIndex+1,
                        TemplateFieldsRecPtr^.Field_Contents, TemplateFieldsRecPtr^.Field_Type, CurrentRow-1, CurrentGameData, 0);
                      Modal.RecordGrid.RowColor[i] := clAqua;
                      Modal.RecordGrid.CellReadOnly[3,i] := roOn;
                    end;
                    //Increment the template fields match counter
                    Inc (i);
                  end;
                end;
              end;
              //Show the dialog
              Control := Modal.ShowModal;
            finally
              //Set new values if user didn't cancel out
              if (Control = mrOK) then
              begin
                //Set values based on data in grid
                if (Modal.RecordGrid.Rows > 0) then
                begin
                  i := 1;
                  for j := 1 to Modal.RecordGrid.Rows do
                  begin
                    if (Modal.RecordGrid.CellReadOnly[3,j] = roOff) then
                    begin
                      //Check for visitor manual winner indication record type
                      if (Modal.RecordGrid.Cell[4,j] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                      begin
                        User_Data[24] := 'TRUE';
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                      begin
                        User_Data[24] := 'FALSE';
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                      begin
                        User_Data[25] := 'TRUE';
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                      begin
                        User_Data[25] := 'FALSE';
                        Dec(i);
                      end
                      //Check for manual league
                      else if (Modal.RecordGrid.Cell[4,j] = 8) then
                      begin
                        //If user changed league via dropdown, set new value
                        if (Modal.CustomHeader <> '') then
                          BugRecPtr^.League := Modal.CustomHeader;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(i);
                      end
                      else begin
                        //Set user value text and increment
                        User_Data[i] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                      end;
                      Inc(i);
                    end;
                  end;
                end;
                //Set other values
                BugRecPtr^.StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                    (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                BugRecPtr^.EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                    (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                BugRecPtr^.Enabled := Modal.EntryEnable.Checked;
                BugRecPtr^.DwellTime := Modal.EntryDwellTime.Value*1000;
                BugRecPtr^.Comments := Modal.EntryNote.Text;
                //Edit all matching objects using comparsion of GUIDs
                for j := 0 to StudioBug_Collection.Count-1 do
                begin
                  BugRecPtr := StudioBug_Collection.At(j);
                  if (IsEqualGUID(BugRecPtr^.Event_GUID, CurrentEventGUID)) then
                  begin
                    With BugRecPtr^ do
                    begin
                      //Set values for collection record
                      for i := 1 to 25 do UserData[i] := User_Data[i];
                      BugRecPtr^.StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                          (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                      BugRecPtr^.EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                          (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                      BugRecPtr^.Enabled := Modal.EntryEnable.Checked;
                      BugRecPtr^.DwellTime := Modal.EntryDwellTime.Value*1000;
                      BugRecPtr^.Comments := Modal.EntryNote.Text;
                      //BugRecPtr^.League := Modal.League;
                    end;
                  end;
                end;
              end;
              Modal.Free;
              //Refresh grid
              RefreshPlaylistGrid;
              //Refresh the fields grid
              RefreshEntryFieldsGrid(CurrentTickerDisplayMode);
            end;
          end;
          //GameTrax
       2: begin
            GameTraxRecPtr := GameTrax_Collection.At(CurrentRow-1);
            CurrentEventGUID := GameTraxRecPtr^.Event_GUID;
            //Setup and launch editing dialog if applicable
            try
              Modal := TZipperEntryEditorDlg.Create(Application);
              //Add style chips
              if (StyleChip_Collection.Count > 0) then
              begin
                Modal.StyleChipsGrid.StoreData := TRUE;
                Modal.StyleChipsGrid.Cols := 2;
                Modal.StyleChipsGrid.Rows := StyleChip_Collection.Count;
                for i := 0 to StyleChip_Collection.Count-1 do
                begin
                  StyleChipRecPtr := StyleChip_Collection.At(i);
                  Modal.StyleChipsGrid.Cell[1,i+1] := StyleChipRecPtr^.StyleChip_Code;
                  Modal.StyleChipsGrid.Cell[2,i+1] := StyleChipRecPtr^.StyleChip_Description;
                end;
              end;
              Modal.EntryEnable.Checked := GameTraxRecPtr^.Enabled;
              Modal.EntryNote.Text := GameTraxRecPtr^.Comments;
              Modal.EntryStartEnableDate.Date := GameTraxRecPtr^.StartEnableDateTime;
              Modal.EntryStartEnableTime.Time := GameTraxRecPtr^.StartEnableDateTime;
              Modal.EntryEndEnableDate.Date := GameTraxRecPtr^.EndEnableDateTime;
              Modal.EntryEndEnableTime.Time := GameTraxRecPtr^.EndEnableDateTime;
              //If a sponsor dwell was specified, it's a sponsor logo
              //Modified for Version 3.0.5
              if (GametraxRecPtr^.SponsorLogo_Dwell > 0) then
              begin
                Modal.EntryDwellTime.Value := Trunc(GametraxRecPtr^.SponsorLogo_Dwell);
                Modal.SponsorLogoRegionLabel.Visible := TRUE;
                Modal.SponsorLogoRegionLabel2.Visible := TRUE;
                Modal.SponsorLogoRegion.Visible := TRUE;
                Modal.SponsorLogoRegion.Value := GametraxRecPtr^.SponsorLogo_Region;
                Modal.PersistentLogoCheckbox.Checked := FALSE;
                Modal.PersistentLogoCheckbox.Enabled := TRUE;
              end
              else if (GametraxRecPtr^.SponsorLogo_Dwell = -1) then
              begin
                Modal.PersistentLogoCheckbox.Enabled := TRUE;
                Modal.PersistentLogoCheckbox.Checked := TRUE;
              end
              else begin
                Modal.EntryDwellTime.Value := Trunc(GametraxRecPtr^.DwellTime/1000);
                Modal.SponsorLogoRegionLabel.Visible := FALSE;
                Modal.SponsorLogoRegionLabel2.Visible := FALSE;
                Modal.SponsorLogoRegion.Visible := FALSE;
                Modal.PersistentLogoCheckbox.Checked := FALSE;
                Modal.PersistentLogoCheckbox.Enabled := FALSE;
              end;
              Modal.BitBtn4.Caption := 'Replace Existing Entry';
              //Set initial values for grid
              CurrentTemplateID := GameTraxRecPtr^.Template_ID;
              NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);
              CurrentTemplateDefs := GetTemplateInformation(CurrentTemplateID);
              Modal.TemplateName.Caption := CurrentTemplateDefs.Template_Description;
              if (NumTemplateFields > 0) then
              begin
                //Clear grid values
                if (Modal.RecordGrid.Rows > 0) then
                  Modal.RecordGrid.DeleteRows (1, Modal.RecordGrid.Rows);
                Modal.RecordGrid.StoreData := TRUE;
                Modal.RecordGrid.Cols := 4;
                Modal.RecordGrid.Rows := NumTemplateFields;
                //Populate the grid
                i := 1; k := 1;
                //Check if manual league; if not, get game data
                if (CurrentTemplateDefs.ManualLeague = FALSE) then
                begin
                  CurrentGameData := EngineInterface.GetGameData(GameTraxRecPtr^.League, GameTraxRecPtr^.SI_GCode,
                    GameTraxRecPtr^.ST_Game_ID, GameTraxRecPtr^.CSS_GameID, GameTraxRecPtr^.NFLDM_GameID,
                    GameTraxRecPtr^.Primary_Data_Source, GameTraxRecPtr^.Backup_Data_Source);
                end;
                for j := 0 to Template_Fields_Collection.Count-1 do
                begin
                  TemplateFieldsRecPtr := Template_Fields_Collection.At(j);
                  if (TemplateFieldsRecPtr^.Template_ID = CurrentTemplateID) then
                  begin
                    Modal.RecordGrid.Cell[1,i] := TemplateFieldsRecPtr^.Field_ID; //Index
                    Modal.RecordGrid.Cell[2,i] := TemplateFieldsRecPtr^.Field_Label; //Description
                    Modal.RecordGrid.Cell[4,i] := TemplateFieldsRecPtr^.Field_Type; //Field type
                    //Set color & read only property
                    Modal.RecordGrid.CellReadOnly[1,i] := roOn;
                    Modal.RecordGrid.CellReadOnly[2,i] := roOn;
                    Modal.RecordGrid.CellReadOnly[4,i] := roOn;
                    if (TemplateFieldsRecPtr^.Field_Is_Manual = TRUE) then
                    begin
                      //Firt check to see if it's a manual game winner indicator or animation
                      if (TemplateFieldsRecPtr^.Field_Type = 5) then
                      begin
                        Modal.RecordGrid.CellControlType[3,i] := ctCheck;
                        if (GameTraxRecPtr^.UserData[24] = 'TRUE') then
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbChecked
                        else
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbUnchecked;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      else if (TemplateFieldsRecPtr^.Field_Type = 6) then
                      begin
                        Modal.RecordGrid.CellControlType[3,i] := ctCheck;
                        if (GameTraxRecPtr^.UserData[25] = 'TRUE') then
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbChecked
                        else
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbUnchecked;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      //Next, check to see if it's a manual next page icon animation
                      else if (TemplateFieldsRecPtr^.Field_Type = 9) then
                      begin
                        Modal.RecordGrid.CellControlType[3,i] := ctCheck;
                        if (GameTraxRecPtr^.UserData[23] = 'TRUE') then
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbChecked
                        else
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbUnchecked;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      //Next, check to see if it's a conference chip rec; if so, display conference chips in dropdown
                      else if (TemplateFieldsRecPtr^.Field_Type = 10) then
                      begin
                        Modal.RecordGrid.AssignCellCombo(3,i);
                        Modal.RecordGrid.CellButtonType[3,i] := btCombo;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Init first combo box entry
                        Modal.RecordGrid.Cell[3,i] := GameTraxRecPtr^.UserData[k];
                        Modal.ConferenceChipName := GameTraxRecPtr^.UserData[k];
                        //Set flag
                        Modal.HasConferenceChips := TRUE;
                      end
                      //Next, check to see if it's the manual league entry; if so, display choices in dropdown
                      else if (TemplateFieldsRecPtr^.Field_Type = 8) then
                      begin
                        Modal.RecordGrid.AssignCellCombo(3,i);
                        Modal.RecordGrid.CellButtonType[3,i] := btCombo;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Init combo box entry
                        Modal.RecordGrid.Cell[3,i] := GameTraxRecPtr^.League;
                        //Set flag
                        Modal.HasManualLeague := TRUE;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      //Show as standard manual text input field
                      else begin
                        Modal.RecordGrid.RowColor[i] := clWindow;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        Modal.RecordGrid.Cell[3,i] := Trim(GameTraxRecPtr^.UserData[k]);
                      end;
                      Inc(k);
                    end
                    else begin
                      //Set cell value
                      Modal.RecordGrid.Cell[3,i] := EngineInterface.GetValueOfSymbol(PlaylistSelectTabControl.TabIndex+1,
                        TemplateFieldsRecPtr^.Field_Contents, TemplateFieldsRecPtr^.Field_Type, CurrentRow-1, CurrentGameData, 0);
                      Modal.RecordGrid.RowColor[i] := clAqua;
                      Modal.RecordGrid.CellReadOnly[3,i] := roOn;
                    end;
                    //Increment the template fields match counter
                    Inc (i);
                  end;
                end;
              end;
              //Show the dialog
              Control := Modal.ShowModal;
            finally
              //Set new values if user didn't cancel out
              if (Control = mrOK) then
              begin
                //Set values based on data in grid
                if (Modal.RecordGrid.Rows > 0) then
                begin
                  i := 1;
                  for j := 1 to Modal.RecordGrid.Rows do
                  begin
                    if (Modal.RecordGrid.CellReadOnly[3,j] = roOff) then
                    begin
                      //Check for visitor manual winner indication record type
                      if (Modal.RecordGrid.Cell[4,j] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                      begin
                        User_Data[24] := 'TRUE';
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                      begin
                        User_Data[24] := 'FALSE';
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                      begin
                        User_Data[25] := 'TRUE';
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                      begin
                        User_Data[25] := 'FALSE';
                        Dec(i);
                      end
                      //Check for next page icon
                      else if (Modal.RecordGrid.Cell[4,j] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                      begin
                        User_Data[24] := 'TRUE';
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                      begin
                        User_Data[23] := 'FALSE';
                        Dec(i);
                      end
                      //Check for manual league
                      else if (Modal.RecordGrid.Cell[4,j] = 8) then
                      begin
                        //If user changed league via dropdown, set new value
                        if (Modal.CustomHeader <> '') then
                          GameTraxRecPtr^.League := Modal.CustomHeader;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(i);
                      end
                      else begin
                        //Check for conference info
                        if (Modal.RecordGrid.Cell[4,j] = 10) then
                        begin
                          //If user changed league via dropdown, set new value
                          if (Modal.ConferenceChipName <> '') then
                            User_Data[I] := Modal.ConferenceChipName;
                          //Decrement the user data counter - league is stored in ticker record explicitly
                          Dec(i);
                        end
                        else
                          //Set user value text and increment
                          User_Data[i] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                      end;
                      Inc(i);
                    end;
                  end;
                end;
                //Set additional fields
                GameTraxRecPtr^.StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                    (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                GameTraxRecPtr^.EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                    (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                GameTraxRecPtr^.Enabled := Modal.EntryEnable.Checked;
                GameTraxRecPtr^.Comments := Modal.EntryNote.Text;
                GametraxRecPtr^.SponsorLogo_Region := Modal.SponsorLogoRegion.Value;
                //If a sponsor logo dwell was specified, it's a sponsor logo
                if (GameTraxRecPtr^.SponsorLogo_Dwell = -1) or (GameTraxRecPtr^.SponsorLogo_Dwell > 0) then
                begin
                  if (Modal.PersistentLogoCheckbox.Checked = TRUE) then
                    GameTraxRecPtr^.SponsorLogo_Dwell := -1
                  else if (Modal.PersistentLogoCheckbox.Checked = FALSE) and (Modal.EntryDwellTime.Value > 0) then
                    GameTraxRecPtr^.SponsorLogo_Dwell := Modal.EntryDwellTime.Value;
                end
                else
                  GameTraxRecPtr^.DwellTime := Modal.EntryDwellTime.Value*1000;
                //Edit all matching objects using comparsion of GUIDs
                for j := 0 to GameTrax_Collection.Count-1 do
                begin
                  GameTraxRecPtr := GameTrax_Collection.At(j);
                  if (IsEqualGUID(GameTraxRecPtr^.Event_GUID, CurrentEventGUID)) then
                  begin
                    With GameTraxRecPtr^ do
                    begin
                      //Set values for collection record
                      for i := 1 to 25 do UserData[i] := User_Data[i];
                      GameTraxRecPtr^.StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                          (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                      GameTraxRecPtr^.EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                          (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                      GameTraxRecPtr^.Enabled := Modal.EntryEnable.Checked;
                      GametraxRecPtr^.SponsorLogo_Region := Modal.SponsorLogoRegion.Value;
                      //If a sponsor logo dwell was specified, it's a sponsor logo
                      if (GameTraxRecPtr^.SponsorLogo_Dwell > 0) then
                        GameTraxRecPtr^.SponsorLogo_Dwell := Modal.EntryDwellTime.Value
                      else if (GameTraxRecPtr^.SponsorLogo_Dwell = -1) then
                        GameTraxRecPtr^.SponsorLogo_Dwell := -1
                      else
                        GameTraxRecPtr^.DwellTime := Modal.EntryDwellTime.Value*1000;
                      GameTraxRecPtr^.Comments := Modal.EntryNote.Text;
                      //GameTraxRecPtr^.League := Modal.League;
                    end;
                  end;
                end;
              end;
              Modal.Free;
              //Refresh grid
              RefreshPlaylistGrid;
              //Refresh the fields grid
              RefreshEntryFieldsGrid(CurrentTickerDisplayMode);
            end;
          end;
      end;
    end;
    PlaylistGrid.CurrentDataRow := CurrentRow;
    PlaylistGrid.TopRow := SaveTopRow;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES AND HANDLERS FOR ZIPPER PLAYLIST STORAGE,, LOADING & DELETION
////////////////////////////////////////////////////////////////////////////////
//Procedure to delete a specified zipper playlist from the database
procedure TMainForm.DeleteZipperPlaylist (PlaylistType: SmallInt; Playlist_ID: Double; Playlist_Description: String);
begin
  Case PlaylistType of
      //Ticker
   1: begin
        try
          if (dmMain.tblTicker_Groups.RecordCount > 0) then
          begin
            dmMain.tblTicker_Groups.First;
            While(dmMain.tblTicker_Groups.EOF = FALSE) do
            begin
              if (dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblTicker_Groups.Delete;
              end
              else dmMain.tblTicker_Groups.Next;
            end;
          end;

          //Delete any associated elements
          if (dmMain.tblTicker_Elements.RecordCount > 0) then
          begin
            dmMain.tblTicker_Elements.First;
            While(dmMain.tblTicker_Elements.EOF = FALSE) do
            begin
              if (dmMain.tblTicker_Elements.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblTicker_Elements.Delete;
              end
              else dmMain.tblTicker_Elements.Next;
            end;
          end;
        except
          //Alert operator
          MessageDlg('An error occurred while trying to delete the existing copy of the ' +
                     'ticker playlist from the database.', mtError, [mbOK], 0)
        end;
      end;
      //Bug
   2: begin
        try
          if (dmMain.tblBug_Groups.RecordCount > 0) then
          begin
            dmMain.tblBug_Groups.First;
            While(dmMain.tblBug_Groups.EOF = FALSE) do
            begin
              if (dmMain.tblBug_Groups.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblBug_Groups.Delete;
              end
              else dmMain.tblBug_Groups.Next;
            end;
          end;

          //Delete any associated elements
          if (dmMain.tblBug_Elements.RecordCount > 0) then
          begin
            dmMain.tblBug_Elements.First;
            While(dmMain.tblBug_Elements.EOF = FALSE) do
            begin
              if (dmMain.tblBug_Elements.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblBug_Elements.Delete;
              end
              else dmMain.tblBug_Elements.Next;
            end;
          end;
        except
          //Alert operator
          MessageDlg('An error occurred while trying to delete the existing copy of the ' +
                     'bug playlist from the database.', mtError, [mbOK], 0)
        end;
      end;
      //Extra line
   3: begin
        try
          if (dmMain.tblGameTrax_Groups.RecordCount > 0) then
          begin
            dmMain.tblGameTrax_Groups.First;
            While(dmMain.tblGameTrax_Groups.EOF = FALSE) do
            begin
              if (dmMain.tblGameTrax_Groups.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblGameTrax_Groups.Delete;
              end
              else dmMain.tblGameTrax_Groups.Next;
            end;
          end;

          //Delete any associated elements
          if (dmMain.tblGameTrax_Elements.RecordCount > 0) then
          begin
            dmMain.tblGameTrax_Elements.First;
            While(dmMain.tblGameTrax_Elements.EOF = FALSE) do
            begin
              if (dmMain.tblGameTrax_Elements.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblGameTrax_Elements.Delete;
              end
              else dmMain.tblGameTrax_Elements.Next;
            end;
          end;
        except
          //Alert operator
          MessageDlg('An error occurred while trying to delete the existing copy of the ' +
                     'extra line playlist from the database.', mtError, [mbOK], 0)
        end;
      end;
  end;
end;

//Function to check to see if it's a college playlist and if it includes more than one college
//league, which is not allowed
function TMainForm.CheckForMultipleCollegeLeagues: Boolean;
var
  i: SmallInt;
  GameTraxRecPtr: ^GameTraxRec;
  FoundCFB, FoundCBB: Boolean;
  OutVal: Boolean;
begin
  FoundCFB := FALSE;
  FoundCBB := FALSE;
  OutVal := FALSE;
  if (Gametrax_Collection.Count > 0) then
  begin
    for i := 0 to Gametrax_Collection.Count-1 do
    begin
      GameTraxRecPtr := Gametrax_Collection.At(i);
      if (GametraxRecPtr^.League = 'CFB') then FoundCFB := TRUE;
      if (GametraxRecPtr^.League = 'CBB') then FoundCBB := TRUE;
    end;
  end;
  if (FoundCFB) AND (FoundCBB) then OutVal := TRUE;
  CheckForMultipleCollegeLeagues := OutVal;
end;

//Handler for save playlist button WITHOUT clear
procedure TMainForm.BitBtn6Click(Sender: TObject);
begin
  //Save playlist, but don't clear
  SaveTickerPlaylist(PlaylistSelectTabControl.TabIndex+1, FALSE);
end;
//Handler for save playlist button WITH clear
procedure TMainForm.BitBtn1Click(Sender: TObject);
begin
  //Save playlist and clear
  SaveTickerPlaylist(PlaylistSelectTabControl.TabIndex+1, TRUE);
end;
//General procedure to save zipper collection out to database
function TMainForm.SaveTickerPlaylist(PlaylistType: SmallInt; ClearCollection: Boolean): Boolean;
var
  i: SmallInt;
  Playlist_Description: String;
  Playlist_ID: Double;
  TickerRecPtr: ^TickerRec;
  BugRecPtr: ^BugRec;
  GameTraxRecPtr: ^GameTraxRec;
  Control: Word;
  FoundMatch: Boolean;
  OKToGo: Boolean;
  OriginalCreationDate: TDateTime;
  Save_Cursor: TCursor;
  Success, Failure: Boolean;
  FoundBreakingNews, FoundNonBreakingNews: Boolean;
begin
  //Init
  OKToGo := TRUE;
  Success := TRUE;
  Failure := FALSE;

  //First, check to make sure the playlist does not include CFB and CBB games
  //Disabled for Version 1.7.3
  //if (CheckForMultipleCollegeLeagues = TRUE) then
  //begin
  //  //Alert operator
  //  MessageDlg('The playlist contains both CFB and CBB games. The playlist may contain games from only one college league.',
  //    mtError, [mbOk], 0);
  //  //Clear OK to Go flag to prevent save
  //  OKToGo := FALSE;
  //end;

  //Proceed if multiple college leagues not found - Check disabled for Version 1.7.3
  if (OKToGo) then
  begin
    //Change cursor
    Save_Cursor := Screen.Cursor;
    //Show hourglass cursor
    Screen.Cursor := crHourGlass;

    //Set playlist name
    Playlist_Description := Trim (Edit1.Text);

    //Check to see if a playlist with a matching ID exists. If so, prompt operator
    //for overwrite confirmation
    Case PlaylistType of
      1: FoundMatch := dmMain.tblTicker_Groups.Locate('Playlist_Description', Playlist_Description, []);
      2: FoundMatch := dmMain.tblBug_Groups.Locate('Playlist_Description', Playlist_Description, []);
      3: FoundMatch := dmMain.tblGameTrax_Groups.Locate('Playlist_Description', Playlist_Description, []);
    end;
    if (Foundmatch = TRUE) then
    begin
      Control := MessageDlg('A playlist with a matching description was found in the ' +
                            'database. Do you wish to overwrite it?',
                            mtConfirmation, [mbYes, mbNo], 0);
      if (Control = mrYes) then
      begin
        Case PlaylistType of
            //Ticker
         1: begin
              Playlist_ID := dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsFloat;
              OriginalCreationDate := dmMain.tblTicker_Groups.FieldByName('Entry_Date').AsDateTime;
              DeleteZipperPlaylist (1, Playlist_ID, Playlist_Description);
            end;
            //Bug
         2: begin
              Playlist_ID := dmMain.tblBug_Groups.FieldByName('Playlist_ID').AsFloat;
              OriginalCreationDate := dmMain.tblBug_Groups.FieldByName('Entry_Date').AsDateTime;
              DeleteZipperPlaylist (2, Playlist_ID, Playlist_Description);
            end;
            //Extra line
         3: begin
              Playlist_ID := dmMain.tblGameTrax_Groups.FieldByName('Playlist_ID').AsFloat;
              OriginalCreationDate := dmMain.tblGameTrax_Groups.FieldByName('Entry_Date').AsDateTime;
              DeleteZipperPlaylist (3, Playlist_ID, Playlist_Description);
            end;
        end;
      end
      else OKToGo := FALSE;
    end;

    //Proceed if confirmed
    if (OKToGo = TRUE) then
    begin
      begin
        if (Length(Playlist_Description) >= 3) then
        begin
          Case PlaylistType of
           //Studio Ticker
           1: begin
                //Only proceed if at least one element in playlist
                if (StudioTicker_Collection.Count > 0) then
                begin
                  //If not overwriting the list, generate a new playlist ID
                  if (FoundMatch = FALSE) then
                  begin
                    Playlist_ID := Trunc(Now*100000);
                    OriginalCreationDate := Now;
                  end;
                  //Set Playlist_ID to 0 if it's the CURRENT playlist
                  if (Playlist_Description = 'CURRENT') then Playlist_ID := 0;
                  try
                    //Append new record for playlist
                    dmMain.tblTicker_Groups.AppendRecord ([
                      Playlist_ID,
                      Playlist_Description,
                      CurrentTickerDisplayMode,
                      OriginalCreationDate, //Creation date/timestamp
                      'N/A', //Operator name
                      Now //Edit date/time
                    ]);
                    //Now, insert all elements into ticker elements table
                    try
                      //Iterate for all records in collection
                      for i := 0 to StudioTicker_Collection.Count-1 do
                      begin
                        //Point to collection object
                        TickerRecPtr := StudioTicker_Collection.At(i);
                        //Append graphic record to graphics pages table in database}
                        dmMain.tblTicker_Elements.AppendRecord ([
                          Playlist_ID, //Block ID
                          i+1, //Index
                          //Set GUID as string
                          GUIDToString(TickerRecPtr^.Event_GUID),
                          //Default to NOT a mini playlist with a zero playlist ID
                          TickerRecPtr^.Enabled,
                          TickerRecPtr^.Is_Child,
                          TickerRecPtr^.League,
                          TickerRecPtr^.Subleague_Mnemonic_Standard,
                          TickerRecPtr^.Mnemonic_LiveEvent,
                          TickerRecPtr^.Template_ID,
                          TickerRecPtr^.Record_Type,
                          TickerRecPtr^.SI_GCode,
                          TickerRecPtr^.ST_Game_ID,
                          TickerRecPtr^.CSS_GameID,
                          TickerRecPtr^.NFLDM_GameID,
                          TickerRecPtr^.Primary_Data_Source,
                          TickerRecPtr^.backup_Data_Source,
                          TickerRecPtr^.SponsorLogo_Name, //Sponsor logo name
                          TickerRecPtr^.SponsorLogo_Dwell, //Dwell for sponsor logo
                          TickerRecPtr^.SponsorLogo_Region, //Region ID for sponsor logo
                          //For stats
                          TickerRecPtr^.StatStoredProcedure,
                          TickerRecPtr^.StatType,
                          TickerRecPtr^.StatTeam,
                          TickerRecPtr^.StatLeague,
                          TickerRecPtr^.StatRecordNumber,
                          TickerRecPtr^.UserData[1],
                          TickerRecPtr^.UserData[2],
                          TickerRecPtr^.UserData[3],
                          TickerRecPtr^.UserData[4],
                          TickerRecPtr^.UserData[5],
                          TickerRecPtr^.UserData[6],
                          TickerRecPtr^.UserData[7],
                          TickerRecPtr^.UserData[8],
                          TickerRecPtr^.UserData[9],
                          TickerRecPtr^.UserData[10],
                          TickerRecPtr^.UserData[11],
                          TickerRecPtr^.UserData[12],
                          TickerRecPtr^.UserData[13],
                          TickerRecPtr^.UserData[14],
                          TickerRecPtr^.UserData[15],
                          TickerRecPtr^.UserData[16],
                          TickerRecPtr^.UserData[17],
                          TickerRecPtr^.UserData[18],
                          TickerRecPtr^.UserData[19],
                          TickerRecPtr^.UserData[20],
                          TickerRecPtr^.UserData[21],
                          TickerRecPtr^.UserData[22],
                          TickerRecPtr^.UserData[23],
                          TickerRecPtr^.UserData[24],
                          TickerRecPtr^.UserData[25],
                          TickerRecPtr^.StartEnableDateTime,
                          TickerRecPtr^.EndEnableDateTime,
                          TickerRecPtr^.DwellTime,
                          TickerRecPtr^.Description,
                          TickerRecPtr^.Comments
                        ]);
                      end;
                    except
                      MessageDlg ('Error occurred while trying to insert ticker element into database.',
                                   mtError, [mbOk], 0);
                    end;
                    //Update label & array
                    PlaylistInfo[TICKER].PlaylistName := Edit1.Text;
                    PlaylistNameLabel.Caption := Playlist_Description;
                  except
                    MessageDlg ('Error occurred while trying to insert ticker playlist into database.',
                                 mtError, [mbOk], 0);
                  end;
                  //Clear out & pack the collection if enabled
                  if (ClearCollection) then
                  begin
                    StudioTicker_Collection.Clear;
                    StudioTicker_Collection.Pack;
                  end;
                end
                else begin
                  MessageDlg('You must specify one or more elements to save out a playlist.',
                             mtInformation, [mbOk], 0);
                end;
              end;
           //Studio Bug
           2: begin
                //Only proceed if at least one element in playlist
                if (StudioBug_Collection.Count > 0) then
                begin
                  //If not overwriting the list, generate a new playlist ID
                  if (FoundMatch = FALSE) then
                  begin
                    Playlist_ID := Trunc(Now*100000);
                    OriginalCreationDate := Now;
                  end;
                  //Set Playlist_ID to 0 if it's the CURRENT playlist
                  if (Playlist_Description = 'CURRENT') then Playlist_ID := 0;
                  try
                    //Append new record for playlist
                    dmMain.tblBug_Groups.AppendRecord ([
                      Playlist_ID,
                      Playlist_Description,
                      0, //Bug mode fixed for now
                      OriginalCreationDate, //Creation date/timestamp
                      'N/A', //Operator name
                      Now //Edit date/time
                    ]);
                    //Now, insert all elements into bug elements table
                    try
                      //Iterate for all records in collection
                      for i := 0 to StudioBug_Collection.Count-1 do
                      begin
                        //Point to collection object
                        BugRecPtr := StudioBug_Collection.At(i);
                        //Append graphic record to graphics pages table in database}
                        dmMain.tblBug_Elements.AppendRecord ([
                          Playlist_ID, //Block ID
                          i+1, //Index
                          //Set GUID as string
                          GUIDToString(BugRecPtr^.Event_GUID),
                          //Default to NOT a mini playlist with a zero playlist ID
                          BugRecPtr^.Enabled,
                          BugRecPtr^.Is_Child,
                          BugRecPtr^.League,
                          BugRecPtr^.Template_ID,
                          BugRecPtr^.Record_Type,
                          BugRecPtr^.SI_GCode,
                          BugRecPtr^.ST_Game_ID,
                          BugRecPtr^.CSS_GameID,
                          BugRecPtr^.NFLDM_GameID,
                          BugRecPtr^.Primary_Data_Source,
                          BugRecPtr^.backup_Data_Source,
                          BugRecPtr^.UserData[1],
                          BugRecPtr^.UserData[2],
                          BugRecPtr^.UserData[3],
                          BugRecPtr^.UserData[4],
                          BugRecPtr^.UserData[5],
                          BugRecPtr^.UserData[6],
                          BugRecPtr^.UserData[7],
                          BugRecPtr^.UserData[8],
                          BugRecPtr^.UserData[9],
                          BugRecPtr^.UserData[10],
                          BugRecPtr^.UserData[11],
                          BugRecPtr^.UserData[12],
                          BugRecPtr^.UserData[13],
                          BugRecPtr^.UserData[14],
                          BugRecPtr^.UserData[15],
                          BugRecPtr^.UserData[16],
                          BugRecPtr^.UserData[17],
                          BugRecPtr^.UserData[18],
                          BugRecPtr^.UserData[19],
                          BugRecPtr^.UserData[20],
                          BugRecPtr^.UserData[21],
                          BugRecPtr^.UserData[22],
                          BugRecPtr^.UserData[23],
                          BugRecPtr^.UserData[24],
                          BugRecPtr^.UserData[25],
                          BugRecPtr^.StartEnableDateTime,
                          BugRecPtr^.EndEnableDateTime,
                          BugRecPtr^.DwellTime,
                          BugRecPtr^.Description,
                          BugRecPtr^.Comments
                        ]);
                      end;
                    except
                      MessageDlg ('Error occurred while trying to insert bug element into database.',
                                   mtError, [mbOk], 0);
                    end;
                    //Update label & array
                    PlaylistInfo[BUG].PlaylistName := Edit1.Text;
                    PlaylistNameLabel.Caption := Playlist_Description;
                  except
                    MessageDlg ('Error occurred while trying to insert bug playlist into database.',
                                 mtError, [mbOk], 0);
                  end;
                  //Clear out & pack the collection if enabled
                  if (ClearCollection) then
                  begin
                    StudioBug_Collection.Clear;
                    StudioBug_Collection.Pack;
                  end;
                end
                else begin
                  MessageDlg('You must specify one or more elements to save out a playlist.',
                             mtInformation, [mbOk], 0);
                end;
              end;
           //GameTrax
           3: begin
                //Only proceed if at least one element in playlist
                if (GameTrax_Collection.Count > 0) then
                begin
                  //If not overwriting the list, generate a new playlist ID
                  if (FoundMatch = FALSE) then
                  begin
                    Playlist_ID := Trunc(Now*100000);
                    OriginalCreationDate := Now;
                  end;
                  //Set Playlist_ID to 0 if it's the breaking news playlist
                  try
                    //If breaking news, check to make sure all entries are breaking news entries
                    OKToGo := TRUE;
                    if (Playlist_ID = 0) then
                    begin
                      for i := 0 to GameTrax_Collection.Count-1 do
                      begin
                        //Point to collection object
                        GameTraxRecPtr := GameTrax_Collection.At(i);
                        if (GameTraxRecPtr^.Template_ID <> 501) AND (GameTraxRecPtr^.Template_ID <> 502) then
                          FoundNonBreakingNews := TRUE;
                      end;
                      if (FoundNonBreakingNews = TRUE) then
                      begin
                        OKToGo := FALSE;
                        MessageDlg ('One or more non-breaking news entries were found in the playlist. A breaking news ' +
                         'playlist can only contain breaking news records.', mtInformation, [mbOk], 0);
                      end;
                    end
                    //If NOT breaking news, check to make sure no entries are breaking news entries
                    else if (Playlist_ID <> 0) then
                    begin
                      for i := 0 to GameTrax_Collection.Count-1 do
                      begin
                        //Point to collection object
                        GameTraxRecPtr := GameTrax_Collection.At(i);
                        if (GameTraxRecPtr^.Template_ID = 501) OR (GameTraxRecPtr^.Template_ID = 502) then
                          FoundBreakingNews := TRUE;
                      end;
                      if (FoundBreakingNews = TRUE) then
                      begin
                        OKToGo := FALSE;
                        MessageDlg ('One or more breaking news entries were found in the playlist. A non-breaking news ' +
                         'playlist cannot contain breaking news records.', mtInformation, [mbOk], 0);
                      end;
                    end;
                    if (OKToGo) then
                    begin
                      //Append new record for playlist
                      dmMain.tblGameTrax_Groups.AppendRecord ([
                        Playlist_ID,
                        Playlist_Description,
                        0, //Extra line mode fixed for now
                        OriginalCreationDate, //Creation date/timestamp
                        'N/A', //Operator name
                        Now //Edit date/time
                      ]);
                      //Now, insert all elements into zipper elements table
                      try
                        //Iterate for all records in collection
                        for i := 0 to GameTrax_Collection.Count-1 do
                        begin
                          //Point to collection object
                          GameTraxRecPtr := GameTrax_Collection.At(i);
                          //Append graphic record to graphics pages table in database}
                          dmMain.tblGameTrax_Elements.AppendRecord ([
                            Playlist_ID, //Block ID
                            i+1, //Index
                            //Set GUID as string
                            GUIDToString(GameTraxRecPtr^.Event_GUID),
                            //Default to NOT a mini playlist with a zero playlist ID
                            GameTraxRecPtr^.Enabled,
                            GameTraxRecPtr^.Is_Child,
                            GameTraxRecPtr^.League,
                            GameTraxRecPtr^.Template_ID,
                            GameTraxRecPtr^.Record_Type,
                            GameTraxRecPtr^.SI_GCode,
                            GameTraxRecPtr^.ST_Game_ID,
                            GameTraxRecPtr^.CSS_GameID,
                            GametraxRecPtr^.NFLDM_GameID,
                            GametraxRecPtr^.Primary_Data_Source,
                            GametraxRecPtr^.Backup_Data_Source,
                            GametraxRecPtr^.SponsorLogo_Name, //Sponsor logo name
                            GametraxRecPtr^.SponsorLogo_Dwell, //Dwell for sponsor logo
                            GametraxRecPtr^.SponsorLogo_Region, //Region ID for sponsor logo
                            GameTraxRecPtr^.UserData[1],
                            GameTraxRecPtr^.UserData[2],
                            GameTraxRecPtr^.UserData[3],
                            GameTraxRecPtr^.UserData[4],
                            GameTraxRecPtr^.UserData[5],
                            GameTraxRecPtr^.UserData[6],
                            GameTraxRecPtr^.UserData[7],
                            GameTraxRecPtr^.UserData[8],
                            GameTraxRecPtr^.UserData[9],
                            GameTraxRecPtr^.UserData[10],
                            GameTraxRecPtr^.UserData[11],
                            GameTraxRecPtr^.UserData[12],
                            GameTraxRecPtr^.UserData[13],
                            GameTraxRecPtr^.UserData[14],
                            GameTraxRecPtr^.UserData[15],
                            GameTraxRecPtr^.UserData[16],
                            GameTraxRecPtr^.UserData[17],
                            GameTraxRecPtr^.UserData[18],
                            GameTraxRecPtr^.UserData[19],
                            GameTraxRecPtr^.UserData[20],
                            GameTraxRecPtr^.UserData[21],
                            GameTraxRecPtr^.UserData[22],
                            GameTraxRecPtr^.UserData[23],
                            GameTraxRecPtr^.UserData[24],
                            GameTraxRecPtr^.UserData[25],
                            GameTraxRecPtr^.StartEnableDateTime,
                            GameTraxRecPtr^.EndEnableDateTime,
                            GameTraxRecPtr^.DwellTime,
                            GameTraxRecPtr^.Description,
                            GameTraxRecPtr^.Comments
                          ]);
                        end;
                      except
                        MessageDlg ('Error occurred while trying to insert Gametrax Ticker record into database.',
                                     mtError, [mbOk], 0);
                      end;
                      //Update label & array
                      PlaylistInfo[GameTrax].PlaylistName := Edit1.Text;
                      PlaylistNameLabel.Caption := Playlist_Description;
                    end;
                  except
                    MessageDlg ('Error occurred while trying to insert Gametrax Ticker playlist into database.',
                                 mtError, [mbOk], 0);
                  end;
                  //Clear out & pack the collection if enabled
                  if (ClearCollection) then
                  begin
                    GameTrax_Collection.Clear;
                    GameTrax_Collection.Pack;
                  end;
                end
                else begin
                  MessageDlg('You must specify one or more elements to save out a playlist.',
                             mtInformation, [mbOk], 0);
                end;
              end;
          end; //Case

          //Now, walk through scheduled playlists, and update time/datestamp for any matching entries
          //to insure refresh is picked up
          Case PlaylistType of
           //Ticker
           1: begin
                dmMain.tblScheduled_Ticker_Groups.Active := FALSE;
                dmMain.tblScheduled_Ticker_Groups.Active := TRUE;
                if (dmMain.tblScheduled_Ticker_Groups.RecordCount > 0) then
                begin
                  for i := 1 to dmMain.tblScheduled_Ticker_Groups.RecordCount do
                  begin
                    if (Playlist_ID = dmMain.tblScheduled_Ticker_Groups.FieldByName('Playlist_ID').AsFloat) then
                    begin
                      //Match was found, so update the timestamp
                      dmMain.tblScheduled_Ticker_Groups.Edit;
                      dmMain.tblScheduled_Ticker_Groups.FieldByName('Entry_Date').AsFloat := Now;
                      dmMain.tblScheduled_Ticker_Groups.Post;
                    end;
                  end;
                end;
              end;
           //Bug
           2: begin
                dmMain.tblScheduled_Bug_Groups.Active := FALSE;
                dmMain.tblScheduled_Bug_Groups.Active := TRUE;
                if (dmMain.tblScheduled_Bug_Groups.RecordCount > 0) then
                begin
                  for i := 1 to dmMain.tblScheduled_Bug_Groups.RecordCount do
                  begin
                    if (Playlist_ID = dmMain.tblScheduled_Bug_Groups.FieldByName('Playlist_ID').AsFloat) then
                    begin
                      //Match was found, so update the timestamp
                      dmMain.tblScheduled_Bug_Groups.Edit;
                      dmMain.tblScheduled_Bug_Groups.FieldByName('Entry_Date').AsFloat := Now;
                      dmMain.tblScheduled_Bug_Groups.Post;
                    end;
                  end;
                end;
              end;
           //Extra Line
           3: begin
                dmMain.tblScheduled_GameTrax_Groups.Active := FALSE;
                dmMain.tblScheduled_GameTrax_Groups.Active := TRUE;
                if (dmMain.tblScheduled_GameTrax_Groups.RecordCount > 0) then
                begin
                  for i := 1 to dmMain.tblScheduled_GameTrax_Groups.RecordCount do
                  begin
                    if (Playlist_ID = dmMain.tblScheduled_GameTrax_Groups.FieldByName('Playlist_ID').AsFloat) then
                    begin
                      //Match was found, so update the timestamp
                      dmMain.tblScheduled_GameTrax_Groups.Edit;
                      dmMain.tblScheduled_GameTrax_Groups.FieldByName('Entry_Date').AsFloat := Now;
                      dmMain.tblScheduled_GameTrax_Groups.Post;
                    end;
                  end;
                end;
              end;
          end;
          if (ClearCollection) then
          begin
            //Clear the edit boxes for playlist name
            Edit1.Text := '';
            PlaylistInfo[PlaylistType].PlaylistName := '';
            PlaylistNameLabel.Caption := '';
            LastSaveTimeLabel.Caption := '';
            PlaylistInfo[PlaylistType].LastPlaylistSaveTimeString :=
              LastSaveTimeLabel.Caption;
          end
          else begin
            LastSaveTimeLabel.Caption := DateTimeToStr(Now);
            PlaylistInfo[PlaylistType].LastPlaylistSaveTimeString :=
              LastSaveTimeLabel.Caption;
          end;
        end
        else begin
          //Clear flag
          Success := FALSE;
          MessageDlg('You must specify a Playlist Name/Description of at least four (4) characters ' +
                     'before committing to the database.',
                      mtInformation, [mbOk], 0);
        end
      end;
      //Refresh the grids
      RefreshPlaylistGrid;
      RefreshEntryFieldsGrid(CurrentTickerDisplayMode);
    end;
    //Restore cursor
    Screen.Cursor := Save_Cursor;
    //Set success value
    SaveTickerPlaylist := Success;
  end
  else //Set success value to failure
    SaveTickerPlaylist := Failure;
end;

//Handler to load zipper collection from database
procedure TMainForm.BitBtn24Click(Sender: TObject);
begin
  LoadZipperCollection;
end;
//General procedure to load a zipper collection; modifed to handle different playlist types
procedure TMainForm.LoadZipperCollection;
var
  Control, Control2: Word;
  OKToGo: Boolean;
  Modal: TPlaylistSelectDlg;
  SelectedPlaylistID: Double;
  CurrentPlaylistType: SmallInt;
  CollectionCount: SmallInt;
  Confirmed: Boolean;
begin
  //Reset the auto logout timer & init
  OKToGo := TRUE;
  if (OKToGo = TRUE) then
  begin
    //Display the dialog
    Modal := TPlaylistSelectDlg.Create(Application);
    try
      //Set intial values
      CurrentPlaylistType := PlaylistSelectTabControl.TabIndex+1;
      Modal.PlaylistType := CurrentPlaylistType;
      Control := Modal.ShowModal;
    finally
      //Init
      Confirmed := TRUE;
      //CLEAR AND LOAD NEW PLAYLIST
      if (Control = mrOK) then
      begin
        Case CurrentPlaylistType of
         1: CollectionCount := StudioTicker_Collection.Count;
         2: CollectionCount := StudioBug_Collection.Count;
         3: CollectionCount := GameTrax_Collection.Count;
        end;
        SelectedPlaylistID := Modal.SelectedPlaylistID;
        if (CollectionCount > 0) then
        begin
          Control2 := MessageDlg('There are currently entries in the playlist. ' +
            'Do you wish to save them before loading the new playlist?',
            mtWarning, [mbYes, mbNo], 0);
          //If operator confirmed, save the playlist out
          if (Control2 = mrYes) then Confirmed := SaveTickerPlaylist(CurrentPlaylistType, TRUE);
        end;
        //Load the new playlist as long as no existing entries or the old one save out OK
        if (Confirmed) then LoadPlaylistCollection (CurrentPlaylistType, 0, SelectedPlaylistID);
      end
      //APPEND PLAYLIST
      else if (Control= mrYes) then
      begin
        SelectedPlaylistID := Modal.SelectedPlaylistID;
        if (StudioTicker_Collection.Count > 0) then
        begin
          Control2 := MessageDlg('There are currently entries in the playlist. ' +
            'Do you wish to append the selected playlist to the existing playlist?',
            mtWarning, [mbYes, mbNo], 0);
          if (Control2 = mrYes) then
            LoadPlaylistCollection (CurrentPlaylistType, 1, SelectedPlaylistID);
        end
        else LoadPlaylistCollection (CurrentPlaylistType, 1, SelectedPlaylistID);
      end
      //INSERT PLAYLIST
      else if (Control= mrYesToAll) then
      begin
        SelectedPlaylistID := Modal.SelectedPlaylistID;
        if (StudioTicker_Collection.Count > 0) then
        begin
          Control2 := MessageDlg('There are currently entries in the playlist. ' +
            'Do you wish to insert the selected playlist into the existing playlist?',
            mtWarning, [mbYes, mbNo], 0);
          if (Control2 = mrYes) then
            LoadPlaylistCollection (CurrentPlaylistType, 2, SelectedPlaylistID);
        end
        else
          LoadPlaylistCollection (CurrentPlaylistType, 2, SelectedPlaylistID);
      end;
      //Free up the dialog
      Modal.Free;
    end;
  end;
end;

//Procedure to load default zipper collection from database
procedure TMainForm.LoadPlaylistCollection (PlaylistType: SmallInt; AppendMode: SmallInt; PlaylistID: Double);
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  BugRecPtr: ^BugRec;
  GameTraxRecPtr: ^GameTraxRec;
  SelectedPlaylistID: Double;
  InsertPoint: SmallInt;
  Save_Cursor: TCursor;
begin
  //Change cursor
  Save_Cursor := Screen.Cursor;
  //Show hourglass cursor
  Screen.Cursor := crHourGlass;
  Case AppendMode of
      //Clear and load new collection
   0: begin
        Case PlaylistType of
            //Studio Ticker
         1: begin
              //Clear the collection
              StudioTicker_Collection.Clear;
              StudioTicker_Collection.Pack;
              //Set controls to values from selected block
              SelectedPlaylistID := PlaylistID;
              dmMain.tblTicker_Groups.Locate('Playlist_ID', SelectedPlaylistID, []);
              Edit1.Text := dmMain.tblTicker_Groups.FieldByName('Playlist_Description').AsString;
              PlaylistInfo[PlaylistType].PlaylistName := Edit1.Text;
              PlaylistNameLabel.Caption := Edit1.Text;
              //Store playlist info
              //if (dmMain.tblTicker_Groups.FieldByName('Ticker_Mode').AsInteger = 1) then
              //  SetToSingleLineMode
              //else
              //Set to double line mode by default
              //SetToDoubleLineMode;
              //Load the collection from the database
              //Query for the elements table for those that match the playlist ID
              dmMain.Query1.Close;
              dmMain.Query1.SQL.Clear;
              dmMain.Query1.SQL.Add('SELECT * FROM Ticker_Elements ' +
                                       'WHERE Playlist_ID = ' + FloatToStr(SelectedPlaylistID));
              dmMain.Query1.Open;
              if (dmMain.Query1.RecordCount > 0) then

              //Iterate through all records in the table
              repeat
                GetMem (TickerRecPtr, SizeOf(TickerRec));
                With TickerRecPtr^ do
                begin
                  //Set values for collection record
                  Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                  Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                  Is_Child := dmMain.Query1.FieldByName('Is_Child').AsBoolean;
                  Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                  League := dmMain.Query1.FieldByName('League').AsString;
                  Subleague_Mnemonic_Standard := dmMain.Query1.FieldByName('Subleague_Mnemonic_Standard').AsString;
                  Mnemonic_LiveEvent := dmMain.Query1.FieldByName('Mnemonic_LiveEvent').AsString;
                  Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                  Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                  SI_GCode := dmMain.Query1.FieldByName('SI_GCode').AsString;
                  ST_Game_ID := dmMain.Query1.FieldByName('ST_Game_ID').AsString;
                  CSS_GameID := dmMain.Query1.FieldByName('CSS_GameID').AsInteger;
                  NFLDM_GameID := dmMain.Query1.FieldByName('NFLDM_GameID').AsInteger;
                  Primary_Data_Source := dmMain.Query1.FieldByName('Primary_Data_Source').AsString;
                  Backup_Data_Source := dmMain.Query1.FieldByName('Backup_Data_Source').AsString;
                  SponsorLogo_Name := dmMain.Query1.FieldByName('SponsorLogoName').AsString;
                  SponsorLogo_Dwell := dmMain.Query1.FieldByName('SponsorLogoDwell').AsInteger;
                  SponsorLogo_Region := dmMain.Query1.FieldByName('SponsorLogoRegion').AsInteger;
                  StatStoredProcedure := dmMain.Query1.FieldByName('StatStoredProcedure').AsString;
                  StatType := dmMain.Query1.FieldByName('StatType').AsInteger;
                  StatTeam := dmMain.Query1.FieldByName('StatTeam').AsString;
                  StatLeague := dmMain.Query1.FieldByName('StatLeague').AsString;
                  StatRecordNumber := dmMain.Query1.FieldByName('StatRecordNumber').AsInteger;
                  //Load the user-defined text fields
                  for i := 1 to 25 do
                    UserData[i] := dmMain.Query1.FieldByName('UserData_' + IntToStr(i)).AsString;
                  StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                  EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                  DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                  Description := dmMain.Query1.FieldByName('Description').AsString;
                  Selected := FALSE;
                  Comments := dmMain.Query1.FieldByName('Comments').AsString;
                  //Insert the object
                  StudioTicker_Collection.Insert(TickerRecPtr);
                  StudioTicker_Collection.Pack;
                  //Go to next record in query
                  dmMain.Query1.Next;
                end;
              until (dmMain.Query1.EOF = TRUE);
            end;
            //Studio Bug
         2: begin
              //Clear the collection
              StudioBug_Collection.Clear;
              StudioBug_Collection.Pack;
              //Set controls to values from selected block
              SelectedPlaylistID := PlaylistID;
              dmMain.tblBug_Groups.Locate('Playlist_ID', SelectedPlaylistID, []);
              Edit1.Text := dmMain.tblBug_Groups.FieldByName('Playlist_Description').AsString;
              PlaylistInfo[PlaylistType].PlaylistName := Edit1.Text;
              PlaylistNameLabel.Caption := Edit1.Text;
              //Store playlist info
              //Load the collection from the database
              //Query for the elements table for those that match the playlist ID
              dmMain.Query1.Close;
              dmMain.Query1.SQL.Clear;
              dmMain.Query1.SQL.Add('SELECT * FROM Bug_Elements ' +
                                       'WHERE Playlist_ID = ' + FloatToStr(SelectedPlaylistID));
              dmMain.Query1.Open;
              if (dmMain.Query1.RecordCount > 0) then

              //Iterate through all records in the table
              repeat
                GetMem (BugRecPtr, SizeOf(BugRec));
                With BugRecPtr^ do
                begin
                  //Set values for collection record
                  Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                  Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                  Is_Child := dmMain.Query1.FieldByName('Is_Child').AsBoolean;
                  Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                  League := dmMain.Query1.FieldByName('League').AsString;
                  Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                  Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                  SI_GCode := dmMain.Query1.FieldByName('SI_GCode').AsString;
                  ST_Game_ID := dmMain.Query1.FieldByName('ST_Game_ID').AsString;
                  CSS_GameID := dmMain.Query1.FieldByName('CSS_GameID').AsInteger;
                  NFLDM_GameID := dmMain.Query1.FieldByName('NFLDM_GameID').AsInteger;
                  Primary_Data_Source := dmMain.Query1.FieldByName('Primary_Data_Source').AsString;
                  Backup_Data_Source := dmMain.Query1.FieldByName('Backup_Data_Source').AsString;
                  //Load the user-defined text fields
                  for i := 1 to 25 do
                    UserData[i] := dmMain.Query1.FieldByName('UserData_' + IntToStr(i)).AsString;
                  StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                  EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                  DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                  Description := dmMain.Query1.FieldByName('Description').AsString;
                  Selected := FALSE;
                  Comments := dmMain.Query1.FieldByName('Comments').AsString;
                  //Insert the object
                  StudioBug_Collection.Insert(BugRecPtr);
                  StudioBug_Collection.Pack;
                  //Go to next record in query
                  dmMain.Query1.Next;
                end;
              until (dmMain.Query1.EOF = TRUE);
            end;
            //GameTrax
         3: begin
              //Clear the collection
              GameTrax_Collection.Clear;
              GameTrax_Collection.Pack;
              //Set controls to values from selected block
              SelectedPlaylistID := PlaylistID;
              dmMain.tblGameTrax_Groups.Locate('Playlist_ID', SelectedPlaylistID, []);
              Edit1.Text := dmMain.tblGameTrax_Groups.FieldByName('Playlist_Description').AsString;
              PlaylistInfo[PlaylistType].PlaylistName := Edit1.Text;
              PlaylistNameLabel.Caption := Edit1.Text;
              //Store playlist info
              //Load the collection from the database
              //Query for the elements table for those that match the playlist ID
              dmMain.Query1.Close;
              dmMain.Query1.SQL.Clear;
              dmMain.Query1.SQL.Add('SELECT * FROM GameTrax_Elements ' +
                                       'WHERE Playlist_ID = ' + FloatToStr(SelectedPlaylistID));
              dmMain.Query1.Open;
              if (dmMain.Query1.RecordCount > 0) then

              //Iterate through all records in the table
              repeat
                GetMem (GameTraxRecPtr, SizeOf(GameTraxRec));
                With GameTraxRecPtr^ do
                begin
                  //Set values for collection record
                  Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                  Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                  Is_Child := dmMain.Query1.FieldByName('Is_Child').AsBoolean;
                  Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                  League := dmMain.Query1.FieldByName('League').AsString;
                  Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                  Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                  SI_GCode := dmMain.Query1.FieldByName('SI_GCode').AsString;
                  ST_Game_ID := dmMain.Query1.FieldByName('ST_Game_ID').AsString;
                  CSS_GameID := dmMain.Query1.FieldByName('CSS_GameID').AsInteger;
                  NFLDM_GameID := dmMain.Query1.FieldByName('NFLDM_GameID').AsInteger;
                  Primary_Data_Source := dmMain.Query1.FieldByName('Primary_Data_Source').AsString;
                  Backup_Data_Source := dmMain.Query1.FieldByName('Backup_Data_Source').AsString;
                  SponsorLogo_Name := dmMain.Query1.FieldByName('SponsorLogoName').AsString;
                  SponsorLogo_Dwell := dmMain.Query1.FieldByName('SponsorLogoDwell').AsInteger;
                  SponsorLogo_Region := dmMain.Query1.FieldByName('SponsorLogoRegion').AsInteger;
                  //Load the user-defined text fields
                  for i := 1 to 25 do
                    UserData[i] := dmMain.Query1.FieldByName('UserData_' + IntToStr(i)).AsString;
                  StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                  EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                  DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                  Description := dmMain.Query1.FieldByName('Description').AsString;
                  Selected := FALSE;
                  Comments := dmMain.Query1.FieldByName('Comments').AsString;
                  //Insert the object
                  GameTrax_Collection.Insert(GameTraxRecPtr);
                  GameTrax_Collection.Pack;
                  //Go to next record in query
                  dmMain.Query1.Next;
                end;
              until (dmMain.Query1.EOF = TRUE);
            end;
        end;
        //Refresh the grid
        RefreshPlaylistGrid;
        //Refresh the field entries grid
        RefreshEntryFieldsGrid(CurrentTickerDisplayMode);
      end;
      //Append or insert to existing collection
   1,2: begin
        Case PlaylistType of
            //Studio Ticker
         1: begin
              if (dmMain.tblTicker_Groups.RecordCount > 0) then
              begin
                //Set controls to values from selected block
                SelectedPlaylistID := PlaylistID;
                dmMain.tblTicker_Groups.Locate('Playlist_ID', SelectedPlaylistID, []);
                Edit1.Text := dmMain.tblTicker_Groups.FieldByName('Playlist_Description').AsString;
                PlaylistInfo[PlaylistType].PlaylistName := Edit1.Text;
                PlaylistNameLabel.Caption := Edit1.Text;
                //Load the collection from the database
                //Query for the elements table for those that match the playlist ID
                dmMain.Query1.Close;
                dmMain.Query1.SQL.Clear;
                dmMain.Query1.SQL.Add('SELECT * FROM Ticker_Elements ' +
                                         'WHERE Playlist_ID = ' + FloatToStr(SelectedPlaylistID));
                dmMain.Query1.Open;
                if (dmMain.Query1.RecordCount > 0) then

                //Iterate through all records in the table
                repeat
                  GetMem (TickerRecPtr, SizeOf(TickerRec));
                  With TickerRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                    Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                    Is_Child := dmMain.Query1.FieldByName('Is_Child').AsBoolean;
                    Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                    League := dmMain.Query1.FieldByName('League').AsString;
                    Subleague_Mnemonic_Standard := dmMain.Query1.FieldByName('Subleague_Mnemonic_Standard').AsString;
                    Mnemonic_LiveEvent := dmMain.Query1.FieldByName('Mnemonic_LiveEvent').AsString;
                    Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                    Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                    SI_GCode := dmMain.Query1.FieldByName('SI_GCode').AsString;
                    ST_Game_ID := dmMain.Query1.FieldByName('ST_Game_ID').AsString;
                    CSS_GameID := dmMain.Query1.FieldByName('CSS_GameID').AsInteger;
                    NFLDM_GameID := dmMain.Query1.FieldByName('NFLDM_GameID').AsInteger;
                    Primary_Data_Source := dmMain.Query1.FieldByName('Primary_Data_Source').AsString;
                    Backup_Data_Source := dmMain.Query1.FieldByName('Backup_Data_Source').AsString;
                    SponsorLogo_Name := dmMain.Query1.FieldByName('SponsorLogoName').AsString;
                    SponsorLogo_Dwell := dmMain.Query1.FieldByName('SponsorLogoDwell').AsInteger;
                    SponsorLogo_Region := dmMain.Query1.FieldByName('SponsorLogoRegion').AsInteger;
                    StatStoredProcedure := dmMain.Query1.FieldByName('StatStoredProcedure').AsString;
                    StatType := dmMain.Query1.FieldByName('StatType').AsInteger;
                    StatTeam := dmMain.Query1.FieldByName('StatTeam').AsString;
                    StatLeague := dmMain.Query1.FieldByName('StatLeague').AsString;
                    StatRecordNumber := dmMain.Query1.FieldByName('StatRecordNumber').AsInteger;
                    //Load the user-defined text fields
                    for i := 1 to 25 do
                      UserData[i] := dmMain.Query1.FieldByName('UserData_' + IntToStr(i)).AsString;
                    StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                    EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                    DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                    Description := dmMain.Query1.FieldByName('Description').AsString;
                    Selected := FALSE;
                    Comments := dmMain.Query1.FieldByName('Comments').AsString;
                    //Insert the object
                    if (PlaylistGrid.CurrentDataRow < 1) then PlaylistGrid.CurrentDataRow := 1;
                    InsertPoint := PlaylistGrid.CurrentDataRow-1;
                    if (AppendMode = 1) then
                      StudioTicker_Collection.Insert(TickerRecPtr)
                    else if (AppendMode = 2) then
                      StudioTicker_Collection.AtInsert(InsertPoint, TickerRecPtr);
                    StudioTicker_Collection.Pack;
                    //Go to next record in query
                    dmMain.Query1.Next;
                  end;
                until (dmMain.Query1.EOF = TRUE);
                //Refresh the grid
                RefreshPlaylistGrid;
              end;
            end;
            //Studio Bug
         2: begin
              if (dmMain.tblBug_Groups.RecordCount > 0) then
              begin
                //Set controls to values from selected block
                SelectedPlaylistID := PlaylistID;
                dmMain.tblBug_Groups.Locate('Playlist_ID', SelectedPlaylistID, []);
                Edit1.Text := dmMain.tblBug_Groups.FieldByName('Playlist_Description').AsString;
                PlaylistInfo[PlaylistType].PlaylistName := Edit1.Text;
                PlaylistNameLabel.Caption := Edit1.Text;
                //Load the collection from the database
                //Query for the elements table for those that match the playlist ID
                dmMain.Query1.Close;
                dmMain.Query1.SQL.Clear;
                dmMain.Query1.SQL.Add('SELECT * FROM Bug_Elements ' +
                                         'WHERE Playlist_ID = ' + FloatToStr(SelectedPlaylistID));
                dmMain.Query1.Open;
                if (dmMain.Query1.RecordCount > 0) then

                //Iterate through all records in the table
                repeat
                  GetMem (BugRecPtr, SizeOf(BugRec));
                  With BugRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                    Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                    Is_Child := dmMain.Query1.FieldByName('Is_Sublist').AsBoolean;
                    Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                    League := dmMain.Query1.FieldByName('League').AsString;
                    Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                    Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                    SI_GCode := dmMain.Query1.FieldByName('SI_GCode').AsString;
                    ST_Game_ID := dmMain.Query1.FieldByName('ST_Game_ID').AsString;
                    CSS_GameID := dmMain.Query1.FieldByName('CSS_GameID').AsInteger;
                    NFLDM_GameID := dmMain.Query1.FieldByName('NFLDM_GameID').AsInteger;
                    Primary_Data_Source := dmMain.Query1.FieldByName('Primary_Data_Source').AsString;
                    Backup_Data_Source := dmMain.Query1.FieldByName('Backup_Data_Source').AsString;
                    //Load the user-defined text fields
                    for i := 1 to 25 do
                      UserData[i] := dmMain.Query1.FieldByName('UserData_' + IntToStr(i)).AsString;
                    StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                    EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                    DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                    Description := dmMain.Query1.FieldByName('Description').AsString;
                    Selected := FALSE;
                    Comments := dmMain.Query1.FieldByName('Comments').AsString;
                    //Insert the object
                    if (PlaylistGrid.CurrentDataRow < 1) then PlaylistGrid.CurrentDataRow := 1;
                    InsertPoint := PlaylistGrid.CurrentDataRow-1;
                    if (AppendMode = 1) then
                      StudioBug_Collection.Insert(BugRecPtr)
                    else if (AppendMode = 2) then
                      StudioBug_Collection.AtInsert(InsertPoint, BugRecPtr);
                    StudioBug_Collection.Pack;
                    //Go to next record in query
                    dmMain.Query1.Next;
                  end;
                until (dmMain.Query1.EOF = TRUE);          //Refresh the grid
                RefreshPlaylistGrid;
              end;
            end;
            //GameTrax
         3: begin
              if (dmMain.tblGameTrax_Groups.RecordCount > 0) then
              begin
                //Set controls to values from selected block
                SelectedPlaylistID := PlaylistID;
                dmMain.tblGameTrax_Groups.Locate('Playlist_ID', SelectedPlaylistID, []);
                Edit1.Text := dmMain.tblGameTrax_Groups.FieldByName('Playlist_Description').AsString;
                PlaylistInfo[PlaylistType].PlaylistName := Edit1.Text;
                PlaylistNameLabel.Caption := Edit1.Text;
                //Load the collection from the database
                //Query for the elements table for those that match the playlist ID
                dmMain.Query1.Close;
                dmMain.Query1.SQL.Clear;
                dmMain.Query1.SQL.Add('SELECT * FROM GameTrax_Elements ' +
                                         'WHERE Playlist_ID = ' + FloatToStr(SelectedPlaylistID));
                dmMain.Query1.Open;
                if (dmMain.Query1.RecordCount > 0) then

                //Iterate through all records in the table
                repeat
                  GetMem (GameTraxRecPtr, SizeOf(GameTraxRec));
                  With GameTraxRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                    Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                    Is_Child := dmMain.Query1.FieldByName('Is_Child').AsBoolean;
                    Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                    League := dmMain.Query1.FieldByName('League').AsString;
                    Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                    Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                    SI_GCode := dmMain.Query1.FieldByName('SI_GCode').AsString;
                    ST_Game_ID := dmMain.Query1.FieldByName('ST_Game_ID').AsString;
                    CSS_GameID := dmMain.Query1.FieldByName('CSS_GameID').AsInteger;
                    NFLDM_GameID := dmMain.Query1.FieldByName('NFLDM_GameID').AsInteger;
                    Primary_Data_Source := dmMain.Query1.FieldByName('Primary_Data_Source').AsString;
                    Backup_Data_Source := dmMain.Query1.FieldByName('Backup_Data_Source').AsString;
                    //Load the user-defined text fields
                    SponsorLogo_Name := dmMain.Query1.FieldByName('SponsorLogoName').AsString;
                    SponsorLogo_Dwell := dmMain.Query1.FieldByName('SponsorLogoDwell').AsInteger;
                    SponsorLogo_Region := dmMain.Query1.FieldByName('SponsorLogoRegion').AsInteger;
                    for i := 1 to 25 do
                      UserData[i] := dmMain.Query1.FieldByName('UserData_' + IntToStr(i)).AsString;
                    StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                    EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                    DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                    Description := dmMain.Query1.FieldByName('Description').AsString;
                    Selected := FALSE;
                    Comments := dmMain.Query1.FieldByName('Comments').AsString;
                    //Insert the object
                    if (PlaylistGrid.CurrentDataRow < 1) then PlaylistGrid.CurrentDataRow := 1;
                    InsertPoint := PlaylistGrid.CurrentDataRow-1;
                    if (AppendMode = 1) then
                      GameTrax_Collection.Insert(GameTraxRecPtr)
                    else if (AppendMode = 2) then
                      GameTrax_Collection.AtInsert(InsertPoint, GameTraxRecPtr);
                    GameTrax_Collection.Pack;
                    //Go to next record in query
                    dmMain.Query1.Next;
                  end;
                until (dmMain.Query1.EOF = TRUE);
                //Refresh the grid
                RefreshPlaylistGrid;
              end;
            end;
        end;
      end;
  end;
  //Restore default cursor
  Screen.Cursor := Save_Cursor;
end;

//Handler to refresh the entries grid when a new playlist record is selected
procedure TMainForm.PlaylistGridRowChanged(Sender: TObject; OldRow,
  NewRow: Integer);
begin
  if (PlaylistGrid.Rows > 0) then RefreshEntryFieldsGrid(CurrentTickerDisplayMode);
end;
//Procedure to update the entry fields grid with the data for the currently selected
//record in the playlist; also updates other entry specific data entry controls
procedure TMainForm.RefreshEntryFieldsGrid (CurrentTickerMode: SmallInt);
var
  i, j, k: SmallInt;
  TickerRecPtr: ^TickerRec;
  BugRecPtr: ^BugRec;
  GameTraxRecPtr: ^GameTraxRec;
  CurrentTemplateID: SmallInt;
  NumTemplateFields: SmallInt;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CollectionCount: SmallInt;
  UserDataBias: SmallInt;
  TemplateInfo: TemplateDefsRec;
  CurrentGameData: GameRec;
begin
  Case PlaylistSelectTabControl.TabIndex of
   0: CollectionCount := StudioTicker_Collection.Count;
   1: CollectionCount := StudioBug_Collection.Count;
   2: CollectionCount := GameTrax_Collection.Count;
  end;
  //Get data for current record in grid
  if (CollectionCount > 0) then
  begin
    Case PlaylistSelectTabControl.TabIndex of
        //Ticker
     0: begin
          TickerRecPtr := StudioTicker_Collection.At(PlaylistGrid.CurrentDataRow-1);
          CurrentTemplateID := TickerRecPtr^.Template_ID;
        end;
        //Bug
     1: begin
          BugRecPtr := StudioBug_Collection.At(PlaylistGrid.CurrentDataRow-1);
          CurrentTemplateID := BugRecPtr^.Template_ID;
        end;
        //Extra line
     2: begin
          GameTraxRecPtr := GameTrax_Collection.At(PlaylistGrid.CurrentDataRow-1);
          CurrentTemplateID := GameTraxRecPtr^.Template_ID;
        end;
    end;
    TemplateDescription.Caption := GetTemplateInformation(CurrentTemplateID).Template_Description;
    NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);
    if (NumTemplateFields > 0) then
    begin
      //Clear grid values
      if (EntryFieldsGrid.Rows > 0) then
        EntryFieldsGrid.DeleteRows (1, EntryFieldsGrid.Rows);
      EntryFieldsGrid.StoreData := TRUE;
      EntryFieldsGrid.Cols := 3;
      EntryFieldsGrid.Rows := NumTemplateFields;
      //Populate the grid
      i := 0;
      k := 0;
      //Get game data if template requires it
      Case PlaylistSelectTabControl.TabIndex of
          //Ticker
       0: begin
            TemplateInfo := EngineInterface.LoadTempTemplateFields(TickerRecPtr^.Template_ID);
            if (TemplateInfo.UsesGameData) then
            begin
              //Use full game data
              CurrentGameData := EngineInterface.GetGameData(TickerRecPtr^.League, TickerRecPtr^.SI_GCode,
                TickerRecPtr^.ST_Game_ID, TickerRecPtr^.CSS_GameID, TickerRecPtr^.NFLDM_GameID,
                TickerRecPtr^.Primary_Data_Source, TickerRecPtr^.Backup_Data_Source);
            end;
          end;
          //Bug
       1: begin
            TemplateInfo := EngineInterface.LoadTempTemplateFields(BugRecPtr^.Template_ID);
            if (TemplateInfo.UsesGameData) then
            begin
              //Use full game data
              CurrentGameData := EngineInterface.GetGameData(BugRecPtr^.League, BugRecPtr^.SI_GCode,
                BugRecPtr^.ST_Game_ID, BugRecPtr^.CSS_GameID, BugRecPtr^.NFLDM_GameID,
                BugRecPtr^.Primary_Data_Source, BugRecPtr^.Backup_Data_Source);
            end;
          end;
          //Gametrax
       2: begin
            TemplateInfo := EngineInterface.LoadTempTemplateFields(GameTraxRecPtr^.Template_ID);
            if (TemplateInfo.UsesGameData) then
            begin
              //Use full game data
              CurrentGameData := EngineInterface.GetGameData(GametraxRecPtr^.League, GametraxRecPtr^.SI_GCode,
                GametraxRecPtr^.ST_Game_ID, GametraxRecPtr^.CSS_GameID, GametraxRecPtr^.NFLDM_GameID,
                GametraxRecPtr^.Primary_Data_Source, GametraxRecPtr^.Backup_Data_Source);
            end;
          end;
      end;
      //Get the manually entered fields
      for j := 0 to Template_Fields_Collection.Count-1 do
      begin
        TemplateFieldsRecPtr := Template_Fields_Collection.At(j);
        if (TemplateFieldsRecPtr^.Template_ID = CurrentTemplateID) then
        begin
          EntryFieldsGrid.Cell[1,i+1] := TemplateFieldsRecPtr^.Field_ID; //Index
          EntryFieldsGrid.Cell[2,i+1] := TemplateFieldsRecPtr^.Field_Label; //Description
          if (TemplateFieldsRecPtr^.Field_Is_Manual = TRUE) then
          begin
            //Set bias
            Case CurrentTickerDisplayMode of
               //Standard mode
             -1: UserDataBias := 0;
               //Live mode
              1: UserDataBias := 25;
            end;
            EntryFieldsGrid.RowColor[i+1] := clWindow;
            Case PlaylistSelectTabControl.TabIndex of
                //Ticker
             0: begin
                  //Insert league mnemonic and apply offset to account for league value being
                  //stored explicitly in ticker record
                  if (TemplateFieldsRecPtr^.Field_Type = 8) then
                  begin
                    EntryFieldsGrid.Cell[3,i+1] := TickerRecPtr^.Subleague_Mnemonic_Standard;
                    Dec(k);
                  end
                  else
                    EntryFieldsGrid.Cell[3,i+1] := TickerRecPtr^.UserData[UserDataBias + k+1];
                end;
                //Bug
             1: EntryFieldsGrid.Cell[3,i+1] := BugRecPtr^.UserData[k+1];
                //Extra line
             2: EntryFieldsGrid.Cell[3,i+1] := GameTraxRecPtr^.UserData[k+1];
            end;
            Inc(k);
          end
          else begin
            //Set cell value
            Case PlaylistSelectTabControl.TabIndex of
                //Ticker
             0: begin
                  //Get game data if template requires it
                  EntryFieldsGrid.Cell[3,i+1] :=
                    EngineInterface.GetValueOfSymbol(TICKER, TemplateFieldsRecPtr^.Field_Contents, TemplateFieldsRecPtr^.Field_Type,
                      PlaylistGrid.CurrentDataRow-1, CurrentGameData, CurrentTickerDisplayMode);
                end;
                //Bug
             1: begin
                  EntryFieldsGrid.Cell[3,i+1] :=
                    EngineInterface.GetValueOfSymbol(BUG, TemplateFieldsRecPtr^.Field_Contents, TemplateFieldsRecPtr^.Field_Type,
                      PlaylistGrid.CurrentDataRow-1, CurrentGameData, CurrentTickerDisplayMode);
                end;
                //Extra line
             2: begin
                  EntryFieldsGrid.Cell[3,i+1] :=
                    EngineInterface.GetValueOfSymbol(GAMETRAX, TemplateFieldsRecPtr^.Field_Contents, TemplateFieldsRecPtr^.Field_Type,
                      PlaylistGrid.CurrentDataRow-1, CurrentGameData, CurrentTickerDisplayMode);
                end;
            end;
            EntryFieldsGrid.RowColor[i+1] := clAqua;
          end;
          //Increment the manual fields counter
          Inc (i);
        end;
      end;
    end;
    //Update other fields
    Case PlaylistSelectTabControl.TabIndex of
        //Ticker
     0: begin
          EntryNote.Text := TickerRecPtr^.Comments;
          EntryEnable.Checked := TickerRecPtr^.Enabled;
          EntryStartEnableDate.Date := TickerRecPtr^.StartEnableDateTime;
          EntryStartEnableTime.Time := TickerRecPtr^.StartEnableDateTime;
          EntryEndEnableDate.Date := TickerRecPtr^.EndEnableDateTime;
          EntryEndEnableTime.Time := TickerRecPtr^.EndEnableDateTime;
          EntryDwellTime.Value := Trunc(TickerRecPtr^.DwellTime/1000);
        end;
        //Bug
     1: begin
          EntryNote.Text := BugRecPtr^.Comments;
          EntryEnable.Checked := BugRecPtr^.Enabled;
          EntryStartEnableDate.Date := BugRecPtr^.StartEnableDateTime;
          EntryStartEnableTime.Time := BugRecPtr^.StartEnableDateTime;
          EntryEndEnableDate.Date := BugRecPtr^.EndEnableDateTime;
          EntryEndEnableTime.Time := BugRecPtr^.EndEnableDateTime;
          EntryDwellTime.Value := Trunc(BugRecPtr^.DwellTime/1000);
        end;
        //Extra line
     2: begin
          EntryNote.Text := GameTraxRecPtr^.Comments;
          EntryEnable.Checked := GameTraxRecPtr^.Enabled;
          EntryStartEnableDate.Date := GameTraxRecPtr^.StartEnableDateTime;
          EntryStartEnableTime.Time := GameTraxRecPtr^.StartEnableDateTime;
          EntryEndEnableDate.Date := GameTraxRecPtr^.EndEnableDateTime;
          EntryEndEnableTime.Time := GameTraxRecPtr^.EndEnableDateTime;
          EntryDwellTime.Value := Trunc(GameTraxRecPtr^.DwellTime/1000);
        end;
    end;
  end
  else begin
    if (EntryFieldsGrid.Rows > 0) then
      EntryFieldsGrid.DeleteRows (1, EntryFieldsGrid.Rows);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// PLAYLIST SCHEDULE FUNCTIONS - TICKER, BUGS, EXTRA LINE
////////////////////////////////////////////////////////////////////////////////
//Handler for adding a ticker playlist to the ticker schedule
procedure TMainForm.BitBtn42Click(Sender: TObject);
begin
  AddScheduleEntry;
end;
procedure TMainForm.AddScheduleEntry;
var
  EncodedStartEnableTime: TDateTime;
  EncodedEndEnableTime: TDateTime;
  FoundMatchingPlaylist: Boolean;
  Control: Word;
  OKToGo: Boolean;
  OverlapCount: SmallInt;
  EncodedStartEnableTimeStr: String;
  EncodedEndEnableTimeStr: String;
  CurrentRecordCount: SmallInt;
  QueryStr: String;
begin
  begin
    //Check to see if a playlist with a matching ID exists. If so, prompt operator
    //for overwrite confirmation
    OKToGo := TRUE;

    //Set the record count based on the playlist type selected
    Case PlaylistSelectTabControl.TabIndex of
         //Ticker
      0: CurrentRecordCount := dmMain.tblTicker_Groups.RecordCount;
         //Bug
      1: CurrentRecordCount := dmMain.tblBug_Groups.RecordCount;
         //Extra line
      2: CurrentRecordCount := dmMain.tblGameTrax_Groups.RecordCount;
    end;

    //Only proceed if at least playlist in database
    if (CurrentRecordCount > 0) then
    begin
      try
        //Encode start and end enable times
        EncodedStartEnableTime := Trunc(ScheduleEntryStartDate.Date) +
          (ScheduleEntryStartTime.Time - Trunc(ScheduleEntryStartTime.Time));
        EncodedEndEnableTime := Trunc(ScheduleEntryEndDate.Date) +
          (ScheduleEntryEndTime.Time - Trunc(ScheduleEntryEndTime.Time));

        //Make sure end time is greater than start time
        if (EncodedStartEnableTime >= EncodedEndEnableTime) then
        begin
          OKToGo := FALSE;
          MessageDlg('You must enter an End Enable Time that occurs AFTER the Start Enable Time',
                     mtError, [mbOK], 0);
        end;

        //Check for matching start time in schedule
        Case PlaylistSelectTabControl.TabIndex of
            //Ticker
         0: FoundMatchingPlaylist :=
              dmMain.tblScheduled_Ticker_Groups.Locate('Start_Enable_Time', EncodedStartEnableTime, []);
            //Bug
         1: FoundMatchingPlaylist :=
              dmMain.tblScheduled_Bug_Groups.Locate('Start_Enable_Time', EncodedStartEnableTime, []);
            //Extra line
         2: FoundMatchingPlaylist :=
              dmMain.tblScheduled_GameTrax_Groups.Locate('Start_Enable_Time', EncodedStartEnableTime, []);
        end;
        if (FoundMatchingPlaylist = TRUE) then
        begin
          Control := MessageDlg('A playlist with the same start enable time was found in the ' +
                                'database. Do you wish to overwrite it?', mtConfirmation,
                                [mbYes, mbNo], 0);
          if (Control = mrYes) then
          begin
            Case PlaylistSelectTabControl.TabIndex of
                //Ticker
             0: DeletePlaylistFromSchedule(1, dmMain.tblScheduled_Ticker_Groups.FieldByName('Start_Enable_Time').AsFloat);
                //Bug
             1: DeletePlaylistFromSchedule(2, dmMain.tblScheduled_Bug_Groups.FieldByName('Start_Enable_Time').AsFloat);
                //Extra line
             2: DeletePlaylistFromSchedule(3, dmMain.tblScheduled_GameTrax_Groups.FieldByName('Start_Enable_Time').AsFloat);
            end;
          end
          else OKToGo := FALSE;
        end;

        //Check to make sure there are no overlapping tickers; if so, alert operator and promot for continue
        if (OKToGo = TRUE) then
        begin
          EncodedStartEnableTimeStr := DateTimeToStr(EncodedStartEnableTime);
          EncodedEndEnableTimeStr := DateTimeToStr(EncodedEndEnableTime);
          dmMain.Query1.Close;
          dmMain.Query1.SQL.Clear;
          //Set the query string
          Case PlaylistSelectTabControl.TabIndex of
              //Ticker
           0: QueryStr := 'SELECT * FROM Scheduled_Ticker_Groups WHERE ' +
                'NOT (((' + QuotedStr(EncodedStartEnableTimeStr) + '> Start_Enable_Time) AND (' +
                            QuotedStr(EncodedStartEnableTimeStr) + '> End_Enable_Time)) OR ' +
                     '((' + QuotedStr(EncodedEndEnableTimeStr) + '< Start_Enable_Time) AND (' +
                            QuotedStr(EncodedEndEnableTimeStr) + '< End_Enable_Time)))';
              //Bug
           1: QueryStr := 'SELECT * FROM Scheduled_Bug_Groups WHERE ' +
                'NOT (((' + QuotedStr(EncodedStartEnableTimeStr) + '> Start_Enable_Time) AND (' +
                            QuotedStr(EncodedStartEnableTimeStr) + '> End_Enable_Time)) OR ' +
                     '((' + QuotedStr(EncodedEndEnableTimeStr) + '< Start_Enable_Time) AND (' +
                            QuotedStr(EncodedEndEnableTimeStr) + '< End_Enable_Time)))';
              //Extra line
           2: QueryStr := 'SELECT * FROM Scheduled_GameTrax_Groups WHERE ' +
                'NOT (((' + QuotedStr(EncodedStartEnableTimeStr) + '> Start_Enable_Time) AND (' +
                            QuotedStr(EncodedStartEnableTimeStr) + '> End_Enable_Time)) OR ' +
                     '((' + QuotedStr(EncodedEndEnableTimeStr) + '< Start_Enable_Time) AND (' +
                            QuotedStr(EncodedEndEnableTimeStr) + '< End_Enable_Time)))';
          end;
          dmMain.Query1.SQL.Add(QueryStr);
          dmMain.Query1.Open;
          //Get the count
          OverlapCount := dmMain.Query1.RecordCount;
          //Close the query and return
          dmMain.Query1.Close;
          //Alert if any overlapping playlists were found
          if (OverlapCount > 0) then
          begin
            Control := MessageDlg('One or more playlists were found in the database that overlap all or part ' +
                                  'of the time window specified for this playlist. Do you wish to schedule this ' +
                                  'new playlist anyway?', mtWarning, [mbYes, mbNo], 0);
            if (Control = mrNo) then OKToGo := FALSE;
          end;
        end;

        //If no problems, append the record
        if (OKToGo) then
        begin
          Case PlaylistSelectTabControl.TabIndex of
              //Ticker
           0: begin
                //Append new record for playlist
                dmMain.tblScheduled_Ticker_Groups.AppendRecord ([
                  dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsFloat, //Playlist ID
                  dmMain.tblTicker_Groups.FieldByName('Playlist_Description').AsString, //Playlist ID
                  EncodedStartEnableTime, //Start date/time
                  EncodedEndEnableTime, //End date/time
                  dmMain.tblTicker_Groups.FieldByName('Ticker_Mode').AsInteger, //1-Line/2-Line ticker mode
                  Now, //Creation date/timestamp
                  'N/A' //Operator name
                ]);
                //Refresh the table
                RefreshScheduleGrid;
              end;
              //Bug
           1: begin
                //Append new record for playlist
                dmMain.tblScheduled_Bug_Groups.AppendRecord ([
                  dmMain.tblBug_Groups.FieldByName('Playlist_ID').AsFloat, //Playlist ID
                  dmMain.tblBug_Groups.FieldByName('Playlist_Description').AsString, //Playlist ID
                  EncodedStartEnableTime, //Start date/time
                  EncodedEndEnableTime, //End date/time
                  dmMain.tblBug_Groups.FieldByName('Bug_Mode').AsInteger,
                  Now, //Creation date/timestamp
                  'N/A' //Operator name
                ]);
                //Refresh the table
                RefreshScheduleGrid;
              end;
              //Gametrax
           2: begin
                //Append new record for playlist
                dmMain.tblScheduled_GameTrax_Groups.AppendRecord ([
                  dmMain.tblGameTrax_Groups.FieldByName('Playlist_ID').AsFloat, //Playlist ID
                  dmMain.tblGameTrax_Groups.FieldByName('Playlist_Description').AsString, //Playlist ID
                  EncodedStartEnableTime, //Start date/time
                  EncodedEndEnableTime, //End date/time
                  dmMain.tblGameTrax_Groups.FieldByName('GameTrax_Mode').AsInteger,
                  Now, //Creation date/timestamp
                  'N/A' //Operator name
                ]);
                //Refresh the table
                RefreshScheduleGrid;
              end;
          end;
        end;
      except
        MessageDlg ('Error occurred while trying to insert playlist into schedule database.',
                     mtError, [mbOk], 0);
      end;
    end
    else begin
      MessageDlg('There must be at least one playlist in the database to save out to the schedule.',
                 mtInformation, [mbOk], 0);
    end;
  end;
end;

//Procedure to delete a specified stack from the database
procedure TMainForm.DeletePlaylistFromSchedule (PlaylistType: SmallInt; Start_Enable_Time: Double);
begin
  Case PlaylistType of
      //Ticker
   1: begin
        try
          //Refresh table
          dmMain.tblScheduled_Ticker_Groups.Active := FALSE;
          dmMain.tblScheduled_Ticker_Groups.Active := TRUE;
          //Do the delete
          if (dmMain.tblScheduled_Ticker_Groups.RecordCount > 0) then
          begin
            dmMain.tblScheduled_Ticker_Groups.First;
            While(dmMain.tblScheduled_Ticker_Groups.EOF = FALSE) do
            begin
              if (dmMain.tblScheduled_Ticker_Groups.FieldByName('Start_Enable_Time').AsFloat = Start_Enable_Time) then
              begin
                dmMain.tblScheduled_Ticker_Groups.Delete;
              end
              else dmMain.tblScheduled_Ticker_Groups.Next;
            end;
          end;
        except
          MessageDlg ('Error occurred while trying to delete the ticker playlist from the schedule database.',
                       mtError, [mbOk], 0);
        end;
      end;
      //Bug
   2: begin
        try
          //Refresh table
          dmMain.tblScheduled_Bug_Groups.Active := FALSE;
          dmMain.tblScheduled_Bug_Groups.Active := TRUE;
          //Do the delete
          if (dmMain.tblScheduled_Bug_Groups.RecordCount > 0) then
          begin
            dmMain.tblScheduled_Bug_Groups.First;
            While(dmMain.tblScheduled_Bug_Groups.EOF = FALSE) do
            begin
              if (dmMain.tblScheduled_Bug_Groups.FieldByName('Start_Enable_Time').AsFloat = Start_Enable_Time) then
              begin
                dmMain.tblScheduled_Bug_Groups.Delete;
              end
              else dmMain.tblScheduled_Bug_Groups.Next;
            end;
          end;
        except
          MessageDlg ('Error occurred while trying to delete the bug playlist from the schedule database.',
                       mtError, [mbOk], 0);
        end;
      end;
      //Extra line
   3: begin
        try
          //Refresh table
          dmMain.tblScheduled_GameTrax_Groups.Active := FALSE;
          dmMain.tblScheduled_GameTrax_Groups.Active := TRUE;
          //Do the delete
          if (dmMain.tblScheduled_GameTrax_Groups.RecordCount > 0) then
          begin
            dmMain.tblScheduled_GameTrax_Groups.First;
            While(dmMain.tblScheduled_GameTrax_Groups.EOF = FALSE) do
            begin
              if (dmMain.tblScheduled_GameTrax_Groups.FieldByName('Start_Enable_Time').AsFloat = Start_Enable_Time) then
              begin
                dmMain.tblScheduled_GameTrax_Groups.Delete;
              end
              else dmMain.tblScheduled_GameTrax_Groups.Next;
            end;
          end;
        except
          MessageDlg ('Error occurred while trying to delete the extra line playlist from the schedule database.',
                       mtError, [mbOk], 0);
        end;
      end;
  end;
end;

//Handler for Delete Ticker Playlist from Schedule button
procedure TMainForm.BitBtn44Click(Sender: TObject);
var
  Control: Word;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        if (dmMain.tblScheduled_Ticker_Groups.RecordCount = 1) then
        begin
          Control := MessageDlg ('There must always be at least one ticker playlist in the schedule.',
                                  mtInformation, [mbOk], 0);
        end
        else if (dmMain.tblScheduled_Ticker_Groups.RecordCount > 1) then
        begin
          //Confirm
          Control := MessageDlg ('Are you sure you want to delete the selected ticker playlist schedule entry?',
                                  mtConfirmation, [mbYes, mbNo], 0);
          if (Control = mryes) then
            DeletePlaylistFromSchedule (1, dmMain.tblScheduled_Ticker_Groups.FieldByName('Start_Enable_Time').AsFloat);
        end;
      end;
      //Bug
   1: begin
        if (dmMain.tblScheduled_Bug_Groups.RecordCount = 1) then
        begin
          Control := MessageDlg ('There must always be at least one bug playlist in the schedule.',
                                  mtInformation, [mbOk], 0);
        end
        else if (dmMain.tblScheduled_Bug_Groups.RecordCount > 1) then
        begin
          //Confirm
          Control := MessageDlg ('Are you sure you want to delete the selected bug playlist schedule entry?',
                                  mtConfirmation, [mbYes, mbNo], 0);
          if (Control = mryes) then
            DeletePlaylistFromSchedule (2, dmMain.tblScheduled_Bug_Groups.FieldByName('Start_Enable_Time').AsFloat);
        end;
      end;
      //Extra line
   2: begin
        if (dmMain.tblScheduled_GameTrax_Groups.RecordCount > 0) then
        begin
          //Confirm
          Control := MessageDlg ('Are you sure you want to delete the selected extra line playlist schedule entry?',
                                  mtConfirmation, [mbYes, mbNo], 0);
          if (Control = mryes) then
            DeletePlaylistFromSchedule (3, dmMain.tblScheduled_GameTrax_Groups.FieldByName('Start_Enable_Time').AsFloat);
        end;
      end;
  end;
end;

//Procedure to refresh the schedule grid
procedure TMainForm.RefreshScheduleGrid;
var
  SaveID: Double;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        //Save the current record being pointed to in the grid
        SaveID := dmMain.tblScheduled_Ticker_Groups.FieldByName('Playlist_ID').AsFloat;
        //Refresh the table
        dmMain.tblScheduled_Ticker_Groups.Active := FALSE;
        dmMain.tblScheduled_Ticker_Groups.Active := TRUE;
        //Search to the saved record
        dmMain.tblScheduled_Ticker_Groups.Locate('Playlist_ID', SaveID, []);
      end;
      //Bug
   1: begin
        //Save the current record being pointed to in the grid
        SaveID := dmMain.tblScheduled_Bug_Groups.FieldByName('Playlist_ID').AsFloat;
        //Refresh the table
        dmMain.tblScheduled_Bug_Groups.Active := FALSE;
        dmMain.tblScheduled_Bug_Groups.Active := TRUE;
        //Search to the saved record
        dmMain.tblScheduled_Bug_Groups.Locate('Playlist_ID', SaveID, []);
      end;
      //Extra line
   2: begin
        //Save the current record being pointed to in the grid
        SaveID := dmMain.tblScheduled_GameTrax_Groups.FieldByName('Playlist_ID').AsFloat;
        //Refresh the table
        dmMain.tblScheduled_GameTrax_Groups.Active := FALSE;
        dmMain.tblScheduled_GameTrax_Groups.Active := TRUE;
        //Search to the saved record
        dmMain.tblScheduled_GameTrax_Groups.Locate('Playlist_ID', SaveID, []);
      end;
  end;
end;

//Handler for menu selection to purge expired scheduled playlists
procedure TMainForm.PurgeScheduledEvents1DayOld1Click(Sender: TObject);
var
  Control: Word;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        //Refresh
        dmMain.tblScheduled_Ticker_Groups.Active := FALSE;
        dmMain.tblScheduled_Ticker_Groups.Active := TRUE;
        //Confirm
        if (dmMain.tblScheduled_Ticker_Groups.RecordCount > 0) then
        begin
          Control := MessageDlg ('Are you sure you want to delete the expired schedule events from the ticker schedule?',
                                  mtConfirmation, [mbYes, mbNo], 0);
          if (Control = mrYes) then
          try
            //Delete the expired events
            if (dmMain.tblScheduled_Ticker_Groups.RecordCount > 0) then
            begin
              Repeat
                if (dmMain.tblScheduled_Ticker_Groups.FieldByName('End_Enable_Time').AsFloat < Now) then
                dmMain.tblScheduled_Ticker_Groups.Delete
              else dmMain.tblScheduled_Ticker_Groups.Next;
              Until (dmMain.tblScheduled_Ticker_Groups.EOF = TRUE);
              //Refresh the table
              RefreshScheduleGrid;
            end;
          finally
          end;
        end;
      end;
      //Bug
   1: begin
        //Refresh
        dmMain.tblScheduled_Bug_Groups.Active := FALSE;
        dmMain.tblScheduled_Bug_Groups.Active := TRUE;
        //Confirm
        if (dmMain.tblScheduled_Bug_Groups.RecordCount > 0) then
        begin
          Control := MessageDlg ('Are you sure you want to delete the expired schedule events from the bug schedule?',
                                  mtConfirmation, [mbYes, mbNo], 0);
          if (Control = mrYes) then
          try
            //Delete the expired events
            if (dmMain.tblScheduled_Bug_Groups.RecordCount > 0) then
            begin
              Repeat
                if (dmMain.tblScheduled_Bug_Groups.FieldByName('End_Enable_Time').AsFloat < Now) then
                dmMain.tblScheduled_Bug_Groups.Delete
              else dmMain.tblScheduled_Bug_Groups.Next;
              Until (dmMain.tblScheduled_Bug_Groups.EOF = TRUE);
              //Refresh the table
              RefreshScheduleGrid;
            end;
          finally
          end;
        end;
      end;
      //Extra line
   2: begin
        //Refresh
        dmMain.tblScheduled_GameTrax_Groups.Active := FALSE;
        dmMain.tblScheduled_GameTrax_Groups.Active := TRUE;
        //Confirm
        if (dmMain.tblScheduled_GameTrax_Groups.RecordCount > 0) then
        begin
          Control := MessageDlg ('Are you sure you want to delete the expired schedule events from the extra line schedule?',
                                  mtConfirmation, [mbYes, mbNo], 0);
          if (Control = mrYes) then
          try
            //Delete the expired events
            if (dmMain.tblScheduled_GameTrax_Groups.RecordCount > 0) then
            begin
              Repeat
                if (dmMain.tblScheduled_GameTrax_Groups.FieldByName('End_Enable_Time').AsFloat < Now) then
                dmMain.tblScheduled_GameTrax_Groups.Delete
              else dmMain.tblScheduled_GameTrax_Groups.Next;
              Until (dmMain.tblScheduled_GameTrax_Groups.EOF = TRUE);
              //Refresh the table
              RefreshScheduleGrid;
            end;
          finally
          end;
        end;
      end;
  end;
end;

//Handler for edit selected schedule entry
procedure TMainForm.BitBtn43Click(Sender: TObject);
begin
  EditCurrentScheduleEntry;
end;
procedure TMainForm.ScheduledPlaylistsGridDblClick(Sender: TObject);
begin
  EditCurrentScheduleEntry;
end;
//Procedure to edit the current schedule entry
procedure TMainForm.EditCurrentScheduleEntry;
var
  OKToGo: Boolean;
  Modal: TScheduleEntryEditDlg;
  Start_Enable_Time, End_Enable_Time: TDateTime;
  StartHour, StartMinute, EndHour, EndMinute, Sec, mSec: Word;
  Control: Word;
begin
  //Init
  OKToGo := TRUE;
  //Setup and launch editing dialog
  Modal := TScheduleEntryEditDlg.Create(Application);
  try
    //Set control values on dialog
    Case PlaylistSelectTabControl.TabIndex of
        //Ticker
     0: begin
          Start_Enable_Time := dmMain.tblScheduled_Ticker_Groups.FieldByName('Start_Enable_Time').AsDateTime;
          End_Enable_Time := dmMain.tblScheduled_Ticker_Groups.FieldByName('End_Enable_Time').AsDateTime;
        end;
        //Bug
     1: begin
          Start_Enable_Time := dmMain.tblScheduled_Bug_Groups.FieldByName('Start_Enable_Time').AsDateTime;
          End_Enable_Time := dmMain.tblScheduled_Bug_Groups.FieldByName('End_Enable_Time').AsDateTime;
        end;
        //Extra line
     2: begin
          Start_Enable_Time := dmMain.tblScheduled_GameTrax_Groups.FieldByName('Start_Enable_Time').AsDateTime;
          End_Enable_Time := dmMain.tblScheduled_GameTrax_Groups.FieldByName('End_Enable_Time').AsDateTime;
        end;
    end;

    //Set initial value of controls
    Modal.ScheduleEntryStartDate.Date := Trunc(Start_Enable_Time);
    Modal.ScheduleEntryEndDate.Date := Trunc(End_Enable_Time);
    Modal.ScheduleEntryStartTime.Time :=
      (Start_Enable_Time - Trunc(Start_Enable_Time));
    Modal.ScheduleEntryEndTime.Time :=
      (End_Enable_Time - Trunc(End_Enable_Time));

    Control := Modal.ShowModal;
    //Set initial values
  finally
    //Set new values if user didn't cancel out
    if (Control = mrOK) then
    begin
      //Encode start and end enable times
      Start_Enable_Time := Trunc(Modal.ScheduleEntryStartDate.Date) +
        (Modal.ScheduleEntryStartTime.Time - Trunc(Modal.ScheduleEntryStartTime.Time));
      End_Enable_Time := Trunc(Modal.ScheduleEntryEndDate.Date) +
        (Modal.ScheduleEntryEndTime.Time - Trunc(Modal.ScheduleEntryEndTime.Time));

      //Make sure end time is greater than start time
      if (Start_Enable_Time >= End_Enable_Time) then
      begin
        OKToGo := FALSE;
        MessageDlg('You must enter an End Enable Time that occurs AFTER the Start Enable Time',
                   mtError, [mbOK], 0);
      end;

      //If OK, edit the values
      if (OKToGo = TRUE) then
      begin
        try
          //Edit the values
          Case PlaylistSelectTabControl.TabIndex of
              //Ticker
           0: begin
                dmMain.tblScheduled_Ticker_Groups.Edit;
                //Encode start and end enable times
                dmMain.tblScheduled_Ticker_Groups.FieldByName('Start_Enable_Time').AsDateTime := Start_Enable_Time;
                dmMain.tblScheduled_Ticker_Groups.FieldByName('End_Enable_Time').AsDateTime := End_Enable_Time;
                dmMain.tblScheduled_Ticker_Groups.Post;
              end;
              //Bug
           1: begin
                dmMain.tblScheduled_Bug_Groups.Edit;
                dmMain.tblScheduled_Bug_Groups.FieldByName('Start_Enable_Time').AsDateTime := Start_Enable_Time;
                dmMain.tblScheduled_Bug_Groups.FieldByName('End_Enable_Time').AsDateTime := End_Enable_Time;
                dmMain.tblScheduled_Bug_Groups.Post;
              end;
              //Extra line
           2: begin
                dmMain.tblScheduled_GameTrax_Groups.Edit;
                dmMain.tblScheduled_GameTrax_Groups.FieldByName('Start_Enable_Time').AsDateTime := Start_Enable_Time;
                dmMain.tblScheduled_GameTrax_Groups.FieldByName('End_Enable_Time').AsDateTime := End_Enable_Time;
                dmMain.tblScheduled_GameTrax_Groups.Post;
              end;
          end;
          //Refresh the grid
          RefreshSchedulegrid;
        except
          MessageDlg('Error occurred while trying to edit the schedule entry in the database.',
                      mtError, [mbOK], 0);
        end;
      end;
    end;
    Modal.Free
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES AND HANDLERS FOR DATABASE MAINTENANCE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Refresh database maintenance playlist list grid
procedure TMainForm.BitBtn32Click(Sender: TObject);
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker playlists
   0: begin
        dmMain.tblTicker_Groups.Active := FALSE;
        dmMain.tblTicker_Groups.Active := TRUE;
      end;
      //Bug playlists
   1: begin
        dmMain.tblBug_Groups.Active := FALSE;
        dmMain.tblBug_Groups.Active := TRUE;
      end;
      //GameTrax playlists
   2: begin
        dmMain.tblGameTrax_Groups.Active := FALSE;
        dmMain.tblGameTrax_Groups.Active := TRUE;
      end;
  end;
end;

//Handler for deleting selected zipper group entry from database
procedure TMainForm.BitBtn34Click(Sender: TObject);
begin
  DeleteSelectedPlaylist;
end;
procedure TMainForm.DeletePlaylist1Click(Sender: TObject);
begin
  DeleteSelectedPlaylist;
end;
procedure TMainForm.DeleteSelectedPlaylist;
var
  MyPlaylistID: Double;
  PlaylistIsScheduled: Boolean;
  Control: Word;
  OKToGo: Boolean;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker playlists
   0: begin
        try
          OKToGo := TRUE;
          if (dmMain.tblTicker_Groups.RecordCount > 0) then
          begin
            MyPlaylistID := dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsFloat;
            //First, check to see if this playlist is scheduled and alert operator if so
            PlaylistIsScheduled := dmMain.tblScheduled_Ticker_Groups.Locate('Playlist_ID', MyPlaylistID, []);
            if (PlaylistIsScheduled) then
            begin
              Control := MessageDlg('At least one schedule entry has been found that includes this ticker playlist. ' +
                                    'You must delete any associated ticker schedule entries before deleting the ' +
                                    'playlist.', mtError, [mbOk], 0);
              OKToGo := FALSE;
            end;
            if (OkToGo = TRUE) then
            begin
              //Alert operator
              Control := MessageDlg('Are you sure you want to delete the selected ticker playlist?',
                         mtConfirmation, [mbYes, mbNo], 0);
              if (Control = mrYes) then
              begin
                //Delete the entry in the stacks table
                dmMain.tblTicker_Groups.Delete;
                dmMain.tblTicker_Groups.Active := FALSE;
                dmMain.tblTicker_Groups.Active := TRUE;
                //Delete any associated events
                if (dmMain.tblTicker_Elements.RecordCount > 0) then
                begin
                  dmMain.tblTicker_Elements.First;
                  While(dmMain.tblTicker_Elements.EOF = FALSE) do
                  begin
                    if (dmMain.tblTicker_Elements.FieldByName('Playlist_ID').AsString = FloatToStr(MyPlaylistID)) then
                    begin
                      dmMain.tblTicker_Elements.Delete;
                    end
                    else dmMain.tblTicker_Elements.Next;
                  end;
                end;
              end;
            end;
          end;
        except
          //Alert operator
          MessageDlg('An error occurred while trying to delete the ticker playlist.',
                      mtError, [mbOK], 0);
        end;
      end;
      //Bug playlists
   1: begin
        try
          OKToGo := TRUE;
          if (dmMain.tblBug_Groups.RecordCount > 0) then
          begin
            MyPlaylistID := dmMain.tblBug_Groups.FieldByName('Playlist_ID').AsFloat;
            //First, check to see if this playlist is scheduled and alert operator if so
            PlaylistIsScheduled := dmMain.tblScheduled_Bug_Groups.Locate('Playlist_ID', MyPlaylistID, []);
            if (PlaylistIsScheduled) then
            begin
              Control := MessageDlg('At least one schedule entry has been found that includes this bug playlist. ' +
                                    'You must delete any associated schedule entries before deleting the ' +
                                    'bug playlist.', mtError, [mbOk], 0);
              OKToGo := FALSE;
            end;
            if (OkToGo = TRUE) then
            begin
              //Alert operator
              Control := MessageDlg('Are you sure you want to delete the selected bug playlist?',
                         mtConfirmation, [mbYes, mbNo], 0);
              if (Control = mrYes) then
              begin
                //Delete the entry in the stacks table
                dmMain.tblBug_Groups.Delete;
                dmMain.tblBug_Groups.Active := FALSE;
                dmMain.tblBug_Groups.Active := TRUE;
                //Delete any associated events
                if (dmMain.tblBug_Elements.RecordCount > 0) then
                begin
                  dmMain.tblBug_Elements.First;
                  While(dmMain.tblBug_Elements.EOF = FALSE) do
                  begin
                    if (dmMain.tblBug_Elements.FieldByName('Playlist_ID').AsString = FloatToStr(MyPlaylistID)) then
                    begin
                      dmMain.tblBug_Elements.Delete;
                    end
                    else dmMain.tblBug_Elements.Next;
                  end;
                end;
              end;
            end;
          end;
        except
          //Alert operator
          MessageDlg('An error occurred while trying to delete the bug playlist.',
                      mtError, [mbOK], 0);
        end;
      end;
      //GameTrax playlists
   2: begin
        try
          OKToGo := TRUE;
          if (dmMain.tblGameTrax_Groups.RecordCount > 0) then
          begin
            MyPlaylistID := dmMain.tblGameTrax_Groups.FieldByName('Playlist_ID').AsFloat;
            //First, check to see if this playlist is scheduled and alert operator if so
            PlaylistIsScheduled := dmMain.tblScheduled_GameTrax_Groups.Locate('Playlist_ID', MyPlaylistID, []);
            if (PlaylistIsScheduled) then
            begin
              Control := MessageDlg('At least one schedule entry has been found that includes this extra line playlist. ' +
                                    'You must delete any associated schedule entries before deleting the ' +
                                    'extra line playlist.', mtError, [mbOk], 0);
              OKToGo := FALSE;
            end;
            if (OkToGo = TRUE) then
            begin
              //Alert operator
              Control := MessageDlg('Are you sure you want to delete the selected extra line playlist?',
                         mtConfirmation, [mbYes, mbNo], 0);
              if (Control = mrYes) then
              begin
                //Delete the entry in the stacks table
                dmMain.tblGameTrax_Groups.Delete;
                dmMain.tblGameTrax_Groups.Active := FALSE;
                dmMain.tblGameTrax_Groups.Active := TRUE;
                //Delete any associated events
                if (dmMain.tblGameTrax_Elements.RecordCount > 0) then
                begin
                  dmMain.tblGameTrax_Elements.First;
                  While(dmMain.tblGameTrax_Elements.EOF = FALSE) do
                  begin
                    if (dmMain.tblGameTrax_Elements.FieldByName('Playlist_ID').AsString = FloatToStr(MyPlaylistID)) then
                    begin
                      dmMain.tblGameTrax_Elements.Delete;
                    end
                    else dmMain.tblGameTrax_Elements.Next;
                  end;
                end;
              end;
            end;
          end;
        except
          //Alert operator
          MessageDlg('An error occurred while trying to delete the extra line playlist.',
                      mtError, [mbOK], 0);
        end;
      end;
  end; //Case
end;

procedure TMainForm.TimeOfDayTimerTimer(Sender: TObject);
begin
  DateTimeLabel.Caption := DateTimeToStr(Now);
end;

//Handler for hotkeys for playlist operations (cut, copy, paste, duplicate)
procedure TMainForm.PlaylistGridKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (ssCtrl in Shift) and (Key = Ord('X')) then Cut
  else if (ssCtrl in Shift) and (Key = Ord('C')) then Copy
  else if (ssCtrl in Shift) and (Key = Ord('V')) then Paste
  else if (ssCtrl in Shift) and (Key = Ord('E')) then PasteEnd
  else if (ssCtrl in Shift) and (Key = Ord('D')) then Duplicate
  else if (ssCtrl in Shift) and (Key = Ord('A')) then SelectAll
  else if (ssCtrl in Shift) and (Key = Ord('U')) then DeSelectAll
  else if (ssShift in Shift) and (Key = VK_UP) then MoveRecord(UP)
  else if (ssShift in Shift) and (Key = VK_DOWN) then MoveRecord(DOWN)
  else if (Key = VK_DELETE) then DeleteGraphicFromZipperPlaylist(TRUE);
end;

//Handler for Select ALL
procedure TMainForm.SelectAll1Click(Sender: TObject);
begin
  SelectAll;
end;
procedure TMainForm.SelectAll;
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  BugRecPtr: ^BugRec;
  GameTraxRecPtr: ^GameTraxRec;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Studio Ticker playlists
   0: begin
        if (StudioTicker_Collection.Count > 0) then
        begin
          for i := 0 to StudioTicker_Collection.Count-1 do
          begin
            TickerRecPtr := StudioTicker_Collection.At(i);
            PlaylistGrid.RowSelected[i+1] := TRUE;
            TickerRecPtr^.Selected := TRUE;
          end;
        end;
      end;
      //Studio Bug playlists
   1: begin
        if (StudioBug_Collection.Count > 0) then
        begin
          for i := 0 to StudioBug_Collection.Count-1 do
          begin
            BugRecPtr := StudioBug_Collection.At(i);
            PlaylistGrid.RowSelected[i+1] := TRUE;
            BugRecPtr^.Selected := TRUE;
          end;
        end;
      end;
      //GameTrax playlists
   2: begin
        if (GameTrax_Collection.Count > 0) then
        begin
          for i := 0 to GameTrax_Collection.Count-1 do
          begin
            GameTraxRecPtr := GameTrax_Collection.At(i);
            PlaylistGrid.RowSelected[i+1] := TRUE;
            GameTraxRecPtr^.Selected := TRUE;
          end;
        end;
      end;
  end;
end;

//Handler for De-Select ALL
procedure TMainForm.DeSelectAll1Click(Sender: TObject);
begin
  DeSelectAll;
end;
procedure TMainForm.DeSelectAll;
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  BugRecPtr: ^BugRec;
  GameTraxRecPtr: ^GameTraxRec;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker playlists
   0: begin
        if (StudioTicker_Collection.Count > 0) then
        begin
          for i := 0 to StudioTicker_Collection.Count-1 do
          begin
            TickerRecPtr := StudioTicker_Collection.At(i);
            PlaylistGrid.RowSelected[i+1] := FALSE;
            TickerRecPtr^.Selected := FALSE;
          end;
        end;
      end;
      //Bug playlists
   1: begin
        if (StudioBug_Collection.Count > 0) then
        begin
          for i := 0 to StudioBug_Collection.Count-1 do
          begin
            BugRecPtr := StudioBug_Collection.At(i);
            PlaylistGrid.RowSelected[i+1] := FALSE;
            BugRecPtr^.Selected := FALSE;
          end;
        end;
      end;
      //Extra Line playlists
   2: begin
        if (GameTrax_Collection.Count > 0) then
        begin
          for i := 0 to GameTrax_Collection.Count-1 do
          begin
            GameTraxRecPtr := GameTrax_Collection.At(i);
            PlaylistGrid.RowSelected[i+1] := FALSE;
            GameTraxRecPtr^.Selected := FALSE;
          end;
        end;
      end;
  end;
end;

//Handler for click on cell - draws current page
procedure TMainForm.PlaylistGridClick(Sender: TObject);
begin
  if (EngineParameters.Enabled) then PreviewCurrentPlaylistRecord;
  RefreshEntryFieldsGrid(-1);
end;
//Procedure for previewing the currently selected record in the playlist
procedure TMainForm.PreviewCurrentPlaylistRecord;
var
  SaveTop: LongInt;
begin
  //Save the top row
  SaveTop := PlaylistGrid.TopRow;
  PlaylistGrid.SetFocus;
  if (PlaylistGrid.CurrentDataRow <= 0) then PlaylistGrid.CurrentDataRow := 1;
  Case PlaylistSelectTabControl.TabIndex of
   0: begin
        EngineInterface.SendTickerRecord(FALSE, PlaylistGrid.CurrentDataRow-1);
      end;
   1: begin
        EngineInterface.SendBugRecord(FALSE, PlaylistGrid.CurrentDataRow-1);
      end;
   2: begin
        EngineInterface.SendGameTraxRecord(FALSE, PlaylistGrid.CurrentDataRow-1);
      end;
  end;
  //Restore the top row
  PlaylistGrid.TopRow := SaveTop;
end;
//Handler for ENTER key press on cell - draws current page
procedure TMainForm.PlaylistGridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (PlaylistGrid.CurrentDataRow <= 0) then PlaylistGrid.CurrentDataRow := 1;
  Case PlaylistSelectTabControl.TabIndex of
   0: begin
//        EngineInterface.SendTickerRecord(FALSE, PlaylistGrid.CurrentDataRow-1);
      end;
   1: begin
//        EngineInterface.SendBugRecord(FALSE, PlaylistGrid.CurrentDataRow-1);
      end;
   2: begin
//        EngineInterface.SendGameTraxRecord(FALSE, PlaylistGrid.CurrentDataRow-1);
      end;
  end;
end;

//Handler for arrow up/down when playlist grid has the focus
procedure TMainForm.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if ((Key = VK_UP) OR (Key = VK_DOWN)) AND (PlaylistGrid.IsFocused) then
  begin
    if (PlaylistGrid.CurrentDataRow <= 0) then PlaylistGrid.CurrentDataRow := 1;
    Case PlaylistSelectTabControl.TabIndex of
     0: begin
          EngineInterface.SendTickerRecord(FALSE, PlaylistGrid.CurrentDataRow-1);
        end;
     1: begin
          EngineInterface.SendBugRecord(FALSE, PlaylistGrid.CurrentDataRow-1);
        end;
     2: begin
          EngineInterface.SendGameTraxRecord(FALSE, PlaylistGrid.CurrentDataRow-1);
        end;
    end;
  end;
end;

//Handler to reconnect to graphics engine
procedure TMainForm.ReconnecttoGraphicsEngine1Click(Sender: TObject);
begin
  //Connect to the graphics engine if it's enabled
  try
    EngineInterface.EnginePort.Close;
    EngineInterface.EnginePort.Address := EngineParameters.IPAddress;
    EngineInterface.EnginePort.Port := StrToInt(EngineParameters.Port);
    EngineInterface.EnginePort.Open;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      //WriteToErrorLog
      EngineInterface.WriteToErrorLog('Error occurred while trying connect to database engine');
    end;
  end;
end;

//Handler for template change - sets dwell time
procedure TMainForm.AvailableTemplatesDBGridRowChanged(Sender: TObject; OldRow, NewRow: Variant);
begin
  EntryDwellTime.Value :=
    Trunc(GetTemplateInformation(dmMain.Query2.FieldByName('Template_ID').AsInteger).Default_Dwell/1000);
end;

//General procedure for manual game override
procedure TMainForm.ManualGameOverride;
var
  i: SmallInt;
  CurrentPhase: SmallInt;
  Modal: TManualGameDlg;
  CategoryRecPtr: ^CategoryRec;
  GamePhaseRecPtr: ^GamePhaseRec;
  Control: Word;
  VisitorOddsID, HomeOddsID: Double;
begin
  if (dmMain.SportbaseQuery.RecordCount > 0) then
  begin
    Modal := TManualGameDlg.Create(Application);
    try
      //GAME DATA
      //Set initial values
      Modal.SelectedGameLabel.Caption := dmMain.SportbaseQuery.FieldByName('League').AsString + ': ' +
        dmMain.SportbaseQuery.FieldByName('VCity').AsString + ' @ ' +
        dmMain.SportbaseQuery.FieldByName('HCity').AsString;
      Modal.ManualGamesOverrideEnable.Checked := dmMain.SportbaseQuery.FieldByName('Manual').AsBoolean;
      Modal.ManualGamesOverrideEnable.Checked := dmMain.SportbaseQuery.FieldByName('Manual').AsBoolean;
      Modal.VisitorName.Text := dmMain.SportbaseQuery.FieldByName('VCity').AsString + ' ' +
        dmMain.SportbaseQuery.FieldByName('VTeam').AsString;
      Modal.HomeName.Text := dmMain.SportbaseQuery.FieldByName('HCity').AsString + ' ' +
        dmMain.SportbaseQuery.FieldByName('HTeam').AsString;
      Modal.VisitorScore.Value := dmMain.SportbaseQuery.FieldByName('VScore').AsInteger;
      Modal.HomeScore.Value := dmMain.SportbaseQuery.FieldByName('HScore').AsInteger;
      Modal.StartTime.Time :=  dmMain.SportbaseQuery.FieldByName('Start').AsDateTime;
      //Set game state
      if (dmMain.SportbaseQuery.FieldByName('State').AsString = 'Pre-Game') then
        Modal.GameState.ItemIndex := 0
      else if (dmMain.SportbaseQuery.FieldByName('State').AsString = 'In-Progress') then
        Modal.GameState.ItemIndex := 1
      else
        Modal.GameState.ItemIndex := 2;
      //Set game phase and populate dropdown
      if (Game_Phase_Collection.Count > 0) then
      begin
        for i := 0 to Game_Phase_Collection.Count-1 do
        begin
          GamePhaseRecPtr := Game_Phase_Collection.At(i);
          if (GamePhaseRecPtr^.League = dmMain.SportbaseQuery.FieldByName('League').AsString) then
          begin
            Modal.GamePhase.Items.Add(GamePhaseRecPtr^.Label_Period);
            //Special case for baseball - phase codes start at 0
            if (dmMain.SportbaseQuery.FieldByName('League').AsString = 'MLB') OR
               (dmMain.SportbaseQuery.FieldByName('League').AsString = 'AL') OR
               (dmMain.SportbaseQuery.FieldByName('League').AsString = 'NL') then
            begin
              if (dmMain.SportbaseQuery.FieldByName('Period').AsInteger+1 = Modal.GamePhase.Items.Count) then
                CurrentPhase := Modal.GamePhase.Items.Count-1;
            end
            else begin
              if (dmMain.SportbaseQuery.FieldByName('Period').AsInteger = Modal.GamePhase.Items.Count) then
                CurrentPhase := Modal.GamePhase.Items.Count;
            end;
          end;
        end;
        //Special case for baseball - phase codes start at 0
        if (dmMain.SportbaseQuery.FieldByName('League').AsString = 'MLB') OR
           (dmMain.SportbaseQuery.FieldByName('League').AsString = 'AL') OR
           (dmMain.SportbaseQuery.FieldByName('League').AsString = 'NL') then
        begin
          if (CurrentPhase >= 0) then Modal.GamePhase.ItemIndex := CurrentPhase;
        end
        else begin
          if (CurrentPhase > 0) then Modal.GamePhase.ItemIndex := CurrentPhase-1;
        end;
      end;
      //Show the dialog
      Control := Modal.ShowModal;
    finally
      //Set new values if user didn't cancel out
      if (Control = mrOK) then
      begin
        //GAME DATA
        //Put games table into edit mode
        dmMain.SportbaseQuery.Edit;
        //Set edited fields
        dmMain.SportbaseQuery.FieldByName('Manual').AsBoolean := Modal.ManualGamesOverrideEnable.Checked;
        if (Modal.ManualGamesOverrideEnable.Checked) then
        begin
          dmMain.SportbaseQuery.FieldByName('VScore').AsInteger := Modal.VisitorScore.Value;
          dmMain.SportbaseQuery.FieldByName('HScore').AsInteger := Modal.HomeScore.Value;
          //Set game state
          Case Modal.GameState.ItemIndex of
           0: dmMain.SportbaseQuery.FieldByName('State').AsString := 'Pre-Game';
           1: dmMain.SportbaseQuery.FieldByName('State').AsString := 'In-Progress';
           2: dmMain.SportbaseQuery.FieldByName('State').AsString := 'Final';
          end;
          //Set period
          //Special case for baseball - phase codes start at 0
          if (dmMain.SportbaseQuery.FieldByName('League').AsString = 'MLB') OR
             (dmMain.SportbaseQuery.FieldByName('League').AsString = 'AL') OR
             (dmMain.SportbaseQuery.FieldByName('League').AsString = 'NL') then
          begin
            dmMain.SportbaseQuery.FieldByName('Period').AsInteger := Modal.GamePhase.ItemIndex;
          end
          else begin
            dmMain.SportbaseQuery.FieldByName('Period').AsInteger := Modal.GamePhase.ItemIndex+1;
          end;
          //Set clock values
          //If end of period, set to min, sec = 0 so that proper phase is displayed
          if (Modal.EndOfPeriod.Checked) then
          begin
            dmMain.SportbaseQuery.FieldByName('TimeMin').AsInteger := 0;
            dmMain.SportbaseQuery.FieldByName('TimeSec').AsInteger := 0;
          end
          else begin
            //Set clock values to -1 to flag manual mode; clock will not be shown
            dmMain.SportbaseQuery.FieldByName('TimeMin').AsInteger := -1;
            dmMain.SportbaseQuery.FieldByName('TimeSec').AsInteger := -1;
          end;
          //Set game start time
          dmMain.SportbaseQuery.FieldByName('Start').AsDateTime :=
            Modal.StartTime.Time - Trunc(Modal.StartTime.Time);
        end;
        //Post the edits to the games table
        dmMain.SportbaseQuery.Post;

        //ODDS DATA
        //Put odds table into edit mode
        dmMain.SportbaseQuery3.Edit;
        //Set edited fields
        dmMain.SportbaseQuery3.FieldByName('Manual').AsBoolean := Modal.ManualOddsOverrideEnable.Checked;
        if (Modal.ManualOddsOverrideEnable.Checked) then
        begin
          try
            dmMain.SportbaseQuery3.FieldByName('OpeningAwaySpread').AsFloat :=
              StrToFloatDef(Modal.VOpenSpread.Text, -9999.0);
            dmMain.SportbaseQuery3.FieldByName('OpeningAwayMoneyLine').AsFloat :=
              StrToFloatDef(Modal.VOpenMoneyLine.Text, -9999.0);
            dmMain.SportbaseQuery3.FieldByName('OpeningHomeSpread').AsFloat :=
              StrToFloatDef(Modal.HOpenSpread.Text, -9999.0);
            dmMain.SportbaseQuery3.FieldByName('OpeningHomeMoneyLine').AsFloat :=
              StrToFloatDef(Modal.HOpenMoneyLine.Text, -9999.0);
            dmMain.SportbaseQuery3.FieldByName('OpeningTotal').AsFloat :=
              StrToFloatDef(Modal.OpenTotal.Text, -9999.0);
            dmMain.SportbaseQuery3.FieldByName('CurrentAwaySpread').AsFloat :=
              StrToFloatDef(Modal.VCurrentSpread.Text, -9999.0);
            dmMain.SportbaseQuery3.FieldByName('CurrentAwayMoneyLine').AsFloat :=
              StrToFloatDef(Modal.VCurrentMoneyLine.Text, -9999.0);
            dmMain.SportbaseQuery3.FieldByName('CurrentHomeSpread').AsFloat :=
              StrToFloatDef(Modal.HCurrentSpread.Text, -9999.0);
            dmMain.SportbaseQuery3.FieldByName('CurrentHomeMoneyLine').AsFloat :=
              StrToFloatDef(Modal.HCurrentMoneyLine.Text, -9999.0);
            dmMain.SportbaseQuery3.FieldByName('CurrentTotal').AsFloat :=
              StrToFloatDef(Modal.CurrentTotal.Text, -9999.0);
            //Set game start time
            dmMain.SportbaseQuery3.FieldByName('GameTime').AsDateTime :=
              Modal.OddsStartTime.Time - Trunc(Modal.OddsStartTime.Time);
          except
            //Alert operator
            MessageDlg('An error occurred while trying to convert the manual oddes data. ' +
              'Default values of -9999.0 will be used as required.', mtError, [mbOK], 0);
          end;
        end;
        //Post the edits to the odds table
        dmMain.SportbaseQuery3.Post;
      end;
      Modal.Free
    end;
  end;
end;

//Handlers to clear graphics from CAL output
procedure TMainForm.ClearGraphicsOutput1Click(Sender: TObject);
begin
  EngineInterface.TriggerGraphic(2);
end;
procedure TMainForm.ClearGraphicsOutput2Click(Sender: TObject);
begin
  EngineInterface.TriggerGraphic(2);
end;

////////////////////////////////////////////////////////////////////////////////
// Handler for editing default start/end enable times for selected template
////////////////////////////////////////////////////////////////////////////////
procedure TMainForm.EditDefaultTemplateStartEndEnableTimes1Click(Sender: TObject);
var
  i: SmallInt;
  CurrentTemplateID: SmallInt;
  MyTemplateIndex: SmallInt;
  Modal: TScheduleEntryEditDlg;
  Control: Word;
  TemplateDefsRecPtr, TemplateDefsRecPtr2: ^TemplateDefsRec;
  CollectionCount: SmallInt;
  FoundRecord: Boolean;
begin
  //Get ID of current template
  CurrentTemplateID := dmMain.Query2.FieldByName('Template_ID').AsInteger;
  //Find index of template record in collection
  if (Template_Defs_Collection.Count > 0) then
  begin
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_ID = CurrentTemplateID) then
        MyTemplateIndex := i;
    end;
  end;
  //Get the pointer to the selected template in the collection
  TemplateDefsRecPtr := Template_Defs_Collection.At(MyTemplateIndex);
  //Setup and launch editing dialog if applicable
  try
    Modal := TScheduleEntryEditDlg.Create(Application);
    Modal.Caption := 'Template Default Enable Time Editor';
    Modal.DefaultDwellTimeLabel.Visible := TRUE;
    Modal.DefaultDwellTime.Visible := TRUE;
    Modal.ScheduleEntryStartDate.Date := TemplateDefsRecPtr^.StartEnableDateTime;
    Modal.ScheduleEntryStartTime.Time := TemplateDefsRecPtr^.StartEnableDateTime;
    Modal.ScheduleEntryEndDate.Date := TemplateDefsRecPtr^.EndEnableDateTime;
    Modal.ScheduleEntryEndTime.Time := TemplateDefsRecPtr^.EndEnableDateTime;
    Modal.DefaultDwellTime.Value := Trunc(TemplateDefsRecPtr^.Default_Dwell/1000);
    //Show the dialog
    Control := Modal.ShowModal;
  finally
    //Set new values if user didn't cancel out
    if (Control = mrOK) then
    begin
      //Update the values in the collection
      TemplateDefsRecPtr^.StartEnableDateTime := Trunc(Modal.ScheduleEntryStartDate.Date) +
          (Modal.ScheduleEntryStartTime.Time - Trunc(Modal.ScheduleEntryStartTime.Time));
      TemplateDefsRecPtr^.EndEnableDateTime := Trunc(Modal.ScheduleEntryEndDate.Date) +
          (Modal.ScheduleEntryEndTime.Time - Trunc(Modal.ScheduleEntryEndTime.Time));
      TemplateDefsRecPtr^.Default_Dwell := Modal.DefaultDwellTime.Value*1000;
      //Update the database values
      dmMain.tblTemplate_Defs.Active := FALSE;
      dmMain.tblTemplate_Defs.Active := TRUE;
      if (dmMain.tblTemplate_Defs.RecordCount > 0) then
      begin
        FoundRecord := dmMain.tblTemplate_Defs.Locate('Template_ID', CurrentTemplateID, []);
        if (FoundRecord) then
        begin
          dmMain.tblTemplate_Defs.Edit;
          dmMain.tblTemplate_Defs.FieldByName('StartEnableTime').AsDateTime := TemplateDefsRecPtr^.StartEnableDateTime;
          dmMain.tblTemplate_Defs.FieldByName('EndEnableTime').AsDateTime := TemplateDefsRecPtr^.EndEnableDateTime;
          dmMain.tblTemplate_Defs.FieldByName('Default_Dwell').AsInteger := TemplateDefsRecPtr^.Default_Dwell;
          dmMain.tblTemplate_Defs.Post;
        end;
      end;
      dmMain.tblTemplate_Defs.Active := FALSE;
    end;
    Modal.Free;
  end;
end;

//Handler for launching sponsor logo editor
procedure TMainForm.EditSponsorLogoDefinitions1Click(Sender: TObject);
var
  Modal: TSponsorLogoEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblSponsor_Logos.Active := FALSE;
  dmMain.tblSponsor_Logos.Active := TRUE;
  Modal := TSponsorLogoEditorDlg.Create(Application);
  try
    Modal.Caption := 'Sponosr Logo Database Table Editor';
    Modal.Label1.Caption := 'Sponosr Logos in Database';
    Modal.tsDBGrid1.DataSource := dmMain.dsSponsor_Logos;
    Modal.dbNavigator1.DataSource := dmMain.dsSponsor_Logos;
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free
  end;
end;

//Handler for launching college team logo editor
procedure TMainForm.EditCollegeTeamLogosClick(Sender: TObject);
var
  Modal: TCollegeTeamLogoEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblCFB_Team_Logos_Names.Active := FALSE;
  dmMain.tblCFB_Team_Logos_Names.Active := TRUE;
  Modal := TCollegeTeamLogoEditorDlg.Create(Application);
  try
    Modal.Caption := 'College Team Logo Database Table Editor';
    Modal.Label1.Caption := 'College Team Logos in Database';
    Modal.tsDBGrid1.DataSource := dmMain.dsCFB_Team_Logos_Names;
    Modal.dbNavigator1.DataSource := dmMain.dsCFB_Team_Logos_Names;
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free
  end;
end;

//Handler for launching promo logo editor
procedure TMainForm.EditPromoLogoInformation1Click(Sender: TObject);
var
  Modal: TSponsorLogoEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblPromo_Logos.Active := FALSE;
  dmMain.tblPromo_Logos.Active := TRUE;
  Modal := TSponsorLogoEditorDlg.Create(Application);
  try
    Modal.Caption := 'Promo Logo Database Table Editor';
    Modal.Label1.Caption := 'Promo Logos in Database';
    Modal.tsDBGrid1.DataSource := dmMain.dsPromo_Logos;
    Modal.dbNavigator1.DataSource := dmMain.dsPromo_Logos;

    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free
  end;
end;

//Handler for launching team info editor
procedure TMainForm.EditTeamInformation1Click(Sender: TObject);
var
  Modal: TDBEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblTeams.Active := FALSE;
  dmMain.tblTeams.Active := TRUE;
  Modal := TDBEditorDlg.Create(Application);
  try
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free
  end;
end;

//Handler for launching game phase info editor dialog
procedure TMainForm.EditGamePhaseData1Click(Sender: TObject);
var
  Modal: TGamePhaseEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblGame_Phase_Codes.Active := FALSE;
  dmMain.tblGame_Phase_Codes.Active := TRUE;
  Modal := TGamePhaseEditorDlg.Create(Application);
  try
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free;
    ReloadDataFromDatabase;
    dmMain.tblGame_Phase_Codes.Active := FALSE;
  end;
end;

//Handler for launching dialog to edit custom categories
procedure TMainForm.EditCustomHeaderCategories1Click(Sender: TObject);
var
  Modal: TCustomHeaderEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblCustom_News_Headers.Active := FALSE;
  dmMain.tblCustom_News_Headers.Active := TRUE;
  Modal := TCustomHeaderEditorDlg.Create(Application);
  try
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free;
    ReloadDataFromDatabase;
    dmMain.tblCustom_News_Headers.Active := FALSE;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// EXCLUSION LIST FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Handler to refresh available games gird (for exclusion)
procedure TMainForm.RefreshExclusionGamesGrid;
var
  i: SmallInt;
  ScheduleGameRecPtr: ^ScheduleGameRec;
  CollectionCount: SmallInt;
begin
  //Get current row
  //Clear grid values
  if (ExclusionGamesGrid.Rows > 0) then
    ExclusionGamesGrid.DeleteRows (1, ExclusionGamesGrid.Rows);
  //Init values
  CollectionCount := Schedule_Game_Collection.Count;
  if (CollectionCount > 0) then
  begin
    ExclusionGamesGrid.StoreData := TRUE;
    ExclusionGamesGrid.Cols := 1;
    ExclusionGamesGrid.Rows := CollectionCount;
    //Populate the grid
    for i := 0 to CollectionCount-1 do
    begin
      ScheduleGameRecPtr := Schedule_Game_Collection.At(i);
      ExclusionGamesGrid.Cell[1,i+1] := ScheduleGameRecPtr^.Description;
    end;
  end;
end;

//Handler for adding a game to the exlusion list
procedure TMainForm.BitBtn2Click(Sender: TObject);
var
  FoundRecord: Boolean;
  ScheduleGameRecPtr: ^ScheduleGameRec;
begin
  //Put table in edit mode
  dmMain.tblGameTrax_Regional_Game_Exclusions.Active := FALSE;
  dmMain.tblGameTrax_Regional_Game_Exclusions.Active := TRUE;
  //Check for existing record with same ID
  FoundRecord := dmMain.tblGameTrax_Regional_Game_Exclusions.Locate('RegionID', RegionID.Value, []);
  if (FoundRecord) then
    //Alert operator
    MessageDlg('A game has already been specified for exlusion for this region.', mtError, [mbOK], 0)
  else begin
    //Get data from collection object
    if (ExclusionGamesGrid.CurrentDataRow < 1) then ExclusionGamesGrid.CurrentDataRow := 1;
    ScheduleGameRecPtr := Schedule_Game_Collection.At(ExclusionGamesGrid.CurrentDataRow-1);
    //Add record
    dmMain.tblGameTrax_Regional_Game_Exclusions.Append;
    dmMain.tblGameTrax_Regional_Game_Exclusions.FieldByname('RegionID').AsInteger := RegionID.Value;
    dmMain.tblGameTrax_Regional_Game_Exclusions.FieldByname('CSS_GameID').AsInteger := ScheduleGameRecPtr^.CSS_GameID;
    dmMain.tblGameTrax_Regional_Game_Exclusions.FieldByname('NFLDM_GameID').AsInteger := ScheduleGameRecPtr^.NFLDM_GameID;
    dmMain.tblGameTrax_Regional_Game_Exclusions.FieldByname('SI_GameID').AsString := ScheduleGameRecPtr^.SI_GameID;
    dmMain.tblGameTrax_Regional_Game_Exclusions.FieldByname('ST_GameID').AsString := ScheduleGameRecPtr^.ST_GameID;
    dmMain.tblGameTrax_Regional_Game_Exclusions.FieldByname('Description').AsString := ScheduleGameRecPtr^.Description;
    dmMain.tblGameTrax_Regional_Game_Exclusions.Post;
  end;
end;

//Handler for clear all excluded games button
procedure TMainForm.ClearAllExcludedGamesClick(Sender: TObject);
var
  i: SmallInt;
  Control: Word;
begin
  //Only act if entries in exclusion table
  if (dmMain.tblGameTrax_Regional_Game_Exclusions.RecordCount > 0) then
  begin
    //Prompt operator for confirm
    Control := MessageDlg('Are you sure you want to delete all previously specified regional game exlusions?', mtConfirmation, [mbYes, mbNo], 0);
    if (Control = mrYes) then
    begin
      //Delete all entries
      for i := 1 to dmMain.tblGameTrax_Regional_Game_Exclusions.RecordCount do
      begin
        dmMain.tblGameTrax_Regional_Game_Exclusions.Delete;
      end;
    end;
  end;
end;

//Hamder for viewing CSS game notes
procedure TMainForm.BitBtn4Click(Sender: TObject);
var
  Modal: TCSSGameNoteViewerDlg;
begin
  Modal := TCSSGameNoteViewerDlg.Create(Application);
  try
    //Execute required stored procedure
    dmMain.CSSQuery2.Active := FALSE;
    dmMain.CSSQuery2.SQL.Clear;
    dmMain.CSSQuery2.SQL.Add('/* */vds_GetGame ' + IntToStr(dmMain.CSSQuery.FieldByName('game').AsInteger));
    dmMain.CSSQuery2.Open;
    //Check for record count > 0 and process
    if (dmMain.CSSQuery2.RecordCount > 0) then
    begin
      Modal.Edit1.Text := dmMain.CSSQuery2.FieldByName('onelinenoteA').AsString;
      Modal.Edit2.Text := dmMain.CSSQuery2.FieldByName('onelinenoteB').AsString;
    end;
    Modal.ShowModal;
  finally
    dmMain.CSSQuery2.Active := FALSE;
    Modal.Free
  end;
end;

////////////////////////////////////////////////////////////////////////////////
//  Handler for adding a Game Update record to the database
////////////////////////////////////////////////////////////////////////////////
procedure TMainForm.BitBtn7Click(Sender: TObject);
var
  i: SmallInt;
  CategoryRecPtr: ^CategoryRec;
  CSS_GameID: SmallInt;
  SI_GCode: String;
  NFLDM_GameID: LongInt;
  ST_Game_ID: String;
  NFLSeasonTypeStr: String;
begin
  //Check to see if category tab is a sport; if so, show and populate games grid
  CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
  if ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS')) OR
     ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS')) then
  begin
    CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
    NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
    SI_GCode := '0';
    ST_Game_ID := '0'; //Default to blank for now
  end
  else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC')) OR
         ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC'))then
  begin
    CSS_GameID := 0;
    SI_GCode := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
    NFLDM_GameID := 0; //Default to blank for now
    ST_Game_ID := '0'; //Default to blank for now
  end
  else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'NFLDM')) OR
          ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'NFLDM')) then
  begin
    if (ConnectToCSS) then
    begin
      CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
      NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
    end
    else begin
      CSS_GameID := 0;
      NFLDM_GameID := dmMain.DatamartQuery.FieldByname('GameKey').AsInteger; //Default to blank for now
    end;
    SI_GCode := '0';
    ST_Game_ID := '0'; //Default to blank for now
  end;
  //Append update recorda to database
  dmMain.tblGame_Updates.Active := TRUE;
  for i := 1 to 8 do
  begin
    dmMain.tblGame_Updates.Append;
    dmMain.tblGame_Updates.FieldByName('RegionID').AsInteger := i;
    dmMain.tblGame_Updates.FieldByName('Timestamp').AsDateTime := Now;
    dmMain.tblGame_Updates.FieldByName('CSS_GameID').AsInteger := CSS_GameID;
    dmMain.tblGame_Updates.FieldByName('SI_GCode').AsString := SI_GCode;
    dmMain.tblGame_Updates.FieldByName('NFLDM_GameID').AsInteger := NFLDM_GameID;
    dmMain.tblGame_Updates.FieldByName('ST_Game_ID').AsString := ST_Game_ID;
    dmMain.tblGame_Updates.Post;
  end;
  dmMain.tblGame_Updates.Active := FALSE;
end;

////////////////////////////////////////////////////////////////////////////////
//  Handler for adding a Fantasy Update record to the database
////////////////////////////////////////////////////////////////////////////////
procedure TMainForm.DisplayFantasyStatsBtnClick(Sender: TObject);
var
  i: SmallInt;
begin
  //Append update recorda to database based on state of enable checkmarks
  dmMain.tblFantasy_Updates.Active := TRUE;
  //Region #1
  if (EnableRegion1Fantasy.Checked) then
  begin
    dmMain.tblFantasy_Updates.Append;
    dmMain.tblFantasy_Updates.FieldByName('RegionID').AsInteger := 1;
    dmMain.tblFantasy_Updates.FieldByName('Timestamp').AsDateTime := Now;
    dmMain.tblFantasy_Updates.FieldByName('EnablePassing').AsBoolean := EnablePassingStats.Checked;
    dmMain.tblFantasy_Updates.FieldByName('EnableRushing').AsBoolean := EnableRushingStats.Checked;
    dmMain.tblFantasy_Updates.FieldByName('EnableReceiving').AsBoolean := EnableReceivingStats.Checked;
    dmMain.tblFantasy_Updates.Post;
  end;
  //Region #2
  if (EnableRegion2Fantasy.Checked) then
  begin
    dmMain.tblFantasy_Updates.Append;
    dmMain.tblFantasy_Updates.FieldByName('RegionID').AsInteger := 2;
    dmMain.tblFantasy_Updates.FieldByName('Timestamp').AsDateTime := Now;
    dmMain.tblFantasy_Updates.FieldByName('EnablePassing').AsBoolean := EnablePassingStats.Checked;
    dmMain.tblFantasy_Updates.FieldByName('EnableRushing').AsBoolean := EnableRushingStats.Checked;
    dmMain.tblFantasy_Updates.FieldByName('EnableReceiving').AsBoolean := EnableReceivingStats.Checked;
    dmMain.tblFantasy_Updates.Post;
  end;
  //Region #3
  if (EnableRegion3Fantasy.Checked) then
  begin
    dmMain.tblFantasy_Updates.Append;
    dmMain.tblFantasy_Updates.FieldByName('RegionID').AsInteger := 3;
    dmMain.tblFantasy_Updates.FieldByName('Timestamp').AsDateTime := Now;
    dmMain.tblFantasy_Updates.FieldByName('EnablePassing').AsBoolean := EnablePassingStats.Checked;
    dmMain.tblFantasy_Updates.FieldByName('EnableRushing').AsBoolean := EnableRushingStats.Checked;
    dmMain.tblFantasy_Updates.FieldByName('EnableReceiving').AsBoolean := EnableReceivingStats.Checked;
    dmMain.tblFantasy_Updates.Post;
  end;
  //Region #4
  if (EnableRegion4Fantasy.Checked) then
  begin
    dmMain.tblFantasy_Updates.Append;
    dmMain.tblFantasy_Updates.FieldByName('RegionID').AsInteger := 4;
    dmMain.tblFantasy_Updates.FieldByName('Timestamp').AsDateTime := Now;
    dmMain.tblFantasy_Updates.FieldByName('EnablePassing').AsBoolean := EnablePassingStats.Checked;
    dmMain.tblFantasy_Updates.FieldByName('EnableRushing').AsBoolean := EnableRushingStats.Checked;
    dmMain.tblFantasy_Updates.FieldByName('EnableReceiving').AsBoolean := EnableReceivingStats.Checked;
    dmMain.tblFantasy_Updates.Post;
  end;
  //Region #5
  if (EnableRegion5Fantasy.Checked) then
  begin
    dmMain.tblFantasy_Updates.Append;
    dmMain.tblFantasy_Updates.FieldByName('RegionID').AsInteger := 5;
    dmMain.tblFantasy_Updates.FieldByName('Timestamp').AsDateTime := Now;
    dmMain.tblFantasy_Updates.FieldByName('EnablePassing').AsBoolean := EnablePassingStats.Checked;
    dmMain.tblFantasy_Updates.FieldByName('EnableRushing').AsBoolean := EnableRushingStats.Checked;
    dmMain.tblFantasy_Updates.FieldByName('EnableReceiving').AsBoolean := EnableReceivingStats.Checked;
    dmMain.tblFantasy_Updates.Post;
  end;
  //Region #6
  if (EnableRegion6Fantasy.Checked) then
  begin
    dmMain.tblFantasy_Updates.Append;
    dmMain.tblFantasy_Updates.FieldByName('RegionID').AsInteger := 6;
    dmMain.tblFantasy_Updates.FieldByName('Timestamp').AsDateTime := Now;
    dmMain.tblFantasy_Updates.FieldByName('EnablePassing').AsBoolean := EnablePassingStats.Checked;
    dmMain.tblFantasy_Updates.FieldByName('EnableRushing').AsBoolean := EnableRushingStats.Checked;
    dmMain.tblFantasy_Updates.FieldByName('EnableReceiving').AsBoolean := EnableReceivingStats.Checked;
    dmMain.tblFantasy_Updates.Post;
  end;
  //Region #7
  if (EnableRegion7Fantasy.Checked) then
  begin
    dmMain.tblFantasy_Updates.Append;
    dmMain.tblFantasy_Updates.FieldByName('RegionID').AsInteger := 7;
    dmMain.tblFantasy_Updates.FieldByName('Timestamp').AsDateTime := Now;
    dmMain.tblFantasy_Updates.FieldByName('EnablePassing').AsBoolean := EnablePassingStats.Checked;
    dmMain.tblFantasy_Updates.FieldByName('EnableRushing').AsBoolean := EnableRushingStats.Checked;
    dmMain.tblFantasy_Updates.FieldByName('EnableReceiving').AsBoolean := EnableReceivingStats.Checked;
    dmMain.tblFantasy_Updates.Post;
  end;
  //Region #8
  if (EnableRegion8Fantasy.Checked) then
  begin
    dmMain.tblFantasy_Updates.Append;
    dmMain.tblFantasy_Updates.FieldByName('RegionID').AsInteger := 8;
    dmMain.tblFantasy_Updates.FieldByName('Timestamp').AsDateTime := Now;
    dmMain.tblFantasy_Updates.FieldByName('EnablePassing').AsBoolean := EnablePassingStats.Checked;
    dmMain.tblFantasy_Updates.FieldByName('EnableRushing').AsBoolean := EnableRushingStats.Checked;
    dmMain.tblFantasy_Updates.FieldByName('EnableReceiving').AsBoolean := EnableReceivingStats.Checked;
    dmMain.tblFantasy_Updates.Post;
  end;
  dmMain.tblFantasy_Updates.Active := FALSE;
end;

//Handler to clear team rankings
procedure TMainForm.ClearCFBTeamRanings1Click(Sender: TObject);
begin
  dmMain.SportbaseQuery4.Close;
  dmMain.SportbaseQuery4.SQL.Clear;
  dmMain.SportbaseQuery4.SQL.Add('UPDATE Teams SET Rank = ' + QuotedStr('') + ' WHERE League = ' + QuotedStr('CFB')
    + ' OR League = ' + QuotedStr('CBB'));
end;

//Handler for editing NCAA conference chips
procedure TMainForm.N1Click(Sender: TObject);
var
  Modal: TConferenceChipEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblCFB_Conference_Chips.Active := FALSE;
  dmMain.tblCFB_Conference_Chips.Active := TRUE;
  Modal := TConferenceChipEditorDlg.Create(Application);
  try
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free;
    dmMain.tblCFB_Conference_Chips.Active := FALSE;
  end;
end;

//Handler for menu selection to edit NCAA Tournament Round Information
procedure TMainForm.EditNCAATournamentRoundInfoClick(Sender: TObject);
var
  Modal: TNCAATournamentChipEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblTournament_Headers.Active := FALSE;
  dmMain.tblTournament_Headers.Active := TRUE;
  Modal := TNCAATournamentChipEditorDlg.Create(Application);
  try
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free;
    dmMain.tblTournament_Headers.Active := FALSE;
    ReloadDataFromDatabase;
  end;
end;

//Handler for menu selection to edit default template dwell times
procedure TMainForm.E1Click(Sender: TObject);
var
  Modal: TTemplateDwellEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblTemplate_Defs.Active := FALSE;
  dmMain.tblTemplate_Defs.Active := TRUE;
  Modal := TTemplateDwellEditorDlg.Create(Application);
  try
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free;
    dmMain.tblTemplate_Defs.Active := FALSE;
    ReloadDataFromDatabase;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
//NFL GSIS Game Data Override Functions
////////////////////////////////////////////////////////////////////////////////
//Delete current entry from override table
procedure TMainForm.BitBtn3Click(Sender: TObject);
begin
  if (dmMain.tblNFL_GSIS_Overrides.RecordCount > 0) then
  try
    dmMain.tblNFL_GSIS_Overrides.Delete;
  except
  end;
end;
//Add game to override list
//Double-click on grid
procedure TMainForm.CSSGamesDBGridDblClick(Sender: TObject);
begin
  AddCurrentCSSGameToOverrideList;
end;
//Popup menu
procedure TMainForm.AddGametoGSISOverrideList1Click(Sender: TObject);
begin
  AddCurrentCSSGameToOverrideList;
end;
//Function to add game to override list
procedure TMainForm.AddCurrentCSSGameToOverrideList;
var
  Control: Word;
begin
  Control := MessageDlg('Are you sure you want to add the selected game to the GSIS override list? ' +
    'If so, click Yes and remember to delete the game from the list to go back to using GSIS data.',
    mtConfirmation, [mbYes, mbNo], 0);
  if (Control = mrYes) then
  begin
    //Add the game to the database table
    try
      with dmMain.tblNFL_GSIS_Overrides do
      begin
        Active := TRUE;
        Append;
        FieldByName('CSS_GameID').AsInteger := dmMain.CSSQuery.FieldByName('game').AsInteger;
        FieldByName('Game_Description').AsString := dmMain.CSSQuery.FieldByName('awayName').AsString + ' @ ' +
          dmMain.CSSQuery.FieldByName('homeName').AsString;
        Post;
      end;
    except
      MessageDlg('An error occured while trying to set the selected game for override mode.',
        mtError, [mbOk], 0);
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// NEWS/PROMO UPDATE (OVERRIDE) FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Function to refresh the News/Promos Update grid
procedure TMainForm.RefreshNewsPromoUpdateGrid;
var
  i: SmallInt;
  NewsPromoUpdateRecPtr: ^NewsPromoUpdateRec;
  CollectionCount: SmallInt;
begin
  //Clear grid values
  if (NewsPromoUpdateGrid.Rows > 0) then
    NewsPromoUpdateGrid.DeleteRows (1, NewsPromoUpdateGrid.Rows);
  CollectionCount := NewsPromo_Update_Collection.Count;
  if (CollectionCount > 0) then
  begin
    NewsPromoUpdateGrid.StoreData := TRUE;
    NewsPromoUpdateGrid.Cols := 2;
    NewsPromoUpdateGrid.Rows := CollectionCount;
    //Populate the grid
    for i := 0 to CollectionCount-1 do
    begin
      NewsPromoUpdateRecPtr := NewsPromo_Update_Collection.At(i);
      //Check if automated league; if so, lookup display mnemonic
      NewsPromoUpdateGrid.Cell[1,i+1] := NewsPromoUpdateRecPtr^.Header;
      NewsPromoUpdateGrid.Cell[2,i+1] := NewsPromoUpdateRecPtr^.UserData;
    end;
  end;
end;

//Handler for adding selected note to list
procedure TMainForm.AddSelectedNotetoNewsPromoUpdateList1Click(Sender: TObject);
var
  i: SmallInt;
  CurrentRow: SmallInt;
  GametraxRecPtr: ^GametraxRec;
  NewsPromoUpdateRecPtr: ^NewsPromoUpdateRec;
begin
  //Check to see if template is for news/promos; if not, alert operator
  if (Gametrax_Collection.Count > 0) then
  begin
    CurrentRow := PlaylistGrid.CurrentDataRow;
    //Only proceed if a row was selected
    if (CurrentRow <> -1) then
    begin
      GametraxRecPtr := Gametrax_Collection.At(CurrentRow-1);
      //Check to make sure it's a note or promo
      if (GametraxRecPtr^.Template_ID <> NEWS_PROMO_TEMPLATE) then
      begin
        //Alert operator
        MessageDlg('The entry you selected is not a news or promo template page.',
                    mtError, [mbOk], 0);
      end
      else begin
        //Add element to news/promos update collection
        GetMem (NewsPromoUpdateRecPtr, SizeOf(NewsPromoUpdateRec));
        With NewsPromoUpdateRecPtr^ do
        begin
          Header := GametraxRecPtr^.League;
          UserData := GametraxRecPtr^.UserData[1];
          if (NewsPromo_Update_Collection.Count <= 20) then
          begin
            NewsPromo_Update_Collection.Insert(NewsPromoUpdateRecPtr);
            NewsPromo_Update_Collection.Pack;
          end;
        end;
        //Refresh the grid
        RefreshNewsPromoUpdateGrid;
      end;
    end;
  end;
end;

//Handler for deleting the selecting News/Promo update item from the list
procedure TMainForm.DeleteNewsPromoUpdateItemClick(Sender: TObject);
var
  i: SmallInt;
begin
  if (NewsPromoUpdateGrid.Rows > 0) then
  begin
    if (NewsPromoUpdateGrid.CurrentDataRow > 0) then
    begin
      //Delete the item
      NewsPromo_Update_Collection.AtDelete(NewsPromoUpdateGrid.CurrentDataRow-1);
      //Refresh the grid
      RefreshNewsPromoUpdateGrid;
    end
    else begin
      //Alert operator
      MessageDlg('You must select an item in the grid before trying to delete it.',
                  mtError, [mbOk], 0)
    end;
  end;
end;

//Handler for saving the News/Promo Update data out to the database
procedure TMainForm.DisplayNewsPromoUpdateBtnClick(Sender: TObject);
begin
  if (Enableregion1NewsPromos.Checked) then SaveNewsPromoUpdateToDatabase(1);
  if (Enableregion2NewsPromos.Checked) then SaveNewsPromoUpdateToDatabase(2);
  if (Enableregion3NewsPromos.Checked) then SaveNewsPromoUpdateToDatabase(3);
  if (Enableregion4NewsPromos.Checked) then SaveNewsPromoUpdateToDatabase(4);
  if (Enableregion5NewsPromos.Checked) then SaveNewsPromoUpdateToDatabase(5);
  if (Enableregion6NewsPromos.Checked) then SaveNewsPromoUpdateToDatabase(6);
  if (Enableregion7NewsPromos.Checked) then SaveNewsPromoUpdateToDatabase(7);
  if (Enableregion8NewsPromos.Checked) then SaveNewsPromoUpdateToDatabase(8);
  //Clear collection
  NewsPromo_Update_Collection.Clear;
  NewsPromo_Update_Collection.Pack;
  //Refresh the grid
  RefreshNewsPromoUpdateGrid;
end;
//General procedure to add a News/Promo Update to the database
procedure TMainForm.SaveNewsPromoUpdateToDatabase(RegionID: SmallInt);
var
  i: SmallInt;
  NewsPromoUpdateRecPtr: ^NewsPromoUpdateRec;
begin
  if (NewsPromo_Update_Collection.Count > 0) then
  begin
    try
      //Save out to database
      for i := 0 to NewsPromo_Update_Collection.Count-1 do
      begin
        try
          NewsPromoUpdateRecPtr := NewsPromo_Update_Collection.At(i);
          with dmMain.tblNews_Promo_Updates do
          begin
            Active := TRUE;
            Append;
            FieldByname('RegionID').AsInteger := RegionID;
            FieldByname('Timestamp').AsDateTime := Now;
            FieldByName('NewsPromoIndex').AsInteger := i+1;
            FieldByName('Header').AsString := NewsPromoUpdateRecPtr^.Header;
            FieldByName('UserData').AsString := NewsPromoUpdateRecPtr^.UserData;
            Post;
          end;
        except
          //Alert operator
          MessageDlg('Error occurred while trying to save News/Promo Update to database.',
                      mtError, [mbOk], 0)
        end;
      end;
    except
      //Alert operator
      MessageDlg('Error occurred while trying to save News/Promo Update to database.',
                  mtError, [mbOk], 0)
    end;
  end;
end;

//For NCAA ranking enable
procedure TMainForm.UseNCAARankClick(Sender: TObject);
begin
  //Set checkmarks
  UseNCAARank.Checked := TRUE;
  UseNCAASeed.Checked := FALSE;
  //Clear flag
  UseNCAASeeding := FALSE;
  //Store out
  StorePrefs;
end;
procedure TMainForm.UseNCAASeedClick(Sender: TObject);
begin
  //Set checkmarks
  UseNCAARank.Checked := FALSE;
  UseNCAASeed.Checked := TRUE;
  //Clear flag
  UseNCAASeeding := TRUE;
  //Store out
  StorePrefs;
end;

////////////////////////////////////////////////////////////////////////////////
// TWITTER MESSAGE LIST FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Handler to refresh available games gird (for exclusion)
procedure TMainForm.RefreshTwitterTalentGrid;
var
  i, j: SmallInt;
  TwitterTalentRecPtr: ^TwitterTalentRec;
  CollectionCount: SmallInt;
  CategoryRecPtr: ^CategoryRec;
begin
  //Refresh collection
  try
    //Init values - filter by selected sport
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    //Load in the data from the Twitter Talent table; iterate for all records in table
    Twitter_Talent_Collection.Clear;
    Twitter_Talent_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    //Filter by sport and talent enabled
    dmMain.Query1.SQL.Add('SELECT * FROM Talent_Twitter_Info WHERE ((League_Mnemonic = ' + QuotedStr(CategoryRecPtr^.Category_Sport) +
      ') AND (Talent_Enable=1))');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to CFB conference chips collection
        GetMem (TwitterTalentRecPtr, SizeOf(TwitterTalentRec));
        With TwitterTalentRecPtr^ do
        begin
          Twitter_Handle := dmMain.Query1.FieldByName('Twitter_Handle').AsString;
          Talent_Name := dmMain.Query1.FieldByName('Talent_Name').AsString;
          List_Name := dmMain.Query1.FieldByName('List_Name').AsString;
          League_Mnemonic := dmMain.Query1.FieldByName('League_Mnemonic').AsString;
          Talent_Enable := dmMain.Query1.FieldByName('Talent_Enable').AsBoolean;
          If (Twitter_Talent_Collection.Count <= 100) then
          begin
            //Add to collection
            Twitter_Talent_Collection.Insert(TwitterTalentRecPtr);
            Twitter_Talent_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;
  except

  end;
  //Clear grid values
  if (TwitterTalentGrid.Rows > 0) then
    TwitterTalentGrid.DeleteRows (1, TwitterTalentGrid.Rows);
  CollectionCount := Twitter_Talent_Collection.Count;
  if (CollectionCount > 0) then
  begin
    TwitterTalentGrid.StoreData := TRUE;
    TwitterTalentGrid.Cols := 1;
    TwitterTalentGrid.Rows := CollectionCount;
    //Populate the grid
    for i := 0 to CollectionCount-1 do
    begin
      TwitterTalentRecPtr := Twitter_Talent_Collection.At(i);
      //Only add if sport matches
      if (CategoryRecPtr^.Category_Sport = TwitterTalentRecPtr^.League_Mnemonic) then
      begin
        TwitterTalentGrid.Cell[1,i+1] := TwitterTalentRecPtr^.Talent_Name;
      end;
    end;
  end;
end;

//Handler to refresh available games grid (for exclusion)
procedure TMainForm.RefreshTwitterGamesGrid;
var
  i: SmallInt;
  ScheduleGameRecPtr: ^ScheduleGameRec;
  CollectionCount: SmallInt;
begin
  //Get current row
  //Clear grid values
  if (TwitterGamesGrid.Rows > 0) then
    TwitterGamesGrid.DeleteRows (1, TwitterGamesGrid.Rows);
  //Init values
  CollectionCount := Schedule_Game_Collection.Count;
  if (CollectionCount > 0) then
  begin
    TwitterGamesGrid.StoreData := TRUE;
    TwitterGamesGrid.Cols := 1;
    TwitterGamesGrid.Rows := CollectionCount;
    //Populate the grid
    for i := 0 to CollectionCount-1 do
    begin
      ScheduleGameRecPtr := Schedule_Game_Collection.At(i);
      TwitterGamesGrid.Cell[1,i+1] := ScheduleGameRecPtr^.Description;
    end;
  end;
end;

procedure TMainForm.ClearAllGameAssignmentsClick(Sender: TObject);
var
  i: SmallInt;
  Control: Word;
begin
  //Only act if entries in exclusion table
  if (dmMain.tblTwitterGameAssignments.RecordCount > 0) then
  begin
    //Prompt operator for confirm
    Control := MessageDlg('Are you sure you want to delete all previously specified Talent/Game Assignments for Twitter?', mtConfirmation, [mbYes, mbNo], 0);
    if (Control = mrYes) then
    begin
      //Delete all entries
      for i := 1 to dmMain.tblTwitterGameAssignments.RecordCount do
      begin
        dmMain.tblTwitterGameAssignments.Delete;
      end;
    end;
  end;
end;

//Handler for adding a game assignment
procedure TMainForm.AddGameAssignmentBtnClick(Sender: TObject);
var
  FoundRecord: Boolean;
  ScheduleGameRecPtr: ^ScheduleGameRec;
  TwitterTalentRecPtr: ^TwitterTalentRec;
begin
  //Add record
  if (Schedule_Game_Collection.Count > 0) and (Twitter_Talent_Collection.Count > 0) then
  begin
    //Refresh and get pointers to selected talent and game
    dmMain.tblTwitterGameAssignments.Active := FALSE;
    dmMain.tblTwitterGameAssignments.Active := TRUE;
    if (TwitterTalentGrid.CurrentDataRow < 1) then TwitterTalentGrid.CurrentDataRow := 1;
    if (TwitterGamesGrid.CurrentDataRow < 1) then TwitterGamesGrid.CurrentDataRow := 1;
    ScheduleGameRecPtr := Schedule_Game_Collection.At(TwitterGamesGrid.CurrentDataRow-1);
    TwitterTalentRecPtr := Twitter_Talent_Collection.At(TwitterTalentGrid.CurrentDataRow-1);

    //Check for existing record with same ID
    FoundRecord := dmMain.tblTwitterGameAssignments.Locate('Twitter_Handle; NFLDM_GameID', VarArrayOf([TwitterTalentRecPtr^.Twitter_Handle , ScheduleGameRecPtr^.NFLDM_GameID]), []);
    if (FoundRecord) then
      //Alert operator
      MessageDlg('A game has already been specified for the selected talent and game.', mtError, [mbOK], 0)
    else begin
      //Get data from collection object & add record
      dmMain.tblTwitterGameAssignments.Append;
      dmMain.tblTwitterGameAssignments.FieldByname('Twitter_Handle').AsString := TwitterTalentRecPtr^.Twitter_Handle;
      dmMain.tblTwitterGameAssignments.FieldByname('CSS_GameID').AsInteger := ScheduleGameRecPtr^.CSS_GameID;
      dmMain.tblTwitterGameAssignments.FieldByname('NFLDM_GameID').AsInteger := ScheduleGameRecPtr^.NFLDM_GameID;
      dmMain.tblTwitterGameAssignments.FieldByname('SI_GameID').AsInteger := 0;
      dmMain.tblTwitterGameAssignments.FieldByname('ST_GameID').AsString := '0';
      dmMain.tblTwitterGameAssignments.FieldByname('Description').AsString := TwitterTalentRecPtr^.Talent_Name + ': ' + ScheduleGameRecPtr^.Description;
      dmMain.tblTwitterGameAssignments.Post;
    end;
  end;
end;

//Handler to view & enable/disable Twitter messages
procedure TMainForm.ViewTwitterMessagesButtonClick(Sender: TObject);
var
  i, j, SetComboIndex: SmallInt;
  CategoryRecPtr: ^CategoryRec;
  Modal: TTwitterMessageEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblTwitterMessages.Active := FALSE;
  dmMain.tblTwitterMessages.Active := TRUE;
  Modal := TTwitterMessageEditorDlg.Create(Application);
  try
    //Populate the league combo box
    if (Categories_Collection.Count > 0) then
    begin
      j := 0;
      SetComboIndex := 0;
      for i := 0 to Categories_Collection.Count-1 do
      begin
        CategoryRecPtr := Categories_Collection.At(i);
        if (CategoryRecPtr^.Category_Is_Sport) then
        begin
          inc (j);
          Modal.LeagueSelectComboBox.Items.Add(CategoryRecPtr^.Category_Sport);
          if (i = LeagueTab.TabIndex) then SetComboIndex := j-1;
        end;
      end;
    end;
    CategoryRecPtr := Categories_Collection.At(LeagueTab.TabIndex);
    Modal.LeagueSelectComboBox.ItemIndex := SetComboIndex;
    Modal.AddMessagesBtn.Visible := FALSE;
    Modal.NoteLabel.Visible := FALSE;
    Modal.UseTemplateWithoutSubHeader.Visible := FALSE;
    Modal.TwitterMessagesGrid.DataSource := dmMain.dsTwitterMessages;
    Modal.dbNavigator1.DataSource := dmMain.dsTwitterMessages;
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free
  end;
end;

//Handler for displaying editor dialog for Twitter Talent Info
procedure TMainForm.EditTalentTwitterInfo1Click(Sender: TObject);
var
  Modal: TTwitterTalentEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblTwitterTalentInfo.Active := FALSE;
  dmMain.tblTwitterTalentInfo.Active := TRUE;
  Modal := TTwitterTalentEditorDlg.Create(Application);
  try
    Modal.TwitterTalentGrid.DataSource := dmMain.dsTwitterTalentInfo;
    Modal.dbNavigator1.DataSource := dmMain.dsTwitterTalentInfo;
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free
  end;
end;

//Handler to delete old Twitter messages
procedure TMainForm.DeleteTwitterMessagesButtonClick(Sender: TObject);
var
  Control: Word;
begin
  //Confirm with operator
  Control := MessageDlg('This will delete all Twitter messages older than today. Do you want to proceed?.', mtConfirmation, [mbYes, mbNo], 0);
  if (Control = mrYes) then
  try
    with dmMain.TwitterDataQuery do
    begin
      Close;
      SQL.Clear;
      SQL.Add('sp_FlushTwitterMessages ' + QuotedStr(DateTimeToStr(Now)));
      ExecSQL;
    end;
  except

  end;
end;

//Handler for Add Twitter Messages to Playlist button
procedure TMainForm.AddTwitterMessagesBtnClick(Sender: TObject);
var
  i, j, SetComboIndex: SmallInt;
  CategoryRecPtr: ^CategoryRec;
  Modal: TTwitterMessageEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblTwitterMessages.Active := FALSE;
  dmMain.tblTwitterMessages.Active := TRUE;
  Modal := TTwitterMessageEditorDlg.Create(Application);
  try
    //Populate the league combo box
    if (Categories_Collection.Count > 0) then
    begin
      j := 0;
      SetComboIndex := 0;
      for i := 0 to Categories_Collection.Count-1 do
      begin
        CategoryRecPtr := Categories_Collection.At(i);
        if (CategoryRecPtr^.Category_Is_Sport) then
        begin
          inc (j);
          Modal.LeagueSelectComboBox.Items.Add(CategoryRecPtr^.Category_Sport);
          if (i = LeagueTab.TabIndex) then SetComboIndex := j-1;
        end;
      end;
    end;
    CategoryRecPtr := Categories_Collection.At(LeagueTab.TabIndex);
    Modal.LeagueSelectComboBox.ItemIndex := SetComboIndex;
    //Setup dialog
    Modal.AddMessagesBtn.Caption := 'Add Selected Messages to Playlist';
    Modal.AddMessagesBtn.Visible := TRUE;
    Modal.NoteLabel.Visible := TRUE;
    Modal.UseTemplateWithoutSubHeader.Visible := TRUE;
    Modal.InsertMode := FALSE;
    Modal.TwitterMessagesGrid.DataSource := dmMain.dsTwitterMessages;
    Modal.dbNavigator1.DataSource := dmMain.dsTwitterMessages;
    //Set to listbox mode
    Modal.TwitterMessagesGrid.GridMode := gmListBox;
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free;
    dmMain.tblTwitterMessages.Filtered := FALSE;
  end;
end;

//Handler for Insert Twitter Messages into Playlist button
procedure TMainForm.InsertTwitterMessagesBtnClick(Sender: TObject);
var
  i,j: SmallInt;
  CategoryRecPtr: ^CategoryRec;
  Modal: TTwitterMessageEditorDlg;
  Control: Word;
  SetComboIndex: SmallInt;
begin
  //Refresh & launch dialog
  dmMain.tblTwitterMessages.Active := FALSE;
  dmMain.tblTwitterMessages.Active := TRUE;
  Modal := TTwitterMessageEditorDlg.Create(Application);

  try
    //Populate the league combo box
    if (Categories_Collection.Count > 0) then
    begin
      j := 0;
      SetComboIndex := 0;
      for i := 0 to Categories_Collection.Count-1 do
      begin
        CategoryRecPtr := Categories_Collection.At(i);
        if (CategoryRecPtr^.Category_Is_Sport) then
        begin
          inc (j);
          Modal.LeagueSelectComboBox.Items.Add(CategoryRecPtr^.Category_Sport);
          if (i = LeagueTab.TabIndex) then SetComboIndex := j-1;
        end;
      end;
    end;
    CategoryRecPtr := Categories_Collection.At(LeagueTab.TabIndex);
    Modal.LeagueSelectComboBox.ItemIndex := SetComboIndex;
    //Setup dialog
    Modal.AddMessagesBtn.Caption := 'Insert Selected Messages into Playlist';
    Modal.AddMessagesBtn.Visible := TRUE;
    Modal.NoteLabel.Visible := TRUE;
    Modal.UseTemplateWithoutSubHeader.Visible := TRUE;
    Modal.InsertMode := TRUE;
    Modal.TwitterMessagesGrid.DataSource := dmMain.dsTwitterMessages;
    Modal.dbNavigator1.DataSource := dmMain.dsTwitterMessages;
    //Set to listbox mode
    Modal.TwitterMessagesGrid.GridMode := gmListBox;
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free;
    dmMain.tblTwitterMessages.Filtered := FALSE;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// SCORE ALERT TEST FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Test Score Alert animation
procedure TMainForm.TestScoreAlertAnimationBtnClick(Sender: TObject);
var
  k: SmallInt;
  CurrentGameData: GameRec;
  TemplateInfo: TemplateDefsRec;
  GametraxRecPtr: ^GametraxRec;
  ScoringUpdateData: ScoringUpdateRec;
  CSS_GameID: SmallInt;
  SI_GCode: String;
  NFLDM_GameID: LongInt;
  ST_Game_ID: String;
  EventGUID: TGUID;
  CategoryRecPtr: ^CategoryRec;
begin
  try
    //Get the game ID
    //Check to see if category tab is a sport; if so, show and populate games grid
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    if ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS')) OR
       ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS')) then
    begin
      CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
      NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
      SI_GCode := '0';
      ST_Game_ID := '0'; //Default to blank for now
    end
    else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC')) OR
           ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC'))then
    begin
      CSS_GameID := 0;
      SI_GCode := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
      NFLDM_GameID := 0; //Default to blank for now
      ST_Game_ID := '0'; //Default to blank for now
    end
    else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'NFLDM')) OR
            ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'NFLDM')) then
    begin
      if (ConnectToCSS) then
      begin
        CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
        NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
      end
      else begin
        CSS_GameID := 0;
        NFLDM_GameID := dmMain.DatamartQuery.FieldByname('GameKey').AsInteger; //Default to blank for now
      end;
      SI_GCode := '0';
      ST_Game_ID := '0'; //Default to blank for now
    end;

    //Get the game information using the Game ID from the GameUpdateRec - force variables for league & data sources
    CurrentGameData := EngineInterface.GetGameData('NFL', SI_GCode, ST_Game_ID, CSS_GameID, NFLDM_GameID, 'NFLDM', 'CBSCSS');

    //Load temlate for special game update (reserved ID 9999)
    TemplateInfo := EngineInterface.LoadTempTemplateFields(SCORE_ALERT_ANIMATION);

    //Setup record to do play description - append to end of collection; will be deleted after played
    GetMem (GametraxRecPtr, SizeOf(GametraxRec));

    //If the game is final, show the team record; otherwise, show the last scoring play
    //Set game ID values
    GametraxRecPtr^.CSS_GameID := CSS_GameID;
    GametraxRecPtr^.SI_GCode := SI_GCode;
    GametraxRecPtr^.NFLDM_GameID := NFLDM_GameID;
    GametraxRecPtr^.ST_Game_ID := ST_Game_ID;
    //Set remaining values
    With GametraxRecPtr^ do
    begin
      //Set values for collection record
      Event_Index := GameTrax_Collection.Count+1;
      CreateGUID(EventGUID);
      Event_GUID := EventGUID;
      Enabled := TRUE;
      Template_ID := SCORE_ALERT_ANIMATION;
      Record_Type := GetRecordTypeFromTemplateID(SCORE_ALERT_ANIMATION);
      Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
      Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
      if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
      League := 'NFL';
      Description := 'SCORE ALERT';
      SponsorLogo_Name := ' ';
      SponsorLogo_Dwell := 0;
      SponsorLogo_Region := 0;
      Is_Child := FALSE;
      for k := 1 to 25 do UserData[k] := '';
      //Set the start/end enable times to the template defaults
      StartEnableDateTime := Now-1;
      EndEnableDateTime := Now+1;
      DwellTime := GetTemplateInformation(SCORE_ALERT_ANIMATION).Default_Dwell;
      Comments := ' ';
      Selected := FALSE;
      //Append object to end of collection
      GameTrax_Collection.Insert(GameTraxRecPtr);
      GameTrax_Collection.Pack;
    end;

    //Trigger the display of the last record in the collection (will be alert animation)
    EngineInterface.SendGameTraxRecord(FALSE, Gametrax_Collection.Count-1);
    //Clear the record from the end of the collection
    if (Gametrax_Collection.Count > 0) then
    begin
      Gametrax_Collection.AtDelete(Gametrax_Collection.Count-1);
      Gametrax_Collection.Pack;
    end;
  except

  end;
end;

//Test Score Alert play description
procedure TMainForm.TestScoreAlertPlayBtnClick(Sender: TObject);
var
  k: SmallInt;
  CurrentGameData: GameRec;
  TemplateInfo: TemplateDefsRec;
  GametraxRecPtr: ^GametraxRec;
  ScoringUpdateData: ScoringUpdateRec;
  CSS_GameID: SmallInt;
  SI_GCode: String;
  NFLDM_GameID: LongInt;
  ST_Game_ID: String;
  EventGUID: TGUID;
  CategoryRecPtr: ^CategoryRec;
begin
  try
    //Get the game ID
    //Check to see if category tab is a sport; if so, show and populate games grid
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    if ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'CBSCSS')) OR
       ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'CBSCSS')) then
    begin
      CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
      NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
      SI_GCode := '0';
      ST_Game_ID := '0'; //Default to blank for now
    end
    else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'STATSINC')) OR
           ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'STATSINC'))then
    begin
      CSS_GameID := 0;
      SI_GCode := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
      NFLDM_GameID := 0; //Default to blank for now
      ST_Game_ID := '0'; //Default to blank for now
    end
    else if ((UseBackupDataSource=TRUE) AND (CategoryRecPtr^.Backup_Data_Source = 'NFLDM')) OR
            ((UseBackupDataSource=FALSE) AND (CategoryRecPtr^.Primary_Data_Source = 'NFLDM')) then
    begin
      if (ConnectToCSS) then
      begin
        CSS_GameID := dmMain.CSSQuery.FieldByname('game').AsInteger;
        NFLDM_GameID := dmMain.CSSQuery.FieldByname('GameKey').AsInteger;
      end
      else begin
        CSS_GameID := 0;
        NFLDM_GameID := dmMain.DatamartQuery.FieldByname('GameKey').AsInteger; //Default to blank for now
      end;
      SI_GCode := '0';
      ST_Game_ID := '0'; //Default to blank for now
    end;

    //Get the game information using the Game ID from the GameUpdateRec - force variables for league & data sources
    CurrentGameData := EngineInterface.GetGameData('NFL', SI_GCode, ST_Game_ID, CSS_GameID, NFLDM_GameID, 'NFLDM', 'CBSCSS');

    //Load temlate for special game update (reserved ID 9999)
    TemplateInfo := EngineInterface.LoadTempTemplateFields(SCORE_ALERT_ANIMATION);

    //Setup record to do play description - append to end of collection; will be deleted after played
    GetMem (GametraxRecPtr, SizeOf(GametraxRec));

    //If the game is final, show the team record; otherwise, show the last scoring play
    //Set game ID values
    GametraxRecPtr^.CSS_GameID := CSS_GameID;
    GametraxRecPtr^.SI_GCode := SI_GCode;
    GametraxRecPtr^.NFLDM_GameID := NFLDM_GameID;
    GametraxRecPtr^.ST_Game_ID := ST_Game_ID;
    //If the game is final, show the team record; otherwise, show the last scoring play
    if (CurrentGameData.GameState = 5) then
    begin
      //Set remaining values
      With GametraxRecPtr^ do
      begin
        //Set values for collection record
        Event_Index := GameTrax_Collection.Count+1;
        CreateGUID(EventGUID);
        Event_GUID := EventGUID;
        Enabled := TRUE;
        Template_ID := NEUTRAL_SCORING_UPDATE_PLAY_DESCRIPTION;
        Record_Type := GetRecordTypeFromTemplateID(SCORE_ALERT_ANIMATION);
        Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
        Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
        if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
        League := 'NFL';
        Description := 'SCORE ALERT';
        SponsorLogo_Name := ' ';
        SponsorLogo_Dwell := 0;
        SponsorLogo_Region := 0;
        Is_Child := FALSE;
        if (CurrentGameData.VScore > CurrentGameData.HScore) then
          UserData[1] := CurrentGameData.VName + ' ' + CurrentGameData.VRecord + ' def ' + CurrentGameData.HName + ' ' + CurrentGameData.HRecord
        else if (CurrentGameData.HScore > CurrentGameData.VScore) then
          UserData[1] := CurrentGameData.HName + ' ' + CurrentGameData.HRecord + ' def ' + CurrentGameData.VName + ' ' + CurrentGameData.VRecord
        //Could not find winning team so indicate tie
        else
          UserData[1] := CurrentGameData.HName + ' ' + CurrentGameData.HRecord + ' ties ' + CurrentGameData.VName + ' ' + CurrentGameData.VRecord;
        //Null out the rest of the user text fields
        for k := 2 to 25 do UserData[k] := '';
        //Set the start/end enable times to the template defaults
        StartEnableDateTime := Now-1;
        EndEnableDateTime := Now+1;
        DwellTime := GetTemplateInformation(SCORE_ALERT_ANIMATION).Default_Dwell;
        Comments := ' ';
        Selected := FALSE;
        //Append object to end of collection
        GameTrax_Collection.Insert(GameTraxRecPtr);
        GameTrax_Collection.Pack;
      end;
    end
    else begin
      //Set remaining values
      With GametraxRecPtr^ do
      begin
        //Set values for collection record
        Event_Index := GameTrax_Collection.Count+1;
        CreateGUID(EventGUID);
        Event_GUID := EventGUID;
        Enabled := TRUE;
        Record_Type := GetRecordTypeFromTemplateID(SCORE_ALERT_ANIMATION);
        Primary_Data_Source := CategoryRecPtr^.Primary_Data_Source;
        Backup_Data_Source := CategoryRecPtr^.Backup_Data_Source;
        if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
        League := 'NFL';
        Description := 'SCORE ALERT';
        SponsorLogo_Name := ' ';
        SponsorLogo_Dwell := 0;
        SponsorLogo_Region := 0;
        Is_Child := FALSE;

        //Use function to get play description and store in UserData field
        ScoringUpdateData := EngineInterface.GetScoringPlayDescription(NFLDM_GameID);
        UserData[1] := ScoringUpdateData.ScoreDescription;

        //Set template ID
        if (ScoringUpdateData.NFLClubCode = CurrentGameData.VNFLMnemonic) then
          Template_ID := VISITOR_SCORING_UPDATE_PLAY_DESCRIPTION
        else if (ScoringUpdateData.NFLClubCode = CurrentGameData.HNFLMnemonic) then
          Template_ID := HOME_SCORING_UPDATE_PLAY_DESCRIPTION
        else
          Template_ID := NEUTRAL_SCORING_UPDATE_PLAY_DESCRIPTION;

        //Null out the rest of the user text fields
        for k := 2 to 25 do UserData[k] := '';
        //Set the start/end enable times to the template defaults
        StartEnableDateTime := Now-1;
        EndEnableDateTime := Now+1;
        DwellTime := GetTemplateInformation(SCORE_ALERT_ANIMATION).Default_Dwell;
        Comments := ' ';
        Selected := FALSE;
        //Append object to end of collection
        GameTrax_Collection.Insert(GameTraxRecPtr);
        GameTrax_Collection.Pack;
      end;
    end;

    //Trigger the display of the last record in the collection (will be alert animation)
    EngineInterface.SendGameTraxRecord(FALSE, Gametrax_Collection.Count-1);
    //Clear the record from the end of the collection
    if (Gametrax_Collection.Count > 0) then
    begin
      Gametrax_Collection.AtDelete(Gametrax_Collection.Count-1);
      Gametrax_Collection.Pack;
    end;
  except

  end;
end;

end.
