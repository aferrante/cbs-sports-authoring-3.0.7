unit ManualGameEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Spin, Buttons, ExtCtrls, Mask, ComCtrls;

type
  TManualGameDlg = class(TForm)
    ManualGameEditPanel: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label2: TLabel;
    VisitorScore: TSpinEdit;
    Label3: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    HomeScore: TSpinEdit;
    Label10: TLabel;
    Label11: TLabel;
    GamePhase: TComboBox;
    Label12: TLabel;
    ManualGamesOverrideEnable: TCheckBox;
    VisitorName: TEdit;
    HomeName: TEdit;
    GameState: TRadioGroup;
    EndOfPeriod: TCheckBox;
    ManualOddsEditPanel: TPanel;
    ManualOddsOverrideEnable: TCheckBox;
    Label1: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label4: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    VOpenSpread: TEdit;
    VOpenMoneyLine: TEdit;
    HOpenSpread: TEdit;
    HOpenMoneyLine: TEdit;
    OpenTotal: TEdit;
    VCurrentSpread: TEdit;
    VCurrentMoneyLine: TEdit;
    HCurrentSpread: TEdit;
    HCurrentMoneyLine: TEdit;
    CurrentTotal: TEdit;
    OddsDataNotAvailableLabel: TLabel;
    Label20: TLabel;
    Panel2: TPanel;
    Label21: TLabel;
    SelectedGameLabel: TLabel;
    StartTime: TDateTimePicker;
    StartTimeLabel: TLabel;
    Label22: TLabel;
    OddsStartTime: TDateTimePicker;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ManualGameDlg: TManualGameDlg;

implementation

{$R *.DFM}

end.
