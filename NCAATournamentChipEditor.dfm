object NCAATournamentChipEditorDlg: TNCAATournamentChipEditorDlg
  Left = 368
  Top = 170
  BorderStyle = bsDialog
  Caption = 'NCAA Tournament Round Info Editor'
  ClientHeight = 547
  ClientWidth = 698
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 16
    Top = 11
    Width = 665
    Height = 470
    BevelWidth = 2
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 14
      Width = 351
      Height = 20
      Caption = 'NCAA Tournament Round (Chip) Info Editor'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 102
      Top = 398
      Width = 37
      Height = 20
      Caption = 'First'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 154
      Top = 398
      Width = 38
      Height = 20
      Caption = 'Prior'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 208
      Top = 398
      Width = 37
      Height = 20
      Caption = 'Next'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 261
      Top = 398
      Width = 36
      Height = 20
      Caption = 'Last'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 375
      Top = 398
      Width = 33
      Height = 20
      Caption = 'Edit'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 422
      Top = 398
      Width = 37
      Height = 20
      Caption = 'Post'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 467
      Top = 398
      Width = 56
      Height = 20
      Caption = 'Cancel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 534
      Top = 398
      Width = 36
      Height = 20
      Caption = 'Refr'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 307
      Top = 398
      Width = 54
      Height = 20
      Caption = 'Delete'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object tsDBGrid1: TtsDBGrid
      Left = 189
      Top = 40
      Width = 287
      Height = 353
      CellSelectMode = cmNone
      CheckBoxStyle = stCheck
      Cols = 2
      DatasetType = dstStandard
      DataSource = dmMain.dsTournament_Headers
      DefaultRowHeight = 18
      ExactRowCount = True
      ExportDelimiter = ','
      FieldState = fsCustomized
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HeadingFont.Charset = DEFAULT_CHARSET
      HeadingFont.Color = clWindowText
      HeadingFont.Height = -13
      HeadingFont.Name = 'MS Sans Serif'
      HeadingFont.Style = [fsBold]
      HeadingHeight = 20
      HeadingParentFont = False
      ParentFont = False
      ParentShowHint = False
      RowChangedIndicator = riAutoReset
      RowMoving = False
      ShowHint = False
      TabOrder = 0
      Version = '2.20.26'
      XMLExport.Version = '1.0'
      XMLExport.DataPacketVersion = '2.0'
      DataBound = True
      ColProperties = <
        item
          DataCol = 1
          FieldName = 'HeaderID'
          Col.FieldName = 'HeaderID'
          Col.Heading = 'ID'
          Col.Width = 60
          Col.AssignedValues = '?'
        end
        item
          DataCol = 2
          FieldName = 'HeaderText'
          Col.FieldName = 'HeaderText'
          Col.Heading = 'Header Text'
          Col.Width = 191
          Col.AssignedValues = '?'
        end>
    end
    object DBNavigator1: TDBNavigator
      Left = 92
      Top = 424
      Width = 486
      Height = 33
      DataSource = dmMain.dsTournament_Headers
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbDelete, nbEdit, nbPost, nbCancel, nbRefresh]
      TabOrder = 1
    end
  end
  object BitBtn1: TBitBtn
    Left = 296
    Top = 496
    Width = 105
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkClose
  end
end
