object PlaylistViewerDlg: TPlaylistViewerDlg
  Left = 736
  Top = 397
  Width = 931
  Height = 538
  Caption = 'Ticker Playlist Entry Viewer'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 10
    Top = 4
    Width = 172
    Height = 16
    Caption = 'Entries in Ticker Playlist:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object tsDBGrid1: TtsDBGrid
    Left = 10
    Top = 24
    Width = 903
    Height = 425
    CellSelectMode = cmNone
    CheckBoxStyle = stCheck
    ColMoving = False
    Cols = 5
    ColSelectMode = csNone
    DatasetType = dstStandard
    DataSource = dmMain.dsQuery1
    DefaultRowHeight = 18
    ExactRowCount = True
    ExportDelimiter = ','
    FieldState = fsCustomized
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    GridMode = gmListBox
    HeadingFont.Charset = DEFAULT_CHARSET
    HeadingFont.Color = clWindowText
    HeadingFont.Height = -13
    HeadingFont.Name = 'MS Sans Serif'
    HeadingFont.Style = [fsBold]
    HeadingHeight = 18
    HeadingParentFont = False
    ParentFont = False
    ParentShowHint = False
    RowChangedIndicator = riAutoReset
    RowMoving = False
    RowSelectMode = rsSingle
    ShowHint = False
    TabOrder = 0
    Version = '2.20.23'
    XMLExport.Version = '1.0'
    XMLExport.DataPacketVersion = '2.0'
    DataBound = True
    ColProperties = <
      item
        DataCol = 1
        FieldName = 'Event_Index'
        Col.FieldName = 'Event_Index'
        Col.Heading = 'Index'
        Col.Width = 53
        Col.AssignedValues = '?'
      end
      item
        DataCol = 2
        FieldName = 'League_ID'
        Col.FieldName = 'League_ID'
        Col.Heading = 'League ID'
        Col.Width = 95
        Col.AssignedValues = '?'
      end
      item
        DataCol = 3
        FieldName = 'Record_Type'
        Col.FieldName = 'Record_Type'
        Col.Heading = 'Segment Type ID'
        Col.Width = 131
        Col.AssignedValues = '?'
      end
      item
        DataCol = 4
        FieldName = 'TeamMnemonic'
        Col.FieldName = 'TeamMnemonic'
        Col.Heading = 'Team'
        Col.Width = 105
        Col.AssignedValues = '?'
      end
      item
        DataCol = 5
        FieldName = 'Zipper_Text'
        Col.FieldName = 'Zipper_Text'
        Col.Heading = 'Ticker Text'
        Col.Width = 828
        Col.AssignedValues = '?'
      end>
  end
  object BitBtn1: TBitBtn
    Left = 409
    Top = 460
    Width = 89
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkOK
  end
end
