unit Preferences;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, StBase, {StShBase, StBrowsr,} Spin, Mask;

type
  TPrefs = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel2: TPanel;
    Label2: TLabel;
    Edit2: TEdit;
    Panel1: TPanel;
    Label1: TLabel;
    SpinEdit1: TSpinEdit;
    Panel3: TPanel;
    Label3: TLabel;
    Edit1: TEdit;
    Edit3: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    Panel5: TPanel;
    RadioGroup2: TRadioGroup;
    Label4: TLabel;
    Edit4: TEdit;
    Label5: TLabel;
    Edit5: TEdit;
    Panel6: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    SpinEdit2: TSpinEdit;
    SpinEdit3: TSpinEdit;
    RadioGroup1: TRadioGroup;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox1: TCheckBox;
    RadioGroup3: TRadioGroup;
    RadioGroup4: TRadioGroup;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    Label6: TLabel;
    Edit6: TEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Prefs: TPrefs;

implementation

{$R *.DFM}

end.
