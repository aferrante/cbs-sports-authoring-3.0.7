object DBEditorDlg: TDBEditorDlg
  Left = 302
  Top = 235
  BorderStyle = bsDialog
  Caption = 'Database Editor'
  ClientHeight = 549
  ClientWidth = 1081
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 16
    Top = 11
    Width = 1049
    Height = 470
    BevelWidth = 2
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 14
      Width = 190
      Height = 20
      Caption = 'Teams Database Editor'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 49
      Top = 398
      Width = 37
      Height = 20
      Caption = 'First'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 151
      Top = 398
      Width = 38
      Height = 20
      Caption = 'Prior'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 250
      Top = 398
      Width = 37
      Height = 20
      Caption = 'Next'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 351
      Top = 398
      Width = 36
      Height = 20
      Caption = 'Last'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 454
      Top = 398
      Width = 33
      Height = 20
      Caption = 'Add'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 546
      Top = 398
      Width = 54
      Height = 20
      Caption = 'Delete'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 656
      Top = 398
      Width = 33
      Height = 20
      Caption = 'Edit'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 755
      Top = 398
      Width = 37
      Height = 20
      Caption = 'Post'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 848
      Top = 398
      Width = 56
      Height = 20
      Caption = 'Cancel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 944
      Top = 398
      Width = 65
      Height = 20
      Caption = 'Refresh'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object tsDBGrid1: TtsDBGrid
      Left = 16
      Top = 40
      Width = 1017
      Height = 353
      CellSelectMode = cmNone
      CheckBoxStyle = stCheck
      Cols = 12
      DatasetType = dstStandard
      DataSource = dmMain.dsTeams
      DefaultRowHeight = 18
      ExactRowCount = True
      ExportDelimiter = ','
      FieldState = fsCustomized
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HeadingFont.Charset = DEFAULT_CHARSET
      HeadingFont.Color = clWindowText
      HeadingFont.Height = -13
      HeadingFont.Name = 'MS Sans Serif'
      HeadingFont.Style = [fsBold]
      HeadingHeight = 20
      HeadingParentFont = False
      ParentFont = False
      ParentShowHint = False
      RowChangedIndicator = riAutoReset
      RowMoving = False
      ShowHint = False
      TabOrder = 0
      Version = '2.20.26'
      XMLExport.Version = '1.0'
      XMLExport.DataPacketVersion = '2.0'
      DataBound = True
      ColProperties = <
        item
          DataCol = 1
          FieldName = 'ID'
          Col.FieldName = 'ID'
          Col.Heading = 'ID'
          Col.AssignedValues = '?'
        end
        item
          DataCol = 2
          FieldName = 'LeagueID'
          Col.FieldName = 'LeagueID'
          Col.Heading = 'League ID'
          Col.Width = 80
          Col.AssignedValues = '?'
        end
        item
          DataCol = 3
          FieldName = 'League'
          Col.FieldName = 'League'
          Col.Heading = 'League'
          Col.AssignedValues = '?'
        end
        item
          DataCol = 4
          FieldName = 'City'
          Col.FieldName = 'City'
          Col.Heading = 'City'
          Col.Width = 121
          Col.AssignedValues = '?'
        end
        item
          DataCol = 5
          FieldName = 'Team'
          Col.FieldName = 'Team'
          Col.Heading = 'Team'
          Col.Width = 123
          Col.AssignedValues = '?'
        end
        item
          DataCol = 6
          FieldName = 'FullName'
          Col.FieldName = 'FullName'
          Col.Heading = 'Full Name'
          Col.Width = 200
          Col.AssignedValues = '?'
        end
        item
          DataCol = 7
          FieldName = 'StatsIncID'
          Col.FieldName = 'StatsIncID'
          Col.Heading = 'Stats Inc ID'
          Col.Width = 89
          Col.AssignedValues = '?'
        end
        item
          DataCol = 8
          FieldName = 'StatsIncAlias'
          Col.FieldName = 'StatsIncAlias'
          Col.Heading = 'Stats Inc Alias'
          Col.Width = 106
          Col.AssignedValues = '?'
        end
        item
          DataCol = 9
          FieldName = 'OddsID'
          Col.FieldName = 'OddsID'
          Col.Heading = 'Odds ID'
          Col.Width = 72
          Col.AssignedValues = '?'
        end
        item
          DataCol = 10
          FieldName = 'ShortDisplayName1'
          Col.FieldName = 'ShortDisplayName1'
          Col.Heading = 'Display Mnemonic'
          Col.Width = 137
          Col.AssignedValues = '?'
        end
        item
          DataCol = 11
          FieldName = 'ShortDisplayName2'
          Col.FieldName = 'ShortDisplayName2'
          Col.Heading = 'Display Name'
          Col.Width = 162
          Col.AssignedValues = '?'
        end
        item
          DataCol = 12
          FieldName = 'Rank'
          Col.FieldName = 'Rank'
          Col.Heading = 'Rank'
          Col.Width = 52
          Col.AssignedValues = '?'
        end>
    end
    object DBNavigator1: TDBNavigator
      Left = 16
      Top = 424
      Width = 1010
      Height = 33
      DataSource = dmMain.dsTeams
      TabOrder = 1
    end
  end
  object BitBtn1: TBitBtn
    Left = 488
    Top = 496
    Width = 105
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkClose
  end
end
