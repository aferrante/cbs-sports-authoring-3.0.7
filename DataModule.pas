unit DataModule;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, {DBISAMTb,} {MSGParserServer,} ScktComp, ExtCtrls;

type
  ERecvError = class(Exception);

  TdmMain = class(TDataModule)
    dbCBS: TADOConnection;
    tblTicker_Groups: TADOTable;
    tblTicker_Elements: TADOTable;
    dsTicker_Groups: TDataSource;
    Query1: TADOQuery;
    dsQuery1: TDataSource;
    tblCustom_Segments: TADOTable;
    tblScheduled_Ticker_Groups: TADOTable;
    dsScheduled_Ticker_Groups: TDataSource;
    tblSponsor_Logos: TADOTable;
    dsSponsor_Logos: TDataSource;
    tblGame_Notes: TADOTable;
    tblLeagues: TADOTable;
    tblPromo_Logos: TADOTable;
    dsPromo_Logos: TDataSource;
    dbSportBase: TADOConnection;
    SportbaseQuery: TADOQuery;
    dsSportbaseQuery: TDataSource;
    Query2: TADOQuery;
    dsQuery2: TDataSource;
    Query3: TADOQuery;
    SportbaseQuery2: TADOQuery;
    tblBug_Elements: TADOTable;
    tblBug_Groups: TADOTable;
    tblScheduled_Bug_Groups: TADOTable;
    dsBug_Groups: TDataSource;
    dsScheduled_Bug_Groups: TDataSource;
    tblGameTrax_Elements: TADOTable;
    tblGameTrax_Groups: TADOTable;
    tblScheduled_GameTrax_Groups: TADOTable;
    dsGameTrax_Groups: TDataSource;
    dsScheduled_GameTrax_Groups: TDataSource;
    SportbaseQuery3: TADOQuery;
    dsSportbaseQuery3: TDataSource;
    tblTemplate_Defs: TADOTable;
    tblTeams: TADOTable;
    dsTeams: TDataSource;
    tblGame_Phase_Codes: TADOTable;
    dsGame_Phase_Codes: TDataSource;
    tblCustom_News_Headers: TADOTable;
    dsLeague_Codes: TDataSource;
    dbCSS: TADOConnection;
    CSSQuery: TADOQuery;
    dbNFLDatamart: TADOConnection;
    DatamartQuery: TADOQuery;
    dsCSSQuery: TDataSource;
    dsDatamartQuery: TDataSource;
    tblFlags: TADOTable;
    CSSQuery2: TADOQuery;
    DatamartQuery2: TADOQuery;
    tblNFLGames: TADOTable;
    DatamartQuery3: TADOQuery;
    tblGameTrax_Regional_Game_Exclusions: TADOTable;
    dsGameTrax_Regional_Game_Exclusions: TDataSource;
    tblGame_Updates: TADOTable;
    SportbaseQuery4: TADOQuery;
    dsCFB_Conference_Chips: TDataSource;
    tblCFB_Conference_Chips: TADOTable;
    SportbaseQuery5: TADOQuery;
    tblFantasy_Updates: TADOTable;
    tblNFL_GSIS_Overrides: TADOTable;
    dsNFL_GSIS_Overrides: TDataSource;
    tblNews_Promo_Updates: TADOTable;
    tblTwitterGameAssignments: TADOTable;
    dsTwitterGameAssignments: TDataSource;
    dsTwitterMessages: TDataSource;
    dbVDSSocialMediaGateway: TADOConnection;
    tblTwitterTalentInfo: TADOTable;
    dsTwitterTalentInfo: TDataSource;
    TwitterDataQuery: TADOQuery;
    TwitterMessagesQuery: TADOQuery;
    tblTwitterMessages: TADOTable;
    DatamartQuery4: TADOQuery;
    tblTournament_Headers: TADOTable;
    dsTournament_Headers: TDataSource;
    Query6: TADOQuery;
    tblCFB_Team_Logos_Names: TADOTable;
    dsCFB_Team_Logos_Names: TDataSource;
    dsTemplate_Defs: TDataSource;
  private
  public
  end;

var
  dmMain: TdmMain;

implementation

uses Main;

{$R *.DFM}


end.
