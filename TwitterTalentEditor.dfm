object TwitterTalentEditorDlg: TTwitterTalentEditorDlg
  Left = 524
  Top = 111
  BorderStyle = bsDialog
  Caption = 'Twitter Talent Info Database Editor'
  ClientHeight = 557
  ClientWidth = 775
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 17
    Top = 10
    Width = 279
    Height = 16
    Caption = 'Twitter Talent Info Records in Database:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object First: TLabel
    Left = 70
    Top = 433
    Width = 31
    Height = 16
    Caption = 'First'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 137
    Top = 433
    Width = 34
    Height = 16
    Caption = 'Prior'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 203
    Top = 433
    Width = 32
    Height = 16
    Caption = 'Next'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 270
    Top = 433
    Width = 30
    Height = 16
    Caption = 'Last'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 339
    Top = 433
    Width = 29
    Height = 16
    Caption = 'Add'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 398
    Top = 433
    Width = 47
    Height = 16
    Caption = 'Delete'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label8: TLabel
    Left = 473
    Top = 433
    Width = 28
    Height = 16
    Caption = 'Edit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label9: TLabel
    Left = 538
    Top = 433
    Width = 32
    Height = 16
    Caption = 'Post'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label10: TLabel
    Left = 597
    Top = 433
    Width = 49
    Height = 16
    Caption = 'Cancel'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label11: TLabel
    Left = 661
    Top = 433
    Width = 55
    Height = 16
    Caption = 'Refresh'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object TwitterTalentGrid: TtsDBGrid
    Left = 16
    Top = 32
    Width = 745
    Height = 393
    CellSelectMode = cmNone
    CheckBoxStyle = stCheck
    Cols = 5
    DatasetType = dstStandard
    DataSource = dmMain.dsSponsor_Logos
    DefaultRowHeight = 20
    ExactRowCount = True
    ExportDelimiter = ','
    FieldState = fsCustomized
    HeadingFont.Charset = DEFAULT_CHARSET
    HeadingFont.Color = clWindowText
    HeadingFont.Height = -13
    HeadingFont.Name = 'MS Sans Serif'
    HeadingFont.Style = [fsBold]
    HeadingHeight = 20
    HeadingParentFont = False
    ParentShowHint = False
    RowChangedIndicator = riAutoReset
    RowMoving = False
    ShowHint = False
    TabOrder = 0
    Version = '2.20.26'
    XMLExport.Version = '1.0'
    XMLExport.DataPacketVersion = '2.0'
    DataBound = True
    ColProperties = <
      item
        DataCol = 1
        FieldName = 'Twitter_Handle'
        Col.FieldName = 'Twitter_Handle'
        Col.Font.Charset = DEFAULT_CHARSET
        Col.Font.Color = clWindowText
        Col.Font.Height = -13
        Col.Font.Name = 'MS Sans Serif'
        Col.Font.Style = []
        Col.Heading = 'Twitter Handle'
        Col.ParentFont = False
        Col.Width = 162
        Col.AssignedValues = '?'
      end
      item
        DataCol = 2
        FieldName = 'Talent_Name'
        Col.FieldName = 'Talent_Name'
        Col.Font.Charset = DEFAULT_CHARSET
        Col.Font.Color = clWindowText
        Col.Font.Height = -13
        Col.Font.Name = 'MS Sans Serif'
        Col.Font.Style = []
        Col.Heading = 'Talent Name'
        Col.ParentFont = False
        Col.Width = 202
        Col.AssignedValues = '?'
      end
      item
        DataCol = 3
        FieldName = 'List_Name'
        Col.FieldName = 'List_Name'
        Col.Font.Charset = DEFAULT_CHARSET
        Col.Font.Color = clWindowText
        Col.Font.Height = -13
        Col.Font.Name = 'MS Sans Serif'
        Col.Font.Style = []
        Col.Heading = 'List Name'
        Col.ParentFont = False
        Col.Width = 138
        Col.AssignedValues = '?'
      end
      item
        DataCol = 4
        FieldName = 'League_Mnemonic'
        Col.FieldName = 'League_Mnemonic'
        Col.Heading = 'League Mnemonic'
        Col.Width = 137
        Col.AssignedValues = '?'
      end
      item
        DataCol = 5
        FieldName = 'Talent_Enable'
        Col.ControlType = ctCheck
        Col.FieldName = 'Talent_Enable'
        Col.Heading = 'Enable'
        Col.AssignedValues = '?'
      end>
  end
  object BitBtn1: TBitBtn
    Left = 279
    Top = 504
    Width = 97
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 407
    Top = 504
    Width = 89
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Kind = bkCancel
  end
  object DBNavigator1: TDBNavigator
    Left = 52
    Top = 456
    Width = 670
    Height = 33
    DataSource = dmMain.dsSponsor_Logos
    TabOrder = 3
  end
end
