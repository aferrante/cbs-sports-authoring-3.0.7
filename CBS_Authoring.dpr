program CBS_Authoring;

uses
  Forms,
  Main in 'Main.pas' {MainForm},
  Preferences in 'Preferences.pas' {Prefs},
  AboutBox in 'AboutBox.pas' {About},
  DataModule in 'DataModule.pas' {dmMain: TDataModule},
  SearchDataEntry in 'SearchDataEntry.pas' {TextSearchDlg},
  PlaylistGraphicsViewer in 'PlaylistGraphicsViewer.pas' {PlaylistViewerDlg},
  NoteEntryEditor in 'NoteEntryEditor.pas' {NoteEntryEditorDlg},
  ScheduleEntryTimeEditor in 'ScheduleEntryTimeEditor.pas' {ScheduleEntryEditDlg},
  NCAAManualGameEntry in 'NCAAManualGameEntry.pas' {NCAAMatchupManualDlg},
  Globals in 'Globals.pas',
  ZipperEntry in 'ZipperEntry.pas' {ZipperEntryDlg},
  PlaylistSelect in 'PlaylistSelect.pas' {PlaylistSelectDlg},
  ZipperEntryEditor in 'ZipperEntryEditor.pas' {ZipperEntryEditorDlg},
  EnableDateTimeEditor in 'EnableDateTimeEditor.pas' {EnableDateTimeEditorDlg},
  DuplicationCountSelect in 'DuplicationCountSelect.pas' {DuplicationCountSelectDlg},
  EngineConnectionPreferences in 'EngineConnectionPreferences.pas' {EnginePrefsDlg},
  EngineIntf in 'EngineIntf.pas' {EngineInterface},
  ManualGameEditor in 'ManualGameEditor.pas' {ManualGameDlg},
  TwitterMessageEditor in 'TwitterMessageEditor.pas' {TwitterMessageEditorDlg},
  NCAATournamentChipEditor in 'NCAATournamentChipEditor.pas' {NCAATournamentChipEditorDlg},
  CSSGameNoteViewer in 'CSSGameNoteViewer.pas' {CSSGameNoteViewerDlg},
  TwitterTalentEditor in 'TwitterTalentEditor.pas' {TwitterTalentEditorDlg},
  CollegeTeamLogoEditor in 'CollegeTeamLogoEditor.pas' {CollegeTeamLogoEditorDlg},
  SponsorLogoEditor in 'SponsorLogoEditor.pas' {SponsorLogoEditorDlg},
  DefaultTemplateDwellTimeEditor in 'DefaultTemplateDwellTimeEditor.pas' {TemplateDwellEditorDlg};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'CBS_Authoring';
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TPrefs, Prefs);
  Application.CreateForm(TAbout, About);
  Application.CreateForm(TdmMain, dmMain);
  Application.CreateForm(TTextSearchDlg, TextSearchDlg);
  Application.CreateForm(TPlaylistViewerDlg, PlaylistViewerDlg);
  Application.CreateForm(TNoteEntryEditorDlg, NoteEntryEditorDlg);
  Application.CreateForm(TScheduleEntryEditDlg, ScheduleEntryEditDlg);
  Application.CreateForm(TNCAAMatchupManualDlg, NCAAMatchupManualDlg);
  Application.CreateForm(TZipperEntryDlg, ZipperEntryDlg);
  Application.CreateForm(TPlaylistSelectDlg, PlaylistSelectDlg);
  Application.CreateForm(TZipperEntryEditorDlg, ZipperEntryEditorDlg);
  Application.CreateForm(TEnableDateTimeEditorDlg, EnableDateTimeEditorDlg);
  Application.CreateForm(TDuplicationCountSelectDlg, DuplicationCountSelectDlg);
  Application.CreateForm(TEnginePrefsDlg, EnginePrefsDlg);
  Application.CreateForm(TEngineInterface, EngineInterface);
  Application.CreateForm(TManualGameDlg, ManualGameDlg);
  Application.CreateForm(TTwitterMessageEditorDlg, TwitterMessageEditorDlg);
  Application.CreateForm(TNCAATournamentChipEditorDlg, NCAATournamentChipEditorDlg);
  Application.CreateForm(TCSSGameNoteViewerDlg, CSSGameNoteViewerDlg);
  Application.CreateForm(TTwitterTalentEditorDlg, TwitterTalentEditorDlg);
  Application.CreateForm(TCollegeTeamLogoEditorDlg, CollegeTeamLogoEditorDlg);
  Application.CreateForm(TSponsorLogoEditorDlg, SponsorLogoEditorDlg);
  Application.CreateForm(TTemplateDwellEditorDlg, TemplateDwellEditorDlg);
  Application.Run;
end.
