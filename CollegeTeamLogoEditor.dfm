object CollegeTeamLogoEditorDlg: TCollegeTeamLogoEditorDlg
  Left = 513
  Top = 123
  BorderStyle = bsDialog
  Caption = 'College Team Logo Database Table Editor'
  ClientHeight = 580
  ClientWidth = 968
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 17
    Top = 10
    Width = 238
    Height = 16
    Caption = 'College Team Logos in Database:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 23
    Top = 495
    Width = 407
    Height = 16
    Caption = 'Note: Team ID Number MUST Come from CBS CSS System'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object First: TLabel
    Left = 56
    Top = 433
    Width = 31
    Height = 16
    Caption = 'First'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 149
    Top = 433
    Width = 34
    Height = 16
    Caption = 'Prior'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 239
    Top = 433
    Width = 32
    Height = 16
    Caption = 'Next'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 333
    Top = 433
    Width = 30
    Height = 16
    Caption = 'Last'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 425
    Top = 433
    Width = 29
    Height = 16
    Caption = 'Add'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 512
    Top = 433
    Width = 47
    Height = 16
    Caption = 'Delete'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label8: TLabel
    Left = 610
    Top = 433
    Width = 28
    Height = 16
    Caption = 'Edit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label9: TLabel
    Left = 702
    Top = 433
    Width = 32
    Height = 16
    Caption = 'Post'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label10: TLabel
    Left = 790
    Top = 433
    Width = 49
    Height = 16
    Caption = 'Cancel'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label11: TLabel
    Left = 879
    Top = 433
    Width = 55
    Height = 16
    Caption = 'Refresh'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object tsDBGrid1: TtsDBGrid
    Left = 16
    Top = 32
    Width = 937
    Height = 393
    CellSelectMode = cmNone
    CheckBoxStyle = stCheck
    Cols = 5
    DatasetType = dstStandard
    DataSource = dmMain.dsSponsor_Logos
    DefaultRowHeight = 20
    ExactRowCount = True
    ExportDelimiter = ','
    FieldState = fsCustomized
    HeadingFont.Charset = DEFAULT_CHARSET
    HeadingFont.Color = clWindowText
    HeadingFont.Height = -13
    HeadingFont.Name = 'MS Sans Serif'
    HeadingFont.Style = [fsBold]
    HeadingHeight = 20
    HeadingParentFont = False
    ParentShowHint = False
    RowChangedIndicator = riAutoReset
    RowMoving = False
    ShowHint = False
    TabOrder = 0
    Version = '2.20.26'
    XMLExport.Version = '1.0'
    XMLExport.DataPacketVersion = '2.0'
    DataBound = True
    ColProperties = <
      item
        DataCol = 1
        FieldName = 'Sport'
        Col.FieldName = 'Sport'
        Col.Font.Charset = DEFAULT_CHARSET
        Col.Font.Color = clWindowText
        Col.Font.Height = -13
        Col.Font.Name = 'MS Sans Serif'
        Col.Font.Style = []
        Col.Heading = 'Sport'
        Col.ParentFont = False
        Col.Width = 73
        Col.AssignedValues = '?'
      end
      item
        DataCol = 2
        FieldName = 'Team_Name'
        Col.FieldName = 'Team_Name'
        Col.Font.Charset = DEFAULT_CHARSET
        Col.Font.Color = clWindowText
        Col.Font.Height = -13
        Col.Font.Name = 'MS Sans Serif'
        Col.Font.Style = []
        Col.Heading = 'Team Name'
        Col.ParentFont = False
        Col.Width = 156
        Col.AssignedValues = '?'
      end
      item
        DataCol = 3
        FieldName = 'GametraxLong'
        Col.FieldName = 'GametraxLong'
        Col.Font.Charset = DEFAULT_CHARSET
        Col.Font.Color = clWindowText
        Col.Font.Height = -13
        Col.Font.Name = 'MS Sans Serif'
        Col.Font.Style = []
        Col.Heading = 'Gametrax Long Name'
        Col.ParentFont = False
        Col.Width = 194
        Col.AssignedValues = '?'
      end
      item
        DataCol = 4
        FieldName = 'DisplayName'
        Col.FieldName = 'DisplayName'
        Col.Font.Charset = DEFAULT_CHARSET
        Col.Font.Color = clWindowText
        Col.Font.Height = -13
        Col.Font.Name = 'MS Sans Serif'
        Col.Font.Style = []
        Col.Heading = 'Display Name'
        Col.ParentFont = False
        Col.Width = 190
        Col.AssignedValues = '?'
      end
      item
        DataCol = 5
        FieldName = 'GametraxTickerLogoFilename'
        Col.FieldName = 'GametraxTickerLogoFilename'
        Col.Font.Charset = DEFAULT_CHARSET
        Col.Font.Color = clWindowText
        Col.Font.Height = -13
        Col.Font.Name = 'MS Sans Serif'
        Col.Font.Style = []
        Col.Heading = 'Gametrax Ticker Logo Filename'
        Col.ParentFont = False
        Col.Width = 284
        Col.AssignedValues = '?'
      end>
  end
  object BitBtn1: TBitBtn
    Left = 379
    Top = 528
    Width = 97
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 499
    Top = 528
    Width = 89
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Kind = bkCancel
  end
  object DBNavigator1: TDBNavigator
    Left = 22
    Top = 456
    Width = 930
    Height = 33
    DataSource = dmMain.dsSponsor_Logos
    TabOrder = 3
  end
end
